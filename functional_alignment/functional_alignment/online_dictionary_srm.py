import numpy as np
from sklearn.decomposition.dict_learning import _update_dict
from sklearn.decomposition.dict_learning import sparse_encode
import logging
from functional_alignment.baseSRM import BaseSRM
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img
from sklearn.utils.validation import check_array
from sklearn.utils import check_random_state
from time import time
from functional_alignment.deterministic_srm import DeterministicSRM
import os

logger = logging.getLogger(__name__)


def _dictsrm_fit(masker,
                 imgs,
                 confounds=None,
                 n_components=10,
                 random_state=None,
                 memory=None,
                 memory_level=0,
                 n_jobs=1,
                 alpha=1.,
                 positive_code=False,
                 batch_size=10,
                 exp_params={}):

    masker.fit()
    np.random.seed(random_state)
    n_subjects = len(imgs)
    dictionary = None
    alpha = float(alpha)
    random_state = check_random_state(random_state)
    confounds = [confound for _, confound in zip(range(n_subjects), confounds)]
    t0 = time()

    voxels_seen = 0
    subjects = np.arange(n_subjects)
    for subject in subjects:
        print(" Subject %i / %i"%(subject, len(subjects)))
        confound = confounds[subject]
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )

        n_voxels, n_timeframes = x_subject.shape
        voxels = np.arange(n_voxels)
        np.random.shuffle(voxels)

        n_batches = n_voxels / batch_size
        batches = np.array_split(voxels, n_batches)

        if dictionary is None:
            dictionary = DeterministicSRM(n_components=n_components, **exp_params).fit(
                imgs).shared_response
            dictionary = check_array(dictionary.T, order='F', dtype=np.float64,
                                     copy=False)
            dictionary = np.require(dictionary, requirements='W')

            # The covariance of the dictionary
            A = np.zeros((n_components, n_components))
            # The data approximation
            B = np.zeros((n_timeframes, n_components))

        for batch in batches:
            print("Progress %i / %i"%(n_subjects * n_voxels, voxels_seen))
            this_X = x_subject[batch]
            this_code = sparse_encode(this_X, dictionary.T, algorithm='lasso_cd',
                                      alpha=alpha, n_jobs=n_jobs,
                                      check_input=False,
                                      positive=positive_code).T

            A *= voxels_seen
            A += np.dot(this_code, this_code.T)
            A /= voxels_seen + len(batch)

            B *= voxels_seen
            B += np.dot(this_X.T, this_code.T)
            B /= voxels_seen + len(batch)

            voxels_seen += len(batch)

            # Update dictionary
            dictionary = _update_dict(dictionary, B, A, verbose=False,
                                      random_state=random_state,
                                      positive=False)
    print("Dictionary fitting time", time() - t0)
    dictionary =dictionary.T

    codes = []
    for subject in range(n_subjects):
        confound = confounds[subject]
        logger.debug("Loading subject %i" % subject)
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )
        code = sparse_encode(x_subject, dictionary, algorithm="lasso_cd", alpha=alpha,
                             n_jobs=n_jobs, check_input=False,
                             positive=positive_code)
        codes.append(code)
    print("Code finding time", time() - t0)

    return masker, codes, dictionary


class OnlineDictionarySRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components, alpha=1.,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=None, memory_level=0,
                 transform_method="mean", n_jobs=1,
                 positive_code=False,
                 batch_size=None,
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            Sparsity controlling parameter.
        init_method: str
            "cca" uses cca as initialization
            "ortho_srm" uses fast deterministic srm as initialization
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method,
                 )

        self.alpha = alpha
        self.positive_code = positive_code
        self.exp_params = {
            "random_state": random_state,
            "mask": mask,
            "smoothing_fwhm": smoothing_fwhm,
            "standardize": standardize,
            "detrend": detrend,
            "low_pass": low_pass,
            "high_pass": high_pass,
            "t_r": t_r,
            "target_affine": target_affine,
            "target_shape": target_shape,
            "mask_strategy": mask_strategy,
            "mask_args": mask_args,
            "n_jobs": n_jobs,
            "verbose": verbose,
            "memory": memory,
            'memory_level': memory_level
        }

        if batch_size is None:
            self.batch_size = n_components
        else:
            self.batch_size = batch_size

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response = cache(
            _dictsrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          n_jobs=self.n_jobs,
          alpha=self.alpha,
          positive_code=self.positive_code,
          batch_size=self.batch_size,
          exp_params=self.exp_params
          )
        return self

