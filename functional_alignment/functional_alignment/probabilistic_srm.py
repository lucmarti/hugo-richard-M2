# Probabilistic shared response model: Implementation based on brainiak's
# see https://github.com/brainiak/brainiak

import logging
import numpy as np
import scipy
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import assert_all_finite
from sklearn.exceptions import NotFittedError
import os.path
from nilearn.input_data import NiftiMasker
from joblib import Memory
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from nilearn.decomposition.base import fast_svd
from copy import deepcopy
from functional_alignment.baseSRM import BaseSRM, check_imgs
import itertools
from nilearn._utils.cache_mixin import cache
from sklearn.utils import check_random_state
from functional_alignment.deterministic_srm import create_orthogonal_matrix
from functional_alignment.utils import load_img

logger = logging.getLogger(__name__)


def _probabilistic_srm(masker,
                imgs,
                confounds=None,
                n_components=10,
                random_state=None,
                n_jobs=1,
                n_iter=10,
                memory=None,
                memory_level=None
                ):
    """
    Fit the probabilistic SRM model
    """
    masker.fit()
    n_subjects = len(imgs)
    rho2 = np.ones(n_subjects)

    basis = []

    sigma_s = np.eye(n_components)
    # Main loop of the algorithm (run
    for iteration in range(n_iter):
        logger.info('Iteration %d' % (iteration + 1))
        # E-step:
        # Sum the inverted the rho2 elements for computing W^T * Psi^-1 * W
        rho0 = (1 / rho2).sum()
        # Invert Sigma_s using Cholesky factorization
        (chol_sigma_s, lower_sigma_s) = scipy.linalg.cho_factor(
            sigma_s, check_finite=False)
        inv_sigma_s = scipy.linalg.cho_solve(
            (chol_sigma_s, lower_sigma_s), np.identity(n_components),
            check_finite=False)

        # Invert (Sigma_s + rho_0 * I) using Cholesky factorization
        sigma_s_rhos = inv_sigma_s + np.identity(n_components) * rho0
        chol_sigma_s_rhos, lower_sigma_s_rhos = \
            scipy.linalg.cho_factor(sigma_s_rhos,
                                    check_finite=False)
        inv_sigma_s_rhos = scipy.linalg.cho_solve(
            (chol_sigma_s_rhos, lower_sigma_s_rhos),
            np.identity(n_components), check_finite=False)

        # Compute the sum of W_i^T * rho_i^-2 * X_i, and the sum of traces
        # of X_i^T * rho_i^-2 * X_i

        wt_invpsi_x = None
        trace_xt_invsigma2_x = 0.0

        for subject, confound in zip(range(n_subjects), confounds):
            logging.info("Loading subject")
            x_subject, masker = cache(
                load_img,
                memory=memory,
                memory_level=memory_level,
                func_memory_level=2
            )(
                masker,
                imgs[subject],
                axis=0,
                confound=confound
            )
            logging.info("Done")
            n_voxels, n_timeframes = x_subject.shape

            if wt_invpsi_x is None:
                wt_invpsi_x = np.zeros((n_components, n_timeframes))

            trace_xtx_subject = np.sum(x_subject ** 2)

            random_state = check_random_state(random_state)
            if len(basis) < n_subjects:
                w = create_orthogonal_matrix(n_voxels, n_components, random_state=random_state)
                basis.append(w)
            if x_subject is not None:
                wt_invpsi_x += (basis[subject].T.dot(x_subject)) / rho2[subject]
                trace_xt_invsigma2_x += trace_xtx_subject / rho2[subject]

        # Update the shared response
        shared_response = sigma_s.dot(
            np.identity(n_components) - rho0 * inv_sigma_s_rhos).dot(
            wt_invpsi_x)

        # M-step

        # Update Sigma_s and compute its trace
        sigma_s = (inv_sigma_s_rhos
                   + shared_response.dot(shared_response.T) / n_timeframes)
        trace_sigma_s = n_timeframes * np.trace(sigma_s)

        # Update each subject's mapping transform W_i and error variance
        # rho_i^2
        for subject, confound in zip(range(n_subjects), confounds):
            x_subject, masker = cache(
                load_img,
                memory=memory,
                memory_level=memory_level,
                func_memory_level=2
            )(
                masker,
                imgs[subject],
                axis=0,
                confound=confound
            )
            trace_xtx_subject = np.sum(x_subject ** 2)
            a_subject = x_subject.dot(shared_response.T)
            perturbation = np.zeros(a_subject.shape)
            np.fill_diagonal(perturbation, 0.001)
            u_subject, s_subject, v_subject = np.linalg.svd(
                a_subject + perturbation, full_matrices=False)
            basis[subject] = u_subject.dot(v_subject)
            rho2[subject] = trace_xtx_subject
            rho2[subject] += -2 * np.sum(basis[subject] * a_subject).sum()
            rho2[subject] += trace_sigma_s
            rho2[subject] /= n_timeframes * n_voxels

    return masker, sigma_s, basis, rho2, shared_response


class ProbabilisticSRM(BaseSRM):
    """ Probabilistic Shared Response Model

    Performs a low rank decomposition on each subject
    which consists of a common response and
    an orthogonal subject-specific basis.

    It finds the shared coordinates S and
    for any subject X_i, the basis W_i
    that the sum of Frobenius norms
    sum_i || X_i - W_i S ||^2
    is minimized under the constraints:
    * W_i.T W_i = I_k

    X_i is the data of subject i
    S are the shared coordinates
    W_i is the basis of subject i

    **References:**
    Po-Hsuan Chen, Janice Chen, Yaara Yeshurun
    Uri Hasson, James V. Haxby, Peter J. Ramadge,
    2015: A reduced-Dimension fMRI Shared Response Model
    """

    def __init__(self, max_iter=10, n_components=20, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None),
                 memory_level=0,verbose=False, n_jobs=1,
                 fit_memory=Memory(cachedir=None),
                 ):
        """
        Probabilistic Shared Response Model
        Performs a low rank decomposition on each subject
        which consists of a common response and
        an orthogonal subject-specific basis.

        It finds the shared coordinates S and
        for any subject X_i, the basis W_i
        that the sum of Frobenius norms
        sum_i || X_i - W_i S ||^2
        is minimized under the constraints:
        * W_i.T W_i = I_k

        X_i is the data of subject i
        S are the shared coordinates
        W_i is the basis of subject i

        Parameters
        ----------
        max_iter : int,
            number of iterations to perform
        
        
        Attributes
        ----------
        `noise_variance`: np array of shape (n_subjects,)
            the noise variance for all subject
        `shared_variance`: 2D np array of shape (n_components, n_components)
            variance of shared response
        `masker` : instance of MultiNiftiMasker
            Masker used to filter and mask data as first step. If an instance of
            MultiNiftiMasker is given in `mask` parameter,
            this is a copy of it. Otherwise, a masker is created using the value
            of `mask` and other NiftiMasker related parameters as initialization.
        `mask`: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker
            Mask to be used on data.


        **References:**
        Po-Hsuan Chen, Janice Chen, Yaara Yeshurun
        Uri Hasson, James V. Haxby, Peter J. Ramadge,
        2015: A reduced-Dimension fMRI Shared Response Model
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=None)

        self.n_iter = max_iter
        self.fit_memory = fit_memory

    def _fit(self, imgs, confounds=None):

        """
        Fit the model
        """
        self.masker, self.shared_variance, self.basis, self.noise_variance, self.shared_response = cache(
            _probabilistic_srm,
            memory=self.fit_memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          n_jobs=self.n_jobs,
          n_iter=self.n_iter,
          memory=self.memory,
          memory_level=self.memory_level
          )
        return self

    def transform(self, X, y=None, index=None, confounds=None):
        """
        Override transform method to make use of orthogonal W
        """

        if index is None:
            index = np.array(range(len(self.basis)))
        else:
            index = np.array(index)

        X = check_imgs(X)

        if confounds is None:
            confounds = itertools.repeat(confounds)

        basis = self.basis

        masker = self.masker

        n_timeframes = None
        common_space = None
        for i, confound in zip(range(len(X)), confounds):
            X_i, masker = cache(
                load_img,
                memory=self.memory,
                memory_level=self.memory_level,
                func_memory_level=2
            )(
                masker,
                X[i],
                axis=0,
                confound=confound
            )
            if n_timeframes is None:
                n_timeframes = X_i.shape[1]
                common_space = np.zeros((self.n_components, n_timeframes))
            s_i = basis[index[i]].T.dot(X_i)
            if common_space is None:
                common_space = s_i
            else:
                common_space += s_i
        common_space /= len(X)
        return common_space
