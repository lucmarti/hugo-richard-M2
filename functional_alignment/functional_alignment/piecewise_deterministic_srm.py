from sklearn.base import BaseEstimator, TransformerMixin
import copy
import numpy as np
from functional_alignment.hyperalignment import hyperalign
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.image import grid_to_graph
from joblib import Parallel, delayed
from time import time
import pickle
from functional_alignment.utils import load_img
from sklearn.linear_model import RidgeCV as SkRidgeCV
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.piecewise_alignment import fast_dot, piecewise_dot, hierarchical_k_means, create_labels, generate_Xi_Yi, optimal_permutation
from functional_alignment.deterministic_srm import _deterministic_srm
from scipy.sparse import dok_matrix, coo_matrix, bsr_matrix
from sklearn.externals.joblib import Memory
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from nilearn.image import load_img
from scipy.sparse import block_diag


def create_fit_matrix(method, labels_values, labels, X_, Y_, n_components=10, n_jobs=1, verbose=0):
    """ Function that does the fit

    Parameters
    ----------
    method: 'RidgeCV', 'RRRCV' or 'mean'
            algorithm used to perform mapping between regions
    labels_values:  numpy array,
                   the unique values of the label.
    labels: numpy array,
        the label of each voxel used in the piecewise fit
    X_: list of arrays,
        input data
    Y_: list of arrays,
        target data
    n_components : researched number of components
    n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
    verbose: integer, optional,
            The verbosity level of the


    Returns
    -------
    basis_: list of arrays,
         the estimated transformation matrices : sc_i * W_i
    scale: list of int,
           sc_i
    """
    """
    fit_matrix_list = []
    for X_i, Y_i, _ in generate_Xi_Yi(labels_values, labels, X_, Y_):
        fit_matrix_list.append(fit_piecewise_align(X_i, Y_i, method))
    """
    infos = None
    fit_matrix_list = Parallel(n_jobs, backend="threading", verbose=verbose)(
        delayed(fit_piecewise_align)(X_i.T, Y_i.T, method, n_components, n_jobs
                                     ) for X_i, Y_i, _ in generate_Xi_Yi(labels_values, labels, X_, Y_, infos)
    )
    # basis refers to sc_i * W_i
    # the scale refers to sc_i
    basis_ = [fit_matrix[0] for fit_matrix in fit_matrix_list]
    scale_ = [fit_matrix[1] for fit_matrix in fit_matrix_list]
    # fit_ = block_diag(matrices_, format='csr')
    for i in range(len(basis_)):
        if np.shape(basis_[i])[0] != np.sum(labels == i):
            pass
    if basis_ == []:
        print("basis is empty")
    return basis_, scale_


'''to generalize to n subjects replace X_i, Y_i by imgs and change upstream'''


def fit_piecewise_align(X_i, Y_i, method, n_components, n_jobs):
    "call to deterministic_srm._deterministic_srm directly on tables"
    masker = None
    _, basis, scale, shared_response = _deterministic_srm(masker,
                                                          [X_i, Y_i],
                                                          confounds=None,
                                                          n_components=n_components,
                                                          random_state=None,
                                                          memory=None,
                                                          memory_level=0,
                                                          n_jobs=n_jobs,
                                                          tol=1e-6,
                                                          use_scaling=False,
                                                          speed="fast"
                                                          )
    return basis, scale


class PieceWiseAlignment_DeterministicSRM(BaseEstimator, TransformerMixin):
    """
    Decompose the source and target images into source and target regions
     Use alignment algorithms to align source and target regions independantly.
    """

    def __init__(self, n_pieces=100, method="srm", perturbation=False, n_bootstrap=1, n_components=10,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Decompose the source and target images into source and target regions
        Use alignment algorithms to align source and target regions independantly.

        Parameters
        ----------
        n_pieces: int
            number of regions
        method: placeholder. For now any input will use srm except mean
        perturbation: bool
            If true, the penalized transformation is the one that regress
             the source X to the difference between the target and the source Y - X.
            If false, the penalized transformaion is the one that regress the source X to
            the target Y
        n_bootstrap: int, optional
            How many bootstrap averages are used. 1 by default
        n_components: number of components found in each parcel by SRM
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        """
        self.n_pieces = n_pieces
        self.method = method
        self.perturbation = perturbation
        self.n_bootstrap = n_bootstrap

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs

        self.verbose = verbose
        self.n_components = n_components
        self.scale_ = None

    def fit(self, X, Y):
        """
        Fit data X and Y and learn transformation to map X to Y
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Y: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           target data
        For srm this is a limited case for now that is going to be extended.

<        Returns
        -------
        self
        """
        from sklearn.model_selection import ShuffleSplit
        self.masker_ = check_embedded_nifti_masker(self)
        self.masker_.n_jobs = self.n_jobs
        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit([X])
        else:
            self.masker_.fit()
        self.mask_img_ = load_img(self.masker_.mask_img)

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T

        Y_ = self.masker_.transform(Y)
        if type(Y_) == list:
            Y_ = np.concatenate(Y_, axis=0)
        Y_ = Y_.T

        if self.perturbation:
            Y_ = Y_ - X_

        self.basis_, self.labels_, self.scale_ = [], [], []
        rs = ShuffleSplit(n_splits=self.n_bootstrap,
                          test_size=.8, random_state=0)
        """
        for train_index, _ in rs.split(Y_.T):
            labels_, basis_, scale_ = fit_one_parcellation(
                X_, Y_, self.masker_.mask_img.get_data(), self.n_pieces, self.method,
                train_index, self.memory, n_jobs=1, verbose=self.verbose,
                infos=self.scale_)
            self.basis_.append(basis_)
            self.labels_.append(labels_)
            self.scale_.append(scale_)
        """
        outputs = Parallel(n_jobs=self.n_jobs)(
            delayed(fit_one_parcellation)(
                X_, Y_, self.masker_.mask_img.get_data(), self.n_pieces, self.method, self.n_components, train_index, self.memory, n_jobs=1, verbose=self.verbose) for train_index, _ in rs.split(Y_.T))

        self.labels_ = [output[0] for output in outputs]
        self.basis_ = [output[1] for output in outputs]
        self.scale_ = [output[2] for output in outputs]

        """
        if self.n_pieces > 1:
            labels = create_labels(Y_, self.masker_.mask_img.get_data(),
                                   self.n_pieces, memory=self.memory)
            self.masker_.inverse_transform(labels).to_filename('/tmp/labels.nii.gz')
        else:
            labels = np.zeros(self.masker_.mask_img.get_data().sum())

        basis_, scale_ = self.memory.cache(
            create_fit_matrix, ignore=["n_jobs", "verbose", "infos"])(
                self.method, np.unique(labels), labels, X_, Y_, n_jobs=self.n_jobs,
                verbose=self.verbose, infos=self.scale_
            )
        self.basis_ = basis_
        self.scale_ = scale_
        self.labels_ = labels
        """
        return self

    def transform(self, X, index=None, index_inverse=None):
        """
        Predict data from X
        Parameters
        X data of same shape as X_ or Y_ input in fit()
        index = 0, index_inverse = 1 will use X_ basis to create a common space
        and basis to transfer it back
        index = 0, index_inverse = 1 will do the opposite
        It is also a limiting usecase calling for generalization to n_subjects
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        index : int
        index_inverse : int
        Returns
        -------
        X_transform: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           predicted data
        """

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T
        if index is None:
            index = np.array(range(len(self.basis_)))
        else:
            index = np.array(index)

        if self.method == "mean":
            return self.masker_.inverse_transform(X_.T)
        else:
            # X_transform = self.memory.cache(fast_dot)(self.basis_, X_)
            X_transform = np.zeros_like(X_)
            for i in range(self.n_bootstrap):
                # replaces the piecewise_dot function
                X_tf = np.zeros_like(X_)
                for label in np.unique(self.labels_[i]).astype(int):
                    if index is None:
                        tf = self.basis_[i][label][0]
                    else:
                        tf = self.basis_[i][label][index]
                        inv_tf = self.basis_[i][label][index_inverse]
                    common_space = tf.T.dot((X_[self.labels_[i] == label]))
                    # for 1 index, don't need to rescale the common_space
                    X_tf[self.labels_[i] == label] = inv_tf.dot(common_space)
                X_transform += X_tf
            X_transform /= self.n_bootstrap
            if self.perturbation:
                X_transform += X_

        return self.masker_.inverse_transform(X_transform.T)


def fit_one_parcellation(X, Y, mask, n_pieces, method, n_components, train_index, mem,
                         n_jobs=1, verbose=True):
    """Do one parcellation + call of fit on the data"""
    if n_pieces > 1:
        labels = create_labels(X[:, train_index], mask, n_pieces, memory=mem)
    else:
        labels = np.zeros(mask.sum())

    unique_labels, counts = np.unique(labels, return_counts=True)
    print(counts)
    basis_, scale_ = create_fit_matrix(method, unique_labels, labels, X, Y, n_components, n_jobs=n_jobs,
                                       verbose=verbose)

    return labels, basis_, scale_
