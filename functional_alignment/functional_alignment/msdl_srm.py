import numpy as np
from sklearn.externals.joblib import Memory
from sklearn.decomposition.dict_learning import sparse_encode
import logging
from functional_alignment.baseSRM import BaseSRM
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img
from joblib import Parallel, delayed
from functional_alignment.baseSRM import cov
from nilearn.decoding.proximal_operators import _prox_tvl1
from functional_alignment.dictionary_srm import _reduced_data_sharedresponse
from sklearn.decomposition.dict_learning import _update_dict


logger = logging.getLogger(__name__)


def _update_dictionary(W_TW, W_TX, max_iter=100, tol=1e-12):
    """
    Parameters
    ----------
    W_TW: np array of shape k,k
        W_TW = sum_i W_i.T.dot(W_i)
    W_TX: np array of shape k, t
        W_TX = sum_i W_i.T.dot(X_i)
    Returns
    -------
    updated dictionary: np array of shape k, t
    """
    P, D, Pt = np.linalg.svd(W_TW)
    B = Pt.dot(W_TX)
    S = (P * (1 / D)).dot(B)

    if np.linalg.norm(S)**2 <= 1:
        # If the shared response already has low norm then everything is fine
        return S
    else:
        # Otherwise we need to solve ||X - WS||**2 subject to ||S||**2 = 1
        # Optimizing the lagrangian yield S = (W.TW + l I)^{-1} W.T X
        # and ||S||**2 = 1
        # ||S||**2 = 1 <=> ||(D + l I)^-1 B ||^2 = 1
        # This is a decreasing function of l
        # we know l = 0 yields ||S|| > 1
        # and if l = ||B|| we have ||S|| < 1
        # We use dichotomy to find l

        l_m = 0
        l_M = np.linalg.norm(B)
        l = (l_m + l_M) / 2

        for i in range(max_iter):
            n = 0.5 * np.linalg.norm((1 / (D + l))[:, np.newaxis] * B)**2
            if n < 1.:
                l_M = l
                l = (l + l_m) / 2
            if n > 1.:
                l_m = l
                l = (l + l_M) / 2
            if np.abs(n - 1) < tol:
                break

        return (P * (1 / (D + l))).dot(B)


def _compute_basis(data, common_basis, shared_response, mu):
    """
    Computes subject's basis using common basis and shared response
    """
    n_components, _ = shared_response.shape
    S_ridgeinv = shared_response.T.dot(np.linalg.pinv(shared_response.dot(shared_response.T) + mu*np.eye(n_components)))

    return (data - common_basis.dot(shared_response)).dot(S_ridgeinv) + common_basis


def _compute_common_basis(basis, alpha, mask_img, mu, l1_ratio):
    w_av = np.mean(basis, axis=0)
    common_basis = np.zeros_like(w_av)
    w_av_buffer = np.zeros(mask_img.shape)
    n_voxels, n_components = w_av.shape
    for k in range(n_components):
        w_av_buffer[mask_img] = w_av[:, k]
        common_basis_k = _prox_tvl1(
            w_av_buffer,
            weight=alpha / (mu * len(basis)),
            l1_ratio=l1_ratio
        )[0].ravel()[mask_img.ravel()]
        common_basis[:, k] = common_basis_k
    return common_basis


def _srm_mdsl_step(x_subject,
                   masker,
                   shared_response,
                   basis=[],
                   common_basis = None,
                   W_TW=None,
                   W_TX=None,
                   alpha=1.,
                   mu=1.,
                   l1_ratio=0.5,
                   subject=1
                   ):
    """
    Performs a step of mdsl using the data of one subject

    Parameters
    ----------
    W_TW: see update dictionary
    W_TX: see update dictionary
    subject: index of current subject
    """

    n_voxels, n_timeframes = x_subject.shape
    n_components, _ = shared_response.shape

    if W_TX is None:
        W_TX = np.zeros((n_components, n_timeframes))

    if common_basis is None:
        common_basis = np.zeros((n_voxels, n_components))

    basis_i = _compute_basis(x_subject, common_basis, shared_response, mu)

    if subject == len(basis):
        basis.append(basis_i)
        W_TX += basis_i.T.dot(x_subject)
        W_TW += basis_i.T.dot(basis_i)
    else:
        W_TX += basis_i.T.dot(x_subject) - basis[subject].T.dot(x_subject)
        W_TW += basis_i.T.dot(basis_i) - basis[subject].T.dot(basis[subject])
        basis[subject] = basis_i

    common_basis = _compute_common_basis(basis,
                                         alpha,
                                         masker.mask_img_.get_data().astype(np.bool),
                                         mu,
                                         l1_ratio
                                         )

    shared_response = _update_dictionary(W_TW, W_TX, max_iter=100, tol=1e-12)
    basis[subject] = basis_i
    return basis, shared_response, common_basis, W_TX, W_TW


def _mdslsrm_fit(masker,
                 imgs,
                 confounds=None,
                 n_components=10,
                 random_state=None,
                 memory=None,
                 memory_level=0,
                 n_jobs=1,
                 alpha=1.,
                 mu=1.,
                 alpha_init=1.,
                 l1_ratio=0.05,
                 n_iter=10,):

    masker.fit()
    # Initializing with dictionary SRM
    shared_response = _reduced_data_sharedresponse(masker,
                                                   imgs,
                                                   confounds=confounds,
                                                   n_components=n_components,
                                                   random_state=random_state,
                                                   memory=memory,
                                                   memory_level=memory_level,
                                                   n_jobs=n_jobs,
                                                   init_method="srm_ortho",
                                                   alpha=alpha_init,
                                                   )
    n_subjects = len(imgs)

    common_basis = None
    W_TW = np.zeros((n_components, n_components))
    W_TX = None
    basis = []
    for _ in range(n_iter):
        for subject, confound in zip(range(n_subjects), confounds):
            x_subject, masker = cache(
                load_img,
                memory=memory,
                memory_level=memory_level,
                func_memory_level=2
            )(
                masker,
                imgs[subject],
                axis=0,
                confound=confound
            )
            basis, shared_response, common_basis, W_TX, W_TW = _srm_mdsl_step(x_subject,
                                                                              masker,
                                                                              shared_response,
                                                                              basis=basis,
                                                                              common_basis=common_basis,
                                                                              W_TW=W_TW,
                                                                              W_TX=W_TX,
                                                                              alpha=alpha,
                                                                              mu=mu,
                                                                              l1_ratio=l1_ratio,
                                                                              subject=subject
                                                                              )

    return masker, basis, shared_response, common_basis


class MSDLSRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components, alpha=1.,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 transform_method="mean", n_jobs=1,
                 l1_ratio=0.05,
                 mu=1.,
                 alpha_init=1.,
                 max_iter=2
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            common map smoothness and sparsity controlling parameter.
        l1_ratio: float
            common map relative importance of sparsity / smoothness
            1: only sparsity penalty
            0: only smoothness penalty
        alpha_init: float
            Sparsity controlling parameter in the initial dictionary learning initialization
        mu: float
            Smoothness between maps controlling parameter

        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method,)

        self.alpha = alpha
        self.l1_ratio = l1_ratio
        self.mu = mu
        self.alpha_init = alpha_init
        self.n_iter = max_iter

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response, self.common_basis = cache(
            _mdslsrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          n_jobs=self.n_jobs,
          alpha=self.alpha,
          l1_ratio=self.l1_ratio,
          mu=self.mu,
          alpha_init=self.alpha_init,
          n_iter=self.n_iter
          )
        return self

