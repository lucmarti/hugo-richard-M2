import numpy as np
import scipy.linalg
from scipy.sparse import diags
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils.testing import assert_array_almost_equal
import copy
import time


def hyperalign(X, Y, scale=False, primal=None):
    """
    Hyperalign X with Y using R and sc such that
    frobenius norm ||sc RX - Y||^2 is minimized and
    R is an orthogonal matrix
    sc is a scalar
    Parameters
    ----------
    X: (n_features, n_timeframes) nd array
        source data
    Y: (n_features, n_timeframes) nd array
        target data
    scale: bool
        If scale is true, computes a floating scaling parameter sc such that:
        ||sc * RX - Y||^2 is minimized and
        - R is an orthogonal matrix
        - sc is a scalar
        If scale is false sc is set to 1
    primal: bool or None, optional,
         Whether the SVD is done on the YX^T (primal) or Y^TX (dual)
         if None primal is used iff n_features <= n_timeframes

    Returns
    ----------
    R: (n_features, n_features) nd array
        transformation matrix
    sc: int
        scaling parameter
    """
    if np.linalg.norm(X) == 0 or np.linalg.norm(Y) == 0:
        return diags(np.ones(X.shape[1])).tocsr(), 1

    if primal is None:
        primal = X.shape[1] >= X.shape[0]

    if primal:
        A = Y.dot(X.T)
        if A.shape[0] == A.shape[1]:
            A += + 1.e-18 * np.eye(A.shape[0])
        U, s, V = scipy.linalg.svd(A, full_matrices=0)
        R = U.dot(V)
    else:  # "dual" mode
        Uy, sy, Vy = scipy.linalg.svd(Y, full_matrices=0)
        Ux, sx, Vx = scipy.linalg.svd(X, full_matrices=0)
        A = np.diag(sy).dot(Vy).dot(Vx.T).dot(np.diag(sx))
        U, s, V = scipy.linalg.svd(A)
        R = Uy.dot(U).dot(V).dot(Ux.T)
    """
    if X.shape[0] > 10000:
        R = diags(np.ones(X.shape[0])).tocsr()
        s = np.sum(Y * X, 1)
    """
    if scale:
        sc = s.sum() / (np.linalg.norm(X) ** 2)
    else:
        sc = 1
    return R, sc


def compute_model_space(scale, X):
    """
    Find a model space for data in X

    Parameters
    ----------
    X: list of array of shape n_features, n_timeframes
    """

    # To derive a single common representational space, we
    # first hyperalign one subject to a reference subject's
    # representational space.
    # Then, we hyperalign a third subject
    # to the mean response vectors for the first 2 subjects.
    # We then hyperalign each successive subject
    # to the mean vectors for the previously
    # hyperaligned subjects.

    ref = copy.copy(X[0])
    ref = zscore(ref)
    X_transform = [X[0]]
    for i in range(1, len(X)):
        R_i, sc_i = hyperalign(X[i], ref, scale)
        X_transform_i = sc_i * R_i.dot(X[i])
        X_transform_i = zscore(X_transform_i)
        ref = (X_transform_i + ref) / 2.
        ref = zscore(ref)
        X_transform.append(X_transform_i)

    #  In a second iteration, we recalculate
    # transformation matrices for each subject to the
    #  mean vectors from the first iteration
    # and then recalculate the space of mean vectors.
    # This space is the model space.
    #

    ref = np.mean(X_transform, axis=0)
    model_space = []
    for i in range(len(X)):
        ref_i = (ref * len(X) - X_transform[i]) / (len(X) - 1)
        ref_i = zscore(ref_i)
        R_i, sc_i = hyperalign(X[i], ref_i, scale=scale)
        X_transform_i = sc_i * R_i.dot(X[i])
        X_transform_i = zscore(X_transform_i)
        model_space.append(X_transform_i)

    model_space = np.mean(model_space, axis=0)
    model_space = zscore(model_space)

    return model_space


def zscore(X):
    X_ = X - X.mean(axis=1, keepdims=True)
    stds = X.std(axis=1)

    for i in range(len(X)):
        if stds[i] == 0:
            X_[i] = np.zeros_like(X[i])
        else:
            X_[i] /= stds[i]
    return X_


class Hyperalignment(BaseEstimator, TransformerMixin):
    """
    Use hyperalignment model to align data X_1,...,X_{n_samples}
    Parameters
    ----------
    Scale: bool
    Attributes
    ----------
    basis_: list of array of shape (n_features, n_features)
    model_space_: array of shape (n_features, n_timeframes)
    """

    def __init__(self, scale=True):
        self.model_space_ = None
        self.basis_ = None
        self.scale = scale
        self.mean = None

    def fit(self, X, y=None):
        """
        Fit X using hyperalignment model
        Parameters
        ---------
        X: list of array of shape n_features, n_timeframes
        """

        # We demean the data and compute the model space
        X_ = [x - x.mean(axis=1, keepdims=True) for x in X]
        model_space = compute_model_space(self.scale, X_)
        self.mean = [x.mean(axis=1, keepdims=True) for x in X]

        # We compute the transformation
        # matrices for each subject to the mean
        # vectors in the common space.

        basis = []
        for i in range(len(X)):
            R_i, sc_i = hyperalign(X_[i], model_space, scale=self.scale)
            basis.append((R_i, sc_i))

        self.model_space_ = model_space
        self.basis_ = basis

        return self

    def transform(self, X, y=None):
        """
        Transform X using hyperalignment model
        Parameters
        ---------
        X: list of array of shape n_features, n_timeframes
        """

        X_transform_list = []
        for i in range(len(X)):
            R_i, sc_i = self.basis_[i]
            X_transform_i = sc_i * R_i.dot(X[i] - self.mean[i])
            X_transform_list.append(X_transform_i)

        return X_transform_list

    def inverse_transform(self, X, y=None):
        """
        Inverse X using hyperalignment model

        Parameters
        ---------
        X: list of array of shape n_features, n_timeframes
        """

        X_transform_list = []
        for i in range(len(X)):
            R_i, sc_i = self.basis_[i]
            X_transform_i = R_i.T.dot(X[i]) / sc_i + self.mean[i]
            X_transform_list.append(X_transform_i)

        return X_transform_list
