import numpy as np
import logging
from functional_alignment.baseSRM import BaseSRM
from nilearn._utils.cache_mixin import cache
from functional_alignment.utils import load_img
from sklearn.utils import check_random_state
from sklearn.externals.joblib import Memory
from functional_alignment.isrm_encode import isrm_encode, structure_matrix, laplacian_matrix, similarity_matrix
from functional_alignment.deterministic_srm import DeterministicSRM
from scipy import linalg
import sys
from time import time


def _update_dict(dictionary, Y, code, delta, common_dict, verbose=False, return_r2=False,
                 random_state=None, positive=False):
    """Update the dense dictionary factor in place.
    Parameters
    ----------
    dictionary : array of shape (n_features, n_components)
        Value of the dictionary at the previous iteration.
    Y : array of shape (n_features, n_samples)
        Data matrix.
    code : array of shape (n_components, n_samples)
        Sparse coding of the data against which to optimize the dictionary.
    verbose:
        Degree of output the procedure will print.
    return_r2 : bool
        Whether to compute and return the residual sum of squares corresponding
        to the computed solution.
    random_state : int, RandomState instance or None, optional (default=None)
        If int, random_state is the seed used by the random number generator;
        If RandomState instance, random_state is the random number generator;
        If None, the random number generator is the RandomState instance used
        by `np.random`.
    positive : boolean, optional
        Whether to enforce positivity when finding the dictionary.
        .. versionadded:: 0.20
    Returns
    -------
    dictionary : array of shape (n_features, n_components)
        Updated dictionary.
    """
    n_components = len(code)
    n_features = Y.shape[0]
    random_state = check_random_state(random_state)
    # Get BLAS functions
    gemm, = linalg.get_blas_funcs(('gemm',), (dictionary, code, Y))
    ger, = linalg.get_blas_funcs(('ger',), (dictionary, code))
    nrm2, = linalg.get_blas_funcs(('nrm2',), (dictionary,))
    # Residuals, computed with BLAS for speed and efficiency
    # R <- -1.0 * U * V^T + 1.0 * Y
    # Outputs R as Fortran array for efficiency
    R = gemm(-1.0, dictionary, code, 1.0, Y)
    for k in range(n_components):
        # R <- 1.0 * U_k * V_k^T + R
        R = ger(1.0, dictionary[:, k], code[k, :], a=R, overwrite_a=True)
        dictionary[:, k] = np.dot(R, code[k, :]) + delta * common_dict[:, k]
        if positive:
            np.clip(dictionary[:, k], 0, None, out=dictionary[:, k])
        # Scale k'th atom
        # (U_k * U_k) ** 0.5
        atom_norm = nrm2(dictionary[:, k])
        if atom_norm < 1e-10:
            if verbose == 1:
                sys.stdout.write("+")
                sys.stdout.flush()
            elif verbose:
                print("Adding new random atom")
            dictionary[:, k] = random_state.randn(n_features)
            if positive:
                np.clip(dictionary[:, k], 0, None, out=dictionary[:, k])
            # Setting corresponding coefs to 0
            code[k, :] = 0.0
            # (U_k * U_k) ** 0.5
            atom_norm = nrm2(dictionary[:, k])
            dictionary[:, k] /= atom_norm
        else:
            dictionary[:, k] /= atom_norm
            # R <- -1.0 * U_k * V_k^T + R
            R = ger(-1.0, dictionary[:, k], code[k, :], a=R, overwrite_a=True)
    if return_r2:
        R = nrm2(R) ** 2.0
        return dictionary, R
    return dictionary


def _batch_isrm_fit(masker,
                     imgs,
                     confounds=None,
                     n_components=10,
                     random_state=None,
                     memory=None,
                     memory_level=0,
                     l1_weight=1.,
                     grad_weight=1,
                     common_weight=1.,
                     n_iter=10,
                     shared_init=None,
                     positive=True,
                     structure_matrix=None,
                     exp_params={}):
    """
    Fit the model
    Parameters
    ----------
    ToDo: Remove common weight
    Returns
    -------

    """
    alpha = l1_weight
    beta = grad_weight

    logger = logging.getLogger(__name__)
    logger.info("Initialization")
    masker.fit()

    random_state = check_random_state(random_state)
    logger.info("Done")

    n_subjects = len(imgs)

    if structure_matrix is None:
        Lx, Ly, Lz = laplacian_matrix(masker.mask_img_.get_data().astype(bool))
        G = (Lx.T.dot(Lx) + Ly.T.dot(Ly) + Lz.T.dot(Lz))
    else:
        G = structure_matrix

    common_shared_response = None
    shared_response = {}
    W = {}
    errors = []
    for nn in range(n_iter):
        pen_error = 0
        l2_error = 0
        common_weight = 10 ** nn
        for subject, confound in zip(range(n_subjects), confounds):
            sys.stdout.write("Loading subject %i" %  subject)
            t0 = time()
            x_subject, masker = cache(
                load_img,
                memory=memory,
                memory_level=memory_level,
                func_memory_level=2
            )(
                masker,
                imgs[subject],
                axis=0,
                confound=confound
            )

            n_voxels, n_timeframes = x_subject.shape

            if common_shared_response is None:
                # Initializing with random dictionary
                if shared_init is None:
                    common_shared_response = np.random.rand(n_timeframes, n_components)
                if type(shared_init) == np.ndarray:
                    common_shared_response = shared_init
                if shared_init == "DetSRM":
                    common_shared_response = DeterministicSRM(n_components=n_components, **exp_params).fit(imgs).shared_response.T

                G = beta * n_timeframes / float(n_components) * G
                alpha = alpha * n_timeframes / float(n_components)

            shared_response[subject] = shared_response.get(subject, common_shared_response)
            W[subject], dE, pen_error, l2_error = isrm_encode(x_subject.T, shared_response[subject], alpha, G,
                                   random_state.randint(0, 1e3), tol=1e-6, max_iter=100, positive=positive)
            print("Encoding time", time() - t0)
            pen_error += pen_error

            logger.debug("Finding dictionary")
            shared_response[subject], residuals = _update_dict(shared_response[subject],
                                                               x_subject.T,
                                                               W[subject],
                                                               common_weight,
                                                               common_shared_response,
                                                               return_r2=True,
                                                               random_state=random_state,
                                                               positive=False)
            l2_error += 0.5 * residuals
        common_shared_response = np.mean([shared_response[s] for s in range(n_subjects)], axis=0)

        pen_error += common_weight * 0.5 * np.sum(
            [np.sum((shared_response[subject] - common_shared_response)**2) for subject in range(n_subjects)]
        )

        print("pen error", pen_error)
        print("l2_error", l2_error)
        errors.append(pen_error + l2_error)

    if n_iter == 0:
        common_shared_response = DeterministicSRM(n_components=n_components, **exp_params).fit(imgs).shared_response.T
        for subject in range(n_subjects):
            shared_response[subject] = common_shared_response

    for subject, confound in zip(range(n_subjects), confounds):
        logger.debug("Loading subject %i" % subject)
        x_subject, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            imgs[subject],
            axis=0,
            confound=confound
        )

        if n_iter == 0:
            n_voxels, n_timeframes = x_subject.shape
            G = beta * n_timeframes / float(n_components) * G
            alpha = alpha * n_timeframes / float(n_components)
            n_iter = 1

        W[subject], _, _, _ = isrm_encode(x_subject.T, shared_response[subject], alpha, G,
                                        random_state.randint(0, 1e3), tol=1e-6, max_iter=5, positive=positive)

    return masker, [W[subject].T for subject in range(n_subjects)], shared_response, None


class BatchISRM(BaseSRM):
    """
    Shared Response Model
    Maps data X_1,...,X_{n_samples} to the model such
    that frobenius norm
    \sum_i ||X_i - W_i S||^2
    is minimized and W_i are sparse

    S are the shared coordinates
    W_i is the basis of sample i
    """

    def __init__(self, n_components,
                 verbose=False, random_state=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 transform_method="mean", n_jobs=1,
                 l1_weight=1.,
                 grad_weight=1.,
                 common_weight=1.,
                 max_iter=2,
                 shared_init=None,
                 positive=True,
                 structure_matrix=None,
                 ):
        """
        Shared Response Model
        Maps data X_1,...,X_{n_samples} to the model such
        that frobenius norm
        \sum_i ||X_i - W_i S||^2
        is minimized and W_i are sparse

        S are the shared coordinates
        W_i is the basis of sample i

        Parameters
        ----------
        alpha: float, optional, default=1.
            common map smoothness and sparsity controlling parameter.
        l1_ratio: float
            common map relative importance of sparsity / smoothness
            1: only sparsity penalty
            0: only smoothness penalty
        alpha_init: float
            Sparsity controlling parameter in the initial dictionary learning initialization
        mu: float
            Smoothness between maps controlling parameter

        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=transform_method,)

        self.exp_params = {
            "random_state": random_state,
            "mask": mask,
            "smoothing_fwhm": smoothing_fwhm,
            "standardize": standardize,
            "detrend": detrend,
            "low_pass": low_pass,
            "high_pass": high_pass,
            "t_r": t_r,
            "target_affine": target_affine,
            "target_shape": target_shape,
            "mask_strategy": mask_strategy,
            "mask_args": mask_args,
            "n_jobs": n_jobs,
            "verbose": verbose,
            "memory": memory,
            'memory_level': memory_level
        }

        self.l1_weight = l1_weight
        self.grad_weight = grad_weight
        self.n_iter = max_iter
        self.shared_init = shared_init
        self.common_weight = common_weight
        self.positive = positive
        self.structure_matrix = structure_matrix

    def _fit(self, imgs, confounds=None):
        """
        Fit the model
        """
        self.masker, self.basis, self.shared_response, self.errors = cache(
            _batch_isrm_fit,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          l1_weight=self.l1_weight,
          grad_weight=self.grad_weight,
          common_weight=self.common_weight,
          n_iter=self.n_iter,
          shared_init=self.shared_init,
          positive=self.positive,
          exp_params=self.exp_params,
          structure_matrix=self.structure_matrix
          )
        return self