from sklearn.base import BaseEstimator, TransformerMixin
from nilearn.regions import Parcellations
import copy
import numpy as np
from functional_alignment.hyperalignment import hyperalign
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.image import grid_to_graph
from joblib import Parallel, delayed
from time import time
import pickle
from functional_alignment.utils import load_img
from sklearn.linear_model import RidgeCV as SkRidgeCV
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.rrr_cv import RRRCV
from scipy.sparse import dok_matrix, coo_matrix, bsr_matrix
from sklearn.externals.joblib import Memory
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
import itertools
from nilearn.image import load_img
from scipy.sparse import block_diag, coo_matrix


def fast_dot(sp_X, Y):
    return sp_X.dot(Y)


def piecewise_dot(labels, tf, X):
    """ piecewise dot product (avoids forming large matrices)"""
    X_ = np.zeros_like(X)
    for i in np.unique(labels):
        X_[labels == i] = tf[i].dot(X[labels == i])
    return X_


def create_fit_matrix(method, labels_values, labels, X_, Y_, n_jobs=1, verbose=0,
                      infos=None):
    """ Function that does the fit

    Parameters
    ----------
    method: 'RidgeCV', 'RRRCV' or 'mean'
            algorithm used to perform mapping between regions
    labels_values:  numpy array,
                   the unique values of the label.
    labels: numpy array,
        the label of each voxel used in the piecewise fit
    X_: list of arrays,
        input data
    Y_: list of arrays,
        target data
    n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
    verbose: integer, optional,
            The verbosity level of the


    Returns
    -------
    fit: list of arrays,
         the estimated transformation matrices
    infos: list of strings,
           Corresponding information
    """
    """
    fit_matrix_list = []
    for X_i, Y_i, infos in generate_Xi_Yi(labels_values, labels, X_, Y_, infos):
        fit_matrix_list.append(fit_piecewise_align(X_i, Y_i, infos, method))
    """
    fit_matrix_list = Parallel(n_jobs, backend="threading", verbose=verbose)(
        delayed(fit_piecewise_align)(
            X_i, Y_i, infos, method
        ) for X_i, Y_i, infos in generate_Xi_Yi(labels_values, labels, X_, Y_, infos)
    )
    fit_ = [fit_matrix[0] for fit_matrix in fit_matrix_list]
    infos_ = [fit_matrix[1] for fit_matrix in fit_matrix_list]
    # fit_ = block_diag(matrices_, format='csr')

    return fit_, infos_


def hierarchical_k_means(X, n_clusters):
    """ use a recursive k-means to cluster X"""
    from sklearn.cluster import MiniBatchKMeans
    n_big_clusters = int(np.sqrt(n_clusters))
    mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_big_clusters, batch_size=1000,
                          n_init=10, max_no_improvement=10, verbose=0,
                          random_state=0).fit(X)
    coarse_labels = mbk.labels_
    fine_labels = np.zeros_like(coarse_labels)
    q = 0
    for i in range(n_big_clusters):
        n_small_clusters = int(
            n_clusters * np.sum(coarse_labels == i) * 1. / X.shape[0])
        n_small_clusters = np.maximum(1, n_small_clusters)
        mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_small_clusters,
                              batch_size=1000, n_init=10, max_no_improvement=10, verbose=0,
                              random_state=0).fit(X[coarse_labels == i])
        fine_labels[coarse_labels == i] = q + mbk.labels_
        q += n_small_clusters

    def _remove_empty_labels(labels):
        vals = np.unique(labels)
        inverse_vals = - np.ones(labels.max() + 1).astype(np.int)
        inverse_vals[vals] = np.arange(len(vals))
        return inverse_vals[labels]

    return _remove_empty_labels(fine_labels)


def create_labels(X, mask, n_pieces, method='k_means', memory=Memory(cachedir=None)):
    """
    Separates input data into pieces

    Parameters
    ----------
    X: numpy array of shape (n_samples, n_features)
        input data
    mask: numpy array
        the mask from which X is produced
    n_pieces: int
        number of different labels

    Returns
    -------
    labels: numpy array of shape (n_samples)
        labels[i] is the label of array i
        (0 <= labels[i] < n_samples)
    """
    if method == 'k_means':
        labels = hierarchical_k_means(X, n_pieces)
    else:
        ward = Parcellations(method='ward', n_parcels=n_pieces,
                             standardize=False, smoothing_fwhm=0,
                             memory=memory, memory_level=1,
                             verbose=1)
        ward.fit(X)
        labels = ward.labels_img_.get_data()[mask.get_data() > 0]
    return labels


def generate_Xi_Yi(labels_values, labels, X, Y, infos):
    for k in range(len(labels_values)):
        label = labels_values[k]
        i = label == labels
        if (k + 1) % 25 == 0:
            print("Fitting area: " + str(k + 1) +
                  "/" + str(len(labels_values)))

        if infos is None:
            yield X[i], Y[i], infos
        elif type(infos) == np.ndarray:
            alpha = infos[i][0]
            yield X[i], Y[i], alpha
        elif len(infos) == 2:
            alpha = infos[0][i][0]
            r = infos[1][i][0]
            yield X[i], Y[i], (alpha, r)
        else:
            print(infos)


def optimal_permutation(X_i, Y_i):
    """ Compute the optmal permutation matrix of X_i toward Y_i
    Fixme: should be moced to somewhere else"""
    from sklearn.utils.linear_assignment_ import linear_assignment
    from sklearn.metrics.pairwise import pairwise_distances
    import scipy.sparse as sps
    dist = pairwise_distances(X_i, Y_i)
    u = linear_assignment(dist)
    permutation = sps.csr_matrix((np.ones(X_i.shape[0]), (u[:, 0], u[:, 1]))).T
    return permutation


def fit_piecewise_align(X_i, Y_i, infos, method):
    if method == "scaled_orthogonal":
        R_i, sc_i = hyperalign(X_i, Y_i, scale=True)
        R_sc_i = R_i * sc_i
        return R_sc_i, None
    if method == "orthogonal":
        R_i, sc_i = hyperalign(X_i, Y_i, scale=False)
        R_sc_i = R_i * sc_i
        return R_sc_i, None
    if method == "RidgeCV":
        R_i = RidgeCV(alphas=[1e-12, 0.1, 1, 10, 100, 1000, 10000],
                      n_splits=4)

        R_i.fit(X_i.T, Y_i.T)
        return R_i.b.T.dot(R_i.a.T), R_i.alpha
    if method == "RRRCV":
        # Heuristic to find a suitable number of components
        forced_k = min(int(np.floor(X_i.T.shape[1] / 2)), 50)
        R_i = RRRCV(alphas=[1e-12, 0.1, 1, 10, 100, 1000, 10000],
                    n_splits=4)
        R_i.fit(X_i.T, Y_i.T, forced_k=forced_k)
        return R_i.b.dot(R_i.a.T), R_i.alpha
    if method == "permutation":
        R_i = optimal_permutation(X_i, Y_i)
        return R_i, None
    if isinstance(method, SkRidgeCV):
        method.fit(X_i.T, Y_i.T)
        return method.coef_, method.alpha_


class PieceWiseAlignment(BaseEstimator, TransformerMixin):
    """
    Decompose the source and target images into source and target regions
     Use alignment algorithms to align source and target regions independantly.
    """

    def __init__(self, n_pieces=100, method="mean", perturbation=False, n_bootstrap=1, clustering_method="k_means",
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Decompose the source and target images into source and target regions
        Use alignment algorithms to align source and target regions independantly.

        Parameters
        ----------
        n_pieces: int
            number of regions
        method: 'mean', 'RidgeCV', 'RRRCV'(with fixed number of components, 'scaled_orthogonal', 'permutation'
            algorithm used to perform mapping between regions
        perturbation: bool
            If true, the penalized transformation is the one that regress
             the source X to the difference between the target and the source Y - X.
            If false, the penalized transformaion is the one that regress the source X to
            the target Y
        n_bootstrap: int, optional
            How many bootstrap averages are used. 1 by default
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        """
        self.n_pieces = n_pieces
        self.method = method
        self.perturbation = perturbation
        self.n_bootstrap = n_bootstrap

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs
        self.clustering_method = clustering_method

        self.verbose = verbose

        self.infos_ = None

    def fit(self, X, Y):
        """
        Fit data X and Y and learn transformation to map X to Y
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Y: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           target data

<        Returns
        -------
        self
        """
        from sklearn.model_selection import ShuffleSplit
        self.masker_ = check_embedded_nifti_masker(self)
        self.masker_.n_jobs = 1  # self.n_jobs
        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit([X])
        else:
            self.masker_.fit()
        self.mask_img_ = load_img(self.masker_.mask_img)

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T

        Y_ = self.masker_.transform(Y)
        if type(Y_) == list:
            Y_ = np.concatenate(Y_, axis=0)
        Y_ = Y_.T

        if self.perturbation:
            Y_ = Y_ - X_

        """
        if self.method == "mean":
            pass
        else:
            labels = create_labels(
                X_, self.mask_img_.get_data(), self.n_pieces, memory=self.memory)
        """

        self.fit_, self.labels_, self.infos_ = [], [], []
        rs = ShuffleSplit(n_splits=self.n_bootstrap,
                          test_size=.8, random_state=0)
        """
        for train_index, _ in rs.split(Y_.T):
            labels_, fit_, infos_ = fit_one_parcellation(
                X_, Y_, self.masker_.mask_img.get_data(), self.n_pieces, self.method,
                train_index, self.memory, n_jobs=1, verbose=self.verbose,
                infos=self.infos_)
            self.fit_.append(fit_)
            self.labels_.append(labels_)
            self.infos_.append(infos_)
        """
        outputs = Parallel(n_jobs=self.n_jobs)(
            delayed(fit_one_parcellation)(
                X_, Y_, self.masker_.mask_img, self.n_pieces, self.method,
                train_index, self.clustering_method, self.memory, n_jobs=1, verbose=self.verbose,
                infos=self.infos_) for train_index, _ in rs.split(Y_.T))

        self.labels_ = [output[0] for output in outputs]
        self.fit_ = [output[1] for output in outputs]
        self.infos_ = [output[2] for output in outputs]

        """
        if self.n_pieces > 1:
            labels = create_labels(Y_, self.masker_.mask_img.get_data(),
                                   self.n_pieces, memory=self.memory)
            self.masker_.inverse_transform(labels).to_filename('/tmp/labels.nii.gz')
        else:
            labels = np.zeros(self.masker_.mask_img.get_data().sum())

        fit_, infos_ = self.memory.cache(
            create_fit_matrix, ignore=["n_jobs", "verbose", "infos"])(
                self.method, np.unique(labels), labels, X_, Y_, n_jobs=self.n_jobs,
                verbose=self.verbose, infos=self.infos_
            )
        self.fit_ = fit_
        self.infos_ = infos_
        self.labels_ = labels
        """
        return self

    def transform(self, X):
        """
        Predict data from X
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Returns
        -------
        X_transform: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           predicted data
        """

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T

        if self.method == "mean":
            return self.masker_.inverse_transform(X_.T)
        else:
            # X_transform = self.memory.cache(fast_dot)(self.fit_, X_)
            X_transform = np.zeros_like(X_)
            for i in range(self.n_bootstrap):
                X_transform += piecewise_dot(self.labels_[i], self.fit_[i], X_)

            X_transform /= self.n_bootstrap
            if self.perturbation:
                X_transform += X_

            return self.masker_.inverse_transform(X_transform.T)

    def sparse_transform(self, bag):
        def get_indexes(x, labels): return [i for (
            y, i) in zip(labels, range(len(labels))) if x == y]
        unique_labels, counts = np.unique(
            self.labels_[bag], return_counts=True)
        row, col, data = [], [], []
        for k in range(len(unique_labels)):
            label = unique_labels[k]
            index_list = get_indexes(label, self.labels_[bag])
            for i in itertools.product(index_list, index_list):
                row.append(i[0])
                col.append(i[1])
            for j in itertools.product(range(counts[k]), range(counts[k])):
                data.append(self.fit_[bag][k][j])
        return coo_matrix((data, (row, col)), shape=(
            len(self.labels_[bag]), len(self.labels_[bag]))).tocsr()


def fit_one_parcellation(X, Y, mask, n_pieces, method, train_index, clustering_method, mem,
                         n_jobs=1, verbose=True, infos=None):
    """Do one parcellation + call of fit on the data"""
    if n_pieces > 1:
        labels = create_labels(X[:, train_index], mask,
                               n_pieces, clustering_method, memory=mem)
    else:
        labels = np.zeros(mask.get_data().sum())

    unique_labels, counts = np.unique(labels, return_counts=True)
    print(counts)
    fit, infos = create_fit_matrix(
        method, unique_labels, labels, X, Y, n_jobs=n_jobs,
        verbose=verbose, infos=None)

    return labels, fit, infos
