from nilearn.image import index_img
import numpy as np
from functional_alignment.pca_srm import PCASRM
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.dictionary_srm import DictionarySRM
from functional_alignment.graphnet_srm import GraphNetSRM
from time import time
import pandas as pd
from scipy.stats import pearsonr


def load_sherlock_data(
        images_path="/storage/data/sherlock_movie_watch_dataset/movie_files/sherlock_movie_s%i.nii",
        label_path=None,
        subjects=[1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]):
    """

    Parameters
    ----------
    images_path: path to images
    label_path: path to labels
    subjects: list
        subject number from 1 to 17 Ex: [1, 2, 3]
        5 needs to be removed because data are incomplete

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for subject in subjects:
        image_list.append(images_path % subject)

    if label_path is None:
        data = []
        for subject in range(len(image_list)):
            data_ = []
            for i in range(0, 50):
                start, stop = i * 39 + 3, (i + 1) * 39 + 3
                fmri_image = index_img(image_list[subject], range(start, stop))
                data_.append(fmri_image)
            data.append(data_)
    else:
        labels = pd.DataFrame.from_csv(
        label_path, sep=";", index_col=0)
        data = []
        for subject in range(len(image_list)):
            data_ = []
            for i in range(0, 50):
                start, stop = labels.iloc[i]['Onset'] - 1, labels.iloc[i]['Offset']
                fmri_image = index_img(image_list[subject], range(start, stop))
                data_.append(fmri_image)
            data.append(data_)
    return data


def build_shared_response(
        images_path="/storage/data/sherlock_movie_watch_dataset/movie_files/sherlock_movie_s%i.nii",
        subjects=[1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17],
        label_path=None,
        mask_memory="/storage/tompouce/hrichard/sherlock",
        mask_img="../sherlock_mask/sherlock1FR_17ss_ISC_thr0.2.nii",
        result_directory="/storage/workspace/hrichard/M2_internship/results/semantic_srm/",
        mask_name="ThresholdedFullbrain"
):
    exp_params = {
        "detrend": True,
        "standardize": True,
        "memory": mask_memory,
        "mask": mask_img,
        "mask_strategy": "epi",
        "n_jobs": 1,
        "memory_level": 5,
        "t_r": 2,
        "verbose": 1
    }

    data = np.array(load_sherlock_data(images_path, subjects=subjects, label_path=label_path))
    algorithms = [
       (PCASRM(n_components=50, **exp_params), "PCA_50"),
       (DictionarySRM(50, alpha=1, init_method="ortho_srm", **exp_params), "FastDictSRM_50_1"),
       (ProbabilisticSRM(n_components=20, **exp_params), "ProbSRM_20"),
       (ProbabilisticSRM(n_components=50, **exp_params), "ProbSRM_50"),
       (DeterministicSRM(n_components=50, **exp_params), "DetSRM_50"),
    ]

    for algorithm, name in algorithms:
       print(name)
       t0 = time()
       algorithm.fit(data[:, 0:25])
       fitting_time = time() - t0
       print("fitting time", fitting_time)
       shared = algorithm.transform(data[:, 0:50])
       transform_time = time() - fitting_time - t0
       print("transform time", transform_time)
       with open("./log", "a") as log:
           log.write(name + "\n")
           log.write("fitting time: " + str(fitting_time) + "\n")
           log.write("transform time: " + str(transform_time) + "\n")

       if label_path is None:
           np.save(result_directory + "label_none" + "mask_" + mask_name + "algo_" + name, shared)
       else:
           np.save(result_directory + "mask_" + mask_name + "algo_" + name, shared)


def load_fmri(result_directory="/storage/workspace/hrichard/M2_internship/results/semantic_srm/",
              mask_name="ThresholdedFullbrain",
              algo_name="ProbSRM_20",
              label_path=None):
    if label_path is None:
        shared = np.load(result_directory + "label_nonemask_" + mask_name + "algo_" + algo_name + ".npy").T
    else:
        shared = np.load(result_directory + "mask_" + mask_name + "algo_" + algo_name + ".npy").T
    return shared


def load_scenes_id(labels_path="/storage/data/sherlock_movie_watch_dataset/labels/movie_scene_labels.csv"):
    labels = pd.DataFrame.from_csv(
        labels_path, sep=";", index_col=0
    )

    id_per_scene = []
    start = 0
    for i in range(0, 50):
        _start, _stop = labels.iloc[i]['Onset'] - 1, labels.iloc[i]['Offset']
        d = _stop - _start
        id_per_scene.append((start,start + d))
        start = start + d
    return np.array(id_per_scene)


def load_semantic(weight_path="../semantic_representations/avg_100dim_wordvec_mat_Sep12_weighted.npz",
                  unweighted_path="../semantic_representations/avg_100dim_wordvec_mat_Sep12_unweighted.npz",
                  labels_path=None
                  ):

    if labels_path is None:
        weighted_semantic = np.load(weight_path)["vecs"][3:1953]
        unweighted_semantic = np.load(unweighted_path)["vecs"][3:1953]
        return weighted_semantic, unweighted_semantic
    else:
        labels = pd.DataFrame.from_csv(
            labels_path, sep=";", index_col=0
        )

        weighted_semantic = np.load(weight_path)["vecs"]
        unweighted_semantic = np.load(unweighted_path)["vecs"]

        weighted_semantic_per_scene = []
        unweighted_semantic_per_scene = []

        for i in range(0, 50):
            start, stop = labels.iloc[i]['Onset'] - 1, labels.iloc[i]['Offset']
            weighted_semantic_per_scene.append(weighted_semantic[start:stop, :])
            unweighted_semantic_per_scene.append(unweighted_semantic[start:stop, :])

        return np.concatenate(weighted_semantic_per_scene), np.concatenate(unweighted_semantic_per_scene)


def scene_decoding(Y_pred, Y_true, indexes):
    ids = indexes - indexes[0, 0]
    assert Y_pred.shape == Y_true.shape
    assert ids[-1, 1] == len(Y_pred)

    dist = np.zeros((len(ids), len(ids)))
    for i in range(len(ids)):
        scene_i = slice(ids[i][0], ids[i][1])
        for j in range(len(ids)):
            scene_j = slice(ids[j][0], ids[j][1])
            true = Y_true[scene_i].mean(axis=0)
            pred = Y_pred[scene_j].mean(axis=0)
            dist[i, j] = pearsonr(true, pred)[0]

    score = np.argmax(dist, axis=0) == np.array(range(25))
    precision = np.sum(score) / float(len(score))
    return precision


def scene_decoding25(Y_pred, Y_true):
    slices = []
    for j in range(0, len(Y_pred), len(Y_pred) / 25):
        slices.append(slice(j, j+len(Y_pred)/25))
    corr = np.zeros((25, 25))
    for i in range(len(slices)):
        for j in range(len(slices)):
            slice_i = slices[i]
            slice_j = slices[j]
            corr[i, j] = pearsonr(Y_pred[slice_i].flatten(), Y_true[slice_j].flatten())[0]

    scores = np.argmax(corr, axis=1) == np.array(range(25))

    precision = np.sum(scores) / float(len(scores))
    return precision


def scene_decoding_n(Y_pred, Y_true, n):
    slices = []
    for j in range(0, len(Y_pred), int(len(Y_pred) / n)):
        slices.append(slice(j, j + int(len(Y_pred) / n)))
    corr = np.zeros((n, n))
    for i in range(len(slices)):
        for j in range(len(slices)):
            slice_i = slices[i]
            slice_j = slices[j]
            corr[i, j] = pearsonr(Y_pred[slice_i].flatten(), Y_true[slice_j].flatten())[0]

    scores = np.argmax(corr, axis=1) == np.array(range(n))

    precision = np.sum(scores) / float(len(scores))
    return precision


def concatenate_past_features(A, k):
    A_list = [A[k:]]
    for i in range(k):
        A_list.append(A[i:-k+i])
    return np.concatenate(A_list, axis=1)
