import numpy as np
from scipy import linalg
from scipy import stats
from scipy.sparse import linalg as splinalg
from sklearn import metrics
from functional_alignment.reduced_rank_regression import RRR
from sklearn.model_selection import ShuffleSplit
from functional_alignment.utils import generate_folds


class RRRCV(RRR):
    """
    scikit-learn like estimator
    """

    def __init__(self, alphas=[1.], ks=[1], n_splits=2):
        self.alphas = alphas
        self.ks = ks
        self.alpha = None
        self.k = None
        self.n_splits = n_splits

    def fit(self, X, Y, forced_alpha=None, forced_k=None):
        self.a, self.b, self.alpha, self.k = rrr_cv_fit(X,
                                                        Y,
                                                        ks=self.ks,
                                                        alphas=self.alphas,
                                                        n_splits=self.n_splits,
                                                        forced_alpha=forced_alpha,
                                                        forced_k=forced_k)
        return self


def rrr_cv_fit(X, Y, ks=[1.], alphas=[1e-6], n_splits=2, forced_alpha=None, forced_k=None):
    """
    Reduced Rank Regression

    estimate a RRR model by solving a model of the form

    ||Y - X A B^T ||^2 + alpha tr(A^T A)

    Parameters
    ----------
    X : array, shape (t, v)
    Y : array, shape (t, v)

    Returns
    -------
    A : array, shape (v, k)
    B : array, shape (v, k)
    """

    if forced_k is None or forced_alpha is None:
        best_score = -np.inf
        for train_index, test_index in generate_folds(X, n_splits):
            X_train = X[train_index]
            Y_train = Y[train_index]
            X_test = X[test_index]
            Y_test = Y[test_index]

            if forced_alpha is not None:
                alphas = [forced_alpha]

            if forced_k is not None:
                ks = [forced_k]

            for alpha in alphas:
                # XXX append column of ones to X
                # here we should distinguish between primal and dual case
                # causing to bug for now in full brain
                YY = Y_train.dot(Y_train.T)
                K1 = X_train.T.dot(Y_train).dot(Y_train.T.dot(
                    X_train)) + 1e-12 * np.eye(X_train.shape[1])
                K2 = X_train.T.dot(
                    X_train) + alpha * np.eye(X_train.shape[1]) + 1e-12 * np.eye(X_train.shape[1])

                eigvals, eigvects = linalg.eigh(K1, b=K2)
                eigvals = eigvals.astype(np.float)
                eigvects = eigvects.astype(np.float)
                order = np.argsort(-eigvals)

                for k in ks:
                    A = eigvects[:, order[:k]]
                    B = Y_train.T.dot(X_train.dot(A))

                    num = ((X_test.dot(A.dot(B.T)) - Y_test) ** 2).sum(axis=0)
                    Y_test_mean = Y_test.mean(axis=0)
                    denom = ((Y_test - Y_test_mean) ** 2).sum(axis=0)
                    score = (1 - num / denom).mean()
                    if best_score <= score:
                        best_score = score
                        best_alpha = alpha
                        best_k = k

        alpha = best_alpha
        k = best_k
    else:
        alpha = forced_alpha
        k = forced_k

    # XXX append column of ones to X
    K1 = X.T.dot(Y).dot(Y.T.dot(X))
    K2 = X.T.dot(X) + alpha * np.eye(X.shape[1])
    eigvals, eigvects = linalg.eigh(K1, b=K2)
    eigvals = eigvals.astype(np.float)
    eigvects = eigvects.astype(np.float)
    order = np.argsort(-eigvals)
    A = eigvects[:, order[:k]]
    B = Y.T.dot(X.dot(A))

    return A, B, alpha, k
