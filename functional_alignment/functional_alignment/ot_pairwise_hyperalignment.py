import ot
import numpy as np
from scipy.spatial.distance import cdist
from sklearn.base import BaseEstimator, TransformerMixin
from nilearn.regions import Parcellations
import copy
import numpy as np
from functional_alignment.hyperalignment import hyperalign
from sklearn.cluster import AgglomerativeClustering
from sklearn.feature_extraction.image import grid_to_graph
from joblib import Parallel, delayed
from time import time
import pickle
from functional_alignment.utils import load_img
from sklearn.linear_model import RidgeCV as SkRidgeCV
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.rrr_cv import RRRCV
from scipy.sparse import dok_matrix, coo_matrix, bsr_matrix
from sklearn.externals.joblib import Memory
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from nilearn.image import load_img
from scipy.sparse import block_diag


def fast_dot(sp_X, Y):
    return sp_X.dot(Y)


def piecewise_dot(labels, tf, X):
    """ piecewise dot product (avoids forming large matrices)"""
    X_ = np.zeros_like(X)
    for i in np.unique(labels):
        X_[labels == i] = tf[i].dot(X[labels == i])
    return X_


def create_fit_matrix(labels_values, labels, X_, Y_, method, metric, reg, n_jobs, verbose):
    """ Function that does the fit

    Parameters
    ----------
    method: str 'regularized' by default use sinkhorn with entropic regularization, can also be 'exact' to use a linear program solver
    reg: float, regularization coefficient to use in sinkhorn
    labels_values:  numpy array,
                   the unique values of the label.
    labels: numpy array,
        the label of each voxel used in the piecewise fit
    X_: list of arrays,
        input data
    Y_: list of arrays,
        target data
    n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
    verbose: integer, optional,
            The verbosity level of the


    Returns
    -------
    fit: list of arrays,
         the estimated transformation matrices
    """

    fit_ = Parallel(n_jobs, backend="threading", verbose=verbose)(
        delayed(fit_piecewise_align)(
            X_i, Y_i, reg=reg, method=method, metric=metric
        ) for X_i, Y_i in generate_Xi_Yi(labels_values, labels, X_, Y_)
    )

    return fit_


def hierarchical_k_means(X, n_clusters):
    """ use a recursive k-means to cluster X"""
    from sklearn.cluster import MiniBatchKMeans
    n_big_clusters = int(np.sqrt(n_clusters))
    mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_big_clusters, batch_size=1000,
                          n_init=10, max_no_improvement=10, verbose=0,
                          random_state=0).fit(X)
    coarse_labels = mbk.labels_
    fine_labels = np.zeros_like(coarse_labels)
    q = 0
    for i in range(n_big_clusters):
        n_small_clusters = int(
            n_clusters * np.sum(coarse_labels == i) * 1. / X.shape[0])
        n_small_clusters = np.maximum(1, n_small_clusters)
        mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_small_clusters,
                              batch_size=1000, n_init=10, max_no_improvement=10, verbose=0,
                              random_state=0).fit(X[coarse_labels == i])
        fine_labels[coarse_labels == i] = q + mbk.labels_
        q += n_small_clusters
    return fine_labels


def create_labels(X, mask, n_pieces, clustering_method='k_means', memory=Memory(cachedir=None)):
    """
    Separates input data into pieces

    Parameters
    ----------
    X: numpy array of shape (n_samples, n_features)
        input data
    mask: numpy array
        the mask from which X is produced
    n_pieces: int
        number of different labels

    Returns
    -------
    labels: numpy array of shape (n_samples)
        labels[i] is the label of array i
        (0 <= labels[i] < n_samples)
    """
    if clustering_method == 'k_means':
        labels = hierarchical_k_means(X, n_pieces)
    else:
        ward = Parcellations(method='ward', n_parcels=n_pieces,
                             standardize=False, smoothing_fwhm=0,
                             memory=memory, memory_level=1,
                             verbose=1)
        ward.fit(X)
        labels = ward.labels_img_.get_data()[mask.get_data() > 0]
    return labels


def generate_Xi_Yi(labels_values, labels, X, Y):
    for k in range(len(labels_values)):
        label = labels_values[k]
        i = label == labels
        if (k + 1) % 25 == 0:
            print("Fitting area: " + str(k + 1) +
                  "/" + str(len(labels_values)))
        yield X[i], Y[i]


def fit_piecewise_align(X_i, Y_i, reg, method, metric):

    # create 1D histograms of length len(X_i) and where all elements have equal weights and sum to 1
    n = len(X_i)
    a = np.ones(n) * 1 / n
    b = np.ones(n) * 1 / n

    M = cdist(X_i, Y_i, metric=metric)

    if method == 'regularized':
        R_i = ot.sinkhorn(a, b, M, reg)
        # entropic regularized OT
    elif method == 'exact':
        R_i = ot.lp.emd(a, b, M)
    elif method == 'stabilized':
        R_i = ot.sinkhorn(
            a, b, M, reg, method='sinkhorn_stabilized')
    elif method == 'epsilon_scaling':
        R_i = ot.sinkhorn(
            a, b, M, reg, method='sinkhorn_epsilon_scaling')
    return R_i * n


def fit_one_parcellation(X, Y, mask, n_pieces, method, metric, regularization_param, train_index, clustering_method, mem, n_jobs, verbose):
    """Do one parcellation + call of fit on the data"""
    if n_pieces > 1:
        labels = create_labels(X[:, train_index], mask,
                               n_pieces, clustering_method, memory=mem)
    else:
        labels = np.zeros(mask.get_data().sum())

    unique_labels, counts = np.unique(labels, return_counts=True)
    print(counts)
    fit = create_fit_matrix(unique_labels, labels, X, Y, method, metric, regularization_param, n_jobs=n_jobs,
                            verbose=verbose)

    return labels, fit


class PieceWiseAlignmentOT(BaseEstimator, TransformerMixin):
    """
    Decompose the source and target images into source and target regions
     Use ot alignment algorithms to align source and target regions independantly.
    """

    def __init__(self, n_pieces=100, method="regularized", metric="kulsinski", reg=10, n_bootstrap=1, clustering_method="k_means",
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Decompose the source and target images into source and target regions
        Use alignment algorithms to align source and target regions independantly.

        Parameters
        ----------
        n_pieces: int
            number of regions
        method: 'regularized' or 'exact'
        n_bootstrap: int, optional
            How many bootstrap averages are used. 1 by default
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing ised.
        """
        self.n_pieces = n_pieces
        self.method = method
        self.metric = metric
        self.regularization_param = reg
        self.n_bootstrap = n_bootstrap

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs
        self.clustering_method = clustering_method
        self.verbose = verbose

    def fit(self, X, Y):
        """
        Fit data X and Y and learn transformation to map X to Y
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Y: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           target data
         Returns
        -------
        self
        """
        from sklearn.model_selection import ShuffleSplit
        self.masker_ = check_embedded_nifti_masker(self)
        self.masker_.n_jobs = self.n_jobs
        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit([X])
        else:
            self.masker_.fit()
        self.mask_img_ = load_img(self.masker_.mask_img)

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T

        Y_ = self.masker_.transform(Y)
        if type(Y_) == list:
            Y_ = np.concatenate(Y_, axis=0)
        Y_ = Y_.T

        self.fit_, self.labels_ = [], []
        rs = ShuffleSplit(n_splits=self.n_bootstrap,
                          test_size=.8, random_state=0)

        outputs = Parallel(n_jobs=self.n_jobs)(
            delayed(fit_one_parcellation)(
                X_, Y_, self.masker_.mask_img, self.n_pieces, self.method, self.metric, self.regularization_param,
                train_index, self.clustering_method, self.memory, n_jobs=1, verbose=self.verbose) for train_index, _ in rs.split(Y_.T))

        self.labels_ = [output[0] for output in outputs]
        self.fit_ = [output[1] for output in outputs]

        return self

    def transform(self, X):
        """
        Predict data from X
        Parameters
        ----------
        X: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           source data
        Returns
        -------
        X_transform: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           predicted data
        """

        X_ = self.masker_.transform(X)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T

        # X_transform = self.memory.cache(fast_dot)(self.fit_, X_)
        X_transform = np.zeros_like(X_)
        for i in range(self.n_bootstrap):
            '''When using ot.da estimators : X_transform_bootstrap = np.zeros_like(X_)
            for i in range(self.n_bootstrap):
                X_transform += piecewise_dot(self.labels_[i], self.fit_[i], X_)

            X_transform /= self.n_bootstrap
            ''
            for j in np.unique(self.labels[i]):
                X_transform_bootstrap[labels[i][j]] = self.fit_[
                    i][j].transform(Xs=X_[labels[i][j]])
            X_transform += X_transform_bootstrap'''
            X_transform += piecewise_dot(self.labels_[i], self.fit_[i], X_)

            # maybe we could introduce some clipping at that point

        X_transform /= self.n_bootstrap

        return self.masker_.inverse_transform(X_transform.T)
