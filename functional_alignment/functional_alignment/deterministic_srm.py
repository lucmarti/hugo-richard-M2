import numpy as np


from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import check_random_state
from sklearn.externals.joblib import Memory
from nilearn._utils.compat import _basestring
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from functional_alignment.hyperalignment import hyperalign
from copy import deepcopy
from functional_alignment.baseSRM import BaseSRM, check_imgs
from nilearn._utils.cache_mixin import cache
from functional_alignment.baseSRM import mask_and_reduce
import itertools
from functional_alignment.utils import load_img
import nibabel as nib


def _reduced_data_sharedresponse(masker,
                                 imgs,
                                 confounds=None,
                                 n_components=10,
                                 random_state=None,
                                 memory=None,
                                 memory_level=0,
                                 n_jobs=1,
                                 max_iter=10,
                                 tol=1e-6,
                                 use_scaling=False
                                 ):
    """
    Compute an initial guess of the shared response in reduced space
    using PCA + SRM algorithm
    the basis refers to sc_i * W_i
    the scale refers to sc_i
    """
    svd_data = mask_and_reduce(
        masker,
        imgs,
        confounds=confounds,
        n_components=n_components,
        random_state=random_state,
        memory=memory,
        memory_level=memory_level,
        n_jobs=n_jobs
    )

    PCA_basis = []
    reduced_data = []
    for i in range(len(svd_data)):
        U, S, V = svd_data[i]

        reduced_data.append(S[:, np.newaxis] * V)
        svd_data[i] = None
        PCA_basis.append(U)

    scale, basis, shared_response = fast_srm(reduced_data,
                                             random_state=random_state,
                                             max_iter=max_iter,
                                             use_scaling=use_scaling
                                             )

    return PCA_basis, scale, basis, shared_response


def _deterministic_srm(masker,
                       imgs,
                       confounds=None,
                       n_components=10,
                       random_state=None,
                       memory=None,
                       memory_level=0,
                       n_jobs=1,
                       tol=1e-6,
                       use_scaling=False,
                       speed="fast"
                       ):
    """
    Fitting of Deterministic SRM
    the basis refers to sc_i * W_i
    the scale refers to sc_i
    """

    if confounds is None:
        confounds = itertools.repeat(confounds)

    if masker is not None:
        masker.fit()
    PCA_basis, scale, basis, shared_response = _reduced_data_sharedresponse(
        masker, imgs, n_components=n_components, random_state=random_state,
        memory=memory, memory_level=memory_level, n_jobs=n_jobs,
        use_scaling=use_scaling, tol=tol, confounds=confounds)

    n_subjects = len(imgs)
    if speed == "very_fast":
        basis = [PCA_basis[i].dot(basis[i]) for i in range(n_subjects)]
    elif speed == "fast":
        basis = []
        scale = []
        for img, confound in zip(imgs, confounds):
            w, s = _compute_subject_basis(img, shared_response, masker,
                                          use_scaling=use_scaling, confound=confound,
                                          memory=memory, memory_level=memory_level)
            basis.append(w)
            scale.append(s)
    else:
        ValueError("The speed parameter %s does not exist in DetSRM" % speed)

    return masker, basis, scale, shared_response


def _compute_shared_response(compressed_data, basis, scale):
    """
    Computes the shared response S using subject basis and scaling
    the basis refers to sc_i * W_i
    the scale refers to sc_i
    """
    s = None
    for m in range(len(basis)):
        data_m = compressed_data[m]
        if s is None:
            s = basis[m].T.dot(data_m)
        else:
            s = s + basis[m].T.dot(data_m)
    s /= np.sum(scale**2)
    return s


def _compute_subject_basis(img, shared_response, masker, use_scaling=False,
                           confound=None, memory=None, memory_level=None):
    """
    Computes subject basis and scaling using shared space
    the basis refers to sc_i * W_i
    the scale refers to sc_i
    """

    if type(img) == np.ndarray:
        X_i = np.array(img).T
    else:
        X_i, masker = cache(
            load_img,
            memory=memory,
            memory_level=memory_level,
            func_memory_level=2
        )(
            masker,
            img,
            axis=0,
            confound=confound
        )
    R, sc = hyperalign(shared_response, X_i, scale=use_scaling)
    return sc * R, sc


def fast_srm(reduced_data, random_state=None, max_iter=10, tol=1e-6,
             use_scaling=False, n_components=None):
    """
    Computes shared response and basis in reduced space
    the basis refers to sc_i * W_i
    the scale refers to sc_i

    Parameters
    ----------
    reduced_data: list of n_subjects np array of shape n_voxels, n_timeframes
        The reduced data
    random_state: RandomState
    max_iter: int
    tol: int
    use_scaling: bool
        If True the scaling procedure is used
    n_components: int or None
        number of components if n_voxels != n_components
    Returns
    -------
    scale: np array of shape n_subjects
    shared_response: np array of shape n_components, n_timeframes
    basis: list of n_subjects arrays of shape n_voxels, n_components
    """

    n_subjects = len(reduced_data)
    basis = []
    scale = []
    random_state = check_random_state(random_state)
    for subject in range(n_subjects):
        n_voxels, n_timeframes = reduced_data[subject].shape
        if n_components is None:
            n_components = n_voxels
        q = create_orthogonal_matrix(n_voxels, n_components,
                                     random_state=random_state)
        basis.append(q)
        scale.append(1.)
    scale = np.array(scale)

    shared_response = _compute_shared_response(reduced_data, basis, scale)
    for n_iter in range(max_iter):
        for i in range(n_subjects):
            X_i = reduced_data[i]
            R, sc = hyperalign(shared_response, X_i, scale=use_scaling)
            basis[i] = sc * R

        shared_response = _compute_shared_response(reduced_data, basis, scale)

        if np.sum([np.linalg.norm(reduced_data[i] - basis[i].dot(shared_response))
                   for i in range(n_subjects)], axis=0) < tol:
            break

    return scale, basis, shared_response


def create_orthogonal_matrix(rows, cols, random_state=None):
    """
    Creates matrix W with orthogonal columns:
    W.T.dot(W) = I
    Parameters
    ----------
    rows: int
        number of rows
    cols: int
        number of columns
    random_state : int or RandomState
        Pseudo number generator state used for random sampling.
    Returns
    ---------
    Matrix W of shape (rows, cols) such that W.T.dot(W) = np.eye(cols)
    """
    v = rows
    k = cols
    if random_state is None:
        rnd_matrix = np.random.rand(v, k)
    else:
        rnd_matrix = random_state.rand(v, k)
    q, r = np.linalg.qr(rnd_matrix)
    return q


class DeterministicSRM(BaseSRM):
    """ Deterministic Shared Response Model

    Performs a low rank decomposition on each subject
    which consists of a common response and
    an orthogonal subject-specific basis.

    It finds the shared coordinates S and
    for any subject X_i, the basis W_i
    that the sum of Frobenius norms
    sum_i || X_i - sc_i W_i S ||^2
    is minimized under the constraints:
    * W_i.T W_i = I_k

    X_i is the data of subject i
    S are the shared coordinates
    W_i is the basis of subject i
    sc is the scaling parameter

    **References:**
    Po-Hsuan Chen, Janice Chen, Yaara Yeshurun
    Uri Hasson, James V. Haxby, Peter J. Ramadge,
    2015: A reduced-Dimension fMRI Shared Response Model
    """

    def __init__(self, n_components=20, max_iter=10,
                 use_scaling=False, random_state=0,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0,
                 tol=1e-6,
                 speed="fast"):
        """Deterministic Shared Response Model
        Performs a low rank decomposition on each subject
        which consists of a common response and
        an orthogonal subject-specific basis.

        It finds the shared coordinates S and
        for any subject X_i, the basis W_i
        that the sum of Frobenius norms
        sum_i || X_i - sc W_i S ||^2
        is minimized under the constraints:
        * W_i.T W_i = I_k

        X_i is the data of subject i
        S are the shared coordinates
        W_i is the basis of subject i
        sc is the scaling parameter

        Parameters
        ----------
        max_iter : int,
            maximum number of iterations to perform
        use_scaling: bool
            if False the scaling parameter is set to 1
            if True, the scaling parameter is optimized
        tol : float,
            tolerance for numerical error
        speed: string
             "fast": PCA is done as a first pass on each subject.
             All iterations are performed on reduced data.
                A final iteration is performed on full data.
            "very fast": PCA is done as a first pass on each subjects.
            All iteration are perforned on reduced data.

        Attributes
        ----------
        `_scale`: list of n_subjects int
            Scale value for all subject
        """

        BaseSRM.__init__(self,
                         n_components=n_components,
                         random_state=random_state, mask=mask,
                         smoothing_fwhm=smoothing_fwhm,
                         standardize=standardize, detrend=detrend,
                         low_pass=low_pass, high_pass=high_pass,
                         t_r=t_r, target_affine=target_affine,
                         target_shape=target_shape,
                         mask_strategy=mask_strategy,
                         mask_args=mask_args, memory=memory,
                         memory_level=memory_level, n_jobs=n_jobs,
                         verbose=verbose,
                         transform_method=None)

        self.n_iter = max_iter
        self.use_scaling = use_scaling
        self.scale_ = None
        self.tol = tol
        self.speed = speed

    def _fit(self, imgs, confounds=None):
        """
        Fit the model.
        """
        self.masker, basis, scale, self.shared_response = cache(
            _deterministic_srm,
            memory=self.memory,
            memory_level=self.memory_level,
            func_memory_level=1
        )(self.masker,
          imgs,
          confounds=confounds,
          n_components=self.n_components,
          random_state=self.random_state,
          memory=self.memory,
          memory_level=self.memory_level,
          n_jobs=self.n_jobs,
          tol=self.tol,
          use_scaling=self.use_scaling,
          speed=self.speed
          )
        self.basis = basis
        self.scale_ = scale
        return self

    def transform(self, X, y=None, index=None, confounds=None):
        """
        Override transform method to make use of orthogonal W
        """

        if index is None:
            index = np.array(range(len(self.basis)))
        else:
            index = np.array(index)

        X = check_imgs(X)

        if confounds is None:
            confounds = itertools.repeat(confounds)

        basis = self.basis
        scale = self.scale_

        masker = self.masker

        n_timeframes = None
        common_space = None
        for i, confound in zip(range(len(X)), confounds):
            X_i, masker = cache(
                load_img,
                memory=self.memory,
                memory_level=self.memory_level,
                func_memory_level=2
            )(
                masker,
                X[i],
                axis=0,
                confound=confound
            )
            if n_timeframes is None:
                n_timeframes = X_i.shape[1]
                common_space = np.zeros((self.n_components, n_timeframes))
            s_i = basis[index[i]].T.dot(X_i)
            if common_space is None:
                common_space = s_i
            else:
                common_space += s_i
        common_space /= np.sum([scale[index[i]]**2 for i in range(len(X))])
        return common_space
