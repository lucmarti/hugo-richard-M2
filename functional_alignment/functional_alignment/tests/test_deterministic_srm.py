import numpy as np
import random
import nibabel
from sklearn.utils.testing import assert_array_almost_equal
from nilearn.input_data import NiftiMasker

from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.utils import create_orthogonal_matrix
from functional_alignment.deterministic_srm import fast_srm


affine = np.eye(4)
shape = (2, 2, 2, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = 8
n_components = 3
nb_time_frames = 10
n_samples = 2


def create_X():
    shared = np.random.rand(n_components, nb_time_frames)
    X = []
    for i in range(n_samples):
        test_basis = create_orthogonal_matrix(n_features, n_components)
        X_ = test_basis.dot(shared)
        X.append([masker.inverse_transform(X_.T)])

    X_scaled = []
    scale = []
    for i in range(n_samples):
        test_basis = create_orthogonal_matrix(n_features, n_components)
        sc = random.random()
        X_ = sc * test_basis.dot(shared)
        scale.append(sc)
        X_scaled.append([masker.inverse_transform(X_.T)])
    return X, X_scaled


def test_shapes_input():
    X, _ = create_X()
    assert len(X) == n_samples
    assert X[0][0].shape == shape


def test_shapes():
    X, _ = create_X()
    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker)
    srm.fit(X)
    basis = srm.basis
    shared = srm.fit_transform(X)
    for i in range(n_samples):
        assert basis[i].shape == (n_features, n_components)
    assert shared.shape == (n_components, nb_time_frames)

    shared2 = srm.transform(X)
    assert shared2.shape == (n_components, nb_time_frames)


def test_orthogonalbasis():
    X, _ = create_X()
    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker)
    srm.fit(X)
    basis = srm.basis
    for i in range(n_samples):
        assert_array_almost_equal(basis[i].T.dot(basis[i]),np.eye(n_components),4)


def test_reconstruction():
    X, _ = create_X()
    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker,
                           verbose=1)
    shared = srm.fit_transform(X)
    basis = srm.basis
    for i in range(n_samples):
        assert_array_almost_equal(masker.transform(X[i][0]).T, basis[i].dot(shared))


def test_reconstruction_scaling():
    _, X_scaled = create_X()
    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker,
                           verbose=1,
                           use_scaling=True)
    shared = srm.fit_transform(X_scaled)
    basis = srm.basis

    for i in range(n_samples):
        assert_array_almost_equal(masker.transform(X_scaled[i][0]).T, basis[i].dot(shared))


def test_multiple_image_shape():
    X, X_scaled = create_X()
    X_m = []
    for i in range(n_samples):
        X_m.append([masker.inverse_transform(np.random.rand(nb_time_frames, n_features)),
                    masker.inverse_transform(np.random.rand(nb_time_frames, n_features)),
                    masker.inverse_transform(np.random.rand(nb_time_frames, n_features))])

    X_m = np.array(X_m)
    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker,
                           verbose=1,
                           use_scaling=True)
    srm.fit(X_m)
    basis = srm.basis
    shared = srm.fit_transform(X_m)
    for i in range(n_samples):
        print("basis shape", i, basis[i].shape)
        assert basis[i].shape == (n_features, n_components)
    print("[fit_transform] shared shape", shared.shape)
    assert shared.shape == (n_components, 3 * nb_time_frames)

    shared2 = srm.transform(X_m)
    print("[transform] shared shape", shared.shape)
    assert shared2.shape == (n_components, 3 * nb_time_frames)

    print("[inverse transform] X shape",
          srm.inverse_transform(shared, index=[0])[0].shape)
    assert(srm.inverse_transform(shared, index=[0])[0].shape == (n_features, 3*nb_time_frames))


def test_one_run():
    XX, _ = create_X()
    X_m = []
    for i in range(n_samples):
        X_m.append(XX[i][0])

    srm = DeterministicSRM(n_components,
                           max_iter=10,
                           mask=masker,
                           verbose=1)

    shared = srm.fit_transform(X_m)
    basis = srm.basis

    for i in range(n_samples):
        assert_array_almost_equal(masker.transform(X_m[i]).T, basis[i].dot(shared))


def test_fast_srm():
    XX, _ = create_X()
    X_m = []
    for i in range(n_samples):
        X_m.append(masker.transform(XX[i][0]).T)

    _, basis, shared_space = fast_srm(X_m, n_components=n_components)

    for i in range(len(X_m)):
        assert_array_almost_equal(X_m[i], basis[i].dot(shared_space))
