import numpy as np
import random
import nibabel
from sklearn.utils.testing import assert_array_almost_equal
from nilearn.input_data import NiftiMasker
import scipy.stats as stats
from functional_alignment.graphnet_srm import GraphNetSRM
import scipy.sparse as sp
from functional_alignment.graphnet_srm import _compute_basis, _srm_graphnet_step, _dictionary_update
from nilearn.decoding.objective_functions import _gradient, _unmask
from copy import deepcopy
from functional_alignment.graphnet_srm import GraphNetSRM
from functional_alignment.graphnet_srm import _energy_datafitting, _energy_smoothness, _energy_common, _energy_smooth
from sklearn.externals.joblib import Parallel
from copy import deepcopy

affine = np.eye(4)
shape = (4, 4, 4, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = 4*4*4
n_components = 10
nb_time_frames = 20
n_samples = 5


def generate_group_sparse_matrix(n_voxels, n_components, n_splits=5, sparsity=0.5):
    """
    Generate a group sparse matrix
    Parameters
    ----------
    n_voxels
    n_components
    sparsity: float
        average proportion of zeros
    Returns
    -------
    X: ndarray of shape (n_voxels, n_components)
        sparse matrix
    """
    K = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(sparsity).rvs).toarray()
    M = np.zeros((n_voxels, n_components))

    split_sizes = np.zeros((n_splits, n_components))
    for k in range(n_components):
        for i in range(n_splits):
            split_sizes[i, k] = np.random.randint(5, 10)

    split_sizes = split_sizes / np.sum(split_sizes, axis=0, keepdims=True) * n_voxels
    split_sizes = np.cumsum(split_sizes, axis=0)
    split_sizes = np.round(split_sizes).astype(int)

    for k in range(n_components):
        for i in range(n_splits):
            if i ==0:
                start = 0
            else:
                start = split_sizes[i-1, k]
            stop = split_sizes[i, k]
            vv = stop - start
            variance = np.eye(vv)*0.1
            variance = variance.T.dot(variance)
            M[start:stop, k] = np.random.multivariate_normal(
                np.zeros(vv),
                variance
            ) * K[i, k]

    return M


def create_X():
    shared = np.random.rand(n_components, nb_time_frames) / 10
    X = []
    X__ = []
    common_basis = generate_group_sparse_matrix(n_features, n_components, 4, 0.5)
    subjects_basis = []
    for i in range(n_samples):
        test_basis = common_basis + generate_group_sparse_matrix(n_features, n_components, 4, 0.5) / 5.
        X_ = test_basis.dot(shared)
        X.append([masker.inverse_transform(X_.T)])
        X__.append(X_)
        subjects_basis.append(test_basis)
    return X__, X, common_basis, subjects_basis, shared


def test_energy_datafitting():
    X_, X, common_basis, subjects_basis, shared = create_X()
    for i in range(len(X_)):
        assert _energy_datafitting(X_[i], subjects_basis[i], shared) <= 1e-20
        assert _energy_datafitting(2 * X_[i], 2 * subjects_basis[i], shared) <= 1e-20
        assert _energy_datafitting(X_[i], 2 * subjects_basis[i], shared / 2) <= 1e-20


def test_energy_smoothness():
    X_, X, common_basis, subjects_basis, shared = create_X()
    for i in range(len(X_)):
        assert _energy_smoothness(subjects_basis[i], 0.,
                                  masker.mask_img_.get_data().astype(bool), Parallel(n_jobs=1)) == 0.
        assert _energy_smoothness(0 * subjects_basis[i] + 5,
                                  15., masker.mask_img_.get_data().astype(bool), Parallel(n_jobs=1)) == 0.


def test_energy_common():
    X_, X, common_basis, subjects_basis, shared = create_X()

    same_basis = [common_basis for _ in range(len(subjects_basis))]

    for i in range(len(X_)):
        assert _energy_common(same_basis[i] , np.sum(same_basis, axis=0) - same_basis[i],
                              10., len(same_basis)) <= 1e-20
        assert _energy_common(subjects_basis[i], 0,
                              10., 1.) <= 1e-20


def test_update_dictionary():
    X_, X, common_basis, subjects_basis, shared = create_X()
    subjects_basis = [subject_basis for subject_basis in subjects_basis]

    X_ = []
    for i in range(len(X)):
        X_.append(masker.transform(X[i][0]).T)

    W_TWs = [subjects_basis[i].T.dot(subjects_basis[i]) for i in range(n_samples)]
    W_TXs = [subjects_basis[i].T.dot(X_[i]) for i in range(n_samples)]

    shared_, _, _, basis_ = _dictionary_update(W_TXs, W_TWs, subjects_basis, None)

    for i in range(len(basis_)):
        print(((basis_[i].dot(shared_) - subjects_basis[i].dot(shared)) ** 2).sum())
        assert_array_almost_equal(basis_[i].dot(shared_), subjects_basis[i].dot(shared))


def test_update_toobig_dictionary():
    X_, X, common_basis, subjects_basis, shared = create_X()
    subjects_basis = [subjects_basis[i] / 10. for i in range(len(subjects_basis))]
    shared = shared * 10.

    X_ = []
    for i in range(len(X)):
        X_.append(masker.transform(X[i][0]).T)

    W_TWs = [subjects_basis[i].T.dot(subjects_basis[i]) for i in range(n_samples)]
    W_TXs = [subjects_basis[i].T.dot(X_[i]) for i in range(n_samples)]

    shared_, _, _,  basis_ = _dictionary_update(W_TXs, W_TWs, deepcopy(subjects_basis), None)

    for i in range(len(basis_)):
        print(((basis_[i].dot(shared_) - subjects_basis[i].dot(shared)) ** 2).sum())
        assert_array_almost_equal(basis_[i].dot(shared_), subjects_basis[i].dot(shared))


def cost(X_, basis, shared_response, grad_weight, common_weight, l1_weight, mask):
    E = 0
    for i in range(len(X_)):
        E += _energy_smooth(X_[i], basis[i], shared_response, np.sum(basis, axis=0) - basis[i], grad_weight,
                            common_weight, len(basis), mask, Parallel(n_jobs=1))
        E += np.sum(np.abs(basis[i])) * l1_weight

    return E


def test_graphnet_step():
    X_, X, common_basis, subjects_basis, shared = create_X()
    mask = masker.mask_img_.get_data().astype(bool)
    subjects_basis_ = []
    shared_ = np.random.rand(n_components, nb_time_frames)

    c_real = cost(X_, subjects_basis, shared, 0.1, 0.1, 0.1, mask)
    c_prec = c_real


    shared_response_ = shared_
    W_TWs = []
    W_TXs = []

    cs = []
    for iter in range(100):
        for subject in range(n_samples):
            x_subject = masker.transform(X[subject][0]).T

            subjects_basis_, shared_response_, W_TXs, W_TWs = _srm_graphnet_step(
                x_subject,
                masker,
                shared_response_,
                basis=subjects_basis_,
                W_TXs=W_TXs,
                W_TWs=W_TWs,
                l1_weight=0.001,
                grad_weight=0.001,
                common_weight=0.,
                subject=subject,
                n_subjects=n_samples,
                random_state=None,
                verbose=False,
            )

        if len(subjects_basis_) == len(X_):
            c = 0
            for i in range(n_samples):
                c += ((subjects_basis_[i].dot(shared_response_) - X_[i]) ** 2).sum()
            cs.append(c)

    for i in range(len(cs)):
        print(cs[i])

    assert cs[0] > cs[-1]


def test_reconstruction():
    X_, X, common_basis, subjects_basis, shared = create_X()
    mask = masker.mask_img_.get_data().astype(bool)
    subjects_basis_ = []
    shared_ = np.random.rand(n_components, nb_time_frames)

    shared_response_ = shared_
    W_TWs = []
    W_TXs = []

    cs = []
    for iter in range(5):
        for subject in range(n_samples):
            x_subject = masker.transform(X[subject][0]).T

            subjects_basis_, shared_response_, W_TXs, W_TWs = _srm_graphnet_step(
                x_subject,
                masker,
                shared_response_,
                basis=subjects_basis_,
                W_TXs=W_TXs,
                W_TWs=W_TWs,
                l1_weight=0.,
                grad_weight=0.,
                common_weight=0.,
                subject=subject,
                n_subjects=n_samples,
                random_state=None,
                verbose=False,
            )
            print("subject", subject)
            print("iter", iter)
            if len(subjects_basis_) == len(X_):
                c = cost(X_, subjects_basis_, shared_, 0.001, 0.001, 0.001, mask)
                cs.append(c)
                print(c)

    for i in range(n_samples):
        print(((subjects_basis_[i].dot(shared_response_) - X_[i]) ** 2).sum())
        assert_array_almost_equal(subjects_basis_[i].dot(shared_response_), X_[i])


def test_srm_reconstruction():
    X_, X, common_basis, subjects_basis, shared = create_X()
    n_components, n_timeframes = shared.shape
    np.random.seed(0)
    shared_init = np.random.rand(n_components, nb_time_frames)
    n_components, n_timeframes = shared.shape
    srm = GraphNetSRM(n_components=n_components, max_iter=5, l1_weight=0., grad_weight=0., common_weight=0., mask=masker,
                      shared_init=shared_init)
    srm.fit(X)
    basis = srm.basis
    shared_response_ = srm.shared_response

    for i in range(n_samples):
        assert_array_almost_equal(basis[i].dot(shared_response_), X_[i], 4)
