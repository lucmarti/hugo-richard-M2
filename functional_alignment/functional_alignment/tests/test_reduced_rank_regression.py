from numpy import linalg
import numpy as np
from functional_alignment.reduced_rank_regression import rrr_fit


def test_rrr():
    # test to compare with least squares
    n, m, p = 100, 10, 20
    X = np.random.randn(n, m)
    Y = np.random.randn(n, p)
    A, B = rrr_fit(X, Y, 10)
    norm_rrr = linalg.norm(Y - X.dot(A.dot(B.T)), 'fro')

    C = linalg.lstsq(X, Y)[0]
    norm_ls = linalg.norm(Y - X.dot(C), 'fro')
    assert abs(norm_rrr - norm_ls) < 1e-3


    # test that it defaults to SVD when X is the identity matrix
    X = np.eye(n)
    A, B = rrr_fit(X, Y, 1)
    U, S , Vt = linalg.svd(Y, full_matrices=False)
    assert linalg.norm(np.outer(U[:, 0] * S[0], Vt[0]) - \
                       np.dot(A, B.T)) < .1