import numpy as np
import nibabel
from nilearn.input_data import NiftiMasker
from sklearn.utils.testing import assert_array_almost_equal

from functional_alignment.template_alignment import TemplateAlignment
from functional_alignment.piecewise_alignment import PieceWiseAlignment, create_labels
from functional_alignment.ridge_cv import RidgeCV

img1 = np.random.rand(10, 10, 5, 10)
img1 = img1 - img1.mean(axis=-1, keepdims=True)
img1 = img1 / img1.std(axis=-1, keepdims=True)
img1 = nibabel.Nifti1Image(img1, np.eye(4))

img2 = np.random.rand(10, 10, 5, 10)
img2 = img2 - img2.mean(axis=-1, keepdims=True)
img2 = img2 / img2.std(axis=-1, keepdims=True)
img2 = nibabel.Nifti1Image(img2, np.eye(4))

mask_img = nibabel.Nifti1Image(np.ones((10, 10, 5)), np.eye(4))

n_labels = 2


def test_shapes():
    pwa = PieceWiseAlignment(n_pieces=n_labels, mask=mask_img,
                             standardize=False, detrend=False, method="RidgeCV")
    template_alignment = TemplateAlignment(mapping=pwa, mask=mask_img)
    template_alignment.fit([img1, img2])
    T = template_alignment.transform([img1, img2])
    img1_ = template_alignment.inverse_transform(T, index=[0])[0]
    assert T.get_data().shape == img1.get_data().shape
    assert img1_.get_data().shape == img1.get_data().shape


def test_build_template():
    pwa = PieceWiseAlignment(n_pieces=n_labels, mask=mask_img, standardize=False, detrend=False,
                             method="RidgeCV")
    template_alignment = TemplateAlignment(mapping=pwa, mask=mask_img)
    template_alignment.fit([img1, img2])
    T = template_alignment.transform([img1, img2])
    assert_array_almost_equal(
        T.get_data(), (img1.get_data() + img2.get_data()) / 2)


def test_transform_inverse_transform():
    pwa = PieceWiseAlignment(n_pieces=n_labels, mask=mask_img, standardize=False, detrend=False,
                             method="RidgeCV")
    template_alignment = TemplateAlignment(mapping=pwa, mask=mask_img)
    template_alignment.fit([img1, img2])
    T = template_alignment.transform([img1, img2])
    img1_ = template_alignment.inverse_transform(T, index=[0])[0]


def test_multiple_image_shape():
    img1_m = [img1, img1]
    img2_m = [img2, img2]
    pwa = PieceWiseAlignment(n_pieces=n_labels, mask=mask_img, standardize=False, detrend=False,
                             method="RidgeCV")
    X_m = [img1_m, img2_m]
    X_m = np.array(X_m)

    template_alignment = TemplateAlignment(mapping=pwa, mask=mask_img)

    template_alignment.fit(X_m)
    shared = template_alignment.transform(X_m)
    shared2 = template_alignment.fit_transform(X_m)
    X__ = template_alignment.inverse_transform(shared, index=[0])[0]
    X_2 = template_alignment.inverse_transform(shared2, index=[0])[0]

    print(shared.shape)
    assert shared.shape == (10, 10, 5, 20)
    print(X__.shape)
    assert X__.shape == (10, 10, 5, 20)
    print(X_2.shape)
    assert X_2.shape == (10, 10, 5, 20)

    img1_concat = np.concatenate([img1.get_data(), img1.get_data()], axis=-1)
    img2_concat = np.concatenate([img2.get_data(), img2.get_data()], axis=-1)

    assert_array_almost_equal(
        shared.get_data(), (img1_concat + img2_concat) / 2)
