import numpy as np
import nibabel
from numpy.testing import assert_array_almost_equal, assert_array_equal
from nilearn.input_data import NiftiMasker
from functional_alignment.utils import create_orthogonal_matrix
from functional_alignment.fastSRM import check_img, check_imgs, reduce_data_single, reduce_data
from functional_alignment.fastSRM import _compute_shared_response, _compute_subject_basis, fast_srm
from functional_alignment.fastSRM import _compute_basis_subject_online, _compute_shared_response_online_single
from functional_alignment.fastSRM import _compute_shared_response_online, FastSRM
from functional_alignment.deterministic_srm import DeterministicSRM
import os

import scipy
from sklearn.utils.extmath import svd_flip

affine = np.eye(4)
shape = (2, 2, 2, 5)
mask_img = nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)
masker = NiftiMasker(mask_img=mask_img).fit()

nibabel.save(mask_img, "./test_masker.nii.gz")
n_sessions = 3
n_subjects = 4
n_components = 3

os.system("mkdir test_temp")
temp_dir = "./test_temp/"


n_voxels = int(np.prod(shape[:3]))
n_timeframes = shape[3]

W = []
for _ in range(n_subjects):
    W.append(create_orthogonal_matrix(n_voxels, n_components).T)

W2 = []
for _ in range(n_subjects):
    W2.append(np.random.rand(n_voxels, n_components).T)

for i in range(len(W)):
    np.save(os.path.join(temp_dir, "basis_%i" % i), W[i])

S = []
for _ in range(n_sessions):
    S_ = np.random.rand(n_timeframes, n_components)
    S_ = S_ - np.mean(S_, axis=0)
    S.append(S_)

X = []
for w in W:
    X_ = []
    for s in S:
        data = s.dot(w)
        X_.append(data)
    X.append(X_)
X = np.array(X)

X2 = []
for w in W:
    X2_ = []
    for s in S:
        data = s.dot(w)
        X2_.append(data)
    X2.append(X2_)
X2 = np.array(X2)


for i in range(n_subjects):
    for j in range(n_sessions):
        data = X[i, j]

        for t in range(n_timeframes):
            nibabel.save(masker.inverse_transform(data[t, :]),
                         "./test_data_%i_%i_timeframe%i.nii.gz" % (i, j, t))
        nibabel.save(masker.inverse_transform(data), "./test_data_%i_%i.nii.gz" % (i, j))

for i in range(n_subjects):
    for j in range(n_sessions):
        data = X2[i, j]

        for t in range(n_timeframes):
            nibabel.save(masker.inverse_transform(data[t, :]),
                         "./test_random_data_%i_%i_timeframe%i.nii.gz" % (i, j, t))
        nibabel.save(masker.inverse_transform(data), "./test_random_data_%i_%i.nii.gz" % (i, j))


def test_check_img():
    imgs = [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
    imgs2 = [["./test_data_%i_%i_timeframe*.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]

    imgs, confounds = check_imgs(imgs, None)
    imgs2, confounds2 = check_imgs(imgs2, None)

    for i in range(n_subjects):
        for j in range(n_sessions):
            assert_array_equal(masker.transform(imgs[i, j], confounds=confounds[i, j]),
                               masker.transform(imgs2[i, j], confounds=confounds2[i, j]))
            assert masker.transform(imgs[i, j], confounds=confounds[i, j]).shape == (n_timeframes, n_voxels)


def test_check_img_single_session():
    imgs = [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
    imgs2 = [["./test_data_%i_%i_timeframe*.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]

    imgs, confounds = check_imgs([imgs[i][0] for i in range(n_subjects)], None)
    imgs2, confounds2 = check_imgs([imgs2[i][0] for i in range(n_subjects)], None)

    for i in range(n_subjects):
        assert_array_equal(masker.transform(imgs[i][0], confounds=confounds[i][0]),
                           masker.transform(imgs2[i][0], confounds=confounds2[i][0]))
        assert masker.transform(imgs[i][0], confounds=confounds[i][0]).shape == (n_timeframes, n_voxels)


def test_check_img_single_session_single_subject():
    imgs = [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
    imgs2 = [["./test_data_%i_%i_timeframe*.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]

    imgs, confounds = check_imgs(imgs[0][0], None)
    imgs2, confounds2 = check_imgs(imgs2[0][0], None)

    assert_array_equal(masker.transform(imgs[0, 0], confounds=confounds[0, 0]),
                       masker.transform(imgs2[0, 0], confounds=confounds2[0, 0]))
    assert masker.transform(imgs[0, 0], confounds=confounds[0, 0]).shape == (n_timeframes, n_voxels)


def test_reduce_data_single():
    imgs, confounds = check_imgs([["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)],
                      None)
    reduced, index = reduce_data_single(imgs[0], masker, confounds[0], 4, None)
    assert_array_equal(index, [slice(i * n_timeframes, (i + 1) * n_timeframes) for i in range(n_sessions)])

    X = []
    for file in imgs[0]:
        X.append(masker.transform(file))
    X = np.concatenate(X, axis=0)

    U, S, V = scipy.linalg.svd(X, full_matrices=False)
    assert_array_almost_equal(np.abs(U.dot(np.diag(S)[:, :4])), np.abs(reduced))


def test_reduce_data():
    imgs, confounds = check_imgs(
        [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)],
        None)

    reduced, index = reduce_data(imgs, masker, confounds, n_reduced_dimensions=4, n_jobs=2)

    for i, red in enumerate(reduced):
        red_, index_ = reduce_data_single(imgs[i], masker, confounds[i], 4, None)
        assert_array_almost_equal(red, red_)
        for k in range(len(index)):
            assert index[k] == index_[k]


def test_compute_shared_response():
    S_ = _compute_shared_response(np.concatenate([X[:, i, :, :] for i in range(n_sessions)], axis=1), W)
    S_ = np.array_split(S_, n_sessions)
    for i in range(len(S_)):
        assert_array_almost_equal(S_[i], S[i])


def test_compute_subject_basis():
    X_ = np.concatenate([X[:, i, :, :] for i in range(n_sessions)], axis=1)
    S_ = np.concatenate(S, axis=0)

    for i in range(n_subjects):
        W_i_ = _compute_subject_basis(S_.T.dot(X_[i]))
        assert_array_almost_equal(W[i], W_i_)


def test_fast_srm():
    X_ = np.concatenate([X[:, i, :, :] for i in range(n_sessions)], axis=1)
    W_, S_ = fast_srm(X_, n_iter=10, n_components=n_components)

    for i in range(len(W_)):
        assert_array_almost_equal(W_[i].dot(W_[i].T), np.eye(n_components))
        assert_array_almost_equal(S_.dot(W_[i]), X_[i])


def test_compute_basis_subject_online():
    imgs, confounds = check_imgs(
        [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)],
        None)

    for i in range(n_subjects):
        W_i = _compute_basis_subject_online(imgs[i], masker, S, confounds[i])
        assert_array_almost_equal(W_i, W[i])


def test_compute_shared_response_online_single():
    imgs, confounds = check_imgs(
        [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)],
        None)

    index = np.random.randint(0, n_subjects, 5)
    index = np.unique(index)

    print(index)

    for s in range(n_sessions):
        print(s)
        S_ = _compute_shared_response_online_single(imgs[index, s], masker, W, None, confounds[:, s], index)
        S___ = _compute_shared_response_online_single(imgs[index, s], masker, None, temp_dir, confounds[:, s], index)
        S__ = np.mean([X[i, s, :, :].dot(W[i].T) for i in index], axis=0)

        print("Memory issue ?")
        assert_array_almost_equal(S_, S___)
        print("Accuracy issue ?")
        assert_array_almost_equal(S_, S__)


def test_compute_shared_response_online():
    imgs, confounds = check_imgs(
        [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)],
        None)

    S_ = _compute_shared_response_online(imgs, masker, W, None, confounds, 2, np.arange(n_subjects))

    for s in range(n_sessions):
        assert_array_almost_equal(S, S_)


def test_reconstruction():
    imgs = [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
    srm = FastSRM(n_components=n_components,
                  n_reduced_dimensions=4,
                  n_iter=10,
                  temp_dir=None,
                  verbose=False,
                  random_state=None,
                  mask=mask_img,
                  n_jobs=2)
    srm.fit(imgs)
    S_ = srm.transform(imgs)
    S__ = srm.fit_transform(imgs)
    print(S_[0].shape)
    print(S__[0].shape)
    for i in range(n_sessions):
        assert_array_almost_equal(S_[i], S__[i])

    X_ = srm.inverse_transform(S_)

    for i in range(n_subjects):
        for j in range(n_sessions):
            assert_array_almost_equal(X[i, j], masker.transform(X_[i, j]))


def test_shapes():
    for output_dir in [None, temp_dir]:
        imgs = [["./test_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
        imgs, confounds = check_imgs(imgs, None)
        srm = FastSRM(n_components=n_components,
                      n_reduced_dimensions=4,
                      n_iter=10,
                      temp_dir=None,
                      verbose=False,
                      random_state=None,
                      mask=mask_img,
                      n_jobs=2)
        srm.fit(imgs[:, :2])
        S_ = srm.transform(imgs[1:, 2], subjects_indexes=[1, 2, 3])
        X_ = srm.inverse_transform(S_, subjects_indexes=[0], output_dir=output_dir)
        assert_array_almost_equal(X[0, 2], masker.transform(X_[0, 0]))


# def test_deterministic_srm():
#     imgs = [["./test_random_data_%i_%i.nii.gz" % (i, j) for j in range(n_sessions)] for i in range(n_subjects)]
#     srm = DeterministicSRM(
#         n_components=n_components,
#         max_iter=10,
#         random_state=None,
#         mask=mask_img
#     )
#
#     srm2 = FastSRM(
#         n_components=n_components,
#         n_iter=10,
#         random_state=None,
#         n_reduced_dimensions=n_components,
#         mask=mask_img
#     )
#
#     srm2.fit(imgs)
#     srm.fit(imgs)


def test_rm():
    os.system("rm test_data*")
    os.system("rm test_masker*")
    os.system("rm -r test_temp")
    os.system("rm test_*random*")

