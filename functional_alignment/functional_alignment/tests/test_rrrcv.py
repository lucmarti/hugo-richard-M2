import numpy as np
from sklearn.utils.testing import assert_array_almost_equal
from functional_alignment.rrr_cv import RRRCV

A = np.random.rand(100, 200)
W = np.random.rand(200, 10).dot(np.random.rand(10, 200))

B = A.dot(W)

def test_rrr():
    rrr = RRRCV(alphas=[1e-6, 1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1],
                ks=np.arange(1, 100, 10))
    rrr.fit(A, B)
    assert_array_almost_equal(rrr.predict(A), B, 0)
