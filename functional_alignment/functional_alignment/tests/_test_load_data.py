from functional_alignment.load_data import get_mask, load_data

masker = get_mask(dataset="ibc")
load_data(masker, dataset="ibc", i=5)
