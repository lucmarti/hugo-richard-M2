import numpy as np
import random
import nibabel
from nilearn.input_data import NiftiMasker
from functional_alignment.batch_isrm import BatchISRM
from functional_alignment.online_isrm import OnlineISRM


affine = np.eye(4)
shape = (5, 5, 5, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = shape[0] * shape[1] * shape[2]
n_components = 10
nb_time_frames = 100
n_samples = 2


def create_X():
    X = []
    for i in range(n_samples):
        X_ = np.random.rand(n_features, nb_time_frames) * 10
        X.append([masker.inverse_transform(X_.T)])
    return X


def test_convergence():
    X = create_X()

    srm = BatchISRM(n_components,
                    max_iter=2,
                    mask=masker,
                    verbose=1,
                    l1_weight=0.1,
                    grad_weight=0.1,)

    srm2 = OnlineISRM(n_components,
                    max_iter=2,
                    mask=masker,
                    verbose=1,
                    l1_weight=0.1,
                    grad_weight=0.1,
                    batch_size=30)

    srm.fit(X)
    srm2.fit(X)

    print("See errors")
    print(srm.errors)
    assert False
    #print(srm2.errors)

    #for i in range(len(srm2.errors) - 1):
    #    assert srm2.errors[i] > srm2.errors[i + 1]