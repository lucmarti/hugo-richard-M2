import numpy as np
import random
import nibabel
from sklearn.utils.testing import assert_array_almost_equal
from nilearn.input_data import NiftiMasker
import scipy.stats as stats
from functional_alignment.interpretable_srm import InterpretableSRM
import scipy.sparse as sp


affine = np.eye(4)
shape = (4, 4, 4, 10)
masker = NiftiMasker(mask_img=nibabel.Nifti1Image(
        np.ones(shape[:3], dtype=np.int8), affine)).fit()
n_features = 4*4*4
n_components = 10
nb_time_frames = 20
n_samples = 5


def generate_group_sparse_matrix(n_voxels, n_components, n_splits=5, sparsity=0.5):
    """
    Generate a group sparse matrix
    Parameters
    ----------
    n_voxels
    n_components
    sparsity: float
        average proportion of zeros
    Returns
    -------
    X: ndarray of shape (n_voxels, n_components)
        sparse matrix
    """
    K = sp.random(n_splits, n_components, 1, data_rvs=stats.bernoulli(sparsity).rvs).toarray()
    M = np.zeros((n_voxels, n_components))

    split_sizes = np.zeros((n_splits, n_components))
    for k in range(n_components):
        for i in range(n_splits):
            split_sizes[i, k] = np.random.randint(5, 10)

    split_sizes = split_sizes / np.sum(split_sizes, axis=0, keepdims=True) * n_voxels
    split_sizes = np.cumsum(split_sizes, axis=0)
    split_sizes = np.round(split_sizes).astype(int)

    for k in range(n_components):
        for i in range(n_splits):
            if i ==0:
                start = 0
            else:
                start = split_sizes[i-1, k]
            stop = split_sizes[i, k]
            vv = stop - start
            variance = np.eye(vv)*0.1
            variance = variance.T.dot(variance)
            M[start:stop, k] = np.random.multivariate_normal(
                np.zeros(vv),
                variance
            ) * K[i, k]

    return M


def create_X():
    shared = np.random.rand(n_components, nb_time_frames) / 10
    X = []
    X__ = []
    common_basis = generate_group_sparse_matrix(n_features, n_components, 4, 0.5)
    subjects_basis = []
    for i in range(n_samples):
        test_basis = common_basis + generate_group_sparse_matrix(n_features, n_components, 4, 0.5) / 5.
        X_ = test_basis.dot(shared)
        X.append([masker.inverse_transform(X_.T)])
        X__.append(X_)
        subjects_basis.append(test_basis)
    return X__, X, common_basis, subjects_basis, shared


def test_srm_reconstruction():
    X_, X, common_basis, subjects_basis, shared = create_X()
    np.random.seed(0)
    n_components, n_timeframes = shared.shape
    srm = InterpretableSRM(n_components=n_components, l1_weight=0., grad_weight=0., mask=masker)
    srm.fit(X)
    basis = srm.basis
    shared_response_ = srm.shared_response

    for i in range(n_samples):
        assert_array_almost_equal(basis[i].dot(shared_response_), X_[i], 4)
