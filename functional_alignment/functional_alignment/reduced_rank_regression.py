import numpy as np
from scipy import linalg
from scipy import stats
from scipy.sparse import linalg as splinalg
from sklearn import base, metrics



class RRR(base.BaseEstimator):
    """
    scikit-learn like estimator
    """

    def __init__(self, alpha=1., k=1):
        self.alpha = alpha
        self.k = k

    def fit(self, X, Y):
        self.a, self.b = rrr_fit(X, Y, k=self.k, alpha=self.alpha)
        return self

    def predict(self, X):
        betas = self.a.dot(self.b.T)
        return np.dot(X, betas)

    def score(self, X, y):
        y_pred = self.predict(X)
        return metrics.r2_score(y.ravel(), y_pred.ravel())


def rrr_fit(X, Y, k=1, alpha=1e-6):
    """
    Reduced Rank Regression

    estimate a RRR model by solving a model of the form

        1 / (n * p)||Y - X A B^T ||^2 + alpha tr(A^T A)

    Parameters
    ----------
    X : array, shape (n, m)
    Y : array, shape (n, p)

    Returns
    -------
    A : array, shape (k, p)
    B : array, shape (k, m)
    """

    if k == -1:
        k = min(X.shape[1], Y.shape[1])

    # XXX append column of ones to X
    YY = Y.dot(Y.T)
    K1 = X.T.dot(Y).dot(Y.T.dot(X))
    K2 = X.T.dot(X) + Y.shape[0] * Y.shape[1] * alpha * np.eye(X.shape[1])

    eigvals, eigvects = linalg.eigh(K1, b=K2)
    eigvals = eigvals.astype(np.float)
    eigvects = eigvects.astype(np.float)
    order = np.argsort(-eigvals)
    eigvals = eigvals[order]
    A = eigvects[:, order[:k]]
    B = Y.T.dot(X.dot(A))
    return A, B



def fit_predict(design, voxels, rank):
    """
    Convenience method. Fits a RRR model and returns
    the predicted data on the same design matrix, i.e.
    does something like RRR.fit(X, y).predict(X)
    """

    # take only orthogonal columns
    # since we care about the prediction
    # we only care about the span of the columns
    # and not about their value
    Q, R = linalg.qr(design, mode='economic')
    Q = Q[:, np.diag(np.abs(R)) > 1e-6]

    A, B = rrr_fit(Q, voxels, rank)
    betas = A.dot(B.T)
    Y_pred = Q.dot(betas)
    residuals = voxels - Y_pred
    return Y_pred, residuals


def partial_corr_sum(a, b):
    """
    compute partial correlation across voxels and sum it.
    """
    n_voxels = a.shape[1]
    return np.sum([stats.pearsonr(a[:, i], b)[0] ** 2
                 for i in range(n_voxels)])


def permutation_scores(design, voxels, idx, permutations):
    """
    Return permuted scores and the true score for the
    design matrices design and design[idx].

    This follows the algorithm outlined in
      Anderson, Marti J., and John Robinson. "Permutation tests for linear models."
      Australian & New Zealand Journal of Statistics 43.1 (2001): 75-88.

    Parameters
    ----------
    idx: mask that when applied to design, yields a design matrix
         with the column of interest removed.
    """

    #
    n_voxels = voxels.shape[1]
    design_nocol = design[:, ~idx]
    assert idx.size == design.shape[1]

    # permuted scores
    statistic_perm = []

    Y_pred_nocol, res0 = fit_predict(design_nocol, voxels, 1)
    betas1 = linalg.lstsq(design_nocol, design[:, idx])[0]
    res1 = design[:, idx] - design_nocol.dot(betas1)
    assert res1.size == voxels.shape[0]

    T0 = partial_corr_sum(res0, res1.ravel())

    for i in range(len(permutations)):
        perm = permutations[i]

        Y_perm = Y_pred_nocol + res0[perm]
        _, res_perm = fit_predict(design_nocol, Y_perm, 1)

        T_i = partial_corr_sum(res_perm, res1.ravel())
        statistic_perm.append(T_i)

    statistic_perm = np.array(statistic_perm)
    assert statistic_perm.size == len(permutations)
    return statistic_perm, T0