from sklearn.base import BaseEstimator, TransformerMixin
from copy import deepcopy
import numpy as np
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from nilearn._utils.compat import _basestring
from nilearn.image import new_img_like
from sklearn.externals.joblib import Memory
from nilearn._utils import check_niimg


def mean_img(imgs):

    running_mean = np.zeros_like(check_niimg(imgs[0]).get_data())

    for (n, img) in enumerate(imgs):
        img = check_niimg(img)
        running_mean = (n * running_mean + img.get_data()) / (n + 1)
    return new_img_like(imgs[0], running_mean)

class TemplateAlignment(BaseEstimator, TransformerMixin):
    """
    Performs decomposition on each subject
    which consists of a template (being the mean response)
    and a subject-specific basis.

    Maps data X_1,...,X_{n_samples} to the model such
    that the distance between X_i and W_i T is minimized
    T is the template (mean response)
    W_i is the basis of sample i

    Parameters
    ----------
    n_components : int,
        Size of the samples' basis
    max_iter : int,
        Maximum number of iterations to perform
    verbose : bool
        Degree of verbosity of the printed output
    mapping: algorithm used with a fit and transform method such that
        algo.fit(X, Y).transform(X) is close to Y
    
    Attributes
    ----------
    n_iter_ : int
        Number of iterations run.
    shared_coord_: array of shape (n_voxels,  n_timeframes)
        shared coordinates
    mappers_: trained mappers
    """

    def __init__(self, mapping=None,
                 mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Template Alignment

        Performs decomposition on each subject
        which consists of a template (being the mean response)
        and a subject-specific basis.

        Maps data X_1,...,X_{n_samples} to the model such
        that the distance between X_i and W_i T is minimized
        T is the template (mean response)
        W_i is the basis of sample i

        Parameters
        ----------
        mapping: instance of a class with a fit and transform method
            algorithms used to fit the template to each subjects
            algo.fit(X, Y).transform(X) should be close to Y
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        """

        self.mapping = mapping

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs

        self.verbose = verbose

    def fit(self, imgs, y=None):
        """
        Fit the model.
        The template T is set to be the mean for all subjects
        For any subject a basis W_i is computed such that
        W_i T is close to X_i

        Parameters
        ----------
        imgs: list of Niimg-like objects
            See http://nilearn.github.io/manipulating_images/input_output.html
            Data on which the mask is calculated. If this is a list,
            the affine is considered the same for all.
        """

        if isinstance(imgs, _basestring) or not hasattr(imgs, '__iter__'):
            # these classes are meant for list of 4D images
            # (multi-subject), we want it to work also on a single
            # subject, so we hack it.
            imgs = [imgs, ]
        if len(imgs) == 0:
            # Common error that arises from a null glob. Capture
            # it early and raise a helpful message
            raise ValueError('Need one or more Niimg-like objects as input, '
                             'an empty list was given.')
        self.masker_ = check_embedded_nifti_masker(self)

        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit(imgs)
        else:
            self.masker_.fit()
        self.mask_img_ = self.masker_.mask_img_

        self.train_template_ = mean_img(imgs)
        train_template = self.train_template_

        if self.mapping is not None:
            mappers = []
            for img in imgs:
                mappers.append(deepcopy(self.mapping).fit(train_template, img))
            self.mappers_ = mappers
        return self


    def fit_transform(self, imgs, y=None):
        """
       Fit the model and return the template.
       The template T is set to be the mean for all subjects
       For any subject a basis W_i is computed such that
       W_i T is close to X_i

       Parameters
       ----------
       imgs: list of Niimg-like objects
           See http://nilearn.github.io/manipulating_images/input_output.html
           Data on which the mask is calculated. If this is a list,
           the affine is considered the same for all.

        Returns
        ---------
        Template: Niimg-like object
            See http://nilearn.github.io/manipulating_images/input_output.html
           mean image
       """

        self.fit(imgs)
        return self.train_template_

    def transform(self, imgs, y=None, index=None):
        """
       Returns the template for input images.

       Parameters
       ----------
       imgs: list of Niimg-like objects
           See http://nilearn.github.io/manipulating_images/input_output.html
           Data on which the mask is calculated. If this is a list,
           the affine is considered the same for all.
        index: list of int
            Index of images that should be used to compute the template

        Returns
        ---------
        Template: Niimg-like object
            See http://nilearn.github.io/manipulating_images/input_output.html
           mean image
       """

        if not (hasattr(self, 'masker_')):
            ValueError('Object does not have a masker_ attribute. '
                       'Fit has not been called')

        if index is None:
            index = np.array(range(len(imgs)))
        else:
            index = np.array(index)

        if isinstance(imgs, _basestring) or not hasattr(imgs, '__iter__'):
            # these classes are meant for list of 4D images
            # (multi-subject), we want it to work also on a single
            # subject, so we hack it.
            imgs = [imgs, ]
        if len(imgs) == 0:
            # Common error that arises from a null glob. Capture
            # it early and raise a helpful message
            raise ValueError('Need one or more Niimg-like objects as input, '
                             'an empty list was given.')

        if index is None:
            index = range(len(imgs))

        test_template = mean_img([imgs[i] for i in index])
        return test_template

    def inverse_transform(self, img, index=None):
        """
        From the input template and trained mappers,
        reconstruct input images from the template.
        Parameters
        ----------
       img: Niimg-like object
           See http://nilearn.github.io/manipulating_images/input_output.html
           Template

        Returns
        -------
        reconstructed_images: list of Niimg-like objects
           See http://nilearn.github.io/manipulating_images/input_output.html
           List of reconstructed images
        index: list or None:
            index of original images to be reconstructed
        """

        if not (hasattr(self, 'mappers_') and hasattr(self, 'masker_')):
            ValueError('Object does not have a masker_ and masker_ attribute. '
                       'Fit has not been called')

        if index is None:
            index = range(len(self.mappers_))

        if self.mapping is not None:
            reconstructed_images = [self.mappers_[i].transform(img) for i in index]
        else:
            reconstructed_images = [img for i in index]
        return reconstructed_images


# functions that could be use to generate more complicated templates
# it is too slow to be used for now
# def build_train_template_(X, masker=None, mapping=None, iteration=0, mean_template=True):
#     """
#     Use X and mapping algorithm to build a template representing X
#     We solve X[i]  = W[i]S
#     """
#
#     #initialisation
#     T = None
#     for x in X:
#         if masker is not None:
#             x_ = load_img(masker, x)
#         else:
#             x_ = x
#         if T is None:
#             T = x_
#         else:
#             T += x_
#         n_voxels, n_timeframes = x_.shape
#     T /= len(X)
#
#     if mean_template:
#         return T
#
#     #iteration
#     for iterate in range(iteration):
#         t0 = time()
#         print("iteration", iterate, time() - t0)
#         t_WW = csr_matrix((n_voxels, n_voxels))
#         t_WX = csr_matrix((n_voxels, n_timeframes))
#
#         print("Size of W", n_voxels*n_voxels)
#         for x in X:
#             print("aligning template and data", time() - t0)
#             if masker is not None:
#                 x_ = load_img(masker, x)
#             else:
#                 x_ = x
#             W_i = mapping.fit(T, x_).fit_
#             print("Number of zeros of W_i", W_i.getnnz())
#             t_WW += W_i.transpose().dot(W_i)
#             print("Number of zeros of t_WW", t_WW.getnnz())
#             t_WX += W_i.transpose().dot(x_)
#
#         T_res = []
#         t_XW = t_WX.T
#
#         print("Now starting iterating procedure")
#         for i in range(len(t_XW)):
#             b = t_XW[i]
#             T_res.append(cg(t_WW, b.T, x0=T[:, i])[0])
#         T = np.array(T_res).T
#
#     return T
#
#
# def build_test_template_(X,
#                         mappers=None,
#                         index=None,
#                         masker=None,
#                         mean_template=True):
#
#     if index is None:
#         index = range(len(X))
#
#     #initialisation
#     T = None
#     for x in X:
#         if masker is not None:
#             x_ = load_img(masker, x)
#         else:
#             x_ = x
#         if T is None:
#             T = x_
#         else:
#             T += x_
#         n_voxels, n_timeframes = x_.shape
#     T /= len(X)
#
#     if mean_template:
#         return T
#
#     #algorithm
#     #We build the basis
#     t_WW = csr_matrix((n_voxels, n_voxels))
#     t_WX = csr_matrix((n_voxels, n_timeframes))
#     for i in index:
#         if masker is not None:
#             x_ = load_img(masker, x)
#         else:
#             x_ = X[i]
#
#         W_i = mappers[i].fit_
#         t_WW += W_i.transpose().dot(W_i)
#         t_WX += W_i.transpose().dot(x_)
#
#     T_res = []
#     t_XW = t_WX.T
#     for i in range(len(t_XW)):
#         b = t_XW[i]
#         T_res.append(cg(t_WW, b.T, x0=T[:, i])[0])
#     T = np.array(T_res).T
#
#     return T
