"""
Base class for decomposition estimators, utilities for masking and dimension
reduction of group data
"""

from __future__ import division
from math import ceil
import itertools
import glob
from distutils.version import LooseVersion

import numpy as np

from scipy import linalg
import sklearn
import nilearn
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.externals.joblib import Memory, Parallel, delayed
from sklearn.linear_model import LinearRegression
from sklearn.utils import check_random_state
from sklearn.utils.extmath import randomized_svd, svd_flip
from nilearn._utils.cache_mixin import CacheMixin, cache
from nilearn._utils.niimg import _safe_get_data
from nilearn._utils.niimg_conversions import _resolve_globbing
from nilearn._utils.compat import _basestring
from nilearn.input_data import NiftiMapsMasker
from nilearn.input_data.masker_validation import check_embedded_nifti_masker
from functional_alignment.utils import load_img
import nibabel as nib


def check_imgs(imgs):
    """
    Help function to correctly format images
    Parameters
    ----------
    imgs: list of 4D Niimg-like objects
        input format
        See http://nilearn.github.io/manipulating_images/input_output.html
    Returns
    -------
    imgs: list of 4D Niimg-like objects
        correctly formated images
    """
    if isinstance(imgs, _basestring):
        if nilearn.EXPAND_PATH_WILDCARDS and glob.has_magic(imgs):
            imgs = _resolve_globbing(imgs)

    if isinstance(imgs, _basestring):
        if nilearn.EXPAND_PATH_WILDCARDS and glob.has_magic(imgs):
            imgs = _resolve_globbing(imgs)

    if isinstance(imgs, _basestring) or not hasattr(imgs, '__iter__'):
        # these classes are meant for list of 4D images
        # (multi-subject), we want it to work also on a single
        # subject, so we hack it.
        imgs = [imgs, ]

    if len(imgs) == 0:
        # Common error that arises from a null glob. Capture
        # it early and raise a helpful message
        raise ValueError('Need one or more Niimg-like objects as input, '
                         'an empty list was given.')
    return imgs


def cov(A):
    return A.T.dot(A)


def fast_svd(X, n_components, random_state=None):
    """ Automatically switch between randomized and lapack SVD (heuristic
        of scikit-learn).
    Parameters
    ==========
    X: array, shape (n_samples, n_features)
        The data to decompose
    n_components: integer
        The order of the dimensionality of the truncated SVD
    random_state: int or RandomState
        Pseudo number generator state used for random sampling.
    Returns
    ========
    U: array, shape (n_samples, n_components)
        The first matrix of the truncated svd
    S: array, shape (n_components)
        The second matric of the truncated svd
    V: array, shape (n_components, n_features)
        The last matric of the truncated svd
    """
    random_state = check_random_state(random_state)
    # Small problem, just call full PCA
    if max(X.shape) <= 500:
        svd_solver = 'full'
    elif 1 <= n_components < .8 * min(X.shape):
        svd_solver = 'randomized'
    # This is also the case of n_components in (0,1)
    else:
        svd_solver = 'full'

    # Call different fits for either full or truncated SVD
    if svd_solver == 'full':
        U, S, V = linalg.svd(X, full_matrices=False)
        # flip eigenvectors' sign to enforce deterministic output
        U, V = svd_flip(U, V)
        # The "copy" are there to free the reference on the non reduced
        # data, and hence clear memory early
        U = U[:, :n_components].copy()
        S = S[:n_components]
        V = V[:n_components].copy()
    else:
        if LooseVersion(sklearn.__version__) >= LooseVersion('0.18'):
            n_iter = 'auto'
        else:
            n_iter = 3
        U, S, V = randomized_svd(X, n_components=n_components,
                                 n_iter=n_iter,
                                 flip_sign=True,
                                 random_state=random_state)
    return U, S, V


def _mask_and_reduce_single(masker,
                            img, confound,
                            reduction_ratio=None,
                            n_samples=None,
                            random_state=None):
    """Utility function for multiprocessing from MaskReducer"""
    if isinstance(img, nib.nifti1.Nifti1Image) or isinstance(img, _basestring):
        this_data = masker.transform(img, confound)
    elif (type(img) == list or type(img) == np.ndarray) and (isinstance(img[0], _basestring) or isinstance(img[0], nib.nifti1.Nifti1Image)):
        this_data = masker.transform(img, confound)
    else:
        this_data = img
    if type(this_data) == list:
        this_data = np.concatenate(this_data, axis=0)
    
    # Now get rid of the img as fast as possible, to free a
    # reference count on it, and possibly free the corresponding
    # data
    del img
    random_state = check_random_state(random_state)

    data_n_samples = this_data.shape[0]
    if reduction_ratio is None:
        assert n_samples is not None
        n_samples = min(n_samples, data_n_samples)
    else:
        n_samples = int(ceil(data_n_samples * reduction_ratio))

    U, S, V = fast_svd(this_data.T, n_samples, random_state=random_state)
    return U, S, V


def mask_and_reduce(masker, imgs,
                    confounds=None,
                    reduction_ratio='auto',
                    n_components=None, random_state=None,
                    memory_level=0,
                    memory=Memory(cachedir=None),
                    n_jobs=1):
    """Mask and reduce provided 4D images with given masker.
    Uses a PCA (randomized for small reduction ratio) or a range finding matrix
    to reduce data size in time direction. For multiple images,
    the concatenation of data is returned, either as an ndarray or a memorymap
    (useful for big datasets that do not fit in memory).
    Parameters
    ----------
    masker: NiftiMasker or MultiNiftiMasker
        Instance used to mask provided data.
    imgs: list of 4D Niimg-like objects
        See http://nilearn.github.io/manipulating_images/input_output.html
        List of subject data to mask, reduce and stack.
    confounds: CSV file path or 2D matrix, optional
        This parameter is passed to signal.clean. Please see the
        corresponding documentation for details.
    reduction_ratio: 'auto' or float between 0. and 1.
        - Between 0. or 1. : controls data reduction in the temporal domain
        , 1. means no reduction, < 1. calls for an SVD based reduction.
        - if set to 'auto', estimator will set the number of components per
          reduced session to be n_components.
    n_components: integer, optional
        Number of components per subject to be extracted by dimension reduction
    random_state: int or RandomState
        Pseudo number generator state used for random sampling.
    memory_level: integer, optional
        Integer indicating the level of memorization. The higher, the more
        function calls are cached.
    memory: joblib.Memory
        Used to cache the function calls.
    Returns
    ------
    data: ndarray or memorymap
        Concatenation of reduced data.
    """
    if not hasattr(imgs, '__iter__'):
        imgs = [imgs]

    if reduction_ratio == 'auto':
        if n_components is None:
            # Reduction ratio is 1 if
            # neither n_components nor ratio is provided
            reduction_ratio = 1
    else:
        if reduction_ratio is None:
            reduction_ratio = 1
        else:
            reduction_ratio = float(reduction_ratio)
        if not 0 <= reduction_ratio <= 1:
            raise ValueError('Reduction ratio should be between 0. and 1.,'
                             'got %.2f' % reduction_ratio)

    if confounds is None:
        confounds = itertools.repeat(confounds)

    if reduction_ratio == 'auto':
        n_samples = n_components
        reduction_ratio = None
    else:
        # We'll let _mask_and_reduce_single decide on the number of
        # samples based on the reduction_ratio
        n_samples = None

    reduced_data_list = Parallel(n_jobs=n_jobs)(
        delayed(cache(_mask_and_reduce_single, memory, func_memory_level=2, memory_level=memory_level))(
            masker,
            img, confound,
            reduction_ratio=reduction_ratio,
            n_samples=n_samples,
            random_state=random_state
        ) for img, confound in zip(imgs, confounds))

    return reduced_data_list


class BaseSRM(BaseEstimator, CacheMixin, TransformerMixin):
    """
    Base class for SRM decomposition
    Handles
    """

    def __init__(self,
                 n_components,
                 verbose=False,
                 random_state=None,
                 mask=None,
                 smoothing_fwhm=None,
                 standardize=False,
                 detrend=False,
                 low_pass=None,
                 high_pass=None,
                 t_r=None,
                 target_affine=None,
                 target_shape=None,
                 mask_strategy='epi',
                 mask_args=None,
                 memory=Memory(cachedir=None),
                 memory_level=0,
                 transform_method="mean",
                 n_jobs=1):
        """
        Parameters
        ----------
        n_components: int
            Number of timecourses of the shared coordinates
        random_state: int or RandomState
            Pseudo number generator state used for random sampling.
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        detrend : boolean, optional, default=True
            If detrend is True, the time-series will be detrended before components extraction.
        mask_args: dict, optional
            If mask is None, these are additional parameters passed to masking.compute_background_mask
            or masking.compute_epi_mask to fine-tune mask computation.
            Please see the related documentation for details.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        n_jobs: integer, optional, default=1
            The number of CPUs to use to do the computation.
             -1 means all CPUs, -2 all CPUs but one, and so on.

        Attributes
        ----------
        `basis`: list of n_subjects 2D numpy-arrays (n_voxels x n_components)
            List of masked basis of all subjects. They can be unmasked thanks to
            the `masker_` attribute
        `shared_coord`: 2D numpy-array of shape (n_components, n_timeframes)
            Array of shared coordinates, using the masked data of all subjects.
        `masker` : instance of MultiNiftiMasker
            Masker used to filter and mask data as first step. If an instance of
            MultiNiftiMasker is given in `mask` parameter,
            this is a copy of it. Otherwise, a masker is created using the value
            of `mask` and other NiftiMasker related parameters as initialization.
        """

        self.n_components = n_components
        self.random_state = random_state
        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs
        self.verbose = verbose
        self.transform_method = transform_method

        self.masker = None
        self.shared_response = None
        self.basis = None

    def fit(self, imgs, y=None, confounds=None):
        """
        Compute the mask and the basis across subjects
        Parameters
        ----------
        imgs: list of Niimg-like objects
            See http://nilearn.github.io/manipulating_images/input_output.html
            Data on which the mask is calculated. If this is a list,
            the affine is considered the same for all.
        counfounds : list of CSV file paths or 2D matrices
            This parameter is passed to nilearn.signal.clean. Please see the
            related documentation for details. Should match with the list
            of imgs given.

        Returns
         -------
         self : object
            Returns the instance itself. Contains attributes listed
            at the object level.

        """
        imgs = check_imgs(imgs)
        self.masker = check_embedded_nifti_masker(self)

        if confounds is None:
            confounds = itertools.repeat(confounds)

        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker.mask_img is None:
            raise ValueError('The masker has not been provided a mask_img')
        self._fit(imgs, confounds)
        return self

    def fit_transform(self, imgs, y=None, **fit_params):
        self.fit(imgs, **fit_params)
        return self.shared_response

    def transform(self, X, y=None, index=None, confounds=None):
        """
        From data in X and basis from training data, computes a common space for data in X.

        Parameters
        ----------
        X : n_samples-list of array of shape (n_voxels, n_timeframes)
        index: list or None:
            use basis W_index[i] to compute the transform for data X_i
        confounds : list of CSV file paths or 2D matrices
            This parameter is passed to nilearn.signal.clean. Please see the
            related documentation for details. Should match with the list
            of imgs given.
        Returns
        -------
        common_space : array of shape (n_components, n_timeframes)
            common space
        """
        X = check_imgs(X)
        masker = self.masker

        if confounds is None:
            confounds = itertools.repeat(confounds)

        if index is None:
            index = np.array(range(len(X)))
        else:
            index = np.array(index)

        if self.transform_method == "mean":
            return self.transform_mean(X, index, masker, confounds)
        if self.transform_method == "stack":
            return self.transform_stack(X, index, masker, confounds)

    def transform_stack(self, X, index=None, masker=None, confounds=None):
        """
        Transform using S = np.pinv(W)X
        Returns
        -------
        common_space : array of shape (n_components, n_timeframes)
            common space
        """

        W_TW_inv = np.linalg.pinv(
            np.sum(
                Parallel(n_jobs=self.n_jobs, backend="threading")(
                    delayed(cov)(self.basis[i])
                    for i in index
                ),
                axis=0
            )
        )

        S = None
        for k, confound in zip(range(len(index)), confounds):
            i = index[k]
            X_k, masker = cache(
                load_img,
                memory=self.memory,
                memory_level=self.memory_level,
                func_memory_level=2
            )(
                masker,
                X[k],
                axis=0,
                confound=confound
            )
            W_i = self.basis[i]
            if S is None:
                S = W_i.T.dot(X_k)
            else:
                S += W_i.T.dot(X_k)
        return W_TW_inv.dot(S)

    def transform_mean(self, X, index=None, masker=None, confounds=None):
        """
        Transform using S = mean(np.pinv(W_i)X_i)
        Returns
        -------
        common_space : array of shape (n_components, n_timeframes)
            common space
        """

        S = []
        for k, confound in zip(range(len(index)), confounds):
            i = index[k]
            W_i = self.basis[i]
            X_k, masker = cache(
                load_img,
                memory=self.memory,
                memory_level=self.memory_level,
                func_memory_level=2
            )(
                masker,
                X[k],
                axis=0,
                confound=confound
            )
            W_i_inv = np.linalg.pinv(W_i.T.dot(W_i)).dot(W_i.T)
            S.append(W_i_inv.dot(X_k))

        return np.mean(S, axis=0)

    def inverse_transform(self, common_space, index=None):
        """
        From common space and the basis from training data
        Retrieve subjects' data described by the common space

        Parameters
        ----------
        common_space : 2D numpy array of shape (n_components, n_timeframes)
            common space
        index: list or None:
            list of indexes corresponding to the subjects to retrieve
        Returns
        -------
        reconstructed_data: list of 2D np array of shape (n_voxels, n_timeframes)
            For each loading, reconstructed data. The reconstructuted data consist
            in the concatenation of all available runs.
        """
        if index is None:
            index = np.array(range(len(self.basis)))
        else:
            index = np.array(index)

        basis = self.basis

        return [
            basis[i].dot(common_space) for i in index
        ]

    def _fit(self, imgs, confounds=None):
        pass
