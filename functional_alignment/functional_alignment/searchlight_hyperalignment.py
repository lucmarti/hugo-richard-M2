import numpy as np
from nilearn import masking
from nilearn.image.resampling import coord_transform
from nilearn.input_data.nifti_spheres_masker import _apply_mask_and_get_affinity
from hyperalignment import compute_model_space, hyperalign
from scipy.sparse import coo_matrix
from sklearn.base import BaseEstimator, TransformerMixin


def searchlight_hyperalign(X, Y, A, scale=True):
    """
    Hyperalign X to Y using searchlight
    The neighbourhood of any voxel i in X can be accessed
    using the ith row of adjacency matrix A
    
    For any searchlight ball sl_X and sl_Y
    finds sl_basis and such that 
    ||sl_basis sl_X - sl_Y||^2 is minimized
    with sl_basis = sc * R with sc an integer and R an orthogonal matrix
    and computes a basis such that basis = sum_{sl} R_{sl}
    
    Parameters
    ----------
    X: np array of shape (voxels, time)
        Source data
    Y: np array of shape (voxels, time)
        Target data
    A: scipy lil sparse matrix
        Adjacency matrix
    scale: bool
        if true we use scaling parameter
        if false sc is set to 1

    Returns
    -------
    basis: sparse matrix
        Transformation matrix such that basis.dot(X) hyperaligns X with Y
    """

    basis_values = []
    basis_rows = []
    basis_cols = []

    for i in range(len(A.rows)):
        neighbours_i = A.rows[i]

        # We find the neighbourhood of voxel i for subject X and Y
        sl_X = X[neighbours_i]
        sl_Y = Y[neighbours_i]

        # We hyperalign neighbourhood of X and Y
        sl_R, sl_scale = hyperalign(sl_X, sl_Y, scale=scale)

        # We store all weights in np arrays
        # They are duplicates since searchlights are overlapping
        basis_values = np.append(basis_values, sl_scale * sl_R.flatten())
        basis_rows = np.append(basis_rows, np.repeat(neighbours_i, len(neighbours_i)))
        basis_cols = np.append(basis_cols, np.tile(neighbours_i, len(neighbours_i)))

    # We compute the final basis, adding all duplicates
    basis = coo_matrix((basis_values, (basis_rows, basis_cols))).tocsc()

    return basis


def searchlight_hyperalign_list(X_list, A, scale=True):
    """
    Hyperalign X_list = [X1, ..., Xn] using searchlight
    The neighbourhood of any voxel in Xi can be accessed
    using the corresponding row of adjacency matrix A
    
    For any searchlight balls, hyperaligns subject data finding
    the search light basis that project the searchlight balls in the
    common space.
    
    The basis operator is computed by summing all searchlight basis.
    
    Parameters
    ----------
    X_list: list of np array of shape (voxels, time)
        Data to hyperalign
    A: scipy lil sparse matrix
        Adjacency matrix of input data
    scale: bool
        if true we use scaling parameter
        if false scaling parameter is set to 1
    Returns
    -------
    """

    basis_ = []
    basis_values = [[] for _ in range(len(X_list))]
    basis_rows = [[] for _ in range(len(X_list))]
    basis_cols = [[] for _ in range(len(X_list))]

    # For each voxel i we compute the searchlight ball of center i
    # and the associated basis for each subject.

    for i in range(len(A.rows)):
        neighbours_i = A.rows[i]

        # We find the neighbourhood of voxel i for all subjects
        sl_X_list = [X[neighbours_i] for X in X_list]

        # We find the common space for the neighbourhood of voxel i for all subjects
        model_space = compute_model_space(True, sl_X_list)

        # We hyperalign the neighbourhood of voxel i in all subject to the common space
        sl_basis = []
        for i in range(len(sl_X_list)):
            R_i, sc_i = hyperalign(sl_X_list[i], model_space, scale=True)
            sl_basis.append(R_i * sc_i)

        # We store the corresponding basis
        for i_subject in range(len(X_list)):
            basis_values[i_subject] = np.append(
                basis_values[i_subject],
                sl_basis[i_subject].flatten()
            )

            basis_rows[i_subject] = np.append(
                basis_rows[i_subject],
                np.repeat(neighbours_i, len(neighbours_i))
            )
            basis_cols[i_subject] = np.append(
                basis_cols[i_subject],
                np.tile(neighbours_i, len(neighbours_i))
            )

    # For each subject, we aggregate basis by adding all the weights
    # estimated for a given voxel-to-feature mapping from all surface searchlights that
    # includes that voxel-feature pair

    for i in range(len(X_list)):
        basis_.append(coo_matrix((basis_values[i], (basis_rows[i], basis_cols[i]))).tocsc())

    return basis_


def get_data(mask_img, process_mask_img, img, radius):
    """
    This function computes search light using a sphere 
    of given radius
    
    Parameters
    ----------
    mask_img: Niimg-like object
        See http://nilearn.github.io/manipulating_images/input_output.html
        boolean image giving location of voxels containing usable signals.
    process_mask_img: Niimg-like object
        See http://nilearn.github.io/manipulating_images/input_output.html
        boolean image giving voxels on which searchlight should be
        computed.
    img: Niimg-like object
        See http://nilearn.github.io/manipulating_images/input_output.html
        input image
    radius : float, optional
        radius of the searchlight ball, in millimeters. Defaults to 3.
    
    Returns
    --------
    X : np array
        voxels x time image
    A: np array
        adjacency matrix: rows i of this matrix gives the index of the neighbors of voxel i
    """
    process_mask, process_mask_affine = masking._load_mask_img(
        process_mask_img)
    process_mask_coords = np.where(process_mask != 0)
    process_mask_coords = coord_transform(
        process_mask_coords[0], process_mask_coords[1],
        process_mask_coords[2], process_mask_affine)
    process_mask_coords = np.asarray(process_mask_coords).T

    X, A = _apply_mask_and_get_affinity(
        process_mask_coords, img, radius, True,
        mask_img=mask_img)

    return X.T, A


class SearchlightHyperalignment(BaseEstimator, TransformerMixin):
    """
    Hyperalignment using searchlight
    
    Parameters
    -----------
    X_list : list of np-array of shape (voxels, time)
        Input data
    A: scipy sparse matrix
        Adjacency matrix of input data
    verbose: bool
        Verbosity level. Default is False
    """
    def __init__(self, adjacency_matrix, verbose=False):
        self.adjacency_matrix = adjacency_matrix
        self.verbose = verbose
        self.basis_ = None

    def fit(self, X_list, y=None):
        """
        Fit the data in the list
        Compute model space and basis to project data in common space
        
        Parameters
        ----------
        X_list : list of np-array of shape (voxels, time)
            Input data
        
        Returns
        -------
        The object itself

        """

        # We record the data in a list. Let us recall that adjacency matrix A
        # is the same for all images

        basis_ = searchlight_hyperalign_list(X_list, self.adjacency_matrix, scale=True)
        self.basis_ = basis_

        return self

    def transform(self, X_list, y=None):
        """
        Transform input data from input space to common space
        
        Parameters
        ----------
        X_list : list of np-array of shape (voxels, time)
            Input data

        Returns
        -------
        X_transformed_list: List of np-array of shape (voxels, time)
            Transformed data

        """

        X_transform_list = []
        for i_subject in range(len(X_list)):
            X = X_list[i_subject]
            X_transform = self.basis_[i_subject].dot(X)
            X_transform_list.append(X_transform)

        return X_transform_list

    def inverse_transform(self, X_list, y=None):
        """
        Inverse transform data: maps data from common space to raw data space
        
        Parameters
        ----------
        X_list : list of np-array of shape (voxels, time)
            Transformed data

        Returns
        -------
        X_inverse_transformed_list: List of np-array of shape (voxels, time)
            Inverse transformed data
        """


        X_inverse_transform_list = []
        for i_subject in range(len(X_list)):
            X = X_list[i_subject]
            X_inverse_transform = self.basis_[i_subject].T.dot(X)
            X_inverse_transform_list.append(X_inverse_transform)

        return X_inverse_transform_list








