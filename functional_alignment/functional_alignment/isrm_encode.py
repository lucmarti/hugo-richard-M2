import numpy as np
import scipy.linalg as linalg
import scipy.sparse as sp
from sklearn.utils import check_random_state
from numba import jit


def laplacian_matrix(mask):
    """
    Computes the laplacian matrix in 3D from the mask
    The gradient is given by sparse matrix multiplication
    Parameters
    ----------
    mask: numpy array

    Returns
    -------
    Lx, Ly, Lz: n_voxels, n_voxels sparse matrices
        Laplacian matrices
        Gradient of data accross X is given by Lx.dot(X)
    """
    n_voxels = int(np.sum(mask))
    mask_coords = list(zip(*np.where(mask != 0)))

    mx, my, mz = mask.shape
    locate = np.zeros((mx, my, mz))
    locate = locate - 1
    locate[mask] = np.arange(n_voxels)

    row_indx = []
    datax = []
    col_indx = []

    row_indy = []
    datay = []
    col_indy = []

    row_indz = []
    dataz = []
    col_indz = []

    for x, y, z in mask_coords:
        v = locate[x, y, z]

        if locate[x - 1, y, z] != -1:
            row_indx.append(v)
            col_indx.append(locate[x - 1, y, z])
            datax.append(-1.)
            row_indx.append(v)
            col_indx.append(v)
            datax.append(1.)

        if locate[x, y - 1, z] != -1:
            row_indy.append(v)
            col_indy.append(locate[x, y - 1, z])
            datay.append(-1.)
            row_indy.append(v)
            col_indy.append(v)
            datay.append(1.)

        if locate[x, y, z - 1] != -1:
            row_indz.append(v)
            col_indz.append(v)
            dataz.append(1.)
            row_indz.append(v)
            col_indz.append(locate[x, y, z - 1])
            dataz.append(-1.)

    Lx = sp.csc_matrix((datax, (row_indx, col_indx)), shape=(n_voxels, n_voxels))
    Ly = sp.csc_matrix((datay, (row_indy, col_indy)), shape=(n_voxels, n_voxels))
    Lz = sp.csc_matrix((dataz, (row_indz, col_indz)), shape=(n_voxels, n_voxels))

    return Lx, Ly, Lz


def similarity_matrix(n_voxels, n_subjects):
    """
    Computes a similarity matrix from number of voxels and subjects
    Parameters
    ----------
    n_voxels: int
    n_subjects: int
    Returns
    -------
    S_1,...S_n: n_voxels * n_subject, n_voxels * n_subject sparse matrix
        S_i X computes X_i - mean(X)

    """
    S = []
    for n in range(n_subjects):
        data = []
        col = []
        row = []
        for v in range(n_voxels):
            for nn in range(n_subjects):
                row.append(v + n * n_subjects)
                col.append(n_voxels * nn + v)
                if nn == n:
                    data.append(1 - 1. / n_subjects)
                else:
                    data.append(-1. / n_subjects)

        S.append(
            sp.csc_matrix((data, (row, col)), shape=(n_voxels * n_subjects, n_voxels * n_subjects))
        )

    return S


def structure_matrix(beta, gamma, laplacian_matrix, similarity_matrix):
    """
    Computes the sparse structure matrix that summarize the effect of laplacian and similarity matrices in the gradient
    Parameters
    ----------
    beta: float
        smoothness controlling parameter
    gamma: float
        similarity controlling parameter
    laplacian_matrix: list of 3 sparse n_voxels * n_voxels matrix,
        [Lx, Ly, Lz] corresponding to laplacian matrices in the 3 directions,
    similarity_matrix: n_subjects list of sparse n_subjects * n_voxels, n_subjects * n_voxels matrix
        S such that sum_i ||W_i - mean_W||***2 = sum_i ||S[i].dot(W)||**2

    Returns
    -------
    structure_matrix: sparse n_voxels * n_subjects, n_voxels * n_subjects matrix
        with Lxx = kron(I, Lx), Lyy = kron(I, Ly) and Lzz = kron(I, Lz)
        G = \beta (Lxx.T.dot(Lxx) + Lyy.T.dot(Lyy) + Lzz.T.dot(Lzz)) + \gamma (\sum_i S[i].T.dot(S[i])
    """
    Lx, Ly, Lz = laplacian_matrix
    n_subjects = len(similarity_matrix)
    G = Lx.T.dot(Lx) + Ly.T.dot(Ly) + Lz.T.dot(Lz)
    G = sp.kron(np.eye(n_subjects), G)
    G = beta * G

    for i in range(n_subjects):
        G += gamma * similarity_matrix[i].T.dot(similarity_matrix[i])
    return G


def _solver_Ax_xB_C(A, B, C):
    """
    Solve equation AX + XB = C
    where A, B are symmetric matrices
    Parameters
    ----------
    A: np array of shape m, m
    B: np array of shape n, n
    C: np array of shape m, n

    Returns
    -------
    X: np array of shape m, n
    """
    Da, U = linalg.eigh(A)
    Db, V = linalg.eigh(B)

    C = U.T.dot(C).dot(V)

    X = np.zeros(C.shape)
    for i in range(len(C)):
        for j in range(len(C[i])):
            X[i, j] = C[i, j] / (Da[i] + Db[j])

    X = U.dot(X).dot(V.T)
    return X


@jit(nopython=True)
def _numba_isrm_encode(indptr, indices_, values_, tol, data, shared_response, alpha, G_diag, positive, max_iter):
    n_timeframes, n_voxels = data.shape
    n_components = shared_response.shape[1]
    W = np.zeros((n_components, n_voxels))
    X = data
    S = shared_response

    voxels = np.arange(n_voxels)
    components = np.arange(n_components)

    dE = np.inf
    prec_error = np.inf

    S_TS = S.T.dot(S)
    STX = S.T.dot(X)

    l2_error = 0.5 * np.sum((X) ** 2)
    pen_error = 0
    error = l2_error + pen_error

    n_iter = 0

    l_values = len(values_)

    while dE > tol:
        if n_iter >= max_iter:
            break
        n_iter += 1
        np.random.shuffle(voxels)

        for v in voxels:
            if l_values > 0:
                indices = indices_[indptr[v]:indptr[v + 1]]
                values = values_[indptr[v]:indptr[v + 1]]
                G_vv = G_diag[v]
                prev_GvW = W[:, indices].dot(values)
            else:
                G_vv = 0
            np.random.shuffle(components)

            prev_W = np.copy(W[:, v])

            for k in components:
                w_kv = STX[k, v]
                w_kv -= S_TS[k, :].dot(W[:, v])
                w_kv += S_TS[k, k] * W[k, v]

                if l_values > 0:
                    w_kv -= prev_GvW[k]
                    w_kv += G_vv * W[k, v]

                if w_kv > alpha:
                    w_kv -= alpha
                elif w_kv < -alpha:
                    w_kv += alpha
                else:
                    w_kv = 0

                if positive:
                    if w_kv < 0:
                        w_kv = 0

                w_kv /= float(S_TS[k, k] + G_vv)
                W[k, v] = w_kv

            S_w = S.dot(W[:, v])
            prev_S_w = S.dot(prev_W)

            l2_error += 0.5 * np.sum((X[:, v] - S_w) ** 2 - (X[:, v] - prev_S_w) ** 2)
            pen_error += alpha * (np.sum(np.abs(W[:, v]) - np.abs(prev_W)))

            if l_values > 0:
                GvW = W[:, indices].dot(values)

                for k in components:
                    pen_error += W[k, v] * GvW[k] - (prev_W[k] * prev_GvW[k])
                    pen_error -= 0.5 * (W[k, v] * G_vv * W[k, v] - (prev_W[k] * G_vv * prev_W[k]))

            error = l2_error + pen_error

        dE = prec_error - error
        prec_error = error
    return W, pen_error, l2_error, dE, n_iter


@jit(nopython=True)
def _numba_online_isrm_encode(batch, indptr, indices_, values_, max_iter, data, shared_response, code,
                              alpha, G_diag, positive):
    n_timeframes, n_seen_voxels = data.shape
    n_components = shared_response.shape[1]
    W = code
    X = data
    S = shared_response

    voxels = np.arange(n_seen_voxels)
    components = np.arange(n_components)

    S_TS = S.T.dot(S)
    STX = S.T.dot(X)

    l_values = len(values_)

    for n_iter in range(max_iter):
        np.random.shuffle(voxels)

        for v_i in voxels:
            v = batch[v_i]
            if l_values > 0:
                indices = indices_[indptr[v]:indptr[v + 1]]
                values = values_[indptr[v]:indptr[v + 1]]
                G_vv = G_diag[v]
                prev_GvW = W[:, indices].dot(values)
            else:
                G_vv = 0
            np.random.shuffle(components)

            for k in components:
                w_kv = STX[k, v_i]
                w_kv -= S_TS[k, :].dot(W[:, v])
                w_kv += S_TS[k, k] * W[k, v]

                if l_values > 0:
                    w_kv -= prev_GvW[k]
                    w_kv += G_vv * W[k, v]

                if w_kv > alpha:
                    w_kv -= alpha
                elif w_kv < -alpha:
                    w_kv += alpha
                else:
                    w_kv = 0

                if positive:
                    if w_kv < 0:
                        w_kv = 0

                w_kv /= float(S_TS[k, k] + G_vv)
                W[k, v] = w_kv

    return W


def isrm_encode(data, shared_response,
                alpha, structure_matrix,
                seed, tol=1e-6, max_iter=10, positive=True):
    """
    Computes basis W using data X and shared response S:
    W = argmin(0.5 * ||X - WS||**2
    + alpha |W|_1
    + beta 0.5 ||grad w||**2
    + gamma * 0.5 sum_i ||W_i - mean_W||***2
    )

    Parameters
    ----------
    data: numpy array of shape n_timeframes, n_voxels
        Batch of the data
    shared_response: numpy array of shape n_timeframes, n_components
        Current dictionary
    alpha: float
        sparsity controlling parameter
    structure_matrix: sparse matrix n_voxels * n_subjects, n_voxels
        summarize effect of laplacian and similarity matrix in the gradient
    seed: int
        seed of the random generator
    tol: float
        maximum distance between current error and error of previous estimate

    Returns
    -------
    code: numpy array of shape n_components, n_voxels * n_subjects
        new estimate for the code
    """

    G = structure_matrix

    indptr = G.indptr
    indices_ = G.indices
    values_ = G.data

    G_diag = G.diagonal()

    np.random.seed(seed)
    W, pen_error, l2_error, dE, n_iter = _numba_isrm_encode(indptr, indices_, values_, tol, data.astype(float, order="F"),
                                                            shared_response.astype(float, order="F"), alpha, G_diag,
                                              positive, max_iter)
    return W, dE, pen_error, l2_error


def _numba_in_1d(ar1, ar2):
    """
    Test whether each element of a 1-D array is also present in a second array.
    Returns a boolean array the same length as `ar1` that is True
    where an element of `ar1` is in `ar2` and False otherwise.
    Parameters
    ----------
    ar1 : (M,) array_like
        Input array.
    ar2 : array_like
        The values against which to test each value of `ar1`.
    Returns
    -------
    in1d : (M,) ndarray, bool
        The values `ar1[in1d]` are in `ar2`.
    """
    mask = np.zeros(len(ar1))
    mask = mask != 0
    for a in ar2:
        mask |= (ar1 == a)
    return mask


def online_isrm_encode(batch, data, shared_response, code,
                       alpha, structure_matrix,
                       seed, iter=10, positive=True):
    """
    Computes basis W using data X and shared response S:
    W = argmin(0.5 * ||X - WS||**2
    + alpha |W|_1
    + beta 0.5 ||grad w||**2
    + gamma * 0.5 sum_i ||W_i - mean_W||***2
    )

    Parameters
    ----------
    batch: np-array
        indexes of batch elements:
        code[:, batch] gives the code corresponding to the voxels in the batch
    data: numpy array of shape n_timeframes, n_seen_voxels
        Batch of the data
    shared_response: numpy array of shape n_timeframes, n_components
        Current dictionary
    code: numpy array of shape n_components, n_voxels
        Current code
    alpha: float
        sparsity controlling parameter
    structure_matrix: sparse matrix n_voxels, n_voxels
        summarize effect of laplacian and similarity matrix in the gradient
    seed: int
        seed of the random generator
    iter: int
        number of iterations

    Returns
    -------
    code: numpy array of shape n_components, n_voxels * n_subjects
        new estimate for the code
    """

    G = structure_matrix

    indptr = G.indptr
    indices_ = G.indices
    values_ = G.data

    G_diag = G.diagonal()

    np.random.seed(seed)
    W = _numba_online_isrm_encode(batch, indptr, indices_, values_, iter, data, shared_response,
                                                     code,
                                                     alpha, G_diag, positive)
    return W