from sklearn.base import BaseEstimator, TransformerMixin
import scipy
import numpy as np


class RidgeHyperalignment(BaseEstimator, TransformerMixin):
    """
    Solves the optimization problem
    || XW - Y ||^2 + alpha ||W||^2
    for X of size (n_samples, n_features)
    where n_features is very large
    
    Parameters
    ----------
    alpha: int
        regularisation parameter
    """

    def __init__(self, alpha=0.1):
        self.alpha = alpha
        self.a = None
        self.b = None

    def fit(self, X, y):
        """
        Fit the model: learns two matrices 
        A, B such that W = A B is solution 
        of the above problem.
        
        We will solve
        (X.T.dot(X) + alpha)W = X.T Y
        
        
        Parameters
        ----------
        X: np array of shape (n_samples, n_features)
            input data
        y: np array of shape (n_samples, n_features)
            target data
        
        Returns
        -------
        the object itself
        """

        alpha = self.alpha
        U, s, Vt = scipy.linalg.svd(X, full_matrices=False)

        b = s/(alpha + s**2)
        b = b.reshape(len(b), 1) * U.T
        b = b.dot(y)

        self.a = Vt.T
        self.b = b

        return self

    def transform(self, X):
        """
        Transform X using the fitted model
        Parameters
        ----------
        X: np array of shape (n_samples, n_features)
            input data
        Returns
        -------
        X_transformed: np array of shape (n_samples, n_features)
            transformed data
        """

        return (X.dot(self.a)).dot(self.b)






