from nilearn.input_data import NiftiMasker
import logging
import nibabel as nib
from joblib import Memory
import glob
import numpy as np
import pandas as pd
from nilearn.image import index_img


def load(
        dataset="",
        subjects=None,
        runs=None,
        data_path="",
):
    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob(data_path % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)

    return np.array(X), dataset


def load_ibc(
        dataset="ibc",
        subjects=None,
        runs=None,
        data_path="",
):

    logging.info("Loading Data ...")
    mask_img = nib.load(mask_path)

    logging.info("IBC Dataset")
    logging.info("Low pass 0.1")
    logging.info("High pass 0.01")
    logging.info("Time repetition 2")
    masker = NiftiMasker(
        mask_img=mask_path,
        standardize=True,
        detrend=True,
        memory=memory,
        low_pass=0.1,
        high_pass=0.01,
        t_r=2,
    ).fit()

    logging.info("Masker loaded")
    logging.debug(masker.affine_)

    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob((data_path) % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)

    return np.array(X), masker, mask_img, dataset


def load_fuzzy_ibc(
        dataset="ibc",
        subjects=[],
        runs=[],
        mask_path="",
        data_path="",
        memory=""
):

    logging.info("Loading Data ...")
    mask_img = nib.load(mask_path)

    logging.info("IBC Dataset")
    logging.info("Low pass 0.1")
    logging.info("High pass 0.01")
    logging.info("Time repetition 2")
    masker = NiftiMasker(
        mask_img=mask_path,
        standardize=True,
        detrend=True,
        memory=memory,
        low_pass=0.1,
        high_pass=0.01,
        t_r=2,
        smoothing_fwhm=5
    ).fit()

    logging.info("Masker loaded")
    logging.debug(masker.affine_)

    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob((data_path) % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)

    return np.array(X), masker, mask_img, dataset


def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=16):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = "/storage/data/sherlock_movie_watch_dataset/preloaded_masker/mask_all_subj_movie_gm.nii"
    # path + 'preloaded_masker/mask_all_subj_movie.nii'
    return image_list, movie_labels, masker_path


def load_forrest_data(n_subjects=20,
                      runs=list(range(1, 8)),
                      data_path="/storage/data/openfmri/ds113/sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"):
    mask_path = "/storage/data/openfmri/ds113/3mm_mask_gm.nii.gz"
    subjects = list(range(1, n_subjects + 1))
    subjects.remove(10)
    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob(data_path % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)
    return np.array(X), mask_path


def load_ibc_3_mm_data(path="/storage/tompouce/bthirion/maps_3mm.csv"):

    mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"

    ibc = pd.DataFrame.from_csv(
        path, sep=",", index_col=0)
    image_list = []
    for subj_i in range(1, 16):
        subj_data = ibc.loc[ibc['subject'] ==
                            "sub-" + "{0:0=2d}".format(subj_i)]
        filtered_data = subj_data.drop_duplicates(
            subset="contrast", keep='last')
        if not filtered_data.empty:
            image_list.append(filtered_data)
    return image_list, mask_path


def make_train_test_sherlock(image_list, labels, masker, n_max=None):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Extract scenes
        Parameters :
        - List of int fold out of 50 scenes
        - Dataframe labels (scene, onset, offset)
        - List of nii files subject_imgs

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass
    if n_max is not None:
        train_session = [train_session[i] for i in range(n_max)]

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list


def make_train_test_forrest(image_list, masker):
    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train, X_test = [], []
        for j in range(7):
            if j < 6:
                X_train.append(masker.transform(image_list[i][j]))
            else:
                X_test.append(masker.transform(image_list[i][j]))
        X_train_list.append(np.vstack(X_train))
        X_test_list.append(np.vstack(X_test))
    return X_train_list, X_test_list


def make_train_test_ibc_3_mm(
        image_list, train_tasks, test_tasks, masker):
    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train, X_test = [], []
        for index, row in image_list[i].iterrows():
            if row["task"] in (train_tasks):
                X_train.append(masker.transform(row["path"]))
            elif row["task"] in (test_tasks):
                X_test.append(masker.transform(row["path"]))
        X_train_list.append(np.vstack(X_train))
        X_test_list.append(np.vstack(X_test))
    return X_train_list, X_test_list
