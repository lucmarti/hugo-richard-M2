from sklearn.base import BaseEstimator, TransformerMixin
import scipy
import numpy as np
from sklearn import metrics
from sklearn.model_selection import ShuffleSplit
from nilearn.signal import clean
from functional_alignment.utils import generate_folds
from time import time


class RidgeCV(BaseEstimator, TransformerMixin):
    """
    Solves the optimization problem
    || XW - Y ||^2 + alpha ||W||^2
    for X of size (n_samples, n_features)
    where n_features is very large

    Parameters
    ----------
    alpha: int
        regularisation parameter
    """

    def __init__(self, alphas=(0.1,), n_splits=2):
        self.alphas = alphas
        self.alpha = None
        self.a = None
        self.b = None
        self.n_splits = n_splits

    def fit(self, X, Y, forced_alpha=None):
        """
        Fit the model: learns two matrices 
        A, B such that W = A B is solution 
        of the above problem.

        We will solve
        (X.T.dot(X) + alpha)W = X.T Y


        Parameters
        ----------
        X: np array of shape (n_samples, n_features)
            input data
        y: np array of shape (n_samples, n_features)
            target data
        forced_alpha: int 
            force the value of alpha (no CV needed)
        Returns
        -------
        the object itself
        """
        alphas = self.alphas

        if forced_alpha is None:
            scores = []
            for train_index, test_index in generate_folds(X, self.n_splits):
                X_train = X[train_index]
                Y_train = Y[train_index]

                X_test = X[test_index]
                Y_test = Y[test_index]

                U, s, Vt = scipy.linalg.svd(X_train, full_matrices=False)
                # print(time() - t0)

                score_ = []
                for alpha in alphas:

                    idx = s > 1e-15  # same default value as scipy.linalg.pinv
                    s_nnz = s[idx][:, np.newaxis]
                    b = np.zeros((s.size, 1), dtype=X.dtype)
                    b[idx] = s_nnz / (s_nnz ** 2 + alpha)
                    a = Vt.T
                    UTy = np.dot(U.T, Y_train)
                    b = b * UTy
                    num = (((X_test.dot(a)).dot(b) - Y_test)**2).sum(axis=0)
                    Y_test_mean = Y_test.mean(axis=0)
                    denom = ((Y_test - Y_test_mean)**2).sum(axis=0)
                    score = (1 - num/denom).mean()
                    score_.append(score)
                scores.append(score_)
                # print(time() - t0)


            # print(time() - t0)

            scores = np.array(scores)
            scores = np.array(scores).mean(axis=0)
            alpha = alphas[np.argmax(scores)]
        else:
            alpha = forced_alpha

        # print("X shape", X.shape)
        U, s, Vt = scipy.linalg.svd(X, full_matrices=False)
        b = s / (alpha + s ** 2)
        b = b.reshape(len(b), 1) * U.T
        b = b.dot(Y)
        # print("b shape", b.shape)
        a = Vt.T
        # print("a shape", a.shape)

        self.a = a
        self.b = b
        self.alpha = alpha

        return self

    def transform(self, X):
        """
        Transform X using the fitted model
        Parameters
        ----------
        X: np array of shape (n_samples, n_features)
            input data
        Returns
        -------
        X_transformed: np array of shape (n_samples, n_features)
            transformed data
        """

        return (X.dot(self.a)).dot(self.b)
