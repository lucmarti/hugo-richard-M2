import numpy as np
from nilearn.image import index_img, mean_img, concat_imgs

# Check for dimension of the images


def euclidian_mean_with_index_img(imgs):
    mean_images = []
    for frame in range(imgs[0].shape[0]):
        images = []
        for subject in imgs:
            images.append(index_img(subject, frame))
        mean_images.append(mean_img(concat_imgs(images)))
    template = concat_imgs(mean_images)
    return template


def euclidian_template(imgs, method='index', masker=None):
    if method is 'index':
        return euclidian_mean_with_index_img(imgs)
    elif method is 'masking':
        return euclidian_mean_with_masking(imgs, masker)


'''
class EuclidianTemplate(BaseEstimator, TransformerMixin):

    def __init__(self, mask=None, smoothing_fwhm=None,
                 standardize=False, detrend=False,
                 low_pass=None, high_pass=None, t_r=None,
                 target_affine=None, target_shape=None,
                 mask_strategy='epi', mask_args=None,
                 memory=Memory(cachedir=None), memory_level=0,
                 n_jobs=1,
                 verbose=0):
        """
        Description of the class


        Parameters
        ----------
        smoothing_fwhm: float, optional
            If smoothing_fwhm is not None, it gives the size in millimeters of the
            spatial smoothing to apply to the signal.
        mask: Niimg-like object, instance of NiftiMasker or MultiNiftiMasker, optional
            Mask to be used on data. If an instance of masker is passed,
            then its mask will be used. If no mask is given,
            it will be computed automatically by a MultiNiftiMasker with default
            parameters.
        standardize : boolean, optional
            If standardize is True, the time-series are centered and normed:
            their variance is put to 1 in the time dimension.
        target_affine: 3x3 or 4x4 matrix, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        target_shape: 3-tuple of integers, optional
            This parameter is passed to image.resample_img. Please see the
            related documentation for details.
        low_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        high_pass: None or float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        t_r: float, optional
            This parameter is passed to signal.clean. Please see the related
            documentation for details
        memory: instance of joblib.Memory or string
            Used to cache the masking process and results of algorithms.
            By default, no caching is done. If a string is given, it is the
            path to the caching directory.
        memory_level: integer, optional
            Rough estimator of the amount of memory used by caching. Higher value
            means more memory for caching.
        n_jobs: integer, optional
            The number of CPUs to use to do the computation. -1 means
            'all CPUs', -2 'all CPUs but one', and so on.
        verbose: integer, optional
            Indicate the level of verbosity. By default, nothing is printed.
        """
        self.n_pieces = n_pieces
        self.method = method
        self.perturbation = perturbation
        self.n_bootstrap = n_bootstrap

        self.mask = mask
        self.smoothing_fwhm = smoothing_fwhm
        self.standardize = standardize
        self.detrend = detrend
        self.low_pass = low_pass
        self.high_pass = high_pass
        self.t_r = t_r
        self.target_affine = target_affine
        self.target_shape = target_shape
        self.mask_strategy = mask_strategy
        self.mask_args = mask_args
        self.memory = memory
        self.memory_level = memory_level
        self.n_jobs = n_jobs

        self.verbose = verbose

        self.infos_ = None
        return self

    def fit(self, imgs):
        ''''''
    Docstring of fit function
    ---
    Parameters
    imgs = list of 3D or 4D Nimmg images
'''
'''
        self.masker_ = check_embedded_nifti_masker(self)
        self.masker_.n_jobs = self.n_jobs
        # Avoid warning with imgs != None
        # if masker_ has been provided a mask_img
        if self.masker_.mask_img is None:
            self.masker_.fit([X])
        else:
            self.masker_.fit()
        self.mask_img_ = load_img(self.masker_.mask_img)
        return self'''
