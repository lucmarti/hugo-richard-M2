import os
import time
from ants import image_read, registration, write_transform, read_transform
import os
import time

import numpy as np

from nilearn.input_data import NiftiMasker
from nilearn.image import index_img, load_img, math_img
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
from sklearn.model_selection import ShuffleSplit

from sklearn.decomposition import PCA
from nilearn.input_data import NiftiMasker
from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from functional_alignment.utils import score_save_table, make_train_test_images
from functional_alignment.load_data import load_sherlock_data, load_forrest_data, load_ibc_3_mm_data, make_train_test_sherlock, make_train_test_forrest, make_train_test_ibc_3_mm
from functional_alignment.template import TemplateAlignment, euclidian_mean_with_masking
from functional_alignment.utils import score_table
from functional_alignment.ot_template_alignment import OptimalTransportTemplate

from nilearn.image import mean_img


def calculate_diffeo_alignment(data_path, target_sub, source_sub, methods, grad_steps, smooth_flow):
    start_time = time.time()
    fixed_ibg = image_read(os.path.join(
        data_path, '%s_train_pca_ibg.nii.gz' % (str(target_sub))))
    moving_ibg = image_read(os.path.join(
        data_path, '%s_train_pca_ibg.nii.gz' % (str(source_sub))))
    for method in methods:
        for grad_step in grad_steps:
            reg = registration(fixed=fixed_ibg, moving=moving_ibg,
                               type_of_transform=method, grad_step=grad_step, flow_sigma=smooth_flow, total_sigma=0, reg_iterations=[100, 40, 20], syn_sampling=32, verbose=True)
            for i, tx in enumerate(reg['fwdtransforms']):
                if i == 0:

                    tr = image_read(tx)
                    tr.to_file(os.path.join(
                        data_path,  "%s_%s_reg_%s_grad_%s.nii.gz" %
                                    (str(source_sub), str(target_sub), method, str(grad_step))))
                else:
                    tr = read_transform(tx)
                    write_transform(tr, os.path.join(
                        data_path,  "%s_%s_reg_%s_grad_%s.mat" %
                                    (str(source_sub), str(target_sub), method, str(grad_step))))
    print("Registrations for pair %s, %s, took %s seconds" %
          (str(source_sub), str(target_sub), str(time.time() - start_time)))


data_dir = os.path.join(THREE_MM, 'group')
dataset = 'ibc'

mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory, mask_img=mask_path)
masker.fit(mask_path)
mask = load_img(mask_path)

subject_list = np.asarray(SUBJECTS)
task_list = ['archi_standard', 'archi_spatial', 'archi_social',
             'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
             'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                 conditions=CONDITIONS, task_list=task_list)
conditions = df[df.modality == 'bold'].contrast.unique()
n_conditions = len(conditions)


masker = NiftiMasker(mask_path)
masker.fit()

train_subjects = []

path_to_results = "/storage/tompouce/tbazeill/ibc/antspy_ap_pa/"
'''
for subject in subject_list:
    subject_path = []
    subject_path.extend([df[df.acquisition == 'ap'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions])
    masked = masker.transform(subject_path)
    pca_contrast = PCA(n_components=1)
    pca_contrast.fit(masked)
    pca_image = masker.inverse_transform(pca_contrast.components_[0])
    pca_image.to_filename(os.path.join(
        path_to_results, '%s_pca.nii.gz' % subject))


# %%
# -1000 background values

for subject in subject_list:
    mean = load_img(os.path.join(
        path_to_results, '%s_pca.nii.gz' % subject))
    mean.get_data()[mask.get_data() == 0] = -1000
    mean.to_filename(os.path.join(
        path_to_results, '%s_train_pca_ibg.nii.gz' % (subject)))
'''
im_mask = image_read(mask_path)
pairs_to_align = [(4, 5), (12, 8), (2, 5), (2, 8), (3, 11),
                  (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
methods = ["SyNBold"]
grad_steps = [0.5]
smooth_flow = 2

for pair in pairs_to_align:
    subj_1, subj_2 = subject_list[pair[0]], subject_list[pair[1]]
    calculate_diffeo_alignment(
        path_to_results, subj_2, subj_1, methods, grad_steps, smooth_flow)
