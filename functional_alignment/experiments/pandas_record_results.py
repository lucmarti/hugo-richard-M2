from glob import glob
import pandas as pd
from nilearn.image import mean_img
import numpy as np

data = []
for file in glob("/storage/workspace/hrichard/M2_internship/results/exp10_roi/*/*mes*"):
    dataset = file.split("/")[-1].split("alignment")[0]
    algo = file.split("algo")[1].split("subject")[0]
    subject = file.split("subject_")[1].split("mes")[0]
    mes = file.split("mes_")[1].split("_exp")[0]
    k = file.split("k_")[1].split("/")[0]
    data.append([file, dataset, algo, subject, mes, k])

data = pd.DataFrame(data=data, columns=["path", "dataset", "algo", "subject", "mes", "n_components"])

# Check that nothing is missing in the database
n_algos = len(data["algo"].unique())
n_subjects = len(data[data["dataset"] == "ibc"]["subject"].unique()) + len(data[data["dataset"] == "forrest_roi"]["subject"].unique())
n_mes = len(data["mes"].unique())
n_components = len(data["n_components"].unique())

print(n_algos * n_subjects * n_mes * n_components)
print(len(data))
assert len(data) == n_algos * n_subjects * n_mes * n_components

# Group subjects together
data = data.groupby(["dataset", "mes", "n_components", "algo"]).aggregate(lambda x: tuple(x))

# Measure performance using mean
res = []
i = 0.

masks = [
    mean_img(
            "/storage/workspace/hrichard/M2_internship/results/exp13/mask_ibc_roi3.nii.gz").get_data().astype(bool),
    mean_img(
            "/storage/workspace/hrichard/M2_internship/results/exp5/mask_forrest_roi2.nii.gz").get_data().astype(bool)
]
for index, cols in data.iterrows():
    i += 1
    dataset = index[0]
    paths = cols["path"]
    X = mean_img(paths).get_data()

    if dataset == "ibc":
        mask = masks[0]
    if dataset == "forrest_roi":
        mask = masks[1]

    X = X[mask]
    res.append(np.mean(X))
    print(i / len(data))

data["perf"] = res
data.to_csv("../../paper/figures/exp10_roi_perfs.csv")