"""
Experiment 5: We first select region of interest using previous algorithms (for forrest dataset)
Then we learn a basis for subject 1 to 10 usinhyperalignmentg .
Can we reconstruct the response for these subject for a similar stimuli ?
"""

from hyperalignment import hyperalign
import nibabel as nib
from nilearn.input_data import NiftiMasker
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from sklearn.linear_model import RidgeCV

np.set_printoptions(precision=3)

print("Loading Data ...")
dataset = "forrest"
subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
runs = ["task001_run001",
        "task001_run002",
        "task001_run003"]

mask_path = "/storage/workspace/hrichard/results/exp5/mask_forrest_roi.nii.gz"
masker = NiftiMasker(
    mask_img=mask_path,
    standardize=True,
    detrend=True,
    memory="/storage/workspace/hrichard/cache_forrest/"
).fit()

print("Masker loaded")
print(masker.affine_)

X = []
for run in runs:
    X_run = []
    for subject in subjects:
        func_filename = ("/storage/data/openfmri/ds113/sub" +
                         "%03d" +
                         "/BOLD/" +
                         run +
                         "/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"
                         ) % subject
        sample = masker.transform(func_filename).T
        print("run: %s, subject: %s"%(run, str(subject)))
        print(sample.shape)
        print(nib.load(func_filename).affine)
        X_run.append(sample)
    X.append(X_run)
print("Done")

X_train1 = X[0]
X_test = X[1]

rs = ShuffleSplit(n_splits=10, test_size=.1)
rs.split(X_train1)



def update(algorithm, param=1, name=None):
    if algorithm == "mean":
        def func(X_train1, Y_train1, X_test):
            return X_test
    if algorithm == "hyperalignment":
        def func(X_train1, Y_train1, X_test):
            R_i, sc_i = hyperalign(X_train1, Y_train1, scale=True)
            R_sc_i = R_i * sc_i
            return R_sc_i.dot(X_test)
    if algorithm == "rrr":
        def func(X_train1, Y_train1, X_test):
            rrr = RRR(k=param[0], alpha=param[1]).fit(X_train1.T, Y_train1.T)
            return rrr.predict(X_test.T).T
    if algorithm == "ridge":
        def func(X_train1, Y_train1, X_test):
            ridge = RidgeCV(alphas=param).fit(X_train1.T, Y_train1.T)
            # We check that the intercept is close to 0
            assert (ridge.intercept_**2).sum() < 1e-6
            return ridge.predict(X_test.T).T
    
    if name is None:
        name = algorithm + str(param)

    return func, name


algorithms = [update("mean"),
              update("hyperalignment"),
              update("rrr", [25, 0.003]),
              update("ridge", tuple(i*100. for i in range(10, 30)), name="ridgeCV")
              ]

mean_exp_var = {}
for algorithm, name in algorithms:
    mean_exp_var[name] = []

for train_index, test_index in rs.split(X_train1):
    print("TRAIN:", train_index, "TEST:", test_index)
    X_train1_left_out = X_train1[test_index[0]]
    X_test_left_out = X_test[test_index[0]]

    for algorithm, name in algorithms:
        print(name)
        Y = []
        for i in train_index:
            Y.append(algorithm(X_train1[i],
                               X_train1_left_out,
                               X_test[i]))
        Y = np.array(Y).mean(axis=0)
        var_e = (X_test_left_out - Y).var(axis=1)
        mean_e = (X_test_left_out - Y).mean(axis=1).sum()
        print((1 - var_e).mean())
        exp_var = 1 - var_e
        mean_exp_var[name].append(exp_var)

for algorithm, name in algorithms:
    mean_exp_var[name] = np.array(mean_exp_var[name]).mean(axis=0)
    img = masker.inverse_transform(mean_exp_var[name])
    nib.save(
        img,
        "/storage/workspace/hrichard/results/exp5/" +
        dataset +
        "ROI" +
        "_algo" +
        name +
        "_exp5.nii"
    )
print("Done")

