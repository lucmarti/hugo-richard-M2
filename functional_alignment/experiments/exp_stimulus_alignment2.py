import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from rrr_cv import RRRCV
from ridge_cv import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
from template_alignment import TemplateAlignment
from deterministic_srm import DeterministicSRM
import os
import logging
import load_data
import pandas as pd
import nibabel
from nilearn.input_data import NiftiMasker
from scipy.stats.stats import pearsonr
from utils import generate_train_test
from nilearn.image import index_img
from template_alignment import TemplateAlignment

n_jobs = 10

#IBC_config

logging.basicConfig(level=logging.DEBUG, filename="logfile_map_stimulus_fuzzy_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

mask_dir = "/storage/store/data/ibc/derivatives/"
mask_path=mask_dir + "group/gm_mask.nii.gz"

# mask_path = "/storage/workspace/hrichard/results/exp13/mask_ibc_roi.nii.gz"

data_dir = "/storage/data/openfmri/ds113/"
storage_dir = "/storage/tompouce/hrichard/fuzzy_ibc/fuzzy_ibc_fit/"

data_dir = "/storage/store/data/ibc/derivatives/"
data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

memory="/storage/tompouce/hrichard/fuzzy_ibc/fuzzy_ibc_cache"
memory2="/storage/tompouce/hrichard/ibc/ibc_cache2"

subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
runs = ["Trn01",
        "Trn02",
        "Trn03",
        "Trn04",
        "Trn05",
        "Trn06",
        "Trn07",
        "Trn08",
        "Trn09",
        "Val01",
        "Val02",
        "Val03",
        "Val04",
        "Val05",
        "Val06",
        "Val07",
        "Val08"]

X, masker, mask_img, dataset = load_data.load_fuzzy_ibc(
    dataset="fuzzy_ibc",
    subjects=subjects,
    runs=runs,
    mask_path=mask_path,
    data_path=data_path,
    memory=memory
)

label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"
print("Loading of data ... Done")

# labels = create_labels(X[0], mask_img.get_data(), n_pieces=2)

# For resilient clustering
if os.path.isfile(label_path_files):
    labels = np.load(label_path_files)
else:
    img_00 = index_img(X[0, 0], 0)
    X_00 = masker.transform(img_00).T
    labels = create_labels(X_00, mask_img.get_data(), n_pieces=1000)
    np.save(label_path_save, labels)
    label_img = masker.inverse_transform(labels)
    nib.save(
        label_img,
        label_path_img
    )
# load experiments data

db = pd.read_csv("/home/parietal/hrichard/hugo-richard-M2/data/database.csv")
names_exp = []

X_test = []
for subject in subjects:
    db_subject = db[db["subject"] == subject]
    names_exp.append(
        db_subject["stim"].values +
        "__" +
        db_subject["task"].values
    )
    for path in db_subject["path"].values:
        print(path)
        img = nibabel.load(path)
        print("shape", img.shape)

    masker_3d = NiftiMasker(
        mask_img=mask_img,
        memory=memory2,
        ).fit()

    X_test_stim_list = []
    for path in db_subject["path"].values:
        X_test_stim = masker_3d.transform(path).T
        if len(X_test_stim.shape) == 1:
            X_test_stim = X_test_stim.reshape((X_test_stim.shape[0], 1))
        X_test_stim_list.append(X_test_stim)
    X_test.append(
        X_test_stim_list
    )

nb_stim = len(X_test[0])
print("number of stim: ", nb_stim)


def predict(Y_, algo, algo_name, i_train_subjects, i_test_subject, train_data, test_data, storage_d):
    total_time = 0
    if isinstance(algo, PieceWiseAlignment):
        for i in i_train_subjects:
            # Y_i_test is the prediction for subject test from subject i
            logging.info("Train subject: " + str(i))
            logging.info("Test subject: " + str(i_test_subject[0]))
            filename = storage_d + \
                       algo_name + \
                       str(i) + \
                       "-".join(map(str, i_test_subject)) + \
                       ".temp"

            filename_info = storage_d + \
                            "info_" + \
                            algo_name + \
                            str(i) + \
                            "-".join(map(str, i_test_subject)) + \
                            ".temp"

            if os.path.isfile(filename):
                algo.load(filename)
                logging.info("No fitting needed, we use memory")
                algo.dump_infos(filename_info)
            else:
                if os.path.isfile(filename_info):
                    algo.load_infos(filename_info)
                algo.fit(train_data[i, :], train_data[i_test_subject[0], :])
                logging.info("fit time: " + str(algo.fit_time_))
                algo.dump_infos(filename_info)

            Y_i_test = algo.transform([test_data[i]])
            logging.info("transform time: " + str(algo.transform_time_))
            Y_ += Y_i_test
            total_time += algo.fit_time_ + algo.transform_time_

        Y_ /= len(i_train_subjects)

    elif isinstance(algo, DeterministicSRM) or isinstance(algo, TemplateAlignment):
        filename = storage_dir + \
                   algo_name + \
                   ".temp"

        if os.path.isfile(filename):
            algo.load(filename)
            logging.info("No fitting needed, we use memory")
        else:
            algo.fit(train_data[:, :])
            algo.dump(filename)
            logging.info("fit time: " + str(algo.fit_time_))

        print(i_train_subjects)
        shared_response = algorithm.transform([test_data[i] for i in i_train_subjects],
                                              index=i_train_subjects)

        logging.info("transform time: " + str(algo.transform_time_))
        Y_ = algorithm.inverse_transform(shared_response, index=i_test_subject)[0]
        logging.info("inverse transform time: " + str(algo.inverse_transform_time_))

        total_time = algo.fit_time_ + algo.transform_time_ + algo.inverse_transform_time_

    return Y_, total_time

algorithms = [
    (DeterministicSRM(20, max_iter=10, masker=masker, scaling=True), "srm_ortho"),
    (PieceWiseAlignment(labels, masker=masker, method="hyperalignment", n_jobs=n_jobs),"hyperalignment"),
    (PieceWiseAlignment(labels, masker=masker, method=RidgeCV(alphas=(
                                                       1e3,
                                                       1e4,
                                                       1e5,
                                                       )), n_jobs=n_jobs, perturbation=True),"ridgeCV"),
    (PieceWiseAlignment(labels, masker=masker, method="mean", n_jobs=n_jobs), "mean"),
    (
        TemplateAlignment(
            mapping=PieceWiseAlignment(
                labels,
                method=RidgeCV(
                    alphas=(1e3, 1e4,
                            1e5)
                ),
                n_jobs=n_jobs,
                perturbation=True
            ),
            masker=masker
        ),
        "RCV_template"
    )
]

for train_subjects, test_subject in generate_train_test(subjects):
    print("TRAIN subject:" +
          train_subjects.__str__() +
          "TEST subject:" +
          test_subject.__str__())

    for algorithm, name in algorithms:
        print(name)
        for stim in range(nb_stim):
            print("Stim: %i / %i"%(stim+1, nb_stim))
            # Y_test is the prediction for subject test from all other subjects

            result_filename = "/storage/workspace/hrichard/results/exp15/" +\
                              dataset +\
                              "predict" +\
                              "_algo" +\
                              name +\
                              "subject" + str(test_subject[0]) +\
                              "_stim_" + names_exp[0][stim] +\
                              "_exp15.nii"

            if not os.path.isfile(result_filename):
                Y = X_test[test_subject[0]][stim]
                X_test_stim = [X_test[i][stim] for i in range(len(X_test))]
                Y_test = np.zeros_like(Y)

                Y_test, time_pred = predict(Y_test,
                                            algorithm,
                                            name,
                                            train_subjects,
                                            test_subject,
                                            X,
                                            X_test_stim,
                                            storage_dir)

                print("prediction time: ", time_pred)
                print("score: " + str(pearsonr(Y.flatten(), Y_test.flatten())))

                nib.save(
                    masker.inverse_transform(Y_test.flatten()),
                    "/storage/workspace/hrichard/results/exp15/" +
                    dataset +
                    "predict" +
                    "_algo" +
                    name +
                    "subject" + str(test_subject[0]) +
                    "_stim_" + names_exp[0][stim] +
                    "_exp15.nii"
                )

                with open("/storage/workspace/hrichard/results/exp15/" +
                                  dataset +
                                  "_timepred_alignment" +
                                  "_algo" +
                                  name +
                                  "subject_" + str(test_subject[0]) +
                                  "_stim_" + names_exp[0][stim] +
                                  "_exp15.nii", "w") as time_file:
                    time_file.write(str(time_pred))

logging.info("Done")
