import os
import time
import numpy as np
from nilearn.image import math_img, resample_img, concat_imgs, index_img
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ridge_cv import RidgeCV
from joblib import Memory, Parallel, delayed
import pandas as pd
# from nistats.second_level_model import SecondLevelModel
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public

from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.masking import intersect_masks
from nilearn.image import index_img
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def score_table(loss, X_gt, X_pred, multioutput='raw_values'):
    if loss is "r2":
        score = r2_score(X_gt, X_pred, multioutput=multioutput)
    elif loss is "zero_mean_r2":
        score = zero_mean_coefficient_determination(
            X_gt, X_pred, multioutput=multioutput)
    else:
        raise NameError("Unknown loss, it should be 'r2' or 'zero_mean_r2'")
    return np.maximum(score, -1)


def score_save_table(masker, path_to_results, header, termination, X_gt, X_pred,  multioutput='raw_values'):
    X_score = score_table(loss, X_gt, X_pred, multioutput)
    masker.inverse_transform(X_score)
    Im_score = masker.inverse_transform(X_score)
    Im_score.to_filename(path_to_results + "/" +
                         header + "/" + termination)


def zero_mean_coefficient_determination(y_true, y_pred, sample_weight=None, multioutput="uniform_average"):
    if y_true.ndim == 1:
        y_true = y_true.reshape((-1, 1))

    if y_pred.ndim == 1:
        y_pred = y_pred.reshape((-1, 1))

    if sample_weight is not None:
        weight = sample_weight[:, np.newaxis]
    else:
        weight = 1.

    numerator = (weight * (y_true - y_pred) ** 2).sum(axis=0, dtype=np.float64)
    denominator = (weight * (y_true) ** 2).sum(axis=0, dtype=np.float64)
    nonzero_denominator = denominator != 0
    nonzero_numerator = numerator != 0
    valid_score = nonzero_denominator & nonzero_numerator
    output_scores = np.ones([y_true.shape[1]])
    output_scores[valid_score] = 1 - (numerator[valid_score] /
                                      denominator[valid_score])
    output_scores[nonzero_numerator & ~nonzero_denominator] = 0

    if multioutput == 'raw_values':
        # return scores individually
        return output_scores
    elif multioutput == 'uniform_average':
        # passing None as weights results is uniform mean
        avg_weights = None
    elif multioutput == 'variance_weighted':
        avg_weights = (weight * (y_true - np.average(y_true, axis=0,
                                                     weights=sample_weight)) ** 2).sum(axis=0, dtype=np.float64)
        # avoid fail on constant y or one-element arrays
        if not np.any(nonzero_denominator):
            if not np.any(nonzero_numerator):
                return 1.0
            else:
                return 0.0
    else:
        avg_weights = multioutput

    return np.average(output_scores, weights=avg_weights)


path_to_results = "/storage/tompouce/tbazeill/ibc/"
clustering_method = "k_means"
all_methods = ["identity", "scaled_orthogonal",
               "ridge_cv", "piecewise", "srm", "piecewise_srm"]
methods = ["optimal_transport", "RidgeCV", "scaled_orthogonal", "permutation"]
# alignment method for piecewise
# in "scaled_orthogonal", "RidgeCV"
n_bootstraps = [1, 20]  # bootstraping for piecewise
reg = 1
# Experiment crucial parameters
loss = "zero_mean_r2"  # "r2" or "zero_mean_r2"
n_jobs = 2

all_ibc_pairs = [(12, 8), (2, 5), (2, 8), (3, 11),
                 (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
subject_pair_list = all_ibc_pairs

# Method specific parameters
n_splits = 4  # n_subjectsumber of folds for validations
n_pieces = 150

# Access to the data
data_dir = os.path.join(THREE_MM, 'group')

# Access to the data
subject_list = SUBJECTS
task_list = ['hcp_language', 'hcp_social', 'hcp_gambling',
             'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm', ]

df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                 conditions=CONDITIONS, task_list=task_list)
conditions = df[df.modality == 'bold'].contrast.unique()
n_conditions = len(conditions)

n_conditions
path_train = []
path_test = []
subjects = ['sub-%02d' %
            i for i in [1, 2, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15]]
n_subjects = len(subjects)

for subject in subjects:
    ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions]
    path_train.append(ap_path)
    pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions]
    path_test.append(pa_path)


# mask_path = os.path.join(data_dir, 'resampled_gm_mask.nii.gz')
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)
for n_bootstrap in n_bootstraps:
    for subject_pair in subject_pair_list:
        start_time = time.time()
        subj_1, subj_2 = subject_pair[0], subject_pair[1]

        X_1_train = masker.transform(path_train[subj_1])
        X_2_train = masker.transform(path_train[subj_2])
        X_1_test = masker.transform(path_test[subj_1])
        X_2_test = masker.transform(path_test[subj_2])
        # maybe necessary to do for loop then np.vstack(X_train)
        print("Data masked in : %s seconds" %
              str(time.time() - start_time))
        start_time = time.time()
        print("Aligning pair : %s" % str(subject_pair))
        Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
            masker, X_1_train, X_2_train, X_1_test)
        for method in methods:
            if method == "optimal_transport":
                piecewise_estim = PieceWiseAlignmentOT(
                    n_pieces=n_pieces, method="epsilon_scaling", metric="euclidean", reg=reg, mask=masker, n_jobs=n_jobs, clustering_method=clustering_method, n_bootstrap=n_bootstrap)
            else:
                piecewise_estim = PieceWiseAlignment(
                    n_pieces=n_pieces, method=method, mask=masker, clustering_method=clustering_method, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
            results_title = "%s_%s_%s_hcp_ap_pa_%s_%s.nii.gz" % (str(subj_1), str(
                subj_2), str(reg), clustering_method, loss)
            piecewise_estim.fit(Im_1_train, Im_2_train)
            Im_1_pred = piecewise_estim.transform(Im_1_test)
            X_1_pred = masker.transform(Im_1_pred)
            method_header = "%s_bootstraps_piecewise_%s" % (str(
                n_bootstrap), method)
            score_save_table(
                masker, path_to_results, method_header, results_title, X_2_test, X_1_pred)
