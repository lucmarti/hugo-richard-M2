import nibabel as nib
from nilearn.input_data import NiftiMasker
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from sklearn.linear_model import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
import os
import logging

logging.basicConfig(level=logging.DEBUG, filename="logfile_piecewise", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

logging.info("Loading Data ...")
dataset = "forrest"
subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
runs = ["task001_run001",
        "task001_run002"]
n_jobs=1

mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
mask_path = mask_dir + "bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"
mask_img = nib.load(mask_path)
masker = NiftiMasker(
    mask_img=mask_path,
    standardize=True,
    detrend=True,
    memory="/storage/workspace/hrichard/cache_forrest/"
).fit()

logging.info("Masker loaded")
logging.debug(masker.affine_)

X = []
for run in runs:
    X_run = []
    for subject in subjects:
        func_filename = ("/storage/data/openfmri/ds113/sub" +
                         "%03d" +
                         "/BOLD/" +
                         run +
                         "/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"
                         ) % subject
        sample = masker.transform(func_filename).T
        logging.info("run: %s, subject: %s" % (run, str(subject)))
        logging.debug(sample.shape)
        logging.debug(nib.load(func_filename).affine)
        X_run.append(sample)
    X.append(X_run)
logging.info("Done")

X_train = X[0]
X_test = X[1]

rs = ShuffleSplit(n_splits=10, test_size=.1)
rs.split(X_train)

# For resilient clustering
if os.path.isfile("/storage/workspace/hrichard/results/exp7/label_array.nii.npy"):
    labels = np.load("/storage/workspace/hrichard/results/exp7/label_array.nii.npy")
else:
    labels = create_labels(X_train[0], mask_img.get_data(), n_pieces=1000)
    np.save("/storage/workspace/hrichard/results/exp7/label_array.nii", labels)
    label_img = masker.inverse_transform(labels)
    nib.save(
        label_img,
        "/storage/workspace/hrichard/results/exp7/label_piecewise_alignment.nii"
    )


def update(algorithm, name=None):
    def func(X_train1, Y_train1, X_test):
            logging.info(X_train1.shape)
            pwa = PieceWiseAlignment(labels, method = algorithm, n_jobs=n_jobs).fit(X_train1, Y_train1)
            return pwa.transform(X_test)

    if name is None:
        name = str(algorithm)

    return func, name

# algorithms = [update("mean"),
#               update("hyperalignment"),
#               update(RRR(k=25, alpha=0.003), name="RRR"),
#               update(RidgeCV(alphas = tuple(i * 100. for i in range(10, 30))), name="ridgeCV")
#               ]

algorithms = [update("hyperalignment")]

mean_exp_var = {}
for algorithm, name in algorithms:
    mean_exp_var[name] = []

for train_index, test_index in rs.split(X_train):
    logging.info("TRAIN:" +
                 train_index.__str__() +
                 "TEST:" +
                 test_index.__str__())

    for algorithm, name in algorithms:
        logging.info(name)

        Y = []
        for i in train_index:
            logging.info("Train: " + str(i))
            Y_predict = algorithm(X_train[i],
                          X_train[test_index[0]],
                          X_test[i]
                          )
            Y.append(Y_predict)
            logging.info((1 - (X_test[test_index[0]] - Y_predict).var(axis=1)).mean())
        Y = np.array(Y).mean(axis=0)
        var_e = (X_test[test_index[0]] - Y).var(axis=1)
        logging.info((1 - var_e).mean())
        exp_var = 1 - var_e
        mean_exp_var[name].append(exp_var)

for algorithm, name in algorithms:
    mean_exp_var[name] = np.array(mean_exp_var[name]).mean(axis=0)
    img = masker.inverse_transform(mean_exp_var[name])
    nib.save(
        img,
        "/storage/workspace/hrichard/results/exp7/" +
        dataset +
        "piecewise_alignment" +
        "_algo" +
        name +
        "_exp7.nii"
    )
logging.info("Done")
