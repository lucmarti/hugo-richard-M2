import os
import numpy as np
from nilearn.input_data import NiftiMasker
from nilearn.image import load_img
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.template import TemplateAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT

methods = ["optimal_transport", "RidgeCV", "scaled_orthogonal"]
n_jobs = 4
path_to_results = '/storage/tompouce/tbazeill/ibc/template_t_test'

data_dir = os.path.join(THREE_MM, 'group')
dataset = 'ibc'
template_method = "mean"
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)
mask = load_img(mask_path)

# Access to the data

subject_list = np.asarray(SUBJECTS)
task_list = ['archi_standard', 'archi_spatial', 'archi_social',
             'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
             'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
df = data_parser(derivatives=DERIVATIVES, subject_list=SUBJECTS,
                 conditions=CONDITIONS, task_list=task_list)
conditions = df[df.modality == 'bold'].contrast.unique()
n_conditions = len(conditions)

good_conditions = [7, 12,
                   27, 34, 44, 48, 49, 51, 52]
["video_sentence", "rotation_side", 'expression_gender', 'reward',
    'match', '2back_face', '0back_tools', '0back_place', '2back_place']
conditions[good_conditions]
conditions

from ibc_public.utils_contrasts import make_contrasts

a = make_contrasts("hcp_gambling")
a
["hcp_gambling", "archi_emotional", "archi_social",
    "hcp_wm", "hcp_social", "hcp_relational", "hcp_emotion"]
speech_sound - non - speech
false_belief_video - video_
df

template_paths = []
gt_folds = []
for subject in subject_list:

    ap_path = ([df[df.acquisition == 'ap'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions])
    template_paths.append(ap_path)

    pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
        df.contrast == condition].path.values[-1] for condition in conditions]
    gt_folds.append(pa_path)

template_train_imgs, gt_test_imgs = template_paths, gt_folds

for method in methods:
    template_estim = TemplateAlignment(
        n_pieces=150, alignment_method=method, n_bags=1, mask=masker, n_jobs=n_jobs, reg=1)
    scale_template = True
    if method == "optimal_transport":
        scale_template = False
    template_estim.fit(template_train_imgs,
                       template_method=template_method, scale_template=scale_template, n_iter=2)
    template = template_estim.template

    path_template = os.path.join(
        path_to_results, "%s_%s_template_for_t_test.nii.gz" % (template_method, method))
    template.to_filename(path_template)

    for subject, subject_ap, subject_pa in zip(subject_list, template_train_imgs, gt_folds):
        if method == "optimal_transport":
            piecewise_estim = PieceWiseAlignmentOT(
                n_pieces=150, method="epsilon_scaling", metric="euclidean", reg=0.7, mask=masker, n_jobs=n_jobs, n_bootstrap=1)
        else:
            piecewise_estim = PieceWiseAlignment(
                n_pieces=150, method=method, mask=masker, n_jobs=n_jobs, n_bootstrap=1)
        piecewise_estim.fit(subject_ap, path_template)
        subject_aligned_pa = piecewise_estim.transform(subject_pa)
        pred_path = os.path.join(path_to_results, "%s_aligned_%s_%s_template.nii.gz" % (
            subject, template_method, method))
        subject_aligned_pa.to_filename(pred_path)
