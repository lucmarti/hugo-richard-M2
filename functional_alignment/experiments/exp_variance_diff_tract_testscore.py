from srm import SRM
from load_data import get_mask, load_data
import nibabel as nib
from nilearn.input_data import NiftiMasker
import logging
from logging.handlers import RotatingFileHandler

logger = logging.getLogger()
logger.setLevel(logging.DEBUG)
formatter = logging.Formatter('%(asctime)s :: %(levelname)s :: %(message)s')
file_handler = RotatingFileHandler('logs.log', 'a', 1000000, 1)
file_handler.setLevel(logging.DEBUG)
file_handler.setFormatter(formatter)
logger.addHandler(file_handler)

params = ["pca", "ortho", "dict"]

logger.info("Loading Data: Forrest")
dataset = "forrest"
subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11]
runs = ["task001_run001",
        "task001_run002"]

mask_path = "/storage/workspace/hrichard/results/exp5/mask_forrest_roi.nii.gz"
masker = NiftiMasker(
    mask_img=mask_path,
    standardize=True,
    detrend=True,
    memory="/storage/workspace/hrichard/cache_forrest/"
).fit()

logger.info("Masker loaded")
logger.debug(masker.affine_)

X = []
names = []
for run in runs:
    X_run = []
    names_run = []
    for subject in subjects:
        func_filename = ("/storage/data/openfmri/ds113/sub" +
                         "%03d" +
                         "/BOLD/" +
                         run +
                         "/bold_dico_bold7Tp1_to_subjbold7Tp1.nii.gz"
                         ) % subject
        sample = masker.transform(func_filename).T
        logger.debug("run: %s, subject: %s"%(run, str(subject)))
        logger.debug(sample.shape)
        logger.debug(nib.load(func_filename).affine)
        X_run.append(sample)
        names_run.append("run: %s, subject: %s"%(run, str(subject)))
    X.append(X_run)
    names.append(names_run)
logger.info("Done")

X_train = X[0]
names_train = names[0]

X_test = X[1]
names_test = names[1]

logger.info("Done")

for param in params:
    n_components = 20
    srm = SRM(n_components, max_iter=10, algo_name=param)
    logger.info("Fitting data")
    srm.fit(X_train)
    logger.info("Done")
    n_half = len(X_test) // 2 + len(X_test) % 2
    logger.info("Transforming Data")
    shared_response = srm.transform(X_test[0:n_half])
    logger.info("Done")
    logger.info("Reconstruction")
    reconstruct_X = srm.inverse_transform(shared_response)
    logger.info("Testing Data")
    logger.info("Compute explained variance")
    #We use the next 10 subjects to compute the explained variance
    for i in range(n_half, len(X_test)):
        var_e = (X_test[i] - reconstruct_X[i]).var(axis=1)
        exp_var = 1 - var_e
        img = masker.inverse_transform(exp_var)
        nib.save(
            img,
            "/storage/workspace/hrichard/results/exp3/" +
            dataset + "_" +
            names_test[i] + "_" +
            "algo" + param + "_" +
            "test_score.nii"
        )
    logger.info("Done")
