#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Wed Apr 18 16:25:52 2018

@author: thomasbazeille
"""
from sklearn.metrics import r2_score
from nilearn import datasets, image
from functional_alignment.deterministic_srm import mask_and_reduce, DeterministicSRM
from exp_haxby_subject_pairs_alignment import make_train_test_sessions, compute_union_masker, split_train_test, make_train_test_images, score_image
from functional_alignment.hyperalignment import hyperalign

n_subjects = 2
n_components = 20
subj_1 = 0
subj_2 = 1
haxby_dataset = datasets.fetch_haxby(subjects=2)
n_jobs = 2


train_session, test_session = make_train_test_sessions(
    n_subjects, haxby_dataset)
masker = compute_union_masker([subj_1, subj_2], haxby_dataset)

X_1_train, X_1_test = split_train_test(
    masker, subj_1, train_session, test_session, haxby_dataset)
X_2_train, X_2_test = split_train_test(
    masker, subj_2, train_session, test_session, haxby_dataset)

#X_train = np.concatenate([X_1_train, X_2_train], axis=0)

Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
    masker, X_1_train, X_2_train, X_1_test)
# either Im_train = image.concat_imgs([Im_1_train, Im_2_train])
# and index_transform : range(len(X_1_train))
# and index_inverse = range(len(X_1_train),len(X_1_train)+len(X_2_train))

# or Im_train = [Im_1_train, Im_2_train] as here
# and index_transform : 0/1
# and index_inverse =  0/1

estim = DeterministicSRM(n_components=20, max_iter=10,
                         detrend=True, mask=masker, n_jobs=n_jobs)
estim.fit([Im_1_train, Im_2_train])
shared_response = estim.transform(Im_1_test, index=[0])
Im_1_pred = estim.inverse_transform(shared_response, index=[1])
print(r2_score(X_2_test, X_1_test))
print score_image(masker, Im_1_pred, X_2_test)
