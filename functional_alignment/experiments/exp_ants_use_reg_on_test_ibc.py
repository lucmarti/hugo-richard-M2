import os
import time
import numpy as np
from nilearn.input_data import NiftiMasker
import pandas as pd
from nilearn.image import iter_img, concat_imgs, load_img
from functional_alignment.utils import score_table
from ants import image_read, registration, write_transform, read_transform, list_to_ndimage, ndimage_to_list, apply_transforms
from nilearn.image import index_img, load_img, math_img
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public


def find_transforms_path(data_path, pairs_to_load, methods, grad_steps):
    df = pd.DataFrame(columns=["source_subject", "target_subject", "method",
                               "grad_step", "transformlist"])
    for pair in pairs_to_load:
        source_sub, target_sub = pair[0], pair[1]
        for method in methods:
            for grad_step in grad_steps:
                name = os.path.join(data_path, "%s_%s_reg_%s_grad_%s" % (
                    SUBJECTS[source_sub], SUBJECTS[target_sub], method, str(grad_step)))
                transformlist = [name + ".nii.gz", name + ".mat"]
                df = df.append({"source_subject": source_sub, "target_subject": target_sub, "method": method,
                                "grad_step": str(grad_step), "transformlist": transformlist}, ignore_index=True)
    return df


def load_ibc_pa_data():
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                 'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
                 'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                     conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    image_list = []
    for subject in subject_list:
        subject_path = []
        subject_path.extend([df[df.acquisition == 'pa'][df.subject == subject][
            df.contrast == condition].path.values[-1] for condition in conditions])
        image_list.append(subject_path)
    return image_list


def load_ibc_3_mm_data(path="/storage/tompouce/bthirion/maps_3mm.csv"):

    mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"

    ibc = pd.DataFrame.from_csv(
        path, sep=",", index_col=0)
    image_list = []
    for subj_i in range(1, 16):
        subj_data = ibc.loc[ibc['subject'] ==
                            "sub-" + "{0:0=2d}".format(subj_i)]
        filtered_data = subj_data.drop_duplicates(
            subset="contrast", keep='last')
        if not filtered_data.empty:
            image_list.append(filtered_data)
    return image_list, mask_path


def apply_transform(data_path, source_sub, target_sub, transformlist, method, grad_step):

    image_list = load_ibc_pa_data()
    source_image_list = image_list[source_sub - 1]

    source_test = image_read(os.path.join(
        data_path, '%s_test.nii.gz' % (target_sub)))
    target_train = image_read(os.path.join(
        data_path, '%s_pca.nii.gz' % (SUBJECTS[target_sub])))
    transformed_image = []

    for im_source in source_image_list:
        transformed_image.append(apply_transforms(
            target_train, image_read(im_source), transformlist))
    image_merged = list_to_ndimage(source_test, transformed_image)
    image_merged.to_file(os.path.join(
        data_path, "%s_%s_warped_test_%s_%s.nii.gz" % (str(source_sub), (str(target_sub)), method, str(grad_step))))


def extrapolate_image(img, smoothing_fwhm):
    from nilearn.image import math_img, smooth_img, new_img_like, load_img
    from nilearn.masking import compute_background_mask
    # Compute mask
    mask = compute_background_mask(img)
    im_eps = new_img_like(mask, np.zeros(mask.get_data().shape))
    im_eps.get_data()[mask.get_data() == 0] = 1e-15

    # extrapolate image dividing smoothed image by smoothed mask. Eps helps avoiding numerical problems in division
    extrapolated_img = math_img(
        'img1 / (img2 + eps)', img1=smooth_img(img, smoothing_fwhm), img2=smooth_img(mask, smoothing_fwhm), eps=im_eps)
    # Replace, in the smoothed_image, voxels that were defined in the original image by their original values
    extrapolated_img.get_data()[mask.get_data() > 0] = load_img(
        img).get_data()[mask.get_data() > 0]
    return extrapolated_img


def extrapolate_4D_image(mask_path, img_4D, smoothing_fwhm):
    list_pred = []
    for im in iter_img(img_4D):
        im_ex = extrapolate_image(im, smoothing_fwhm)
        mask = load_img(mask_path)
        im_ex.get_data()[mask.get_data() == 0] = 0
        list_pred.append(im_ex)
    im_pred = concat_imgs(list_pred)
    return im_pred


def score_pred(data_path, pair, mask_path, Im_1_pred, gt_path, method, grad_step):
    source_sub, target_sub = pair[0], pair[1]
    loss = "zero_mean_r2"
    masker = NiftiMasker(mask_path)
    masker.fit()
    X_2_test = masker.transform(gt_path)
    X_1_pred = masker.transform(Im_1_pred)

    multioutput = 'raw_values'

    X_score = score_table(loss, X_2_test, X_1_pred, multioutput)
    masker.inverse_transform(X_score)
    Im_score = masker.inverse_transform(X_score)
    Im_score.to_filename(os.path.join(
        data_path, "%s_%s_%s_%s_%s.nii.gz" % (str(source_sub), (str(target_sub)), loss, method, str(grad_step))))


path_to_results = "/storage/tompouce/tbazeill/ibc/antspy_ap_pa/"
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
im_mask = image_read(mask_path)
pairs_to_align = [(12, 8), (2, 5), (2, 8), (3, 11),
                  (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
methods = ["SyNBold"]
grad_steps = [0.5]
smooth_flow = 2

df = find_transforms_path(path_to_results, pairs_to_align, methods, grad_steps)

for pair in pairs_to_align:
    for method in methods:
        for grad_step in grad_steps:
            source_sub, target_sub = pair[0], pair[1]
            transformlist = df.loc[(df['source_subject'] == source_sub) & (df['target_subject'] == target_sub) & (
                df['method'] == method) & (df['grad_step'] == str(grad_step))].transformlist.item()
            apply_transform(path_to_results, source_sub, target_sub,
                            transformlist, method, grad_step)

            imlist_to_extrapolate = os.path.join(
                path_to_results, "%s_%s_warped_test_%s_%s.nii.gz" % (str(source_sub), (str(target_sub)), method, str(grad_step)))

            im_pred = extrapolate_4D_image(mask_path, imlist_to_extrapolate, 5)

            im_pred.to_filename(os.path.join(
                path_to_results, "%s_%s_pred_%s_%s.nii.gz" % (str(source_sub), (str(target_sub)), method, str(grad_step))))

            gt_path = os.path.join(
                path_to_results, "%s_test.nii.gz" % (str(target_sub)))

            score_pred(path_to_results, pair, mask_path,
                       im_pred, gt_path, method, grad_step)
