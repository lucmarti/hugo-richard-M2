#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import os
import time

import numpy as np

from nilearn.input_data import NiftiMasker
from nilearn.image import index_img, load_img, math_img
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
from sklearn.model_selection import ShuffleSplit

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from functional_alignment.utils import score_save_table, make_train_test_images
from functional_alignment.load_data import load_sherlock_data, load_forrest_data, load_ibc_3_mm_data, make_train_test_sherlock, make_train_test_forrest, make_train_test_ibc_3_mm
from functional_alignment.template import TemplateAlignment, euclidian_mean_with_masking
from functional_alignment.utils import score_table
from functional_alignment.ot_template_alignment import OptimalTransportTemplate


data_dir = os.path.join(THREE_MM, 'group')
dataset = 'ibc'
n_bootstraps = [20]
regs = [10, 5, 1, .5, .1, 1e-3]
n_jobs = 4
scale_template = False
method = "optimal_transport"
template_method = "mean"
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)
path_to_results = "/storage/tompouce/tbazeill/ibc/template_alignment"
mask = load_img(mask_path)

# Access to the data
if dataset == 'ibc':
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                 'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
                 'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                     conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    rs = ShuffleSplit(n_splits=5, test_size=.4, random_state=0)

    template_folds, mapping_folds, gt_folds = [], [], []

    for train_subjects, test_subjects in rs.split(subject_list):
        template_paths = []
        for subject in subject_list[train_subjects]:
            subject_path = []
            subject_path.extend([df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            subject_path.extend([df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            template_paths.append(subject_path)
        template_folds.append(template_paths)

        test_mapping = []
        test_gt = []
        for subject in subject_list[test_subjects]:
            ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_mapping.append(ap_path)
            pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_gt.append(pa_path)
        mapping_folds.append(test_mapping)
        gt_folds.append(test_gt)

# for template_train_imgs, mapping_test_imgs, gt_test_imgs in zip(template_folds, mapping_folds, gt_folds):

cv = 0
train_index = np.asarray(range(53))
test_index = np.asarray(range(53, 106))
template_train_imgs, mapping_test_imgs, gt_test_imgs = template_folds[
    cv], mapping_folds[cv], gt_folds[cv]

X_gts = []
for i in range(len(gt_test_imgs)):
    X_gt = masker.transform(gt_test_imgs[i])
    X_gts.append(X_gt)

'''
a = []
for i in [5, 6, 3, 1, 4, 0, 2]:
    a.append(template_train_imgs[i])
template_train_imgs = a
'''

for n_bags in n_bootstraps:
    for reg in regs:
        if template_method == "optimal":
            template_estim = OptimalTransportTemplate(
                n_pieces=200, metric="euclidean", reg=reg, n_bootstrap=n_bags, joint_clustering=True, mask=masker, n_jobs=n_jobs)
            template_estim.fit(template_train_imgs, n_iter=4)
        else:
            template_estim = TemplateAlignment(
                n_pieces=200, alignment_method=method, n_bags=n_bags, mask=masker, n_jobs=n_jobs, reg=reg)
            template_estim.fit(template_train_imgs,
                               template_method=template_method, scale_template=scale_template, n_iter=4)
        template_estim.template.to_filename(os.path.join(
            path_to_results, "template_%s_%s_bootstraps_epsilon_scaling_%s_%s_eu_cv_%s.nii.gz" % (template_method, str(n_bags), method, str(reg), str(cv))))
        pred_imgs = template_estim.transform(
            mapping_test_imgs, train_index, test_index)
        for step, template_step in enumerate(template_estim.template_history):
            template_step.to_filename(os.path.join(
                path_to_results, "template_step_%s_%s_%s_bootstraps_epsilon_scaling_%s_%s_eu_cv_%s.nii.gz" % (str(step), template_method, str(n_bags), method, str(reg), str(cv))))
        for i in range(len(gt_test_imgs)):
            X_gt = X_gts[i]
            X_pred = masker.transform(pred_imgs[i])
            for j, loss in enumerate(["reconstruction_err", "zero_mean_r2"]):
                loss_score = score_table(loss, X_gt, X_pred, 'raw_values')
                pred_im = masker.inverse_transform(loss_score)
                pred_im.to_filename(os.path.join(path_to_results, "%s_pred_%s_%s_bootstraps_epsilon_scaling_%s_%s_eu_cv_%s_%s.nii.gz" % (
                    subject_list[test_subjects][i], template_method, str(n_bags), method, str(reg), str(cv), loss)))
