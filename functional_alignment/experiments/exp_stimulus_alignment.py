import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from sklearn.linear_model import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
from ridge_hyperalignment import RidgeHyperalignment
import os
import logging
from load_data import load_ibc
import pandas as pd
import nibabel
from nilearn.input_data import NiftiMasker
from scipy.stats.stats import pearsonr
from utils import generate_train_test

n_jobs = 10

#IBC_config

logging.basicConfig(level=logging.DEBUG, filename="logfile_stimulus_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

mask_dir = "/storage/store/data/ibc/derivatives/"
data_dir = "/storage/store/data/ibc/derivatives/"

subjects = [ 1,  2,  4,  5,  6,  7,  8,  9, 11, 12, 13, 14]

X, masker, mask_img, dataset = load_ibc(
    dataset="ibc",
    subjects=subjects,
    runs=[1, 2, 3],
    mask_path=mask_dir + "group/gm_mask.nii.gz",
    data_path=data_dir + "sub-%02d/ses*/func/wrdcsub*Trn%02d*.nii.gz"
)
label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"


X_train = [np.concatenate([X[0][i], X[1][i], X[2][i]], axis=1)
           for i in range(len(X[0]))]

# load experiments data

db = pd.read_csv("/home/parietal/hrichard/hugo-richard-M2/data/database.csv")
names_exp = []

X_test = []
for subject in subjects:
    db_subject = db[db["subject"] == subject]
    names_exp.append(
        db_subject["stim"].values +
        "__" +
        db_subject["task"].values
    )
    for path in db_subject["path"].values:
        print(path)
        img = nibabel.load(path)
        print("shape", img.shape)

    masker_3d = NiftiMasker(
        mask_img=mask_img,
        memory="/storage/workspace/hrichard/cache_ibc2/",
        ).fit()

    X_test_stim_list = []
    for path in db_subject["path"].values:
        X_test_stim = masker_3d.transform(path).T
        if len(X_test_stim.shape) == 1:
            X_test_stim = X_test_stim.reshape((X_test_stim.shape[0], 1))
        X_test_stim_list.append(X_test_stim)
    X_test.append(
        X_test_stim_list
    )

nb_stim = len(X_test[0])

print(nb_stim)

# For resilient clustering
if os.path.isfile(label_path_files):
    labels = np.load(label_path_files)
else:
    labels = create_labels(X[0][0], mask_img.get_data(), n_pieces=1000)
    np.save(label_path_save, labels)
    label_img = masker.inverse_transform(labels)
    nib.save(
        label_img,
        label_path_img
    )


def update(algorithm, name=None):

    def fit_(X_train, Y_train):
        if isinstance(algorithm, RidgeHyperalignment):
            algorithm.fit(X_train.T, Y_train.T)
        else:
            algorithm.fit(X_train, Y_train)

    def transform_(X_test):
        if isinstance(algorithm, RidgeHyperalignment):
            res = algorithm.transform(X_test.T).T
            return res
        else:
            res = algorithm.transform(X_test)
            return res

    if name is None:
        name = str(algorithm)

    return fit_, transform_, name

algorithms = [
    update(
        PieceWiseAlignment(labels, method="hyperalignment", n_jobs=n_jobs),
        name="hyperalignment"
    ),
    update(
        PieceWiseAlignment(labels, method="mean", n_jobs=n_jobs),
        name="mean"
    ),
    update(
        RidgeHyperalignment(alpha=1e6),
        name="RH_1e6"
        ),
    update(
        RidgeHyperalignment(alpha=1e5),
        name="RH_1e5"
    ),
    update(
        PieceWiseAlignment(labels, method=RidgeCV(alphas=(1500., 2000., 2500.)), n_jobs=n_jobs),
        name="ridgeCV_1500_2000_2500"
    ),
    update(
        PieceWiseAlignment(labels, method=RRR(k=25, alpha=0.003), n_jobs=n_jobs),
           name="RRR_25_003"
    )
]

# mean_exp_var = [np.zeros_like(X_test[0][j][:, 0]) for j in range(nb_stim)]


for train_index, test_index in generate_train_test(subjects):
    print("TRAIN:" +
                 train_index.__str__() +
                 "TEST:" +
                 test_index.__str__())
    logging.info("TRAIN:" +
                 train_index.__str__() +
                 "TEST:" +
                 test_index.__str__())

    for fit, transform, name in algorithms:
        logging.info(name)
        print(name)
        Y_list = []
        for i in train_index:
            print("Train: " + str(i))
            fit(X_train[i], X_train[test_index[0]])
            Y_predict = []
            for j in range(nb_stim):
                Y_predict_j = transform(X_test[i][j])
                Y_predict.append(Y_predict_j)
                assert Y_predict_j.shape[1] == 1
                logging.info(pearsonr(Y_predict_j.flatten(), X_test[test_index[0]][j].flatten()))
            Y_list.append(Y_predict)

        for j in range(nb_stim):
            Y_j = np.array([Y_list[i][j] for i in range(len(train_index))]).mean(axis=0)
            assert Y_j.shape[1] == 1
            # dist_XY_2 = (X_test[test_index[0]][j] - Y_j)**2
            # normed_dist_XY_2 = (dist_XY_2 - dist_XY_2.min()) / (dist_XY_2.max() - dist_XY_2.min())
            # normed_dist_XY_2 = normed_dist_XY_2.flatten()
            # exp_var_j = 1 - normed_dist_XY_2
            # logging.info(exp_var_j.mean())
            # nib.save(
            #     masker.inverse_transform(1 - normed_dist_XY_2.T),
            #     "/storage/workspace/hrichard/results/exp11/" +
            #     dataset +
            #     "diff" +
            #     "_algo" +
            #     name +
            #     "subject" + str(test_index[0]) +
            #     "_stim_" + names_exp[0][j] +
            #     "_exp11.nii"
            # )
            nib.save(
                masker.inverse_transform(Y_j.flatten()),
                "/storage/workspace/hrichard/results/exp11/" +
                dataset +
                "predict" +
                "_algo" +
                name +
                "subject" + str(test_index[0]) +
                "_stim_" + names_exp[0][j] +
                "_exp11.nii"
            )
                # mean_exp_var[j] += exp_var_j/n_splits
        # for j in range(nb_stim):
        #     img = masker.inverse_transform(mean_exp_var[j])
        #     nib.save(
        #         img,
        #         "/storage/workspace/hrichard/results/exp11/" +
        #         dataset +
        #         "stim" +
        #         "_algo" +
        #         name +
        #         "_stim_" + names_exp[0][j] +
        #         "_exp11.nii"
        #     )

logging.info("Done")
