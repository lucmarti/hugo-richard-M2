import nibabel as nib
import numpy as np
import logging
import sys
sys.path.append("./")
from functional_alignment.load_data import load
from nilearn.input_data import MultiNiftiMasker
import os.path
from sklearn.externals.joblib import Parallel, delayed
import itertools
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.pca_srm import PCASRM
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.batch_isrm import BatchISRM
from functional_alignment.graphnet_srm import GraphNetSRM
from functional_alignment.isrm_encode import isrm_encode, structure_matrix, laplacian_matrix
import os
import nibabel as nib
from functional_alignment.online_isrm import OnlineISRM


def corr(X, Y):
    X_ = X - np.mean(X, axis=1, keepdims=True)
    Y_ = Y - np.mean(Y, axis=1, keepdims=True)
    X_ = X_ / (np.linalg.norm(X_, axis=1, keepdims=True) + 1e-12)
    Y_ = Y_ / (np.linalg.norm(Y_, axis=1, keepdims=True) + 1e-12)
    return np.sum(X_ * Y_, axis=1)


def perform_experiment(algorithm, name, mes, X_train, X_test, splits, dataset, k, mask_params):
    print(dataset)
    print(k)
    print(name)
    print(mes)
    algorithm.fit(X_train)
    mask = MultiNiftiMasker(**mask_params).fit()
    for train_index, test_index in splits:
        x_test = [X_test[i] for i in train_index]
        shared_response = algorithm.transform(x_test, index=train_index)
        Y = algorithm.inverse_transform(shared_response, index=test_index)[0]
        X_true = mask.transform(X_test[test_index[0]][0]).T
        if mes == "r2":
            var_e = (X_true - Y).var(axis=1)
            exp_var = 1 - var_e
        elif mes == "corr":
            exp_var = corr(X_true, Y)
        else:
            raise ValueError("how to measure error ?")
        result_directory = "/storage/workspace/hrichard/M2_internship/results/exp10_roi/k_%i/" % k
        if not os.path.exists(result_directory):
            os.makedirs(result_directory)
        nib.save(
            mask.inverse_transform(exp_var),
            result_directory +
            dataset +
            "alignment_srm" +
            "_algo" +
            name +
            "subject_" + str(test_index[0]) +
            "mes_" + mes +
            "_exp10.nii"
        )


def cosmoothing_exp(k, common_weight, l1_weight, grad_weight, positive, iter, mes, X_train, X_test, mask, splits,
                    dataset, exp_params):
    Lx, Ly, Lz = laplacian_matrix(mask.mask_img_.get_data().astype(bool))
    G = (Lx.T.dot(Lx) + Ly.T.dot(Ly) + Lz.T.dot(Lz))
    algorithms = [
        (
            BatchISRM(n_components=k, l1_weight=l1_weight,
                      grad_weight=grad_weight, common_weight=common_weight,
                      max_iter=iter, positive=positive, shared_init="DetSRM", random_state=0, structure_matrix=G,
                      **exp_params),
            "BatchISRM_la%.3f_g%.3f_c%.3f_positive_%i_initDetSRM_meandict_iter%.3f" % (
            l1_weight, grad_weight, common_weight, int(positive), iter)
        ),
    ]

    # algorithms = [
    #     (
    #         BatchISRM(n_components=k, l1_weight=l1_weight,
    #                     grad_weight=grad_weight, common_weight=common_weight,
    #                     max_iter=10, positive=positive, shared_init="DetSRM", **exp_params),
    #         "BatchISRM_la%.3f_g%.3f_c%.3f_positive_%i_initDetSRM" % (l1_weight, grad_weight, common_weight, int(positive))
    #     ),
    # ]

    for algorithm, name in algorithms:
        print(name)
        print("Fitting")
        algorithm.fit(X_train)
        for train_index, test_index in splits:
            result_directory = "/storage/workspace/hrichard/M2_internship/results/exp10_roi/k_%i/" % k
            if not os.path.exists(result_directory +
                                  dataset +
                                  "alignment_srm" +
                                  "_algo" +
                                  name +
                                  "subject_" + str(test_index[0]) +
                                  "mes_" + mes +
                                  "_exp10.nii"):
                x_test = [X_test[i] for i in train_index]
                shared_response = algorithm.transform(x_test, index=train_index)
                Y = algorithm.inverse_transform(shared_response, index=test_index)[0]
                X_true = mask.transform(X_test[test_index[0]][0]).T
                if mes == "r2":
                    var_e = (X_true - Y).var(axis=1)
                    exp_var = 1 - var_e
                elif mes == "corr":
                    exp_var = corr(X_true, Y)
                else:
                    raise ValueError("how to measure error ?")
                if not os.path.exists(result_directory):
                    os.makedirs(result_directory)
                nib.save(
                    mask.inverse_transform(exp_var),
                    result_directory +
                    dataset +
                    "alignment_srm" +
                    "_algo" +
                    name +
                    "subject_" + str(test_index[0]) +
                    "mes_" + mes +
                    "_exp10.nii"
                )


for config in ["forrest", "ibc"]:
    logging.basicConfig(level=logging.DEBUG, filename="logfile_alignment_compare_srm_" + config + "_roi_online", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.info("Start experiment with config %s" % config)

    if config == "forrest":

        data_dir = "/storage/data/openfmri/ds113/"

        masker = "/storage/workspace/hrichard/M2_internship/results/exp5/mask_forrest_roi2.nii.gz"
        mask_memory = "/storage/tompouce/hrichard/forrest_roi/forrest_cache_roi/"

        subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        runs = [1, 2, 3, 4, 5, 6, 7]

        X, dataset = load(
            dataset="forrest_roi",
            subjects=subjects,
            runs=runs,
            data_path=data_dir + "sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz",
        )

        exp_params = {
            "detrend": True,
            "standardize": True,
            "memory": mask_memory,
            "mask": masker,
            "mask_strategy": "epi",
            "n_jobs": 1,
            "memory_level": 5,
            "t_r": 2,
            "verbose": 1
        }
    elif config == "ibc":
        data_dir = "/storage/store/data/ibc/derivatives/"
        data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

        subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
        runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05", "Trn06", "Trn07", "Trn08", "Trn09",
                "Val01", "Val02", "Val03", "Val04", "Val05", "Val06", "Val07", "Val08"]

        X, dataset = load(
            dataset="ibc",
            subjects=subjects,
            runs=runs,
            data_path=data_path,
        )

        masker = "/storage/workspace/hrichard/M2_internship/results/exp13/mask_ibc_roi3.nii.gz"
        mask_memory = "/storage/tompouce/hrichard/ibc_roi/ibc_cache_roi"

        exp_params = {
            "detrend": True,
            "standardize": True,
            "memory": mask_memory,
            "mask": masker,
            "mask_strategy": "epi",
            "n_jobs": 5,
            "memory_level": 5,
            "low_pass": 0.1,
            "high_pass": 0.01,
            "t_r": 2,
            "verbose": 1
        }
    else:
        ValueError("No valid configuration was specified")

    mask_params = exp_params.copy()
    mask_params["mask_img"] = mask_params["mask"]
    del mask_params["mask"]

    splits = []
    for i in range(len(subjects)):
        split_train = []
        split_test = [i]
        for j in range(len(subjects)):
            if j != i:
                split_train.append(j)
        split_test = np.array(split_test)
        split_train = np.array(split_train)
        splits.append((split_train, split_test))

    for i in range(len(splits)):
        logging.debug(splits[i])

    X_train = []
    X_test = []
    for i in range(len(X)):
        logging.debug("Masking Data for subject %i / %i "%(i + 1, len(X)))
        Xi_train = []
        for j in range(len(X[i])):
            X_ij = X[i][j]
            if j == len(X[i]) - 1:
                Xi_test = [X_ij]
            else:
                Xi_train.append(X_ij)
        X_train.append(Xi_train)
        X_test.append(Xi_test)


    Parallel(n_jobs=10)(
        delayed(cosmoothing_exp)(
            k, common_weight, l1_weight, grad_weight, positive, iter, mes, X_train, X_test, MultiNiftiMasker(**mask_params).fit(), splits, dataset, exp_params
        )
            for k in [5, 10, 20, 50, 100]
            for l1_weight in [0., 1., 10., 100., 200.]
            for grad_weight in [0., 1., 10., 100., 1000., 10000.]
            for iter in [0]
            for common_weight in [0.]
            for positive in [False, True]
            for mes in ["corr", "r2"]
    )

    # Parallel(n_jobs=1)(
    #     delayed(perform_experiment)(
    #         algorithm, name, mes, X_train, X_test, splits, dataset, k, mask_params
    #     )
    #     for k in [5, 10, 20, 50, 100]
    #     for algorithm, name in [(PCASRM(n_components=k, **exp_params), "PCA"),
    #                             (ProbabilisticSRM(n_components=k, **exp_params), "ProbSRM"),
    #                             (DeterministicSRM(n_components=k, **exp_params), "FastDetSRM")
    #                             ]
    #     for mes in ["corr", "r2"]
    # )
