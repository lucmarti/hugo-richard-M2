#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import sys
import itertools
import pandas as pd
import csv

from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.masking import intersect_masks
from nilearn.image import index_img
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign, Hyperalignment
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.deterministic_srm import DeterministicSRM


def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=2):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = path + 'preloaded_masker/mask_all_subj_movie.nii'

    return image_list, movie_labels, masker_path


# Define train/test sessions from labels
def make_train_test_sessions_haxby(n_subjects, haxby_dataset):
    train_session = []
    test_session = []
    for i in range(n_subjects):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks = labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1

        train_labels = labels[condition_mask & train_chunks].sort_values(by=[
                                                                         'chunks', 'labels'])
        test_labels = labels[condition_mask & test_chunks].sort_values(
            by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)
    return train_session, test_session


def make_train_test_sherlock(image_list, labels, masker):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Extract scenes
        Parameters :
        - List of int fold out of 50 scenes
        - Dataframe labels (scene, onset, offset)
        - List of nii files subject_imgs

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1
    # to have the same number of images in train and test_sessions
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.fit_transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.fit_transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list


def fetch_vt_mask_haxby(subj, haxby_dataset):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def compute_union_vt_masker_haxby(subjs, haxby_dataset):
    mask_list = []
    for subj in subjs:
        mask_list.append(haxby_dataset.mask_vt[subj])
    union_mask = intersect_masks(mask_list, threshold=0, connected=False)
    masker = NiftiMasker(union_mask, smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def split_train_test(masker, subj, train_session, test_session, haxby_dataset):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def try_methods(masker, X_1_train, X_1_test, X_2_train, X_2_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, method_piecewise="mean", n_jobs=1, n_bootstraps=[1]):
    methods_result = []
    # Hyperalignment   !!! for now we have to transpose X_train and X_test
    R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
    methods_result.append(score_table(
        sc * R.dot((X_1_test.T)), X_2_test.T))

    # Ridge Alignment with Cross-Validation
    ridge_estim = RidgeCV(alphas=alphas_ridge,
                          n_splits=n_cv_splits)
    ridge_estim.fit(X_1_train, X_2_train)
    X_1_pred = ridge_estim.transform(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))

    # Turning train / test set to images through unmasking
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    # PieceWise Alignment
    if method_piecewise == "all":

        for method in ["scaled_orthogonal", "RidgeCV"]:
            for n_bootstrap in n_bootstraps:
                piecewise_estim = PieceWiseAlignment(
                    n_pieces=n_pieces_piecewise, method=method, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                piecewise_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_estim.transform(Im_1_test)
                methods_result.append(score_image(masker, Im_1_pred, X_2_test))
    else:
        piecewise_estim = PieceWiseAlignment(
            n_pieces=n_pieces_piecewise, method=method_piecewise, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
        piecewise_estim.fit(Im_1_train, Im_2_train)
        Im_1_pred = piecewise_estim.transform(Im_1_test)
        methods_result.append(score_image(masker, Im_1_pred, X_2_test))

    # Deterministic SRM
    '''srm_estim = DeterministicSRM(
        n_components=20, max_iter=10, detrend=True, mask=masker, n_jobs=n_jobs)
    srm_estim.fit([Im_1_train, Im_2_train])
    shared_response = srm_estim.transform(Im_1_test, index=[0])
    Im_1_pred = srm_estim.inverse_transform(shared_response, index=[1])
    methods_result.append(score_image(masker, Im_1_pred[0], X_2_test))'''
    return methods_result


def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.fit_transform(Im_1_pred)
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


if __name__ == '__main__':

    n_jobs = 2
    if len(sys.argv) > 1:
        n_jobs = int(sys.argv[1])

    dataset = "haxby"
    if len(sys.argv) > 2:
        dataset = sys.argv[2]
    n_subjects = 4  # number of subjects

    # Methods Parameters
    n_splits = 4  # n_subjectsumber of folds for validations
    alphas_ridge = (0.1, 10, 500, 2000, 10000)  # Ridge-CV params
    ks_rrr = [10, 20, 50]  # RRR-CV params
    alphas_rrr = [0.1, 10, 1000]  # RRR-CV params
    n_pieces = 200  # number of pieces for piecewise alignement
    method = "all"  # method for piecewise alignement
    n_bootstraps = [1, 20]

    if dataset == "sherlock":

        image_list, movie_labels, masker_path = load_sherlock_data(
            n_subjects=n_subjects)

        masker = NiftiMasker(smoothing_fwhm=0)
        masker.fit(masker_path)

        X_train_list, X_test_list = make_train_test_sherlock(
            image_list, movie_labels, masker)

    elif dataset == "haxby":
        haxby_dataset = datasets.fetch_haxby(subjects=n_subjects)
        train_session, test_session = make_train_test_sessions_haxby(
            n_subjects, haxby_dataset)
        masker = NiftiMasker(haxby_dataset.mask, detrend=True)
        # Main loop computing X_train and X_test and testing all methods for each pair of subjects #
        X_train_list, X_test_list = [], []
        for subject in range(n_subjects):
            X_train_i, X_test_i = split_train_test(
                masker, subject, train_session, test_session, haxby_dataset)
            X_train_list.append(X_train_i)
            X_test_list.append(X_test_i)
            #####

            # fetching data

            # preparing the results
    results = []
    if method == "all":
        header = ["identity", "hyperalignment", "ridgecv",
                  "piecewise scaled_orthogonal n_bootstrap = 1", "piecewise scaled_orthogonal n_bootstrap = 20", "piecewise RidgeCV = 1", "piecewise RidgeCV = 20"]  # , "SRM"]
    else:
        header = ["subject pair", "identity", "hyperalignment",
                  "ridgecv", "piecewise_alignment: " + method]  # , "SRM"]
# %%
    # for each pair of subject (symmetrically)
    for i, subject_pair in enumerate(list(itertools.permutations(range(n_subjects), 2))):
        pair_result = []
        subj_1, subj_2 = subject_pair[0], subject_pair[1]
        # pair_result.append(subject_pair)

        X_1_train, X_1_test = X_train_list[subj_1], X_test_list[subj_1]
        X_2_train, X_2_test = X_train_list[subj_2], X_test_list[subj_2]

        baseline = r2_score(X_2_test, X_1_test)  # r_score betwenn raw images
        # a = debug_Hyper(X_1_train, X_2_train)

        pair_result.append(baseline)
        # try every method in the library
        methods_result = try_methods(
            masker, X_1_train, X_1_test, X_2_train, X_2_test, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, method, n_jobs, n_bootstraps)
        pair_result.extend(methods_result)
        results.append(pair_result)

    results_data = pd.DataFrame.from_records(results, columns=header)
    results_title = "results_sherlock_fullbrain_example_pair_alignment.csv"
    results_data.to_csv(results_title)

    print("synthetic result are in" + results_title + "'")
