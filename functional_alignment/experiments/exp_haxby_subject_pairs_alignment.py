#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import itertools
import pandas as pd
import csv

from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.masking import intersect_masks
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign, Hyperalignment
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.deterministic_srm import DeterministicSRM


# Define train/test sessions from labels
def make_train_test_sessions(N, haxby_dataset):
    train_session = []
    test_session = []
    for i in range(N):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks = labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1

        train_labels = labels[condition_mask & train_chunks].sort_values(by=[
                                                                         'chunks', 'labels'])
        test_labels = labels[condition_mask & test_chunks].sort_values(
            by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)
    return train_session, test_session


def fetch_mask(subj, haxby_dataset):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def compute_union_masker(subjs, haxby_dataset):
    mask_list = []
    for subj in subjs:
        mask_list.append(haxby_dataset.mask_vt[subj])
    union_mask = intersect_masks(mask_list, threshold=0, connected=False)
    masker = NiftiMasker(union_mask, smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def split_train_test(masker, subj, train_session, test_session, haxby_dataset):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def try_methods(masker, X_1_train, X_1_test, X_2_train, X_2_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, method_piecewise="mean", n_jobs=1):
    methods_result = []
    # Hyperalignment   !!! for now we have to transpose X_train and X_test
    R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
    methods_result.append(score_table(
        sc * R.dot((X_1_test.T)), X_2_test.T))

    # Ridge Alignment with Cross-Validation
    ridge_estim = RidgeCV(alphas=alphas_ridge,
                          n_splits=n_cv_splits)
    ridge_estim.fit(X_1_train, X_2_train)
    X_1_pred = ridge_estim.transform(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))

    # Reduced Rank Regression with Cross-Validation
    rrr_cv_estim = RRRCV(alphas=alphas_rrr, ks=ks_rrr,
                         n_splits=n_cv_splits)
    rrr_cv_estim.fit(X_1_train, X_2_train)
    X_1_pred = rrr_cv_estim.predict(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))

    # Turning train / test set to images through unmasking
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    # PieceWise Alignment
    if method_piecewise == "all":

        for method in ["scaled_orthogonal", "RidgeCV", "RRRCV", "permutation"]:
            piecewise_estim = PieceWiseAlignment(
                n_pieces=n_pieces_piecewise, method=method, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs)
            piecewise_estim.fit(Im_1_train, Im_2_train)
            Im_1_pred = piecewise_estim.transform(Im_1_test)
            methods_result.append(score_image(masker, Im_1_pred, X_2_test))
    else:
        piecewise_estim = PieceWiseAlignment(
            n_pieces=n_pieces_piecewise, method=method_piecewise, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs)
        piecewise_estim.fit(Im_1_train, Im_2_train)
        Im_1_pred = piecewise_estim.transform(Im_1_test)
        methods_result.append(score_image(masker, Im_1_pred, X_2_test))

    # Deterministic SRM
    '''srm_estim = DeterministicSRM(
        n_components=20, max_iter=10, detrend=True, mask=masker, n_jobs=n_jobs)
    srm_estim.fit([Im_1_train, Im_2_train])
    shared_response = srm_estim.transform(Im_1_test, index=[0])
    Im_1_pred = srm_estim.inverse_transform(shared_response, index=[1])
    methods_result.append(score_image(masker, Im_1_pred[0], X_2_test))'''
    return methods_result


def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.fit_transform(Im_1_pred)
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def debug_Hyper(X_1_train, X_2_train):
    hyper_estim = Hyperalignment()
    hyper_estim.fit([X_1_train.T, X_2_train.T])
    # X_1_pred = hyper_estim.transform(X_1_test.T)
    return hyper_estim
    # hyper renvoie basis_, basis_[0][0] / basis_[1][0] = R (n_voxels, n_voxels)


if __name__ == '__main__':
    # Main loop computing X_train and X_test and testing all methods for each pair of subjects #

    N = 4  # number of subjects

    # Methods Parameters
    n_splits = 4  # Number of folds for validations
    alphas_ridge = (0.1, 1, 10, 100, 1000, 10000, 100000)  # Ridge-CV params
    ks_rrr = range(20)  # RRR-CV params
    alphas_rrr = [0.1, 1, 10, 100, 1000, 10000]  # RRR-CV params
    n_pieces = 20  # number of pieces for piecewise alignement
    method = "all"  # method for piecewise alignement
    #####

    # fetching data
    haxby_dataset = datasets.fetch_haxby(N)
    train_session, test_session = make_train_test_sessions(N, haxby_dataset)

    # preparing the results
    results = []
    if method == "all":
        header = ["subject pair", "identity", "hyperalignment", "ridgecv", "rrr_cv",
                  "piecewise scaled_orthogonal", "piecewise RidgeCV", "piecewise RRRCV", "piecewise permutation", "SRM"]
    else:
        header = ["subject pair", "identity", "hyperalignment",
                  "ridgecv", "rrr_cv", "piecewise_alignment: " + method, "SRM"]

    results.append(header)

    # for each pair of subject (symmetrically)
    for i, subject_pair in enumerate(list(itertools.permutations(range(N), 2))):
        pair_result = []
        subj_1, subj_2 = subject_pair[0], subject_pair[1]
        pair_result.append(subject_pair)
        masker = compute_union_masker(
            [subj_1, subj_2], haxby_dataset)  # make union mask
        X_1_train, X_1_test = split_train_test(
            masker, subj_1, train_session, test_session, haxby_dataset)
        X_2_train, X_2_test = split_train_test(
            masker, subj_2, train_session, test_session, haxby_dataset)

        baseline = r2_score(X_2_test, X_1_test)  # r_score betwenn raw images
        # a = debug_Hyper(X_1_train, X_2_train)

        pair_result.append(baseline)
        # try every method in the library
        methods_result = try_methods(
            masker, X_1_train, X_1_test, X_2_train, X_2_test, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, method)
        pair_result.extend(methods_result)
        results.append(pair_result)

    results_data = pd.DataFrame.from_records(pair_result, columns=header)
    results_title = "exp_haxby_subject_pairs_alignment_results.csv"

    results_data.to_csv(results_title)

    print("result is in" + results_title + "'")
