#!/usr/bin/env python2
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import itertools
import matplotlib.pyplot as plt
import numpy as np
from scipy import linalg, sparse
import pandas as pd
import time
import csv

from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.plotting import plot_stat_map, show
from nilearn.masking import intersect_masks
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign, zscore, Hyperalignment
from functional_alignment.ridge_hyperalignment import RidgeHyperalignment
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.reduced_rank_regression import RRR

# Define train/test sessions from labels
def make_train_test_sessions(N):
    train_session = []
    test_session = []
    for i in range(N):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks =  labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1
    
        train_labels = labels[condition_mask&train_chunks].sort_values(by=['chunks', 'labels'])
        test_labels = labels[condition_mask&test_chunks].sort_values(by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)  
    return train_session, test_session

def fetch_mask(subj):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                     standardize=True, memory="nilearn_cache", memory_level=1,detrend = True)
    return masker

def split_train_test(masker, subj, train_session, test_session):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test

def make_synthetic_noisy_data(X_train, X_test):
    noise = np.random.normal(X_train.mean(axis=0),X_train.std(axis=0),size=X_train.shape)
    test_noise = np.random.normal(X_test.mean(axis=0),X_test.std(axis=0),size=X_test.shape)
    X_train_noise = X_train + noise
    X_test_noise = X_test + test_noise
    return X_train_noise, X_test_noise

def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test

def try_methods(masker, X_1_train, X_1_test, X_2_train, X_2_test):
    methods_result = []
    # Hyperalignment   !!! for now we have to transpose X_train and X_test
    R = hyperalign(X_1_train.T, X_2_train.T)
    methods_result.append(score_table(R[0].dot((X_1_test.T)), X_2_test.T))
    
    # Ridge Alignment
    R_ridge = RidgeHyperalignment()
    R_ridge.fit(X_1_train,X_2_train)
    X_1_pred = R_ridge.transform(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))
    
    # Ridge Alignment with Cross-Validation
    ridge_estim = RidgeCV(alphas=alphas_ridge,n_splits=n_splits)
    ridge_estim.fit(X_1_train,X_2_train)
    X_1_pred = ridge_estim.transform(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))
    
    # Reduced Rank Regression
    rrr_estim = RRR()
    rrr_estim.fit(X_1_train,X_2_train)
    X_1_pred = rrr_estim.predict(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))
    
    # Reduced Rank Regression with Cross-Validation
    rrr_cv_estim = RRRCV(alphas=alphas_rrr, ks=ks_rrr, n_splits=n_splits)
    rrr_cv_estim.fit(X_1_train,X_2_train)
    X_1_pred = rrr_cv_estim.predict(X_1_test)
    methods_result.append(score_table(X_1_pred, X_2_test))
    
    # Turning train / test set to images through unmasking
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(masker, X_1_train, X_2_train, X_1_test)
    
    # PieceWise Alignment
    piecewise_estim = PieceWiseAlignment(n_pieces=n_pieces, method=method, perturbation=False, mask=masker,detrend=True)
    piecewise_estim.fit(Im_1_train,Im_2_train)
    Im_1_pred = piecewise_estim.transform(Im_1_test)
    methods_result.append(score_image(masker,Im_1_pred, X_2_test))

    return methods_result

def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.fit_transform(Im_1_pred)
    score = r2_score(X_2_test,X_1_pred)
    return score

def score_table(X_1_pred, X_2_test):
    score = r2_score(X_2_test,X_1_pred)
    return score

def debug_Hyper(X_1_train, X_2_train):
    hyper_estim = Hyperalignment()
    hyper_estim.fit([X_1_train.T,X_2_train.T])
    # X_1_pred = hyper_estim.transform(X_1_test.T)
    return hyper_estim
    # hyper renvoie basis_, basis_[0][0] / basis_[1][0] = R (n_voxels, n_voxels)

##### Main loop computing X_train and X_test and testing all methods for each pair of subjects #####

N = 4 # number of subjects

##### Methods Parameters
n_splits = 4 # Number of folds for validations
alphas_ridge = (0.1,1,10,100,1000,10000,100000) # Ridge-CV params
ks_rrr = range(10) #RRR-CV params
alphas_rrr = [0.1,1,10,100,1000,10000] # RRR-CV params
n_pieces = 20 # number of pieces for piecewise alignement
method = "mean" # method for piecewise alignement
#####

# fetching data
haxby_dataset = datasets.fetch_haxby(subjects=N)
train_session, test_session = make_train_test_sessions(N)

# preparing the results
results = []
header = ["subject","r_score(X_test,X_test_noise)","r_score hyperalignment"
          ,"r_score ridge(alpha=0.1)","r_score ridgecv","r_score rrr","r_score rrr_cv","r_score piecewise_alignment:"+method]
results.append(header)

for subject in range(N): # for each pair subject
    pair_result = []
    masker = fetch_mask(subject) 
    X_train, X_test = split_train_test(masker, subject, train_session, test_session)
    pair_result.append(subject) 
    X_train_noise, X_test_noise = make_synthetic_noisy_data(X_train, X_test)
    baseline = r2_score(X_test,X_test_noise) # r_score betwenn raw images 
    # a = debug_Hyper(X_1_train, X_2_train)
    pair_result.append(baseline)
    methods_result = try_methods(masker, X_train_noise, X_test_noise, X_train, X_test) # try every method in the library
    pair_result.extend(methods_result)
    results.append(pair_result)
    
with open("exp2_synthetic_results.csv", "w") as f:
    writer = csv.writer(f)
    writer.writerows(results)

print("result is in 'exp2_synthetic_results.csv'")