#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import sys
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import csv
import logging

from nilearn import datasets
from nilearn.input_data import NiftiMasker

from sklearn.metrics import r2_score
from functional_alignment.hyperalignment import hyperalign, Hyperalignment
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.deterministic_srm import DeterministicSRM
# Define train/test sessions from labels

'''All methods copied from exp_haxby_subject_pairs_alignment.py and exp_synthetic_image_baseline.py'''


def make_train_test_sessions(N, haxby_dataset):

    train_session = []
    test_session = []
    for i in range(N):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks = labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1

        train_labels = labels[condition_mask & train_chunks].sort_values(by=[
                                                                         'chunks', 'labels'])
        test_labels = labels[condition_mask & test_chunks].sort_values(
            by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)
    return train_session, test_session


def split_train_test(masker, subj, train_session, test_session, haxby_dataset):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test


def fetch_mask(subj, haxby_dataset):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def create_transform(X_train, transf_mean, transf_std, transformation_type, transf_support='lin', eta=0.5):

    if transformation_type == "translation":
        transformation = None
    else:
        if transf_support == "lin":
            transformation = np.random.normal(
                transf_mean, transf_std, size=X_train[0].shape)
        elif transf_support == "exp":
            transformation = np.exp(eta * np.random.normal(
                transf_mean, transf_std, size=X_train[0].shape))
    return transformation


def apply_transform(X_train, X_test, transformation, type, noise_mean, noise_std, n_permutated=0):
    """
    From X_train, X_test, a transformation matrix and noise, returns transformed and noisy image
    Y_train ans Y_test such as Y = T * X + e
    T : transformation matrix
    e : gaussian noise : N(noise_mean, noise_std)
    """

    train_noise = np.random.normal(
        noise_mean, noise_std, size=X_train.shape)
    test_noise = np.random.normal(
        noise_mean, noise_std, size=X_test.shape)
    if transformation is not None:
        transformation_flattened = transformation.flatten(order='f')
    if type == 'additive':
        Y_train = X_train + \
            np.tile(transformation_flattened, (len(X_train), 1))
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = X_test + np.tile(transformation_flattened,
                                  (len(X_test), 1)) + test_noise

    elif type == 'multiplicative':
        Y_train = X_train * transformation_flattened
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = X_test * transformation_flattened + test_noise

    elif type == 'translation':
        Y_train = np.roll(X_train, 1, axis=0)
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = np.roll(X_test, 1, axis=0)
        Y_test += test_noise
    else:
        raise NameError('Unknown type of transformation')

    return Y_train, Y_test, snr


def make_synthetic_noisy_data(X, mean, noise_std):
    noise = np.random.normal(mean, noise_std, size=X.shape)
    X_noise = X + noise
    return X_noise
    # hyper renvoie basis_, basis_[0][0] / basis_[1][0] = R (n_voxels, n_voxels)


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def try_methods(masker, X_1_train, X_1_test, X_2_train, X_2_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, method_piecewise="mean", n_jobs=1):
    methods_result = []
    # Hyperalignment   !!! for now we have to transpose X_train and X_test

    # Turning train / test set to images through unmasking
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    # PieceWise Alignment
    if method_piecewise == "all":

        for method in ["RRRCV"]:
            for n_bootstrap in [1, 10]:
                piecewise_estim = PieceWiseAlignment(
                    n_pieces=n_pieces_piecewise, n_bootstrap=n_bootstrap, method=method, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs)
                piecewise_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_estim.transform(Im_1_test)
                methods_result.append(score_image(masker, Im_1_pred, X_2_test))
    else:
        piecewise_estim = PieceWiseAlignment(
            n_pieces=n_pieces_piecewise, method=method_piecewise, perturbation=False, mask=masker, detrend=True, n_jobs=n_jobs)
        piecewise_estim.fit(Im_1_train, Im_2_train)
        Im_1_pred = piecewise_estim.transform(Im_1_test)
        methods_result.append(score_image(masker, Im_1_pred, X_2_test))

    # Deterministic SRM
    '''srm_estim = DeterministicSRM(
        n_components=20, max_iter=10, detrend=True, mask=masker, n_jobs=n_jobs)
    srm_estim.fit([Im_1_train, Im_2_train])
    shared_response = srm_estim.transform(Im_1_test, index=[0])
    Im_1_pred = srm_estim.inverse_transform(shared_response, index=[1])
    methods_result.append(score_image(masker, Im_1_pred[0], X_2_test))'''
    return methods_result


def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.fit_transform(Im_1_pred)
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


logging.basicConfig(level=logging.DEBUG, filename="logfile_debug_piecewise_rrr", filemode="a+",
                    format="%(asctime)-15s %(levelname)-8s %(message)s")
if __name__ == '__main__':
    # Main loop computing X_train and X_test and testing all methods for each pair of subjects

    N = 4  # number of subjects
    n_jobs = 1
    if len(sys.argv) > 1:
        n_jobs = int(sys.argv[1])
    # transf_mean = 0
    # transf_std = 1

    # transformation_type = "multiplicative"
    # transf_support = 'lin'

    # Synthetic transformation parameters
    # Identity
    transf_mean = 0
    transf_std = 0
    transformation_type = 'additive'
    transf_support = 'lin'
    n_permutated = 0

    noise_mean = 0
    noise_stds = [0.1, 0.5, 1, 3, 7]
    # Methods Parameters
    n_cv_splits = 4  # Number of folds for validations
    alphas_ridge = (0.1, 10, 500, 10000)  # Ridge-CV params
    ks_rrr = [50, 100]  # RRR-CV params
    alphas_rrr = [0.1, 10, 500, 10000]  # RRR-CV params
    n_pieces = 100  # number of pieces for piecewise alignement
    method = "all"  # method for piecewise alignement
    #####

    # fetching data
    haxby_dataset = datasets.fetch_haxby(subjects=N)
    train_session, test_session = make_train_test_sessions(N, haxby_dataset)

    # Preparing all train/test sets
    X_train_list, X_test_list, mask_list = [], [], []

    masker = NiftiMasker(haxby_dataset.mask, smoothing_fwhm=None,
                         standardize=True, detrend=True)

    for subject in range(N):
        X_train_i, X_test_i = split_train_test(
            masker, subject, train_session, test_session, haxby_dataset)
        X_train_list.append(X_train_i)
        X_test_list.append(X_test_i)

    # Creating transformations
    transformation_list = []
    for subject in range(N):
        transformation = create_transform(X_train_list[subject],
                                          transf_mean, transf_std, transformation_type, transf_support)
        transformation_list.append(transformation)
    # preparing the results
    snr = []
    results = []

    # for various level of noise
    for i, noise_std in enumerate(noise_stds):
        res = []
        logging.info("noise : " + str(noise_std))
        snr_list = []
        for subject in range(N):  # for each subject
            logging.info("subject : " + str(subject + 1))
            transformation = transformation_list[subject]
            # get the train/test set
            X_train, X_test = X_train_list[subject], X_test_list[subject]
            # make noisy data with current std
            Y_train, Y_test, snr_val = apply_transform(
                X_train, X_test, transformation, transformation_type, noise_mean, noise_std, n_permutated)
            snr_list.append(snr_val)
            individual_result = []
            # r_score betwenn raw images
            baseline = max(r2_score(X_test, Y_test), -1)
            # a = debug_Hyper(X_1_train, X_2_train)
            individual_result.append(baseline)
            # try all alignment methods and store results
            # try every method in the library
            methods_result = try_methods(
                masker, Y_train, Y_test, X_train, X_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces, method, n_jobs=n_jobs)
            individual_result.extend(methods_result)
            res.append(individual_result)
        # average the results of the methods on all subjects
        results.append(np.mean(res, axis=0))
        snr.append(np.mean(snr_list, axis=0))

    if method == "all":
        header = ["Identity", "Piecewise RRRCV n_bootstrap = 1",
                  "Piecewise RRRCV n_bootstrap = 10"]  # , "SRM"]

    else:
        header = [
            "Ridgecv", "Piecewise_alignment: " + method, "SRM"]
    transposed_results = np.array(results).T
    results_data = pd.DataFrame()
    results_data["SNR"] = snr
    for i in range(len(transposed_results)):
        results_data[header[i]] = transposed_results[i]
    # Store the results in a CSV
    results_title = 'debug_piecewise_rrr' + \
        str(len(noise_stds)) + '_results.csv'

    results_data.to_csv(results_title)
    print("result are in '" + results_title + "'")
