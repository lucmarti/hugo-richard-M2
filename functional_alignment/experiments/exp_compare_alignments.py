import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from sklearn.linear_model import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
from ridge_hyperalignment import RidgeHyperalignment
import os
import logging
from load_data import load_forrest, load_ibc
from time import time


n_jobs = 10

#Forrest_config
logging.basicConfig(level=logging.DEBUG, filename="logfile_alignment_compare_forrest", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
data_dir = "/storage/data/openfmri/ds113/"

subjects = [1, 2, 3, 4, 5, 6, 7, 8]

X, masker, mask_img, dataset = load_forrest(
    dataset="forrest",
    subjects=subjects,
    runs=[1, 2, 3],
    mask_path=mask_dir + "bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz",
    data_path=data_dir + "sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"
)
label_path_files = "/storage/workspace/hrichard/results/exp7/label_array.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp7/label_array.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp7/label_piecewise_alignment.nii"


#IBC_config

# logging.basicConfig(level=logging.DEBUG, filename="logfile_alignment_compare_ibc", filemode="a+",
#                         format="%(asctime)-15s %(levelname)-8s %(message)s")
#
# mask_dir = "/storage/store/data/ibc/derivatives/"
# data_dir = "/storage/store/data/ibc/derivatives/"
#
# subjects = [1, 2, 4, 5, 6, 7, 8, 9]
#
# X, masker, mask_img, dataset = load_ibc(
#     dataset="ibc",
#     subjects=subjects,
#     runs=[1, 2, 3],
#     mask_path=mask_dir + "group/gm_mask.nii.gz",
#     data_path=data_dir + "sub-%02d/ses*/func/wrdcsub*Trn%02d*.nii.gz"
# )
# label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
# label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
# label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"


X_train = [np.concatenate([X[0][i], X[1][i]], axis=1)
           for i in range(len(X[0]))]
X_test = X[2]

# For resilient clustering
if os.path.isfile(label_path_files):
    labels = np.load(label_path_files)
else:
    labels = create_labels(X[0][0], mask_img.get_data(), n_pieces=1000)
    np.save(label_path_save, labels)
    label_img = masker.inverse_transform(labels)
    nib.save(
        label_img,
        label_path_img
    )


def update(algorithm, name=None):
    def func(X_train, Y_train, X_test):
            if isinstance(algorithm, RidgeHyperalignment):
                t0 = time()
                algorithm.fit(X_train.T, Y_train.T)
                print(time() - t0)
                res = algorithm.transform(X_test.T).T
                print(time() - t0)
                return res
            else:
                t0 = time()
                algorithm.fit(X_train, Y_train)
                print(time() - t0)
                res = algorithm.transform(X_test)
                print(time() - t0)
                return res

    if name is None:
        name = str(algorithm)

    return func, name


algorithms = [
    update(
        PieceWiseAlignment(labels, method="hyperalignment", n_jobs=n_jobs),
        name="hyperalignment"
    ),
    update(
        RidgeHyperalignment(alpha=1e6),
        name="RH_1e6"
        ),
    update(
        RidgeHyperalignment(alpha=1e5),
        name="RH_1e5"
    ),
    update(
        PieceWiseAlignment(labels, method=RidgeCV(alphas=(1500., 2000., 2500.)), n_jobs=n_jobs),
        name="ridgeCV_1500_2000_2500"
    ),
    update(
        PieceWiseAlignment(labels, method="mean", n_jobs=n_jobs),
        name="mean"
        ),
    update(
        PieceWiseAlignment(labels, method=RRR(k=25, alpha=0.003), n_jobs=n_jobs),
           name="RRR_25_003"
    )
]


n_splits = len(subjects)
rs = ShuffleSplit(n_splits=n_splits, test_size=.125, random_state=0)
rs.split(X_train)

mean_exp_var = {}
for algorithm, name in algorithms:
    mean_exp_var[name] = np.zeros_like(X_test[0][:, 0])


for train_index, test_index in rs.split(X_train):
    logging.info("TRAIN:" +
                 train_index.__str__() +
                 "TEST:" +
                 test_index.__str__())

    for algorithm, name in algorithms:
        logging.info(name)

        Y_list = []
        for i in train_index:
            logging.info("Train: " + str(i))
            Y_predict = algorithm(X_train[i],
                                  X_train[test_index[0]],
                                  X_test[i]
                                  )
            Y_list.append(Y_predict)
            logging.info((1 - (X_test[test_index[0]] - Y_predict).var(axis=1)).mean())
        Y = np.array(Y_list).mean(axis=0)
        var_e = (X_test[test_index[0]] - Y).var(axis=1)
        logging.info((1 - var_e).mean())
        exp_var = 1 - var_e
        nib.save(
            masker.inverse_transform(exp_var),
            "/storage/workspace/hrichard/results/exp9/" +
            dataset +
            "alignment" +
            "_algo" +
            name +
            "subject" + str(i) + "_" + str(test_index[0]) +
            "_exp9.nii"
        )
        mean_exp_var[name] += exp_var

for algorithm, name in algorithms:
    mean_exp_var[name] = mean_exp_var[name]/n_splits
    img = masker.inverse_transform(mean_exp_var[name])
    nib.save(
        img,
        "/storage/workspace/hrichard/results/exp9/" +
        dataset +
        "compare_alignment" +
        "_algo" +
        name +
        "_exp9.nii"
    )
logging.info("Done")
