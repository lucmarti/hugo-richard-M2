from nilearn.datasets import fetch_icbm152_brain_gm_mask
from nilearn.masking import intersect_masks
from nilearn.image import resample_to_img
from nilearn.image import load_img
from nilearn.plotting import plot_stat_map
%matplotlib inline
import os
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS)

# IBC
icbm_gm = fetch_icbm152_brain_gm_mask()

data_dir = os.path.join(SMOOTH_DERIVATIVES, 'group')
mask_path = os.path.join(data_dir, 'resampled_gm_mask.nii.gz')
ibc_mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
ibc_initial_mask = load_img(mask_path)
resampled_mask = resample_to_img(ibc_initial_mask, ibc_mask_path)
resampled_mask.to_filename(
    "/storage/tompouce/tbazeill/ibc/gm_mask_3mm_for_2mm_images.nii.gz")

ibc_initial_mask.shape
resampled_mask.shape

plot_stat_map(ibc_initial_mask)
plot_stat_map(resampled_mask)
plot_stat_map(ibc_mask_path)

# Forrest
forrest_mask_path = < "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"
ibc_mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
forrest_initial_mask = load_img(forrest_mask_path)
resampled_icbm = resample_to_img(
    icbm_gm, forrest_initial_mask, interpolation="nearest")
intersected_mask = intersect_masks(
    [forrest_initial_mask, resampled_icbm], threshold=1, connected=True)
intersected_mask_resampled_ibc = resample_to_img(
    intersected_mask, ibc_mask_path, interpolation="nearest")
intersected_mask_resampled_ibc.to_filename(
    "/storage/data/openfmri/ds113/3mm_mask_gm.nii.gz")

# Sherlock
sherlock_mask_path = "/storage/data/sherlock_movie_watch_dataset/preloaded_masker/mask_all_subj_movie.nii"
sherlock_initial = load_img(sherlock_mask_path)
icbm_resampled = resample_to_img(
    icbm_gm, sherlock_initial, interpolation="nearest")
sherlock_gm = intersect_masks(
    [icbm_resampled, sherlock_initial], threshold=1, connected=True)
sherlock_gm.to_filename(
    "/storage/data/sherlock_movie_watch_dataset/preloaded_masker/mask_all_subj_movie_gm.nii")

# HCP

import os
mask_paths = []
for subject in subjects:
    for task in ["EMOTION", "GAMBLING", "LANGUAGE", "MOTOR", "RELATIONAL", "WM", "SOCIAL"]:
        for acq in ["LR", "RL"]:
            path = os.path.join(
                path_to_data, "%s/%s/%s/mask.nii.gz" % (str(subject), task, acq))
            mask_paths.append(path)
from nilearn.masking import intersect_masks
from nilearn.plotting import plot_stat_map
%matplotlib inline
new_mask = intersect_masks(mask_paths, threshold=0.5)
new_mask.to_filename(os.path.join(path_to_results, "mask.nii.gz"))


path_to_results = "/storage/tompouce/tbazeill/hcp"
hcp_initial = load_img(os.path.join(path_to_results, "mask.nii.gz"))
resampled_icbm = resample_to_img(
    icbm_gm, hcp_initial, interpolation="nearest")
intersected_mask = intersect_masks(
    [hcp_initial, resampled_icbm], threshold=1, connected=True)
intersected_mask_resampled_ibc = resample_to_img(
    intersected_mask, ibc_mask_path, interpolation="nearest")
intersected_mask_resampled_ibc.to_filename(
    os.path.join(path_to_results, '3mm_mask_gm.nii.gz'))
