# %%
import numpy as np
import matplotlib.pyplot as plt
from functional_alignment.hyperalignment import hyperalign
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.ridge_cv import RidgeCV
from sklearn.metrics import r2_score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def methods_output(source, X_train, X_test, Y_train, Y_test, target_name, plot=False):

    R, scale = hyperalign(X_train.T, Y_train.T, scale=True)
    hyperalign_output = (scale * R.dot(X_test.T)).T
    hyperalign_mean = np.mean(hyperalign_output, axis=0).reshape(
        source.shape, order='f')
    hyper_score = score_table(hyperalign_output, Y_test)

    alphas = [0.01, 0.1, 1, 10, 100, 1000, 10000]
    ks_rrr = range(10)
    n_splits = 10
    rrr = RRRCV(alphas=alphas, ks=ks_rrr, n_splits=n_splits)
    rrr.fit(X_train, Y_train)
    rrr_components = rrr.k
    rrr_output = rrr.predict(X_test)
    rrr_mean = np.mean(rrr_output, axis=0).reshape(source.shape, order='f')
    rrr_score = score_table(rrr_output, Y_test)

    ridge = RidgeCV(alphas=alphas, n_splits=n_splits)
    ridge.fit(X_train, Y_train)
    ridge_output = ridge.transform(X_test)
    ridge_mean = np.mean(ridge_output, axis=0).reshape(source.shape, order='f')
    ridge_score = score_table(ridge_output, Y_test)

    results = [hyper_score, rrr_score, ridge_score]

    fig = None
    if plot is True:
        fig, axes = plt.subplots(4, 3, subplot_kw=dict())
        fig.suptitle(target_name)
        axes[0, 0].imshow(np.mean(Y_test, axis=0).reshape(
            source.shape, order='f'))
        axes[0, 0].set_title("Y_test mean: " +
                             "%.2g" % score_table(X_test, Y_test), size=10)
        axes[0, 0].axis('off')

        axes[0, 1].imshow(np.std(Y_test, axis=0).reshape(
            source.shape, order='f'))
        axes[0, 1].axis('off')
        axes[0, 1].set_title("Y_test std", size=10)

        axes[0, 2].imshow(np.mean(X_test, axis=0).reshape(
            source.shape, order='f'))
        axes[0, 2].set_title("X_test mean", size=10)
        axes[0, 2].axis('off')

        axes[1, 0].set_title("Hyperalign: " +
                             "%.2g" % hyper_score, size=10)
        axes[1, 0].imshow(hyperalign_mean)
        axes[1, 0].axis('off')

        axes[1, 2].set_title("Transformation", size=10)
        axes[1, 2].imshow(scale * R.T)
        axes[1, 2].axis('off')

        axes[1, 1].imshow(np.std(hyperalign_output, axis=0).reshape(
            source.shape, order='f'))
        axes[1, 1].axis('off')

        axes[2, 0].set_title(
            "RRR: " + "%.2g" % rrr_score, size=10)
        axes[2, 0].imshow(rrr_mean)
        axes[2, 0].axis('off')

        axes[2, 1].imshow(np.std(rrr_output, axis=0).reshape(
            source.shape, order='f'))
        axes[2, 1].axis('off')

        axes[2, 2].imshow(rrr.a.dot(rrr.b.T))
        axes[2, 2].axis('off')

        axes[3, 0].set_title(
            "Ridge: " + "%.2g" % ridge_score, size=10)
        axes[3, 0].imshow(ridge_mean)
        axes[3, 0].axis('off')

        axes[3, 1].imshow(np.std(ridge_output, axis=0).reshape(
            source.shape, order='f'))
        axes[3, 1].axis('off')

        axes[3, 2].imshow(ridge.a.dot(ridge.b))
        axes[3, 2].axis('off')
        plt.subplots_adjust(wspace=0.2, hspace=0.4)

    return fig, results, scale * R.T, rrr, rrr_components, ridge


def create_train_test_set(source, n_train, n_test, distrib_mean, distrib_std, contrast_std=0):
    train_source_set = np.tile(source, (n_train, 1))
    train_contrast = np.random.normal(1, contrast_std, n_train)
    contrasted_train = np.array([train_source * train_contrast[i]
                                 for i, train_source in enumerate(train_source_set)])
    train_distrib = np.random.normal(
        distrib_mean, distrib_std, size=train_source_set.shape)

    X_train = contrasted_train + train_distrib

    test_source_set = np.tile(source, (n_test, 1))
    test_contrast = np.random.normal(1, contrast_std, n_test)
    contrasted_test = np.array([test_source * test_contrast[i]
                                for i, test_source in enumerate(test_source_set)])
    test_distrib = np.random.normal(
        distrib_mean, distrib_std, size=test_source_set.shape)
    X_test = contrasted_test + test_distrib

    return X_train, X_test, train_distrib, test_distrib, train_contrast, test_contrast


def create_transform(X_train, transf_mean, transf_std, transformation_type, transf_support='lin', eta=0.5):

    if transformation_type == "translation":
        transformation = None
    else:
        if transf_support == "lin":
            transformation = np.random.normal(
                transf_mean, transf_std, size=X_train[0].shape)
        elif transf_support == "exp":
            transformation = np.exp(eta * np.random.normal(
                transf_mean, transf_std, size=X_train[0].shape))
    return transformation


def apply_transform(X_train, X_test, transformation, type, noise_mean, noise_std, n_permutated=0):
    """
    From X_train, X_test, a transformation matrix and noise, returns transformed and noisy image
    Y_train ans Y_test such as Y = T * X + e
    T : transformation matrix
    e : gaussian noise : N(noise_mean, noise_std)
    """

    train_noise = np.random.normal(
        noise_mean, noise_std, size=X_train.shape)
    test_noise = np.random.normal(
        noise_mean, noise_std, size=X_test.shape)
    if transformation is not None:
        transformation_flattened = transformation.flatten(order='f')
    if type == 'additive':
        Y_train = X_train + \
            np.tile(transformation_flattened, (len(X_train), 1))
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = X_test + np.tile(transformation_flattened,
                                  (len(X_test), 1)) + test_noise

    elif type == 'multiplicative':
        Y_train = X_train * transformation_flattened
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = X_test * transformation_flattened + test_noise

    elif type == 'translation':
        Y_train = np.zeros(X_train.shape)
        for i in range(X_train.shape[1]):
            Y_train[:, i] = X_train[:,
                                    (i + n_permutated * 10) % X_train.shape[1]]
        snr = 10 * np.log10(np.linalg.norm(X_train) /
                            np.linalg.norm(train_noise))
        Y_train += train_noise

        Y_test = np.zeros(X_test.shape)
        for i in range(X_test.shape[1]):
            Y_test[:, i] = X_test[:, (i + n_permutated * 10) % X_test.shape[1]]
        Y_test += test_noise
    else:
        raise NameError('Unknown type of transformation')

    return Y_train, Y_test, snr


def plot_for_various_values(X_train, X_test, transf_mean, transf_std, transformation_type, transf_support, noise_mean, noise_stds, n_permutated=0):
    results_wt_header = []
    results = []
    snr = []
    header = ["T : Identity (Baseline)", "T : Hyperalignment", "T : RRR cv", "T : Ridge cv"
              ]
    results_wt_header.append(header)

    for i, noise_std in enumerate(noise_stds):
        plot = False
        result = []
        transformation = create_transform(X_train,
                                          transf_mean, transf_std, transformation_type, transf_support)
        # get the train/test set
        Y_train, Y_test, snr_val = apply_transform(
            X_train, X_test, transformation, transformation_type, noise_mean, noise_std, n_permutated)
        snr.append(snr_val)
        # r_score betwenn raw images
        result.append(max(r2_score(X_test, Y_test), -1))
        if i % 5 == 0:
            plot = True
        fig, scores, hyper_transf, rrr, rrr_components, ridge = methods_output(
            source, X_train, X_test, Y_train, Y_test, transformation_type + " SNR :" + "%.2g" % snr_val, plot=plot)

        result.extend(scores)
        results.append(result)

    results_wt_header.append(zip(noise_stds, results))

    fig = plt.figure()
    for y, label in zip(np.array(results).T, header):
        plt.plot(snr, y, label=label)
    plt.axhline(color='k')
    plt.xlabel('Signal to noise Ratio (dB)')
    plt.ylabel('Transformation R2_score : ||Y- T(X)||^2')
    plt.legend()
    plt.title('Alignment methods performance with various level of noise')
    return fig, results, results_wt_header


if __name__ == '__main__':
    # %% Definition of the experiment main parameters
    source = np.array(np.random.rand(10, 10))
    source_mean = source.mean()
    print source_mean
    source_flattened = source.flatten(order="f")
    n_train = 1000
    n_test = 10000
    distrib_mean = 0
    distrib_std = 0.5
    contrast_std = 4

    X_train, X_test, train_distrib, test_distrib, train_contrast, test_contrast = create_train_test_set(
        source_flattened, n_train, n_test, distrib_mean, distrib_std, contrast_std)
    # %% Constant Additive/Multiplicative Transformation
    transf_mean = 0
    transf_std = 0.1

    noise_mean = 0
    noise_stds = [0.01, 0.1, 0.3, 0.5, 0.7, 1, 2, 3, 5, 10, 20]

    n_permutated = 4

    # 'additive' or 'multiplicative' or "translation"
    transformation_type = "translation"
    transf_support = 'lin'
    fig, results, results_wt_header = plot_for_various_values(
        X_train, X_test, transf_mean, transf_std, transformation_type, transf_support, noise_mean, noise_stds, n_permutated)
    # fig.savefig()
    # %% Identity

    transf_mean = 0
    transf_std = 0

    noise_mean = 0
    noise_stds = [0.01, 0.1, 0.3, 0.5, 0.7, 1, 2, 3, 5, 10, 20]

    n_permutated = 0

    # 'additive' or 'multiplicative' or "translation"
    transformation_type = "additive"
    transf_support = 'lin'
    fig, results, results_wt_header = plot_for_various_values(
        X_train, X_test, transf_mean, transf_std, transformation_type, transf_support, noise_mean, noise_stds, n_permutated)

    # %% Transformation : Multiplication by N(0,1)

    transf_mean = 0
    transf_std = 1

    noise_mean = 0
    noise_stds = [0.01, 0.1, 0.3, 0.5, 0.7, 1, 2, 3, 5, 10, 20]

    n_permutated = 0

    # 'additive' or 'multiplicative' or "translation"
    transformation_type = "multiplicative"
    transf_support = 'lin'

    a = create_transform(X_train, transf_mean, transf_std,
                         transformation_type, transf_support)
    plt.imshow(np.diag(a))

    fig, results, results_wt_header = plot_for_various_values(
        X_train, X_test, transf_mean, transf_std, transformation_type, transf_support, noise_mean, noise_stds, n_permutated)
    # %%

    transf_mean = 0
    transf_std = 1

    noise_mean = 0
    noise_stds = [0.01, 0.1, 0.3, 0.5, 0.7, 1, 2, 3, 5, 10, 20]
    n_permutated = 0
    # 'additive' or 'multiplicative' or "translation"
    transformation_type = "multiplicative"
    transf_support = 'exp'

    a = create_transform(X_train, transf_mean, transf_std,
                         transformation_type, transf_support)
    plt.imshow(np.diag(a))

    fig, results, results_wt_header = plot_for_various_values(
        X_train, X_test, transf_mean, transf_std, transformation_type, transf_support, noise_mean, noise_stds, n_permutated)
    # %%
