from srm import SRM
from load_data import get_mask, load_data
import nibabel as nib
import itertools
import matplotlib.pyplot as plt
import numpy as np
import pickle

print("Loading Data ...")
params = ["ortho", "pca", "dict"]
dataset = "forrest"

masker = get_mask(dataset=dataset)
X, names = load_data(masker,
                     dataset=dataset,
                     )

X_train = X[0]
names_train = names[0]
print(X_train[0].shape)

X_test = X[1]
names_test = names[1]

print("Done")

for param in params:
    n_components = 20
    srm = SRM(n_components, max_iter=10, verbose=True, algo_name=param)
    print("Fitting Data")
    srm.fit(X_train)
    print("Done")
    print("Transforming Data")
    basis = srm.transform(X_train)
    print("Done")
    print("Testing Data")
    print("Learning shared response")
    n_half = len(X_test)//2 + len(X_test)% 2
    # We use half of subjects to compute a shared response
    shared_response =  srm.shared_coordinates(X_test[0:n_half], basis[0:n_half])
    print("Compute correlation")
    #We use the next 10 subjects to compute the explained variance

    with open("/storage/workspace/hrichard/results/exp6/basis" + param + ".pickle", "wb") as output_file:
        pickle.dump(basis, output_file)

    with open("/storage/workspace/hrichard/results/exp6/shared_test" + param + ".pickle", "wb") as output_file2:
        pickle.dump(shared_response, output_file2)

    with open("/storage/workspace/hrichard/results/exp6/shared" + param + ".pickle", "wb") as output_file3:
        pickle.dump(srm.shared_coord_, output_file3)


    # mean_correlation = np.zeros((len(X_test) - n_half, len(X_test) - n_half))
    # for i in range(n_half, len(X_test)):
    #     for j in range(n_half, len(X_test)):
    #         projected_i = basis[i].dot(shared_response)
    #         projected_j = basis[j].dot(shared_response)
    #         mean_correlation_ij = 0
    #         for k in range(0, len(projected_i)):
    #             norm_ik = np.linalg.norm(projected_i[k])
    #             norm_jk = np.linalg.norm(projected_j[k])
    #
    #             if norm_ik != 0 and norm_jk != 0:
    #                 mean_correlation_ij += projected_i[k].dot(projected_j[k].T)/(norm_ik*norm_jk)
    #
    #             if norm_ik == 0 and norm_jk == 0:
    #                 mean_correlation_ij += 1
    #
    #         mean_correlation_ij /= len(projected_i)
    #
    #         if i == j:
    #             print("Subject", i)
    #             print("Subject", j)
    #             print("Correlation", mean_correlation_ij)
    #
    #         mean_correlation[i - n_half, j - n_half] = mean_correlation_ij
    #
    # plt.matshow(mean_correlation)
    # plt.colorbar()
    # plt.savefig("/storage/workspace/hrichard/results/exp6/" + param + "_correlation.jpg")
