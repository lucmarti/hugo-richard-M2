import nibabel as nib
import numpy as np
import logging
import sys
sys.path.append("../")
from functional_alignment.load_data import load
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.fastSRM import FastSRM
from nilearn.input_data import MultiNiftiMasker
import os.path
from sklearn.model_selection import KFold
import pandas as pd
from time import time
import joblib

logging.basicConfig(level=logging.DEBUG, filename="logfile_fast_atlas", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

t0 = time()
logs = []
for config in ["sherlock"]:
    logging.info("Start experiment with config %s" % config)
    masker = '/home/parietal/hrichard/cogspaces_data/mask/hcp_mask.nii.gz'
    mask_memory = "/storage/tompouce/hrichard/general_cache/"

    exp_params = {
        "detrend": True,
        "standardize": True,
        "memory": mask_memory,
        "mask": masker,
        "mask_strategy": "epi",
        "n_jobs": 1,
        "memory_level": 5,
        "low_pass": 0.1,
        "high_pass": 0.01,
        "t_r": 2,
        "verbose": 1,
        "smoothing_fwhm": 5,
    }

    if config == "forrest":
        mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
        data_dir = "/storage/data/openfmri/ds113/"

        subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
        runs = [1, 2, 3, 4, 5, 6, 7]

        X, dataset = load(
            dataset="forrest",
            subjects=subjects,
            runs=runs,
            data_path=data_dir + "sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz",
        )

    elif config == "ibc":
        data_dir = "/storage/store/data/ibc/derivatives/"
        data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

        subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
        runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05", "Trn06", "Trn07", "Trn08", "Trn09",
                "Val01", "Val02", "Val03", "Val04", "Val05", "Val06", "Val07", "Val08"]

        X, dataset = load(
            dataset="ibc",
            subjects=subjects,
            runs=runs,
            data_path=data_path,
        )

    elif config == "sherlock":
        subjects = [1, 2, 3, 4, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17]
        runs = [0, 1, 2, 3, 4]
        data_path = "/storage/workspace/hrichard/sherlock_data/%02d_sherlock_movie_s%i.nii"
        X = [[data_path % (run, subject) for run in runs] for subject in subjects]
        X = np.array(X)
        dataset = "sherlock"

    elif config == "raiders":
        subjects = np.array([1, 4, 5, 6, 7, 9, 11, 12, 13, 14])
        # runs = np.array([1., 2., 3., 4., 5., 6., 7., 8., 10., 11., 12.])
        runs = np.array([1., 2., 3., 4., 5., 6., 7., 8., 10.])

        # We need to discard the last 5 fmri images for each run
        data = pd.read_csv("/home/parietal/hrichard/hugo-richard-M2/functional_alignment/experiments/ibc.dat")

        paths = []
        for i, subject in enumerate(subjects):
            paths_ = []
            for j, run in enumerate(runs):
                paths_.append(data[
                    (data["task"] == "Raiders")
                    & (data["subject"] == subject)
                    & (data["run"] == run)
                ].tail(1)["path"].values[0])
            paths.append(paths_)

        X, dataset = np.array(paths), "raiders"

    else:
        ValueError("No valid configuration was specified")

    mask_params = exp_params.copy()
    mask_params["mask_img"] = mask_params["mask"]
    del mask_params["mask"]

    n_subjects, n_sessions = X.shape
    mask = MultiNiftiMasker(**mask_params).fit()

    sess_i = 0
    for k in [5]:
        logging.info("Using %i components" % k)
        result_directory = "/storage/workspace/hrichard/M2_internship/results/exp10/k_%i/" % k
        logging.info("Number of components: %i" % k)
        cv = KFold(n_splits=5, shuffle=False, random_state=0)
        sessions_train, sessions_test = list(cv.split(np.arange(n_sessions)))[0]
        sess_i += 1
        X_train = X[:, sessions_train]
        algorithms = [
            (FastSRM(n_components=k,
                     atlas="/storage/store/data/cogspaces/modl/components_512.nii.gz",
                     n_iter=100,
                     temp_dir=mask_memory,
                     **exp_params), "FastSRM_modl_512"),
        ]

        for _ in range(len(algorithms)):
            algorithm, name = algorithms.pop()
            logging.info(name)
            print(name)
            logging.info("Fitting data")
            logs.append([dataset, k, name, sessions_test.__str__(), "start fitting", time() - t0])
            train_shared_response = algorithm.fit_transform(X_train)
            logs.append([dataset, k, name, sessions_test.__str__(), "done fitting", time() - t0])
            joblib.dump(algorithm.basis, mask_memory + "basc_basis")
            logging.info("Done")
            print("X_train", X_train)
            np.save("/storage/tompouce/hrichard/general_cache/modl_train_shared_response", train_shared_response)
            logs.append([dataset, k, name, sessions_test.__str__(), "start transforming", time() - t0])
            subj_i = 0
            for subjects_train, subjects_test in KFold(n_splits=2, shuffle=True).split(np.arange(n_subjects)):
                logging.info("Subject split number %i / %i" % (subj_i, 5))
                logging.info("Transforming Data")
                if "FastSRM" in name:
                    shared_response = algorithm.transform(X[subjects_train, :][:, sessions_test],
                                                          subjects_indexes=subjects_train)
                else:
                    shared_response = algorithm.transform(X[subjects_train, :][:, sessions_test],
                                                          index=subjects_train)
                np.save("/storage/tompouce/hrichard/general_cache/modl_shared_response", shared_response)
                print("Y_true", X[subjects_test, :][:, sessions_test])
                logging.info("Reconstruction")
                X_true = X[subjects_test, :][:, sessions_test]
                if "FastSRM" in name:
                    Y = algorithm.inverse_transform(shared_response, subjects_indexes=subjects_test)
                else:
                    Y = algorithm.inverse_transform(shared_response, index=subjects_test)

                np.save("/storage/tompouce/hrichard/general_cache/modl_Y_pred", Y)
                break
            logs.append([dataset, k, name, sessions_test.__str__(), "done transforming", time() - t0])
# logs = pd.DataFrame(logs, columns=["dataset", "components", "algo", "test_sessions", "action", "time"])
# logs.to_csv(os.path.join(result_directory, "fast_atlas.csv"))
