import os
import time

import numpy as np

from nilearn.input_data import NiftiMasker
from nilearn.image import index_img, load_img, math_img, concat_imgs
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
from sklearn.model_selection import ShuffleSplit
from functional_alignment.template import TemplateAlignment
from functional_alignment.utils import score_table
from functional_alignment.ot_template_alignment import OptimalTransportTemplate

templates = [
    'template_mean_1_bootstraps_epsilon_scaling_optimal_transport_1_eu_cv_0.nii.gz', 'template_mean_1_bootstraps_orthogonal_cv_0.nii.gz', 'template_mean_1_bootstraps_RidgeCV_cv_0.nii.gz',
    'template_mean_1_bootstraps_scaled_orthogonal_cv_0.nii.gz', 'template_permutation_cv_0.nii.gz', 'mean_cv_0.nii.gz', 'mean_smoothed_7_cv_0.nii.gz']

labels = [
    '1_optimal_transport_temp_1', '1_orthogonal_temp', '1_RidgeCV_temp',
    '1_scaled_orthogonal_temp', '1_permutation_temp', 'average_temp', 'smoothed_7_average_temp']

data_dir = os.path.join(THREE_MM, 'group')
dataset = 'ibc'
n_jobs = 2
scale_template = False
methods = ["RidgeCV", "optimal_transport", "scaled_orthogonal", 'permutation']
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)
path_to_results = "/storage/tompouce/tbazeill/ibc/template_alignment"
mask = load_img(mask_path)

# Access to the data
if dataset == 'ibc':
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                 'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
                 'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm']
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                     conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    rs = ShuffleSplit(n_splits=5, test_size=.4, random_state=0)

    template_folds, mapping_folds, gt_folds = [], [], []

    for train_subjects, test_subjects in rs.split(subject_list):
        test_mapping = []
        test_gt = []
        for subject in subject_list[test_subjects]:
            ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_mapping.append(ap_path)
            pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_gt.append(pa_path)
        mapping_folds.append(test_mapping)
        gt_folds.append(test_gt)
        subject = subject_list[train_subjects[0]]
        temp = []
        temp.extend([df[df.acquisition == 'ap'][df.subject == subject][
            df.contrast == condition].path.values[-1] for condition in conditions])
        temp.extend([df[df.acquisition == 'pa'][df.subject == subject][
            df.contrast == condition].path.values[-1] for condition in conditions])
        templ = concat_imgs(temp)
        label_templ = subject_list[train_subjects[0]]

# for template_train_imgs, mapping_test_imgs, gt_test_imgs in zip(template_folds, mapping_folds, gt_folds):

cv = 0
train_index = np.asarray(range(53))
test_index = np.asarray(range(53, 106))
mapping_test_imgs, gt_test_imgs = mapping_folds[cv], gt_folds[cv]

X_gts = []
for i in range(len(gt_test_imgs)):
    X_gt = masker.transform(gt_test_imgs[i])
    X_gts.append(X_gt)
for method in methods:
    for label, template in zip(labels, templates):
        print(label)
        template_estim = TemplateAlignment(
            n_pieces=150, alignment_method=method, n_bags=1, mask=masker, n_jobs=n_jobs, reg=1)
        template_estim.template = load_img(
            os.path.join(path_to_results, template))
        pred_imgs = template_estim.transform(
            mapping_test_imgs, train_index, test_index)
        for i in range(len(gt_test_imgs)):
            X_gt = X_gts[i]
            X_pred = masker.transform(pred_imgs[i])
            for j, loss in enumerate(["zero_mean_r2"]):
                loss_score = score_table(loss, X_gt, X_pred, 'raw_values')
                pred_im = masker.inverse_transform(loss_score)
                pred_im.to_filename(os.path.join(path_to_results, "%s_pred_%s_aligned_%s_cv_%s_%s.nii.gz" % (
                    subject_list[test_subjects][i], method, label, str(cv), loss)))

"""
    template_estim = TemplateAlignment(
        n_pieces=150, alignment_method=method, n_bags=1, mask=masker, n_jobs=n_jobs, reg=1)
    template_estim.template = templ
    pred_imgs = template_estim.transform(
        mapping_test_imgs, train_index, test_index)
    for i in range(len(gt_test_imgs)):
        X_gt = X_gts[i]
        X_pred = masker.transform(pred_imgs[i])
        for j, loss in enumerate(["zero_mean_r2"]):
            loss_score = score_table(loss, X_gt, X_pred, 'raw_values')
            pred_im = masker.inverse_transform(loss_score)
            pred_im.to_filename(os.path.join(path_to_results, "%s_pred_%s_aligned_%s_cv_%s_%s.nii.gz" % (
                subject_list[test_subjects][i], method, label_templ, str(cv), loss)))
"""
