
import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from rrr_cv import RRRCV
from sklearn.linear_model import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
import os
import logging
import load_data
from utils import generate_train_test, load_img
from nilearn.image import index_img
from deterministic_srm import DeterministicSRM
import random

n_jobs = 10

#IBC_config

logging.basicConfig(level=logging.DEBUG, filename="logfile_map_fuzzy_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

mask_dir = "/storage/store/data/ibc/derivatives/"
mask_path=mask_dir + "group/gm_mask.nii.gz"

# mask_path = "/storage/workspace/hrichard/results/exp13/mask_ibc_roi.nii.gz"

data_dir = "/storage/data/openfmri/ds113/"
storage_dir = "/storage/tompouce/hrichard/fuzzy_ibc/fuzzy_ibc_fit/"

data_dir = "/storage/store/data/ibc/derivatives/"
data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

memory="/storage/tompouce/hrichard/fuzzy_ibc/fuzzy_ibc_cache"

subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
runs = ["Trn01",
        "Trn02",
        "Trn03",
        "Trn04",
        "Trn05",
        "Trn06",
        "Trn07",
        "Trn08",
        "Trn09",
        "Val01",
        "Val02",
        "Val03",
        "Val04",
        "Val05",
        "Val06",
        "Val07",
        "Val08"]

X, masker, mask_img, dataset = load_data.load_fuzzy_ibc(
    dataset="fuzzy_ibc",
    subjects=subjects,
    runs=runs,
    mask_path=mask_path,
    data_path=data_path,
    memory=memory
)

label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"
print("Loading of data ... Done")

# labels = create_labels(X[0], mask_img.get_data(), n_pieces=2)

# For resilient clustering
if os.path.isfile(label_path_files):
    labels = np.load(label_path_files)
else:
    img_00 = index_img(X[0, 0], 0)
    X_00 = masker.transform(img_00).T
    labels = create_labels(X_00, mask_img.get_data(), n_pieces=1000)
    np.save(label_path_save, labels)
    label_img = masker.inverse_transform(labels)
    nib.save(
        label_img,
        label_path_img
    )


def predict(Y_, algo, algo_name, i_train_subjects, i_test_subject, i_train_runs, i_test_runs, data, storage_d):
    total_time = 0
    if isinstance(algo, PieceWiseAlignment):
        for i in i_train_subjects:
            # Y_i_test is the prediction for subject test from subject i
            logging.info("Train subject: " + str(i))
            logging.info("Test subject: " + str(i_test_subject[0]))
            filename = storage_d + \
                       algo_name + \
                       str(i) + \
                       "-".join(map(str, i_test_subject)) + \
                       "-".join(map(str, i_train_runs)) + \
                       ".temp"

            filename_info = storage_d + \
                            "info_" + \
                            algo_name + \
                            str(i) + \
                            "-".join(map(str, i_test_subject)) + \
                            "-".join(map(str, i_train_runs)) + \
                            ".temp"

            if os.path.isfile(filename):
                algo.load(filename)
                logging.info("No fitting needed, we use memory")
                algo.dump_infos(filename_info)
            else:
                if os.path.isfile(filename_info):
                    algo.load_infos(filename_info)
                algo.fit(data[i, :][i_train_runs], data[i_test_subject[0], :][ i_train_runs])
                logging.info("fit time: " + str(algo.fit_time_))
                algo.dump_infos(filename_info)

            Y_i_test = algo.transform(data[i, :][i_test_runs])
            logging.info("transform time: " + str(algo.transform_time_))
            Y_ += Y_i_test
            total_time += algo.fit_time_ + algo.transform_time_

        Y_ /= len(i_train_subjects)

    elif isinstance(algo, DeterministicSRM):
        filename = storage_dir + \
                   algo_name + \
                   "".join(map(str, i_train_runs)) + \
                   ".temp"

        if os.path.isfile(filename):
            algo.load(filename)
            logging.info("No fitting needed, we use memory")
        else:
            algo.fit(data[:, i_train_runs])
            algo.dump(filename)
            logging.info("fit time: " + str(algo.fit_time_))

        shared_response = algorithm.transform(data[i_train_subjects, :][:, i_test_runs],
                                              index=i_train_subjects)

        logging.info("transform time: " + str(algo.transform_time_))
        Y_ = algorithm.inverse_transform(shared_response, index=i_test_subject)[0]
        logging.info("inverse transform time: " + str(algo.inverse_transform_time_))

        total_time = algo.fit_time_ + algo.transform_time_ + algo.inverse_transform_time_

    return Y_, total_time


algorithms = [
    (DeterministicSRM(20, max_iter=10, masker=masker, scaling=True), "srm_ortho"),
    (PieceWiseAlignment(labels, masker=masker, method="hyperalignment", n_jobs=n_jobs),"hyperalignment"),
    (PieceWiseAlignment(labels, masker=masker, method=RidgeCV(alphas=(
                                                       1e3,
                                                       1e4,
                                                       1e5,
                                                       )), n_jobs=n_jobs, perturbation=True),"ridgeCV"),
    (PieceWiseAlignment(labels, masker=masker, method="mean", n_jobs=n_jobs), "mean"),
]

n_splits = 1
rs = ShuffleSplit(n_splits=n_splits, test_size=.1, random_state=0)
time_list = []
random.seed(0)
for train_subjects, test_subject in random.sample(generate_train_test(subjects), 5):
    print("TRAIN subject:" +
          train_subjects.__str__() +
          "TEST subject:" +
          test_subject.__str__())
    for algorithm, name in algorithms:
        print(name)
        for train_runs, test_runs in rs.split(X[0]):
            print("TRAIN runs:" +
                  train_runs.__str__() +
                  "TEST runs:" +
                  test_runs.__str__())

            result_filename = "/storage/workspace/hrichard/results/exp13/" +\
                              dataset +\
                              "alignment" +\
                              "_algo" +\
                              name +\
                              "subject_" + str(test_subject[0]) +\
                              "runs_" + "-".join(map(str, test_runs)) +\
                              "_exp13.nii"

            if not os.path.isfile(result_filename):
                # Y_test is the prediction for subject test from all other subjects
                Y = load_img(masker, X[test_subject, test_runs])
                Y_test = np.zeros_like(Y)

                Y_test, time_pred = predict(Y_test,
                                            algorithm,
                                            name,
                                            train_subjects,
                                            test_subject,
                                            train_runs,
                                            test_runs,
                                            X,
                                            storage_dir)

                print("prediction time: ", time_pred)
                var_e = (Y_test - Y).var(axis=1)
                print("score" + str((1 - var_e).mean()))
                logging.info("score" + str((1 - var_e).mean()))
                exp_var = 1 - var_e
                nib.save(
                    masker.inverse_transform(exp_var),
                    "/storage/workspace/hrichard/results/exp13/" +
                    dataset +
                    "alignment" +
                    "_algo" +
                    name +
                    "subject_" + str(test_subject[0]) +
                    "runs_" + "-".join(map(str, test_runs)) +
                    "_exp13.nii"
                )

                with open("/storage/workspace/hrichard/results/exp13/" +
                    dataset +
                    "_timepred_alignment" +
                    "_algo" +
                    name +
                    "subject_" + str(test_subject[0]) +
                    "runs_" + "-".join(map(str, test_runs)) +
                    "_exp13.nii", "w") as time_file:
                    time_file.write(str(time_pred))

logging.info("Done")
