#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import os
import time

import numpy as np

from nilearn.input_data import NiftiMasker
from nilearn.image import index_img, load_img, math_img
from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
from sklearn.model_selection import ShuffleSplit

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from functional_alignment.utils import score_save_table, make_train_test_images
from functional_alignment.load_data import load_sherlock_data, load_forrest_data, load_ibc_3_mm_data, make_train_test_sherlock, make_train_test_forrest, make_train_test_ibc_3_mm
from functional_alignment.template import TemplateAlignment, euclidian_mean_with_masking
from functional_alignment.utils import score_table


data_dir = os.path.join(THREE_MM, 'group')
dataset = 'ibc'
n_jobs = 3
methods = ["optimal_transport"]
regs = [2, .7, .3]
loss = "zero_mean_r2"
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory, mask_img=mask_path)
masker.fit()
path_to_results = "/storage/tompouce/tbazeill/ibc/template_alignment"
mask = load_img(mask_path)

# Access to the data
if dataset == 'ibc':
    subject_list = np.asarray(SUBJECTS)
    task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                 'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
                 'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm', ]
    df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                     conditions=CONDITIONS, task_list=task_list)
    conditions = df[df.modality == 'bold'].contrast.unique()
    n_conditions = len(conditions)
    rs = ShuffleSplit(n_splits=5, test_size=.4, random_state=0)

    template_folds, mapping_folds, gt_folds = [], [], []

    for train_subjects, test_subjects in rs.split(subject_list):
        template_paths = []
        for subject in subject_list[train_subjects]:
            subject_path = []
            subject_path.extend([df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            subject_path.extend([df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions])
            template_paths.append(subject_path)
        template_folds.append(template_paths)

        test_mapping = []
        test_gt = []
        for subject in subject_list[test_subjects]:
            ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_mapping.append(ap_path)
            pa_path = [df[df.acquisition == 'pa'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            test_gt.append(pa_path)
        mapping_folds.append(test_mapping)
        gt_folds.append(test_gt)

# for template_train_imgs, mapping_test_imgs, gt_test_imgs in zip(template_folds, mapping_folds, gt_folds):
n_pieces = 200
cv = 0
train_index = np.asarray(range(53))
test_index = np.asarray(range(53, 106))
template_train_imgs, mapping_test_imgs, gt_test_imgs = template_folds[
    cv], mapping_folds[cv], gt_folds[cv]
n_bags = 1
for method_alignment in methods:
    for reg in regs:
        for sub, target_img in enumerate(mapping_test_imgs):
            predicted_aligned_imgs = []
            for source_img in template_train_imgs:
                if method_alignment == "optimal_transport":
                    piecewise_estimator = PieceWiseAlignmentOT(n_pieces=n_pieces, method="epsilon_scaling", metric="euclidean", reg=reg, n_bootstrap=n_bags,
                                                               mask=masker, n_jobs=n_jobs)
                else:
                    piecewise_estimator = PieceWiseAlignment(
                        n_pieces=n_pieces, method=method_alignment, n_bootstrap=n_bags, mask=masker, n_jobs=n_jobs)
                piecewise_estimator.fit(
                    index_img(source_img, train_index), target_img)
                predicted_aligned_imgs.append(
                    piecewise_estimator.transform(index_img(source_img, test_index)))
            mean_predicted_img = euclidian_mean_with_masking(
                predicted_aligned_imgs, False, masker)
            mean_predicted_img.to_filename(os.path.join(
                path_to_results, "%s_multi_pred_%s_bootstraps_%s_%s_cv_%s.nii.gz" % (subject_list[test_subjects][sub], str(n_bags), method_alignment, str(reg), str(cv))))

            X_pred = masker.transform(mean_predicted_img)
            X_gt = masker.transform(gt_test_imgs[sub])
            for j, loss in enumerate(["reconstruction_err", "zero_mean_r2"]):
                loss_score = score_table(loss, X_gt, X_pred, 'raw_values')
                pred_im = masker.inverse_transform(loss_score)
                pred_im.to_filename(os.path.join(path_to_results, "%s_multi_pred_%s_bootstraps_%s_%s_cv_%s_%s.nii.gz" % (
                    subject_list[test_subjects][sub], str(n_bags), method_alignment, str(reg), str(cv), loss)))
