import pandas as pd
import sys
import matplotlib.pyplot as plt

print sys.argv[1]
df = pd.read_csv(sys.argv[1], index_col=0)
print df


def plot_synthetic(df):
    # Plot the average r_score of each method against level noise
    ax = df.plot(df.columns[0])
    plt.axhline(color='k')
    plt.xlabel('Signal to noise Ratio (dB)')
    plt.ylabel('Estimator of T R2_score : ||Y- T(X)||^2')
    plt.legend()
    plt.title("Alignment methods performance to estimate T ")
    plt.show()


if __name__ == '__main__':
    plot_synthetic(df)
