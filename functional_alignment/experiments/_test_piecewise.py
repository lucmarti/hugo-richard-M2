import numpy as np
from nilearn.input_data import NiftiMasker
from nilearn import datasets
from functional_alignment.piecewise_alignment import PieceWiseAlignment

from exp_haxby_subject_pairs_alignment import make_train_test_images, score_image, make_train_test_sessions, compute_union_masker, split_train_test

N = 2
subj_1, subj_2 = 0, 1
n_pieces_piecewise = 200
method = "all"
n_jobs = 6


haxby_dataset = datasets.fetch_haxby(subjects=N)
train_session, test_session = make_train_test_sessions(N, haxby_dataset)


masker = compute_union_masker(
    [subj_1, subj_2], haxby_dataset)  # make union mask
X_1_train, X_1_test = split_train_test(
    masker, subj_1, train_session, test_session, haxby_dataset)
X_2_train, X_2_test = split_train_test(
    masker, subj_2, train_session, test_session, haxby_dataset)

Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
    masker, X_1_train, X_2_train, X_1_test)

# PieceWise Alignment

methods_result = []

if method == "all":
    for method_piecewise in ["scaled_orthogonal", "RidgeCV", "RRRCV", "permutation", "mean"]:
        piecewise_estim = PieceWiseAlignment(
            n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, detrend=True, n_jobs=n_jobs)
        piecewise_estim.fit(Im_1_train, Im_2_train)
        Im_1_pred = piecewise_estim.transform(Im_1_test)
        methods_result.append(score_image(masker, Im_1_pred, X_2_test))

else:
    method_piecewise = method
    piecewise_estim = PieceWiseAlignment(
        n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, detrend=True, n_jobs=n_jobs)
    piecewise_estim.fit(Im_1_train, Im_2_train)
    Im_1_pred = piecewise_estim.transform(Im_1_test)
    methods_result.append(score_image(masker, Im_1_pred, X_2_test))
