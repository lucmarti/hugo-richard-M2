
import time
import numpy as np
from scipy.stats import pearsonr
import pandas as pd
import glob

from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.masking import intersect_masks
from nilearn.image import index_img
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM

from nilearn.plotting import plot_stat_map
from nilearn.image import load_img
%matplotlib inline


def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=2):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = "/storage/data/sherlock_movie_watch_dataset/preloaded_masker/mask_all_subj_movie_gm.nii"
    # path + 'preloaded_masker/mask_all_subj_movie.nii'
    return image_list, movie_labels, masker_path


def load_forrest_data(
        dataset="forrest",
        subjects=None,
        runs=None,
        data_path="/storage/data/openfmri/ds113/sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"):
    X = []
    for subject in subjects:
        X_subject = []
        for run in runs:
            func_filename = glob.glob(data_path % (subject, run))[0]
            X_subject.append(func_filename)
        X.append(X_subject)

    return np.array(X), dataset

# Define train/test sessions from labels


def load_ibc_data(path="/storage/tompouce/bthirion/maps_3mm.csv"):
    ibc = pd.DataFrame.from_csv(
        path, sep=",", index_col=0)
    image_list = []
    for subj_i in range(1, 16):
        subj_data = ibc.loc[ibc['subject'] ==
                            "sub-" + "{0:0=2d}".format(subj_i)]
        filtered_data = subj_data.drop_duplicates(
            subset="contrast", keep='last')
        if not filtered_data.empty:
            image_list.append(filtered_data)
    return image_list


def make_train_test_sessions_haxby(n_subjects, haxby_dataset):
    train_session = []
    test_session = []
    for i in range(n_subjects):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks = labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1

        train_labels = labels[condition_mask & train_chunks].sort_values(by=[
                                                                         'chunks', 'labels'])
        test_labels = labels[condition_mask & test_chunks].sort_values(
            by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)
    return train_session, test_session


def make_train_test_sherlock(image_list, labels, masker, n_max=None):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Extract scenes
        Parameters :
        - List of int fold out of 50 scenes
        - Dataframe labels (scene, onset, offset)
        - List of nii files subject_imgs

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass
    if n_max is not None:
        train_session = [train_session[i] for i in range(n_max)]

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list


def make_train_test_forrest(image_list, masker):
    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train, X_test = [], []
        for j in range(7):
            if j < 4:
                X_train.append(masker.transform(image_list[i][j]))
            else:
                X_test.append(masker.transform(image_list[i][j]))
        X_train_list.append(np.vstack(X_train))
        X_test_list.append(np.vstack(X_test))
    return X_train_list, X_test_list


def make_train_test_ibc(
        image_list, train_tasks, test_tasks, masker):
    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train, X_test = [], []
        for index, row in image_list[i].iterrows():
            if row["task"] in (train_tasks):
                X_train.append(masker.transform(row["path"]))
            elif row["task"] in (test_tasks):
                X_test.append(masker.transform(row["path"]))
        X_train_list.append(np.vstack(X_train))
        X_test_list.append(np.vstack(X_test))
    return X_train_list, X_test_list


def fetch_vt_mask_haxby(subj, haxby_dataset):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def compute_union_vt_masker_haxby(subjs, haxby_dataset):
    mask_list = []
    for subj in subjs:
        mask_list.append(haxby_dataset.mask_vt[subj])
    union_mask = intersect_masks(mask_list, threshold=0, connected=False)
    masker = NiftiMasker(union_mask, smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def split_train_test(masker, subj, train_session, test_session, haxby_dataset):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def apply_method_calculate_loss(methods, loss, masker, X_1_train, X_1_test, X_2_train, X_2_test, path_to_results, results_title, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, piecewise_methods=["mean"], n_jobs=1, n_bootstraps=[1], srm_components=20):

    # Turning train / test set to images through unmasking if needed
    # if any(image_method in '\_'.join(methods) for image_method in ["piecewise", "srm"]):
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)
    for method in methods:
        if method == 'identity':
            X_1_pred = X_1_test
            score_save_table(masker, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # Hyperalignment   !!! for now we have to transpose X_train and X_test
        if method == 'scaled_orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = sc * R.dot((X_1_test.T)).T
            score_save_table(masker, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        if method == 'orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = R.dot((X_1_test.T)).T
            score_save_table(masker, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # Ridge Alignment with Cross-Validation
        if method == 'ridge_cv':
            ridge_estim = RidgeCV(alphas=alphas_ridge,
                                  n_splits=n_cv_splits)
            ridge_estim.fit(X_1_train, X_2_train)
            X_1_pred = ridge_estim.transform(X_1_test)
            score_save_table(masker, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # PieceWise Alignment
        if method == "piecewise":
            for method_piecewise in piecewise_methods:
                for n_bootstrap in n_bootstraps:
                    piecewise_estim = PieceWiseAlignment(
                        n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                    piecewise_estim.fit(Im_1_train, Im_2_train)
                    Im_1_pred = piecewise_estim.transform(Im_1_test)

                    X_1_pred = masker.transform(Im_1_pred)
                    method_header = str(
                        n_bootstrap) + "_bootstraps_" + method + "_" + method_piecewise
                    score_save_table(
                        masker, path_to_results, method_header, results_title, X_2_test, X_1_pred)
        if method == "piecewise_srm":
            for n_bootstrap in n_bootstraps:
                piecewise_srm_estim = PieceWiseAlignment_DeterministicSRM(
                    n_pieces=n_pieces_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap, n_components=srm_components)
                piecewise_srm_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_srm_estim.transform(
                    [Im_1_test], index=0, index_inverse=1)
                # Scoring
                X_1_pred = masker.transform(Im_1_pred)
                method_header = str(n_bootstrap) + "_bootstraps_" + method
                score_save_table(masker, path_to_results,
                                 method_header, results_title, X_2_test, X_1_pred)
        # Deterministic SRM
        if method == "srm":
            srm_estim = DeterministicSRM(
                n_components=srm_components, max_iter=10, mask=masker, n_jobs=n_jobs)
            srm_estim.fit([Im_1_train, Im_2_train])
            shared_response = srm_estim.transform([[Im_1_test]], index=[0])
            X_1_pred = srm_estim.basis[1].dot(shared_response).T
            score_save_table(masker, path_to_results, method,
                             results_title, X_2_test, X_1_pred)


def score_table(loss, X_gt, X_pred, multioutput='raw_values'):
    if loss is "r2":
        score = r2_score(X_gt, X_pred, multioutput=multioutput)
    elif loss is "zero_mean_r2":
        score = zero_mean_coefficient_determination(
            X_gt, X_pred, multioutput=multioutput)
    else:
        raise NameError("Unknown loss, it should be 'r2' or 'zero_mean_r2'")
    return np.maximum(score, -1)


def score_save_table(masker, path_to_results, header, termination, X_gt, X_pred,  multioutput='raw_values'):
    X_score = score_table(loss, X_gt, X_pred, multioutput)
    masker.inverse_transform(X_score)
    Im_score = masker.inverse_transform(X_score)
    Im_score.to_filename(path_to_results + "/" +
                         header + "/" + termination)


def zero_mean_coefficient_determination(y_true, y_pred, sample_weight=None, multioutput="uniform_average"):
    if y_true.ndim == 1:
        y_true = y_true.reshape((-1, 1))

    if y_pred.ndim == 1:
        y_pred = y_pred.reshape((-1, 1))

    if sample_weight is not None:
        weight = sample_weight[:, np.newaxis]
    else:
        weight = 1.

    numerator = (weight * (y_true - y_pred) ** 2).sum(axis=0, dtype=np.float64)
    denominator = (weight * (y_true) ** 2).sum(axis=0, dtype=np.float64)
    nonzero_denominator = denominator != 0
    nonzero_numerator = numerator != 0
    valid_score = nonzero_denominator & nonzero_numerator
    output_scores = np.ones([y_true.shape[1]])
    output_scores[valid_score] = 1 - (numerator[valid_score] /
                                      denominator[valid_score])
    output_scores[nonzero_numerator & ~nonzero_denominator] = 0

    if multioutput == 'raw_values':
        # return scores individually
        return output_scores
    elif multioutput == 'uniform_average':
        # passing None as weights results is uniform mean
        avg_weights = None
    elif multioutput == 'variance_weighted':
        avg_weights = (weight * (y_true - np.average(y_true, axis=0,
                                                     weights=sample_weight)) ** 2).sum(axis=0, dtype=np.float64)
        # avoid fail on constant y or one-element arrays
        if not np.any(nonzero_denominator):
            if not np.any(nonzero_numerator):
                return 1.0
            else:
                return 0.0
    else:
        avg_weights = multioutput

    return np.average(output_scores, weights=avg_weights)


# Experiment parameters
# Methods tested
all_methods = ["identity", "scaled_orthogonal",
               "ridge_cv", "piecewise", "srm", "piecewise_srm"]
methods = ["piecewise"]
# alignment method for piecewise
# in "scaled_orthogonal", "RidgeCV"
piecewise_methods = ["RidgeCV"]
n_bootstraps = [20]  # bootstraping for piecewise

# Experiment crucial parameters
loss = "r2"  # "r2" or "zero_mean_r2"
n_jobs = 3

# Dataset and pairs of subjects tried
dataset = "sherlock"
n_max_sherlock = None  # number of timeframes in train set
all_forrest_pairs = [(4, 5), (6, 11), (5, 6), (12, 2),
                     (3, 8), (9, 10), (14, 16), (15, 3), (4,
                                                          9), (1, 15), (2, 13), (9, 13), (8, 7),
                     (8, 1), (3, 6), (5, 11), (10, 11), (4, 14), (16, 2)]
subject_pair_list = [(6, 11)]

# Method specific parameters
n_splits = 4  # n_subjectsumber of folds for validations
alphas_ridge = (0.1, 10, 500, 2000, 10000, 100000)  # Ridge-CV params
ks_rrr = [10, 20, 50]  # RRR-CV params
alphas_rrr = [0.1, 10, 1000]  # RRR-CV params
n_pieces = 150
srm_components = 15  # number of pieces for piecewise alignement


# Data preparation
start_time = time.time()
if dataset == "sherlock":
    n_subjects = 16
    mask_memory = "/storage/tompouce/tbazeill/sherlock_cache"
    image_list, movie_labels, masker_path = load_sherlock_data(
        n_subjects=n_subjects)

    masker = NiftiMasker(memory=mask_memory)
    masker.fit(masker_path)
    path_to_results = "/storage/tompouce/tbazeill/sherlock/"

#    X_train_list, X_test_list = make_train_test_sherlock(
#        image_list, movie_labels, masker)
elif dataset == "forrest":
    n_subjects = 20
    mask_path = "/storage/data/openfmri/ds113/3mm_mask_gm.nii.gz"
    # "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"
    mask_memory = "/storage/tompouce/tbazeill/forrest_cache"
    data_path = "/storage/data/openfmri/ds113/sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz"
    subjects = list(range(1, n_subjects + 1))
    subjects.remove(10)
    runs = list(range(1, 8))
    image_list, dataset = load_forrest_data(
        dataset="forrest", subjects=subjects, runs=runs, data_path=data_path)
    masker = NiftiMasker(standardize=True, detrend=True, smoothing_fwhm=3,
                         memory_level=5, memory=mask_memory, t_r=2)
    masker.fit(mask_path)
    path_to_results = "/storage/tompouce/tbazeill/forrest/"

elif dataset == "ibc":
    data_path = "/storage/tompouce/bthirion/maps_3mm.csv"
    mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
    mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
    # 2, 9 not working i.e. subjects 3 and 10 :
    image_list = load_ibc_data(path=data_path)
    train_tasks = ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "rsvp_language"]
    test_tasks = ["hcp_gambling",
                  "hcp_motor", "hcp_emotion", "hcp_relational", "hcp_wm", "hcp_language", "hcp_social"]
    print("Training tasks : %s" % train_tasks)
    print("Testing tasks : %s" % test_tasks)
    path_to_results = "/storage/tompouce/tbazeill/ibc/"
    masker = NiftiMasker(
        memory_level=5, memory=mask_memory)
    masker.fit(mask_path)


elif dataset == "haxby":
    haxby_dataset = datasets.fetch_haxby(subjects=n_subjects)
    train_session, test_session = make_train_test_sessions_haxby(
        n_subjects, haxby_dataset)
    masker = NiftiMasker(haxby_dataset.mask, detrend=True)
    # Main loop computing X_train and X_test and testing all methods for each pair of subjects #
    X_train_list, X_test_list = [], []
    for subject in range(n_subjects):
        X_train_i, X_test_i = split_train_test(
            masker, subject, train_session, test_session, haxby_dataset)
        X_train_list.append(X_train_i)
        X_test_list.append(X_test_i)

print("Path to results is %s " % path_to_results)
print("Dataset %s loaded in %s seconds " %
      (dataset, time.time() - start_time))
# Alignment method test : for each pair of subject, try all chosen methods
"""
for subject_pair in subject_pair_list:
    start_time = time.time()
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
    if dataset is "sherlock":
        X_train_list, X_test_list = make_train_test_sherlock(
            pair_image_list, movie_labels, masker, n_max=n_max_sherlock)
    elif dataset is "forrest":
        X_train_list, X_test_list = make_train_test_forrest(
            pair_image_list, masker)
    elif dataset is "ibc":
        X_train_list, X_test_list = make_train_test_ibc(
            pair_image_list, train_tasks, test_tasks, masker)

    print("Data masked in : %s seconds" % str(time.time() - start_time))
    start_time = time.time()
    print("Aligning pair : %s" % str(subject_pair))
    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    results_title = str(subj_1) + "_" + \
        str(subj_2) + "_" + loss + ".nii.gz"
    # try every method in the library
    apply_method_calculate_loss(methods, loss,
                                masker, X_1_train, X_1_test, X_2_train, X_2_test, path_to_results, results_title, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, piecewise_methods, n_jobs, n_bootstraps, srm_components)
    print("Methods fitted in %s seconds " %
          str(time.time() - start_time))
    # Creating aligned pair voxelwise R2 and correlation images

"""

subject_pair = (6, 11)
subj_1, subj_2 = subject_pair[0], subject_pair[1]
pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
X_train_list, X_test_list = make_train_test_sherlock(
    pair_image_list, movie_labels, masker, )
X_1_train, X_1_test = X_train_list[0], X_test_list[0]
X_2_train, X_2_test = X_train_list[1], X_test_list[1]

Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
    masker, X_1_train, X_2_train, X_1_test)

piecewise_estim = PieceWiseAlignment(
    n_pieces=150, method='RidgeCV', mask=masker, n_jobs=n_jobs)
piecewise_estim.fit(Im_1_train, Im_2_train)
Im_1_pred = piecewise_estim.transform(Im_1_test)

X_1_pred = masker.transform(Im_1_pred)
sherlock_6_11 = score_table(loss, X_2_test, X_1_pred, multioutput='raw_values')


display = plot_stat_map(masker.inverse_transform(sherlock_6_11), vmax=0.5)
loss = "r2"
method = "1_bootstraps_piecewise_RidgeCV"
pairs_to_load = [subject_pair]
if dataset is "sherlock":
    mask_memory = "/storage/tompouce/tbazeill/sherlock_cache"
    masker_path = "/storage/data/sherlock_movie_watch_dataset/preloaded_masker/mask_all_subj_movie.nii"
    masker = NiftiMasker(standardize=True, memory=mask_memory)
    masker.fit(masker_path)
    path_to_results = "/storage/tompouce/tbazeill/" + dataset + "/"
pair = subject_pair
subj_1, subj_2 = pair[0], pair[1]
pair_loss = []
path = path_to_results + \
    method + "/" + str(subj_1) + "_" + str(subj_2)
Im_history = load_img(path + "_" + loss + ".nii.gz")
R2_history = masker.transform(Im_history)
display = plot_stat_map(Im_history, vmax=0.5)
