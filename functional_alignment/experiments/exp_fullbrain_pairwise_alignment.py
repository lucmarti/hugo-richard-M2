#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""

import time
import glob

from nilearn.input_data import NiftiMasker
from nilearn.image import index_img

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.ot_pairwise_hyperalignment import PieceWiseAlignmentOT
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM
from functional_alignment.utils import score_save_table, make_train_test_images
from functional_alignment.load_data import load_sherlock_data, load_forrest_data, load_ibc_3_mm_data, make_train_test_sherlock, make_train_test_forrest, make_train_test_ibc_3_mm


def apply_method_calculate_loss(methods, loss, masker, X_1_train, X_1_test, X_2_train, X_2_test, path_to_results, results_title, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, piecewise_methods=["mean"], n_jobs=1, n_bootstraps=[1], srm_components=20, regs=[10, 5, 2, 1, 0.7, 0.3, 0.1, 10e-2, 10e-3], source_sub=None, target_sub=None):

    # Turning train / test set to images through unmasking if needed
    # if any(image_method in '\_'.join(methods) for image_method in ["piecewise", "srm"]):
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)
    for method in methods:
        if method == 'identity':
            X_1_pred = X_1_test
            score_save_table(masker, loss, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # Hyperalignment   !!! for now we have to transpose X_train and X_test
        if method == 'scaled_orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = sc * R.dot((X_1_test.T)).T
            score_save_table(masker, loss, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        if method == 'orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = R.dot((X_1_test.T)).T
            score_save_table(masker, loss, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # Ridge Alignment with Cross-Validation
        if method == 'ridge_cv':
            ridge_estim = RidgeCV(alphas=alphas_ridge,
                                  n_splits=n_cv_splits)
            ridge_estim.fit(X_1_train, X_2_train)
            X_1_pred = ridge_estim.transform(X_1_test)
            score_save_table(masker, loss, path_to_results, method,
                             results_title, X_2_test, X_1_pred)
        # PieceWise Alignment
        if method == "piecewise":
            for method_piecewise in piecewise_methods:
                for n_bootstrap in n_bootstraps:
                    piecewise_estim = PieceWiseAlignment(
                        n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                    piecewise_estim.fit(Im_1_train, Im_2_train)
                    Im_1_pred = piecewise_estim.transform(Im_1_test)

                    X_1_pred = masker.transform(Im_1_pred)
                    method_header = str(
                        n_bootstrap) + "_bootstraps_" + method + "_" + method_piecewise
                    score_save_table(
                        masker, loss, path_to_results, method_header, results_title, X_2_test, X_1_pred)
        if method == "piecewise_srm":
            for n_bootstrap in n_bootstraps:
                piecewise_srm_estim = PieceWiseAlignment_DeterministicSRM(
                    n_pieces=n_pieces_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap, n_components=srm_components)
                piecewise_srm_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_srm_estim.transform(
                    [Im_1_test], index=0, index_inverse=1)
                # Scoring
                X_1_pred = masker.transform(Im_1_pred)
                method_header = str(n_bootstrap) + "_bootstraps_" + method
                score_save_table(masker, loss, path_to_results,
                                 method_header, results_title, X_2_test, X_1_pred)

        if method == "optimal_transport":
            for n_bootstrap in n_bootstraps:
                for reg in regs:
                    piecewise_estim = PieceWiseAlignmentOT(
                        n_pieces=n_pieces_piecewise, method="epsilon_scaling", metric="euclidean", reg=reg, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                    piecewise_estim.fit(Im_1_train, Im_2_train)
                    Im_1_pred = piecewise_estim.transform(Im_1_test)
                    X_1_pred = masker.transform(Im_1_pred)
                    method_header = str(
                        n_bootstrap) + "_bootstraps_piecewise_" + method
                    for loss in ["corr", "zero_mean_r2"]:
                        results_title = str(source_sub) + "_" + \
                            str(target_sub) + "_" + str(reg) + loss + ".nii.gz"
                        score_save_table(
                            masker, loss, path_to_results, method_header, results_title, X_2_test, X_1_pred)
        # Deterministic SRM
        if method == "srm":
            srm_estim = DeterministicSRM(
                n_components=srm_components, max_iter=10, mask=masker, n_jobs=n_jobs)
            srm_estim.fit([Im_1_train, Im_2_train])
            shared_response = srm_estim.transform([[Im_1_test]], index=[0])
            X_1_pred = srm_estim.basis[1].dot(shared_response).T
            score_save_table(masker, loss, path_to_results, method,
                             results_title, X_2_test, X_1_pred)



# Experiment parameters
# Methods tested
all_methods = ["identity", "scaled_orthogonal",
               "ridge_cv", "piecewise", "srm", "piecewise_srm"]
methods = ["optimal_transport"]
# alignment method for piecewise
# in "scaled_orthogonal", "RidgeCV"
piecewise_methods = ["permutation"]
n_bootstraps = [1]  # bootstraping for piecewise

# Experiment crucial parameters
loss = "corr"  # "r2" or "zero_mean_r2" or 'corr'
n_jobs = 4

# Dataset and pairs of subjects tried
dataset = "sherlock"
n_max_sherlock = None  # number of timeframes in train set
if dataset == "ibc":
    all_pairs = [(12, 8), (2, 5), (2, 8), (3, 11),
                 (3, 1), (4, 6), (7, 10), (9, 1), (6, 3), (7, 9), (10, 1), (5, 12), (7, 4), (8, 6), (2, 11), (3, 5), (11, 7), (4, 12), (9, 10)]
elif dataset == "forrest":
    all_pairs = [(4, 5), (6, 11), (5, 6), (12, 2),
                 (3, 8), (9, 10), (14, 16), (15, 3), (4,
                                                      9), (1, 15), (2, 13), (9, 13), (8, 7),
                 (8, 1), (3, 6), (5, 11), (10, 11), (4, 14), (16, 2)]
elif dataset == "sherlock":
    all_pairs = [(4, 5), (6, 11), (5, 6), (12, 2),
                 (3, 8), (9, 10), (14, 16), (15, 3), (4,
                                                      9), (1, 15), (2, 13), (9, 13), (8, 7),
                 (8, 1), (3, 6), (5, 11), (10, 11), (4, 14), (16, 2)]
subject_pair_list = all_pairs

# Method specific parameters
n_splits = 4  # n_subjectsumber of folds for validations
alphas_ridge = (0.1, 10, 500, 2000, 10000, 100000)  # Ridge-CV params
ks_rrr = [10, 20, 50]  # RRR-CV params
alphas_rrr = [0.1, 10, 1000]  # RRR-CV params
n_pieces = 200
srm_components = 15  # dnumber of pieces for piecewise alignement


# Data preparation
start_time = time.time()
if dataset == "sherlock":
    n_subjects = 16

    image_list, movie_labels, masker_path = load_sherlock_data(
        n_subjects=n_subjects)

    mask_memory = "/storage/tompouce/tbazeill/sherlock_cache"
    masker = NiftiMasker(memory=mask_memory)
    masker.fit(masker_path)
    path_to_results = "/storage/tompouce/tbazeill/sherlock/"

elif dataset == "forrest":
    n_subjects = 20

    image_list, mask_path = load_forrest_data(n_subjects=20)

    mask_memory = "/storage/tompouce/tbazeill/forrest_cache"
    masker = NiftiMasker(standardize=True, detrend=True, smoothing_fwhm=3,
                         memory_level=5, memory=mask_memory, t_r=2)
    masker.fit(mask_path)
    path_to_results = "/storage/tompouce/tbazeill/forrest/"
    # other_mask "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/bold_dico_brainmask_dico7Tad2grpbold7Tad_nl.nii.gz"

elif dataset == "ibc":

    # 2, 9 not working i.e. subjects 3 and 10 :
    image_list, mask_path = load_ibc_3_mm_data()

    mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
    masker = NiftiMasker(
        memory_level=5, memory=mask_memory)
    masker.fit(mask_path)

    train_tasks = ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "rsvp_language"]
    test_tasks = ["hcp_gambling",
                  "hcp_motor", "hcp_emotion", "hcp_relational", "hcp_wm", "hcp_language", "hcp_social"]
    print("Training tasks : %s" % train_tasks)
    print("Testing tasks : %s" % test_tasks)
    path_to_results = "/storage/tompouce/tbazeill/ibc/"


print("Path to results is %s " % path_to_results)
print("Dataset %s loaded in %s seconds " %
      (dataset, time.time() - start_time))
# Alignment method test : for each pair of subject, try all chosen methods


for subject_pair in subject_pair_list:
    start_time = time.time()
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
    if dataset is "sherlock":
        X_train_list, X_test_list = make_train_test_sherlock(
            pair_image_list, movie_labels, masker, n_max=n_max_sherlock)
    elif dataset is "forrest":
        X_train_list, X_test_list = make_train_test_forrest(
            pair_image_list, masker)
    elif dataset is "ibc":
        X_train_list, X_test_list = make_train_test_ibc_3_mm(
            pair_image_list, train_tasks, test_tasks, masker)

    print("Data masked in : %s seconds" % str(time.time() - start_time))
    start_time = time.time()
    print("Aligning pair : %s" % str(subject_pair))
    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    results_title = str(subj_1) + "_" + \
        str(subj_2) + "_" + loss + ".nii.gz"
    # try every method in the library
    apply_method_calculate_loss(methods, loss,
                                masker, X_1_train, X_1_test, X_2_train, X_2_test, path_to_results, results_title, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, piecewise_methods, n_jobs, n_bootstraps, srm_components, source_sub=subj_1, target_sub=subj_2)
    print("Methods fitted in %s seconds " %
          str(time.time() - start_time))
    # Creating aligned pair voxelwise R2 and correlation images
