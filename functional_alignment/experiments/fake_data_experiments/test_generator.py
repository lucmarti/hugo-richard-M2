from generator import create_orthogonal_matrix, generate_data
from sklearn.utils.testing import assert_array_almost_equal
import numpy as np

a = np.random.randint(1, 20)
b = np.random.randint(1, 20)

v = max(a, b)
k = min(a, b)
t = np.random.randint(1, 20)

# Orthogonal matrix
W = create_orthogonal_matrix(v, k)


def test_generate_data_shapes():
    loads = [np.eye(3, 2), np.eye(3, 2)]
    latent_variance = np.random.rand(2, 2)
    latent_variance = (latent_variance.T).dot(latent_variance)
    noise_variance = np.random.rand(2)**2
    X = generate_data(loads, latent_variance, noise_variance)
    print(X)
    assert len(X) == 2
    for i in range(len(X)):
        assert X[i].shape == (3, 2)