import nibabel as nib
import numpy as np
from ridge_cv import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
import os
import logging
from nilearn.image import index_img
from deterministic_srm import DeterministicSRM
from template_alignment import TemplateAlignment
from glob import glob
from sklearn.externals.joblib import Memory
from exp import exp13
n_jobs = 1

#IBC_config
logging.basicConfig(level=logging.DEBUG, filename="logfile_map_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
dataset = "ibc"
mask_dir = "/storage/store/data/ibc/derivatives/"
mask_path = "/storage/tompouce/bthirion/mask_ibc_roi.nii.gz" # mask_dir + "group/gm_mask.nii.gz"

data_dir = "/storage/store/data/ibc/derivatives/"
data_path = data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"

memory = Memory("/storage/tompouce/bthirion/ibc/ibc_cache")

subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
runs = ["Trn01", "Trn02", "Trn03", "Trn04", "Trn05", "Trn06", "Trn07", "Trn08", "Trn09",
        "Val01", "Val02", "Val03", "Val04", "Val05", "Val06", "Val07", "Val08"]

mask_params = {'mask':mask_path,
               'standardize':True,
               'detrend':True,
               'memory':memory,
               'low_pass':0.1,
               'high_pass':0.01,
               't_r':2,
               'n_jobs':n_jobs}

label_path_files = "/storage/workspace/bthirion/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/bthirion/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/bthirion/results/exp9/label_piecewise_alignment_ibc.nii"
print("Loading of data ... Done")

algorithms = [
    (DeterministicSRM(n_components=20, **mask_params), "srm_ortho"),
    (PieceWiseAlignment(n_pieces=1000, method="hyperalignment", **mask_params),"hyperalignment"),
    (PieceWiseAlignment(n_pieces=1000, method=RidgeCV(alphas=(1e3, 1e4, 1e5)), perturbation=True, **mask_params),"ridgeCV"),
    (PieceWiseAlignment(n_pieces=1000, method="mean", **mask_params),"mean"),
    (TemplateAlignment(mapping=PieceWiseAlignment(n_pieces=1000, method=RidgeCV(alphas=(1e3, 1e4, 1e5)), perturbation=True, **mask_params),
                       **mask_params), "RCV_template")]
# Loading data files
X = []
for subject in subjects:
    X_subject = []
    for run in runs:
        func_filename = glob((data_path) % (subject, run))[0]
        X_subject.append(func_filename)
    X.append(X_subject)
X = np.array(X)

exp13(subjects, algorithms, dataset, X)
