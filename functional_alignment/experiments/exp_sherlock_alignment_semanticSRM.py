import numpy as np
import pandas as pd
from nilearn.image import index_img

from functional_alignment.pca_srm import PCASRM
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.dictionary_srm import DictionarySRM
from functional_alignment.hyperalignment import hyperalign
from sklearn.linear_model import RidgeCV
import logging
from scipy.stats import pearsonr
from time import time
import matplotlib as mpl
from sklearn.cross_validation import train_test_split
from functional_alignment.exp_sherlock_utils import *
mpl.use('Agg')

logging.basicConfig(level=logging.DEBUG, filename="logfile_sherlock_decoding", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

# affine = np.array([[   3.,    0.,    0.,  -90.],
#        [   0.,    3.,    0., -126.],
#        [   0.,    0.,    3.,  -72.],
#        [   0.,    0.,    0.,    1.]])
# mask_data = np.load("../sherlock_mask/sherlock1FR_17ss_ISC_thr0.2.nii.npz")["mask"].reshape(61, 73, 61)
# mask_img = nib.Nifti1Image(mask_data, affine)
# nib.save(mask_img, "sherlock1FR_17ss_ISC_thr0.2.nii")

def exp_shift():
    """
    Test the influence of shifting
    Result: optimal shifting is no shifting.
    """
    fmri = load_fmri()
    semantics = load_semantic()

    print("shapes")
    print(fmri.shape)
    print(semantics[0].shape)
    print(semantics[1].shape)
    print("done")

    # The cuts are there because they might be border effects with the
    # semantic_representation we use.
    # However we checked that a shift does not improve results so that
    # text_representation and fmri data are aligned.

    fmri_train_slice = slice(10, 975 - 10)
    fmri_test_slice = slice(975 + 10, 1950 - 15)

    for shift in range(-10, 10):
        semantic_train_slice = slice(10 + shift, 975 - 10 + shift)
        semantic_test_slice = slice(975 + 10 + shift, 1950 - 15 + shift)

        for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
            R, _ = hyperalign(fmri[fmri_train_slice].T,
                              semantic[semantic_train_slice].T)
            predicted_semantic = (R.dot(fmri[fmri_test_slice].T)).T
            # ridge = RidgeCV(alphas=[0.0001, 0.00001], cv=2)
            # ridge.fit(fmri[fmri_train_slice], semantic[semantic_train_slice])
            # print("alpha", ridge.alpha_)
            # predicted_semantic = ridge.predict(fmri[fmri_test_slice])
            true_semantic = semantic[semantic_test_slice]
            error = scene_decoding25(predicted_semantic, true_semantic)
            print(shift, error)


def exp_semanticSRM_decoding_25():
    """
    Exact same experiment than semantic_SRM
    """
    result = {}
    for algorithm in ["PCA_50", "FastDictSRM_50_1", "ProbSRM_20", "ProbSRM_50", "DetSRM_50"]:
        fmri = load_fmri(algo_name=algorithm, mask_name="Fullbrain")
        semantics = load_semantic()

        train_slice = slice(0, 975)
        test_slice = slice(975, 1950)

        print(algorithm)
        for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
            R, _ = hyperalign(fmri[train_slice].T,
                              semantic[train_slice].T)
            predicted_semantic = (R.dot(fmri[test_slice].T)).T
            true_semantic = semantic[test_slice]
            error = scene_decoding25(predicted_semantic, true_semantic)
            result[semantic_name, algorithm] = error
            print(error)

    return result


def exp_decoding():
    """
    Same experiment than before but we try to differentiate between scenes
    that represent a different part of the story
    """
    for algo_name in ["PCA_50", "FastDictSRM_50_1", "ProbSRM_20", "ProbSRM_50", "DetSRM_50"]:
        fmri = load_fmri(label_path="/storage/data/sherlock_movie_watch_dataset/labels/movie_scene_labels.csv",
                         algo_name=algo_name)
        semantics = load_semantic(labels_path="/storage/data/sherlock_movie_watch_dataset/labels/movie_scene_labels.csv")
        slices = load_scenes_id(labels_path="/storage/data/sherlock_movie_watch_dataset/labels/movie_scene_labels.csv")

        train_slice = slice(slices[0, 0], slices[24, 1])
        test_slice = slice(slices[25, 0], slices[49, 1])

        print(algo_name)
        for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
            R, _ = hyperalign(fmri[train_slice].T,
                              semantic[train_slice].T)
            predicted_semantic = (R.dot(fmri[test_slice].T)).T
            true_semantic = semantic[test_slice]
            n_max = len(predicted_semantic) / 25 * 25
            error = scene_decoding25(predicted_semantic[:n_max], true_semantic[:n_max])
            print(error)


def exp_original_semanticSRM25_CV():
    """
    Original experiment of the paper but with extensive cross validation and more training
    """

    images_path = "/storage/data/sherlock_movie_watch_dataset/movie_files/sherlock_movie_s%i.nii"
    mask_memory = "/storage/tompouce/hrichard/sherlock"
    mask_img = "../sherlock_mask/mask_all_subj_movie.nii"
    result_directory = "/storage/workspace/hrichard/M2_internship/results/semantic_srm/"
    mask_name = "Fullbrain"

    semantics = load_semantic()
    data = np.array(load_sherlock_data(images_path))

    exp_params = {
    "detrend": True,
    "standardize": True,
    "memory": mask_memory,
    "mask": mask_img,
    "mask_strategy": "epi",
    "n_jobs": 1,
    "memory_level": 5,
    "t_r": 2,
    "verbose": 1
    }




    algorithms = [
    (PCASRM(n_components=50, **exp_params), "PCA_50"),
    (DictionarySRM(50, alpha=1, init_method="ortho_srm", **exp_params), "FastDictSRM_50_1"),
    (ProbabilisticSRM(n_components=20, **exp_params), "ProbSRM_20"),
    (ProbabilisticSRM(n_components=50, **exp_params), "ProbSRM_50"),
    (DeterministicSRM(n_components=50, **exp_params), "DetSRM_50"),
    ]

    np.random.seed(0)
    n_splits = 10
    res = {}
    for i in range(n_splits):
        print("split: " + str(i))
        train_split, test_split = train_test_split(np.array(range(50)), test_size=25)
        for algorithm, name in algorithms:
            print(name)
            t0 = time()
            algorithm.fit(data[:, train_split])
            fitting_time = time() - t0
            print("fitting time", fitting_time)
            shared = algorithm.transform(data)
            transform_time = time() - fitting_time - t0
            print("transform time", transform_time)
            with open("./log", "a") as log:
                log.write(name + "\n")
                log.write("fitting time: " + str(fitting_time) + "\n")
                log.write("transform time: " + str(transform_time) + "\n")

            np.save(result_directory + "split_" + str(i) + "label_none" + "mask_" + mask_name + "algo_" + name, shared)

            shared = shared.T
            shared = np.array(np.split(shared, 50, axis=0))
            for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
                semantic = np.array(np.split(semantic, 50, axis=0))
                shared_train = np.concatenate(shared[train_split], axis=0)
                shared_test = np.concatenate(shared[test_split], axis=0)
                semantic_train = np.concatenate(semantic[train_split], axis=0)
                semantic_test = np.concatenate(semantic[test_split], axis=0)
                R, _ = hyperalign(shared_train.T,
                                  semantic_train.T)
                predicted_semantic = (R.dot(shared_test.T)).T
                true_semantic = semantic_test
                error = scene_decoding25(predicted_semantic, true_semantic)
                res[i, name, semantic_name] = error
                print(error)
    np.save(result_directory + "semantic_results", res)


def exp_original_semanticSRM25CV_ThresholdedFullBrain():
    """
    Original experiment of the paper but with extensive cross validation and more training
    """

    images_path = "/storage/data/sherlock_movie_watch_dataset/movie_files/sherlock_movie_s%i.nii"
    mask_memory = "/storage/tompouce/hrichard/sherlock"
    mask_img = "../sherlock_mask/sherlock1FR_17ss_ISC_thr0.2.nii"
    result_directory = "/storage/workspace/hrichard/M2_internship/results/semantic_srm/"
    mask_name = "ThresholdedFullbrain"

    semantics = load_semantic()
    data = np.array(load_sherlock_data(images_path))

    exp_params = {
    "detrend": True,
    "standardize": True,
    "memory": mask_memory,
    "mask": mask_img,
    "mask_strategy": "epi",
    "n_jobs": 1,
    "memory_level": 5,
    "t_r": 2,
    "verbose": 1
    }




    algorithms = [
    (PCASRM(n_components=50, **exp_params), "PCA_50"),
    (DictionarySRM(50, alpha=1, init_method="ortho_srm", **exp_params), "FastDictSRM_50_1"),
    (ProbabilisticSRM(n_components=20, **exp_params), "ProbSRM_20"),
    (ProbabilisticSRM(n_components=50, **exp_params), "ProbSRM_50"),
    (DeterministicSRM(n_components=50, **exp_params), "DetSRM_50"),
    ]

    np.random.seed(0)
    n_splits = 10
    res = {}
    for i in range(n_splits):
        print("split: " + str(i))
        train_split, test_split = train_test_split(np.array(range(50)), test_size=25)
        for algorithm, name in algorithms:
            print(name)
            t0 = time()
            algorithm.fit(data[:, train_split])
            fitting_time = time() - t0
            print("fitting time", fitting_time)
            shared = algorithm.transform(data)
            transform_time = time() - fitting_time - t0
            print("transform time", transform_time)
            with open("./log", "a") as log:
                log.write(name + "\n")
                log.write("fitting time: " + str(fitting_time) + "\n")
                log.write("transform time: " + str(transform_time) + "\n")

            np.save(result_directory + "split_" + str(i) + "label_none" + "mask_" + mask_name + "algo_" + name, shared)

            shared = shared.T
            shared = np.array(np.split(shared, 50, axis=0))
            for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
                semantic = np.array(np.split(semantic, 50, axis=0))
                shared_train = np.concatenate(shared[train_split], axis=0)
                shared_test = np.concatenate(shared[test_split], axis=0)
                semantic_train = np.concatenate(semantic[train_split], axis=0)
                semantic_test = np.concatenate(semantic[test_split], axis=0)
                R, _ = hyperalign(shared_train.T,
                                  semantic_train.T)
                predicted_semantic = (R.dot(shared_test.T)).T
                true_semantic = semantic_test
                error = scene_decoding25(predicted_semantic, true_semantic)
                res[i, name, semantic_name] = error
                print(error)
    np.save(result_directory + "semantic_results", res)


def exp_original_semanticSRM_5_CVFullBrain():
    images_path = "/storage/data/sherlock_movie_watch_dataset/movie_files/sherlock_movie_s%i.nii"
    mask_memory = "/storage/tompouce/hrichard/sherlock"
    mask_img = "../sherlock_mask/mask_all_subj_movie.nii"
    result_directory = "/storage/workspace/hrichard/M2_internship/results/semantic_srm/"
    mask_name = "Fullbrain"

    semantics = load_semantic()
    data = np.array(load_sherlock_data(images_path))

    exp_params = {
    "detrend": True,
    "standardize": True,
    "memory": mask_memory,
    "mask": mask_img,
    "mask_strategy": "epi",
    "n_jobs": 1,
    "memory_level": 5,
    "t_r": 2,
    "verbose": 1
    }




    algorithms = [
    (PCASRM(n_components=50, **exp_params), "PCA_50"),
    (DictionarySRM(50, alpha=1, init_method="ortho_srm", **exp_params), "FastDictSRM_50_1"),
    (ProbabilisticSRM(n_components=20, **exp_params), "ProbSRM_20"),
    (ProbabilisticSRM(n_components=50, **exp_params), "ProbSRM_50"),
    (DeterministicSRM(n_components=50, **exp_params), "DetSRM_50"),
    ]

    np.random.seed(0)
    n_splits = 10
    res = {}
    for i in range(n_splits):
        print("split: " + str(i))
        train_split, test_split = train_test_split(np.array(range(50)), test_size=5)
        for algorithm, name in algorithms:
            print(name)
            t0 = time()
            algorithm.fit(data[:, train_split])
            fitting_time = time() - t0
            print("fitting time", fitting_time)
            shared = algorithm.transform(data)
            transform_time = time() - fitting_time - t0
            print("transform time", transform_time)
            with open("./log", "a") as log:
                log.write(name + "\n")
                log.write("fitting time: " + str(fitting_time) + "\n")
                log.write("transform time: " + str(transform_time) + "\n")

            np.save(result_directory + "testsize_5_split_" + str(i) + "label_none" + "mask_" + mask_name + "algo_" + name, shared)

            shared = shared.T
            shared = np.array(np.split(shared, 50, axis=0))
            for semantic, semantic_name in zip(semantics, ["weighted", "unweighted"]):
                semantic = np.array(np.split(semantic, 50, axis=0))
                shared_train = np.concatenate(shared[train_split], axis=0)
                shared_test = np.concatenate(shared[test_split], axis=0)
                semantic_train = np.concatenate(semantic[train_split], axis=0)
                semantic_test = np.concatenate(semantic[test_split], axis=0)
                R, _ = hyperalign(shared_train.T,
                                  semantic_train.T)
                predicted_semantic = (R.dot(shared_test.T)).T
                true_semantic = semantic_test
                error = scene_decoding_n(predicted_semantic, true_semantic, 5)
                res[i, name, semantic_name] = error
                print(error)
    np.save(result_directory + "semantic_results", res)
exp_original_semanticSRM_5_CVFullBrain()


