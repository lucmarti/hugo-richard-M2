#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""

import time
import numpy as np
from scipy.stats import pearsonr
import pandas as pd
import glob
%matplotlib inline
import matplotlib.pyplot as plt

from nilearn.input_data import NiftiMasker
from nilearn.plotting import plot_stat_map
# %matplotlib inline
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.piecewise_deterministic_srm import PieceWiseAlignment_DeterministicSRM

# Define train/test sessions from labels


def load_ibc_data(path="/storage/tompouce/bthirion/maps_3mm.csv"):
    ibc = pd.DataFrame.from_csv(
        path, sep=",", index_col=0)
    image_list = []
    for subj_i in range(1, 16):
        subj_data = ibc.loc[ibc['subject'] ==
                            "sub-" + "{0:0=2d}".format(subj_i)]
        filtered_data = subj_data.drop_duplicates(
            subset="contrast", keep='last')
        if not filtered_data.empty:
            image_list.append(filtered_data)
    return image_list


def make_train_test_ibc(
        image_list, train_tasks, test_tasks, masker):
    X_train_list, X_test_list = [], []
    train_contrast_list, test_contrast_list = [], []
    for i in range(len(image_list)):
        X_train, X_test = [], []
        train_contrast, test_contrast = [], []
        for index, row in image_list[i].iterrows():
            if row["task"] in (train_tasks):
                X_train.append(masker.transform(row["path"]))
                train_contrast.append(row["contrast"])
            elif row["task"] in (test_tasks):
                X_test.append(masker.transform(row["path"]))
                test_contrast.append(row["contrast"])
        X_train_list.append(np.vstack(X_train))
        X_test_list.append(np.vstack(X_test))
        train_contrast_list.append(train_contrast)
        test_contrast_list.append(test_contrast)
    return X_train_list, X_test_list, train_contrast_list, test_contrast_list


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def try_methods(methods, masker, X_1_train, X_1_test, X_2_train, X_2_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, piecewise_methods=["mean"], n_jobs=1, n_bootstraps=[1], srm_components=20):
    methods_r2_score = []
    # methods_corr_score = []
    methods_pred = []
    methods_header = []
    # Turning train / test set to images through unmasking if needed
    # if any(image_method in '\_'.join(methods) for image_method in ["piecewise", "srm"]):
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    for method in methods:
        if method == 'identity':
            X_1_pred = X_1_test
            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            # methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(method)
        # Hyperalignment   !!! for now we have to transpose X_train and X_test
        if method == 'scaled_orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = sc * R.dot((X_1_test.T))
            methods_r2_score.append(score_table(
                X_1_pred, X_2_test.T))
            # methods_corr_score.append(pearsonr(X_1_pred, X_2_test.T))
            methods_pred.append(X_1_pred.T)
            methods_header.append(method)
        if method == 'orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = R.dot((X_1_test.T))
            methods_r2_score.append(score_table(
                X_1_pred, X_2_test.T))
            # methods_corr_score.append(pearsonr(X_1_pred, X_2_test.T))
            methods_pred.append(X_1_pred.T)
            methods_header.append(method)
        # Ridge Alignment with Cross-Validation
        if method == 'ridge_cv':
            ridge_estim = RidgeCV(alphas=alphas_ridge,
                                  n_splits=n_cv_splits)
            ridge_estim.fit(X_1_train, X_2_train)
            X_1_pred = ridge_estim.transform(X_1_test)
            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            # methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(method)
        # PieceWise Alignment
        if method == "piecewise":
            for method_piecewise in piecewise_methods:
                for n_bootstrap in n_bootstraps:
                    piecewise_estim = PieceWiseAlignment(
                        n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                    piecewise_estim.fit(Im_1_train, Im_2_train)
                    Im_1_pred = piecewise_estim.transform(Im_1_test)

                    # Scoring
                    X_1_pred = masker.transform(Im_1_pred)
                    methods_r2_score.append(score_table(X_1_pred, X_2_test))
                    # methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
                    methods_pred.append(X_1_pred)
                    methods_header.append(
                        str(n_bootstrap) + "_bootstraps_" + method + "_" + method_piecewise)
        if method == "piecewise_srm":
            for n_bootstrap in n_bootstraps:
                piecewise_srm_estim = PieceWiseAlignment_DeterministicSRM(
                    n_pieces=n_pieces_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap, n_components=srm_components)
                piecewise_srm_estim.fit(Im_1_train, Im_2_train)
                Im_1_pred = piecewise_srm_estim.transform(
                    [Im_1_test], index=0, index_inverse=1)
                # Scoring
                X_1_pred = masker.transform(Im_1_pred)
                methods_r2_score.append(score_table(X_1_pred, X_2_test))
                methods_pred.append(X_1_pred)
                methods_header.append(
                    str(n_bootstrap) + "_bootstraps_" + method)
        # Deterministic SRM
        if method == "srm":
            srm_estim = DeterministicSRM(
                n_components=srm_components, max_iter=10, mask=masker, n_jobs=n_jobs)
            srm_estim.fit([Im_1_train, Im_2_train])
            shared_response = srm_estim.transform([[Im_1_test]], index=[0])
            X_1_pred = srm_estim.basis[1].dot(shared_response).T

            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            # methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(method)

    return methods_r2_score, methods_pred, methods_header


def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.transform(Im_1_pred)
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def zero_mean_coefficient_determination(y_true, y_pred, sample_weight=None, multioutput="uniform_average"):
    if y_true.ndim == 1:
        y_true = y_true.reshape((-1, 1))

    if y_pred.ndim == 1:
        y_pred = y_pred.reshape((-1, 1))

    if sample_weight is not None:
        sample_weight = column_or_1d(sample_weight)
        weight = sample_weight[:, np.newaxis]
    else:
        weight = 1.

    numerator = (weight * (y_true - y_pred) ** 2).sum(axis=0, dtype=np.float64)
    denominator = (weight * (y_true) ** 2).sum(axis=0, dtype=np.float64)
    nonzero_denominator = denominator != 0
    nonzero_numerator = numerator != 0
    valid_score = nonzero_denominator & nonzero_numerator
    output_scores = np.ones([y_true.shape[1]])
    output_scores[valid_score] = 1 - (numerator[valid_score] /
                                      denominator[valid_score])
    output_scores[nonzero_numerator & ~nonzero_denominator] = 0

    if multioutput == 'raw_values':
        # return scores individually
        return output_scores
    elif multioutput == 'uniform_average':
        # passing None as weights results is uniform mean
        avg_weights = None
    elif multioutput == 'variance_weighted':
        avg_weights = (weight * (y_true - np.average(y_true, axis=0,
                                                     weights=sample_weight)) ** 2).sum(axis=0, dtype=np.float64)
        # avoid fail on constant y or one-element arrays
        if not np.any(nonzero_denominator):
            if not np.any(nonzero_numerator):
                return 1.0
            else:
                return 0.0
    else:
        avg_weights = multioutput

    return np.average(output_scores, weights=avg_weights)


'''
# Methods tested
# method for piecewise alignement

all_methods = ["identity", "orthogonal", "scaled_orthogonal",
               "ridge_cv", "piecewise", "srm", "piecewise_srm"]
methods = all_methods
# With choice of alignment method and bootstraping for piecewise
piecewise_methods = ["scaled_orthogonal", "RidgeCV"]
n_bootstraps = [1, 20]
n_pieces = 100
# And other methods parameters
n_splits = 4  # n_subjectsumber of folds for validations
alphas_ridge = (0.1, 10, 500, 2000, 10000, 100000)
ks_rrr = [10, 20, 50]  # RRR-CV params
alphas_rrr = [0.1, 10, 1000]  # Ridge-CV params
srm_components = 15
'''


# Dataset and pairs of subjects tried
dataset = "ibc"
n_jobs = 1

# Data preparation, mask fitting
start_time = time.time()
data_path = "/storage/tompouce/bthirion/maps_3mm.csv"
mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
# 2, 9 not working i.e. subjects 3 and 10 :
image_list = load_ibc_data(path=data_path)

path_to_results = "/storage/tompouce/tbazeill/ibc/"
masker = NiftiMasker(
    memory_level=5, memory=mask_memory)
masker.fit(mask_path)
print("Path to results is %s " % path_to_results)

train_set_list = [["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "hcp_gambling",
                   "hcp_social", "hcp_language", "hcp_motor", "hcp_emotion", "hcp_relational", "rsvp_language"],
                  ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "hcp_gambling",
                   "hcp_social", "hcp_language", "hcp_motor", "hcp_emotion", "hcp_relational"],
                  ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "hcp_gambling",
                   "hcp_social", "hcp_language", "hcp_motor"],
                  ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional", "hcp_gambling",
                   "hcp_social"],
                  ["archi_standard", "archi_spatial",
                   "archi_social", "archi_emotional"],
                  ["hcp_social", "hcp_language", "hcp_motor",
                      "hcp_emotion", "hcp_relational", "hcp_wm"]
                  ]
test_set_list = [["hcp_wm"],
                 ["hcp_wm", "rsvp_language"],
                 ["hcp_wm", "rsvp_language", "hcp_emotion", "hcp_relational"],
                 ["hcp_wm", "rsvp_language", "hcp_emotion",
                  "hcp_relational", "hcp_language", "hcp_motor"],
                 ["hcp_wm", "rsvp_language", "hcp_emotion", "hcp_relational",
                  "hcp_language", "hcp_motor", "hcp_gambling", "hcp_social"],
                 ["archi_standard", "archi_spatial",
                  "archi_social", "archi_emotional", "rsvp_language"]]

train_tasks = ["archi_standard", "archi_spatial",
               "archi_social", "archi_emotional", "hcp_gambling",
               "hcp_social", "hcp_language", "hcp_motor", "hcp_emotion", "hcp_relational"]
test_tasks = ["hcp_wm", "rsvp_language"]

id_r2, id_cust_r2, so_r2, so_cust_r2 = [], [], [], []
for train_tasks, test_tasks in zip(train_set_list, test_set_list):
    print("Training tasks : %s" % train_tasks)
    print("Testing tasks : %s" % test_tasks)
    print("Dataset %s loaded in %s seconds " %
          (dataset, time.time() - start_time))
    # Alignment method test : for each pair of subject, try all methods

    # Making the training / testing sets
    # subject_pair_list = [(0, 1)]
    subject_pair = (4, 5)
    start_time = time.time()
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    pair_image_list = [image_list[subj_1 - 1], image_list[subj_2 - 1]]
    X_train_list, X_test_list, train_contrast_list, test_contrast_list = make_train_test_ibc(
        pair_image_list, train_tasks, test_tasks, masker)
    print("Data masked in : %s seconds" % str(time.time() - start_time))
    if train_contrast_list[0] == train_contrast_list[1] and test_contrast_list[0] == test_contrast_list[1]:
        print("The training and testing contrasts are the same for both subjects")
    else:
        raise NameError("Not the same contrasts for both subjects")

    # Alignment through all methods
    start_time = time.time()
    print("Aligning pair : %s" % str(subject_pair))
    pair_result = []
    # pair_result.append(subject_pair)
    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    # identity
    X_pred = X_1_test
    X_GT = X_2_test

    id_r2.append(np.maximum(
        r2_score(X_GT, X_pred, multioutput='raw_values'), -1))
    id_cust_r2.append(np.maximum(zero_mean_coefficient_determination(
        X_GT, X_pred, multioutput='raw_values'), -1))

    # scaled_orthogonal
    R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
    X_pred = sc * R.dot((X_1_test.T)).T
    so_r2.append(np.maximum(
        r2_score(X_GT, X_pred, multioutput='raw_values'), -1))
    so_cust_r2.append(np.maximum(zero_mean_coefficient_determination(
        X_GT, X_pred, multioutput='raw_values'), -1))
# %%
image_list[0].groupby('task').count()


# %%
fig, axes = plt.subplots(figsize=(20, 40), nrows=6, ncols=4)
for i in range(6):
    print("Train set %s : %s " % (i, train_set_list[i]))
    print("Test set %s : %s " % (i, test_set_list[i]))
    Im_r2 = masker.inverse_transform(id_r2[i])
    Im_custom_r2 = masker.inverse_transform(id_cust_r2[i])
    display = plot_stat_map(Im_r2,
                            vmax=0.5, axes=axes[i, 0])
    display.title('Identity R2 map')
    display = plot_stat_map(Im_custom_r2,
                            vmax=0.5, axes=axes[i, 1])
    display.title('Identity Custom R2 map')

    Im_r2 = masker.inverse_transform(so_r2[i])
    Im_custom_r2 = masker.inverse_transform(so_cust_r2[i])
    display = plot_stat_map(Im_r2,
                            vmax=0.5, axes=axes[i, 2])
    display.title('scaled_orthogonal R2 map')
    display = plot_stat_map(Im_custom_r2,
                            vmax=0.5, axes=axes[i, 3])
    display.title('scaled_orthogonal Custom R2 map')

# %%

list = so_cust_r2
average_results = np.mean(list, axis=1)

predicted_voxels_count = [sum(1 for x in method_r2_voxelwise if x > 0)
                          for method_r2_voxelwise in list]
predicted_voxels_weight = [sum(x for x in method_r2_voxelwise if x > 0)
                           for method_r2_voxelwise in list]
train_test = ["all vs hcp_wm", "all vs hcp_wm and rsvp_language",
              "archi vs hcp", "archi++ vs hcp", "archi+ vs hcp", "hcp vs archi"]
data_translation = np.array(
    [train_test, average_results, predicted_voxels_count, predicted_voxels_weight, cu_average_results, cu_predicted_voxels_count, cu_predicted_voxels_weight]).T
columns_labels = ["condition", "r2_mean",
                  "n_pos_voxel_r2", "sum_pos_voxel_r2", "cust_r2_mean",
                  "n_pos_cust_r2_voxel", "sum_pos_voxel_cust_r2"]
results = pd.DataFrame.from_records(
    data_translation, columns=columns_labels)
print(results)


so_results.dtypes
so_results[['r2_mean', 'sum_positive_voxel_r2', 'cust_r2_mean', 'sum_positive_voxel_cust_r2']] = so_results[[
    'r2_mean', 'sum_positive_voxel_r2', 'cust_r2_mean', 'sum_positive_voxel_cust_r2']].apply(pd.to_numeric, errors='coerce')
so_results.round({'r2_mean': 2, 'sum_positive_voxel_r2': 0,
                  'cust_r2_mean': 2, 'sum_positive_voxel_cust_r2': 0})

so_results.rename(index=str, columns={"positive_r2_voxel_count": "n_pos_voxel_r2", "sum_positive_voxel_r2": "sum_pos_voxel_r2", "positive_cust_r2_voxel_count":
                                      "n_pos_cust_r2_voxel", 'sum_positive_voxel_cust_r2': "sum_pos_voxel_cust_r2"})
pd.to_numeric(so_results)

so_results.round({'r2_mean': 0})
so_r2_results

so_cust_r2_results
# %%
"""
# Plot of example initial data
fig, axes = plt.subplots(figsize=(20, 10), nrows=len(X_test_list[0]), ncols=2)
cut_coords = [-8, 8]
display_mode = "x"

for j in range(len(X_test_list[0])):
    for i in range(2):
        Im = masker.inverse_transform(X_test_list[i][j])
        if cut_coords is None:
            display = plot_stat_map(
                Im, axes=axes[j, i], display_mode=display_mode, vmax=5)
            cut_coords = display.cut_coords
        else:
            display = plot_stat_map(Im, axes=axes[j, i], cut_coords=cut_coords,
                                    display_mode=display_mode, vmax=5)
        display.title('Raw data contrast %s for subject %s' %
                      (test_contrast_list[i][j], subject_pair[i]))
"""
# Applying only identity
X_pred = X_1_test
X_GT = X_2_test
X_r2 = np.maximum(r2_score(X_GT, X_pred, multioutput='raw_values'), -1)
X_custom_r2 = np.maximum(zero_mean_coefficient_determination(
    X_GT, X_pred, multioutput='raw_values'), -1)
Im_r2 = masker.inverse_transform(X_r2)
Im_custom_r2 = masker.inverse_transform(X_custom_r2)
display = plot_stat_map(Im_r2,
                        vmax=0.5)
display.title('Identity R2 map')
display = plot_stat_map(Im_custom_r2,
                        vmax=0.5)
display.title('Identity Custom R2 map')

R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
X_pred = sc * R.dot((X_1_test.T)).T
X_r2 = np.maximum(r2_score(X_GT, X_pred, multioutput='raw_values'), -1)
X_custom_r2 = np.maximum(zero_mean_coefficient_determination(
    X_GT, X_pred, multioutput='raw_values'), -1)
Im_R2 = masker.inverse_transform(X_r2)
Im_custom_R2 = masker.inverse_transform(X_custom_r2)
display = plot_stat_map(Im_r2,
                        vmax=0.5)
display.title('scaled_orthogonal R2 map')
display = plot_stat_map(Im_custom_r2,
                        vmax=0.5)
display.title('scaled_orthogonal Custom R2 map')

# try every method in the library and calculate voxelwise R2 and correlation images
'''methods_r2_score, methods_pred, methods_header = try_methods(methods,
                                                             masker, X_1_train, X_1_test, X_2_train, X_2_test, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, piecewise_methods, n_jobs, n_bootstraps, srm_components)
print("Methods fitted in %s seconds " % str(time.time() - start_time))


# Creating aligned pair voxelwise R2 and correlation images
X_GT = X_2_test
methods_voxelwise_R2_score = []
methods_voxelwise_corr = []
for i in range(len(methods_pred)):
    X_pred = methods_pred[i]
    # voxelwise R2 score
    X_R2 = [max(r2_score(X_GT[:, j], X_pred[:, j]), -1)
            for j in range(np.shape(X_GT)[1])]
    methods_voxelwise_R2_score.append(X_R2)
    Im_R2 = masker.inverse_transform(X_R2)
    Im_R2.to_filename(
        path_to_results + "/" + methods_header[i] + "/" + str(subj_1) + "_" + str(subj_2) + "_r2_debug.nii.gz")
    # voxelwise correlation
    X_corr = [pearsonr(X_GT[:, j], X_pred[:, j])
              for j in range(np.shape(X_GT)[1])]
    methods_voxelwise_corr.append(X_corr)
    Im_corr = masker.inverse_transform(np.array(X_corr).T)
    Im_corr.to_filename(
        path_to_results + "/" + methods_header[i] + "/" + str(subj_1) + "_" + str(subj_2) + "_corr_debug.nii.gz")
print("Finished data processing, total time : %s seconds " %
      str(time.time() - start_time))'''
