#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Fri Apr 13 17:07:08 2018

@author: thomasbazeille
"""
import sys
import time
import itertools
import numpy as np
from scipy.stats import pearsonr
import pandas as pd
import csv
import matplotlib.pyplot as plt
# %matplotlib inline

from nilearn.plotting import plot_stat_map
from nilearn import datasets
from nilearn.input_data import NiftiMasker
from nilearn.masking import intersect_masks
from nilearn.image import index_img
from sklearn.metrics import r2_score

from functional_alignment.hyperalignment import hyperalign
from functional_alignment.ridge_cv import RidgeCV
from functional_alignment.piecewise_alignment import PieceWiseAlignment
from functional_alignment.rrr_cv import RRRCV
from functional_alignment.deterministic_srm import DeterministicSRM


def load_sherlock_data(path="/storage/data/sherlock_movie_watch_dataset/", file_name='sherlock_movie_s', file_format='.nii', n_subjects=2):
    """

    Parameters
    ----------
    path: path to the data set
    file_name: initial file name for the data
    file_format: file ending format
    n_subjects: number of subjects in the experiments (maximum 17)

    Returns
    -------
    Python list of data load path
    """

    image_list = []
    for i in range(n_subjects):
        image_list.append(path + "movie_files/" +
                          file_name + str(i + 1) + file_format)
    Path_to_movie_labels = path + "labels/movie_scene_labels.csv"

    movie_labels = pd.DataFrame.from_csv(
        Path_to_movie_labels, sep=";", index_col=0)

    masker_path = path + 'preloaded_masker/mask_all_subj_movie.nii'

    return image_list, movie_labels, masker_path


# Define train/test sessions from labels
def make_train_test_sessions_haxby(n_subjects, haxby_dataset):
    train_session = []
    test_session = []
    for i in range(n_subjects):
        labels = pd.read_csv(haxby_dataset.session_target[i], sep=" ")
        condition_mask = labels['labels'] != b'rest'
        train_chunks = labels['chunks'] % 2 == 0
        test_chunks = labels['chunks'] % 2 == 1

        train_labels = labels[condition_mask & train_chunks].sort_values(by=[
                                                                         'chunks', 'labels'])
        test_labels = labels[condition_mask & test_chunks].sort_values(
            by=['chunks', 'labels'])
        train_session.append(train_labels)
        test_session.append(test_labels)
    return train_session, test_session


def make_train_test_sherlock(image_list, labels, masker):
    """ adapted from sherlock_retreat_project. experiments.utils.get_scene_fold
    Extract scenes
        Parameters :
        - List of int fold out of 50 scenes
        - Dataframe labels (scene, onset, offset)
        - List of nii files subject_imgs

        Returns : list of nii subjects_scenes_fold of len n_subjects
        """
    train_session, test_session = [], []
    for i in range(0, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            train_session.append(ind)
            ind += 1
    for i in range(1, 50, 2):
        ind = labels.iloc[i]['Onset'] - 1
        while ind < labels.iloc[i]['Offset']:
            test_session.append(ind)
            ind += 1
    if len(train_session) > len(test_session):
        train_session = [train_session[i] for i in range(len(test_session))]
    elif len(train_session) < len(test_session):
        test_session = [test_session[i] for i in range(len(train_session))]
    else:
        pass

    X_train_list, X_test_list = [], []
    for i in range(len(image_list)):
        X_train_list.append(
            masker.transform(index_img(image_list[i], train_session)))
        X_test_list.append(
            masker.transform(index_img(image_list[i], test_session)))
    return X_train_list, X_test_list


def fetch_vt_mask_haxby(subj, haxby_dataset):
    masker = NiftiMasker(haxby_dataset.mask_vt[subj], smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def compute_union_vt_masker_haxby(subjs, haxby_dataset):
    mask_list = []
    for subj in subjs:
        mask_list.append(haxby_dataset.mask_vt[subj])
    union_mask = intersect_masks(mask_list, threshold=0, connected=False)
    masker = NiftiMasker(union_mask, smoothing_fwhm=None,
                         standardize=True, detrend=True)
    return masker


def split_train_test(masker, subj, train_session, test_session, haxby_dataset):
    X = masker.fit_transform(haxby_dataset.func[subj])
    X_train = X[train_session[subj].index]
    X_test = X[test_session[subj].index]
    return X_train, X_test


def make_train_test_images(masker, X_1_train, X_2_train, X_1_test):
    Im_1_train = masker.inverse_transform(X_1_train)
    Im_2_train = masker.inverse_transform(X_2_train)
    Im_1_test = masker.inverse_transform(X_1_test)
    return Im_1_train, Im_2_train, Im_1_test


def try_methods(methods, masker, X_1_train, X_1_test, X_2_train, X_2_test, n_cv_splits, alphas_ridge, ks_rrr, alphas_rrr, n_pieces_piecewise, piecewise_methods=["mean"], n_jobs=1, n_bootstraps=[1], srm_components=20):
    methods_r2_score = []
    methods_corr_score = []
    methods_pred = []
    methods_header = []
    # Turning train / test set to images through unmasking if needed
    # if any(image_method in '\_'.join(methods) for image_method in ["piecewise", "srm"]):
    Im_1_train, Im_2_train, Im_1_test = make_train_test_images(
        masker, X_1_train, X_2_train, X_1_test)

    for method in methods:
        if method == 'identity':
            X_1_pred = X_1_test
            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(method)
        # Hyperalignment   !!! for now we have to transpose X_train and X_test
        if method == 'scaled_orthogonal':
            R, sc = hyperalign(X_1_train.T, X_2_train.T, scale=True)
            X_1_pred = sc * R.dot((X_1_test.T))
            methods_r2_score.append(score_table(
                X_1_pred, X_2_test.T))
            methods_corr_score.append(pearsonr(X_1_pred, X_2_test.T))
            methods_pred.append(X_1_pred.T)
            methods_header.append(method)
        # Ridge Alignment with Cross-Validation
        if method == 'ridge_cv':
            ridge_estim = RidgeCV(alphas=alphas_ridge,
                                  n_splits=n_cv_splits)
            ridge_estim.fit(X_1_train, X_2_train)
            X_1_pred = ridge_estim.transform(X_1_test)
            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(method)
        # PieceWise Alignment
        if method == "piecewise":
            for method_piecewise in piecewise_methods:
                for n_bootstrap in n_bootstraps:
                    piecewise_estim = PieceWiseAlignment(
                        n_pieces=n_pieces_piecewise, method=method_piecewise, mask=masker, n_jobs=n_jobs, n_bootstrap=n_bootstrap)
                    piecewise_estim.fit(Im_1_train, Im_2_train)
                    Im_1_pred = piecewise_estim.transform(Im_1_test)

                    # Scoring
                    X_1_pred = masker.transform(Im_1_pred)
                    methods_r2_score.append(score_table(X_1_pred, X_2_test))
                    methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
                    methods_pred.append(X_1_pred)
                    methods_header.append(
                        str(n_bootstrap) + "_bootstraps_" + method + "_" + method_piecewise)
        # Deterministic SRM
        if method == "srm":
            srm_estim = DeterministicSRM(
                n_components=srm_components, max_iter=10, mask=masker, n_jobs=n_jobs)
            srm_estim.fit([Im_1_train, Im_2_train])
            shared_response = srm_estim.transform(Im_1_test, index=[0])
            Im_1_pred = srm_estim.inverse_transform(shared_response, index=[1])

            # Scoring
            X_1_pred = masker.transform(Im_1_pred[0])
            methods_r2_score.append(score_table(X_1_pred, X_2_test))
            methods_corr_score.append(pearsonr(X_1_pred, X_2_test))
            methods_pred.append(X_1_pred)
            methods_header.append(
                method)

    return methods_r2_score, methods_corr_score, methods_pred, methods_header


def score_image(masker, Im_1_pred, X_2_test):
    X_1_pred = masker.transform(Im_1_pred)
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


def score_table(X_1_pred, X_2_test):
    score = max(r2_score(X_2_test, X_1_pred), -1)
    return score


# Methods tested
methods = ["identity", "scaled_orthogonal", "ridge_cv",
           "piecewise"]  # method for piecewise alignement
# With choice of alignment method and bootstraping for piecewise
piecewise_methods = ["scaled_orthogonal", "Ridge_CV"]
n_bootstraps = [1, 20]
# And other methods parameters
n_splits = 4  # n_subjectsumber of folds for validations
alphas_ridge = (0.1, 10, 500, 2000, 10000, 100000)  # Ridge-CV params
ks_rrr = [10, 20, 50]  # RRR-CV params
alphas_rrr = [0.1, 10, 1000]  # RRR-CV params
n_pieces = 200
srm_components = 100  # number of pieces for piecewise alignement


path_to_results = "/storage/tompouce/tbazeill/sherlock_pair_alignment/"
# Dataset and pairs of subjects tried
dataset = "sherlock"
n_subjects = 17  # number of subjects

# %%

# Data preparation
start_time = time.time()
if dataset == "sherlock":

    image_list, movie_labels, masker_path = load_sherlock_data(
        n_subjects=n_subjects)

    masker = NiftiMasker(smoothing_fwhm=0)
    masker.fit(masker_path)

#    X_train_list, X_test_list = make_train_test_sherlock(
#        image_list, movie_labels, masker)

elif dataset == "haxby":
    haxby_dataset = datasets.fetch_haxby(subjects=n_subjects)
    train_session, test_session = make_train_test_sessions_haxby(
        n_subjects, haxby_dataset)
    masker = NiftiMasker(haxby_dataset.mask, detrend=True)
    # Main loop computing X_train and X_test and testing all methods for each pair of subjects #
    X_train_list, X_test_list = [], []
    for subject in range(n_subjects):
        X_train_i, X_test_i = split_train_test(
            masker, subject, train_session, test_session, haxby_dataset)
        X_train_list.append(X_train_i)
        X_test_list.append(X_test_i)

print("Dataset %s loaded in %s seconds " %
      (dataset, time.time() - start_time))

# %%
# for each pair of subject, try all methods
subject_pair_list = [(17, 5), (6, 11), (5, 6), (12, 2),
                     (3, 8), (9, 10), (14, 16), (15, 3), (4, 9)]
n_jobs = 10


for subject_pair in subject_pair_list:
    subj_1, subj_2 = subject_pair[0], subject_pair[1]
    pair_image_list = [image_list[subj_1], image_list[subj_2]]
    X_train_list, X_test_list = make_train_test_sherlock(
        image_list, movie_labels, masker)
    start_time = time.time()
    print("Aligning pair : %s" % str(subject_pair))
    pair_result = []
    # pair_result.append(subject_pair)

    X_1_train, X_1_test = X_train_list[0], X_test_list[0]
    X_2_train, X_2_test = X_train_list[1], X_test_list[1]

    # try every method in the library
    methods_r2_score, methods_corr_score, methods_pred, methods_header = try_methods(
        methods, masker, X_1_train, X_1_test, X_2_train, X_2_test, n_splits,  alphas_ridge, ks_rrr, alphas_rrr, n_pieces, piecewise_methods, n_jobs, n_bootstraps, srm_components)
    print("Methods fitted in %s seconds " %
          str(time.time() - start_time))

    # Saving the methods scores and predictions
    r2_results_title = path_to_results + \
        str(subj_1) + "_" + str(subj_2) + "_r2.csv"
    r2_results = pd.DataFrame.from_records(
        methods_r2_score, columns=methods_header)
    r2_results.to_csv(r2_results_title)
    corr_results_title = path_to_results + \
        str(subj_1) + "_" + str(subj_2) + "_corr.csv"
    corr_results = pd.DataFrame.from_records(
        methods_corr_score, columns=methods_header)
    corr_results.to_csv(r2_results_title)

    # Creating aligned pair voxelwise R2 and correlation images
    X_GT = X_2_test
    methods_voxelwise_R2_score = []
    methods_voxelwise_corr = []
    for i in range(len(methods_pred)):
        X_pred = methods_pred[i]
        # voxelwise R2 score
        X_R2 = [max(r2_score(X_GT[:, j], X_pred[:, j]), -1)
                for j in range(np.shape(X_GT)[1])]
        methods_voxelwise_R2_score.append(X_R2)
        Im_R2 = masker.inverse_transform(X_R2)
        Im_R2.to_filename(
            path_to_results + "/" + methods_header[i] + "/" + str(subj_1) + "_" + str(subj_2) + "_r2.nii.gz")
        # voxelwise correlation
        X_corr = [pearsonr(X_GT[:, j], X_pred[:, j])
                  for j in range(np.shape(X_GT)[1])]
        methods_voxelwise_corr.append(X_corr)
        Im_corr = masker.inverse_transform(X_R2)
        Im_corr.to_filename(
            path_to_results + "/" + methods_header[i] + "/" + str(subj_1) + "_" + str(subj_2) + "_corr.nii.gz")
    print("Finished data processing, total time : %s seconds " %
          str(time.time() - start_time))
