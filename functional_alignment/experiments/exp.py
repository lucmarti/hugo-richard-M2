import nibabel as nib
from sklearn.model_selection import ShuffleSplit
from piecewise_alignment import PieceWiseAlignment, create_labels
import os
import logging
from utils import generate_train_test, load_img
from deterministic_srm import DeterministicSRM
from template_alignment import TemplateAlignment
import random
from time import time
from nilearn.image import mean_img, math_img

def exp13(subjects, algorithms, dataset, X):
    """
    To train the model we use source sessions of all subjects
    and target sessions of source subjects
    The goal is to estimate the target session for the target subject

    Measure of performance is done using the coefficient of determination.

    Experimental conditions:
    For any algorithm to be tested:
    We randomly select target session
    Repeat 5 times
        We randomly select a target subject
        We predict target session for target subject
    Average the results of prediction and save the result

    Parameters
    ----------
    subjects: subjects used for the study
    algorithms: algorithms benchmarked
    dataset: dataset used
    X: location of brain files

    """
    n_splits = 1
    rs = ShuffleSplit(n_splits=n_splits, test_size=.1, random_state=0)
    random.seed(0)
    for train_subjects, test_subject in random.sample(generate_train_test(subjects), 5):
        print("TRAIN subject:" +
          train_subjects.__str__() +
          "TEST subject:" +
          test_subject.__str__())
        for algorithm, name in algorithms:
            print(name)
            for train_runs, test_runs in rs.split(X[0]):
                print("TRAIN runs:" +
                      train_runs.__str__() +
                      "TEST runs:" +
                      test_runs.__str__())

                result_filename = "/storage/workspace/hrichard/results/exp13/" + \
                    dataset + \
                    "alignment" + \
                    "_algo" + \
                    name + \
                    "subject_" + str(test_subject[0]) + \
                    "runs_" + "-".join(map(str, test_runs)) + \
                    "_exp13.nii"

                if not os.path.isfile(result_filename):
                    # Y_test is the prediction for subject test from all other subjects
                    Y = X[test_subject, test_runs]
                    total_time = 0

                    # If we use any piecewise-like algorithm
                    # we just give the average prediction as a result
                    if isinstance(algorithm, PieceWiseAlignment):
                        Y_pred = []
                        for i in train_subjects:
                            # Y_i_test is the prediction for subject test from subject i
                            print("Train subject: " + str(i))
                            print("Test subject: " + str(test_subject[0]))

                            filename_info = "/storage/workspace/hrichard/results/exp13/" + \
                                            "info_" + \
                                            name + \
                                            str(i) + \
                                            "-".join(map(str, test_subject)) + \
                                            "-".join(map(str, train_runs)) + \
                                            ".temp"

                            t0 = time()
                            algorithm.fit(X[i, :][train_runs], X[test_subject[0], :][train_runs])
                            fit_time = time() - t0
                            print("fit time: ", fit_time)
                            nib.save(algorithm.infos, filename_info)

                            t0 = time()
                            Y_i_pred = algorithm.transform(X[i, :][test_runs])
                            transform_time = time() - t0
                            print("transform time: ", transform_time)

                            Y_pred.append(Y_i_pred)
                            total_time += fit_time + transform_time

                        Y_pred = mean_img(Y_pred)

                    # If we use a template algorithms
                    # We create the template and use it for prediction
                    elif isinstance(algorithm, DeterministicSRM) or isinstance(algorithm, TemplateAlignment):

                        t0 = time()
                        algorithm.fit(X[:, train_runs])
                        fit_time = time() - t0
                        print("fit time: ", fit_time)

                        t0 = time()
                        shared_response = algorithm.transform(X[train_subjects, :][:, test_runs],
                                                              index=train_subjects)
                        transform_time = time() - t0
                        print("transform time: ", transform_time)

                        t0 = time()
                        Y_pred = algorithm.inverse_transform(shared_response, index=test_subject)[0]
                        inverse_transform_time = time() - t0
                        print("inverse transform time: ", inverse_transform_time)

                        total_time += fit_time + transform_time + inverse_transform_time

                    print("prediction time: ", total_time)
                    result_img = math_img("1 - np.var(Y_pred - Y, axis=-1)",
                                             Y_pred=Y_pred,
                                             Y=Y)

                    nib.save(result_img, result_filename)

                    with open("/storage/workspace/hrichard/results/exp13/" +
                        dataset +
                        "_timepred_alignment" +
                        "_algo" +
                        name +
                        "subject_" + str(test_subject[0]) +
                        "runs_" + "-".join(map(str, test_runs)) +
                        "_exp13.nii", "w") as time_file:
                        time_file.write(str(total_time))

        print("Done")