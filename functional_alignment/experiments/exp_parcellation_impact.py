from ibc_public.utils_data import (
    data_parser, SMOOTH_DERIVATIVES, SUBJECTS, LABELS, CONTRASTS, DERIVATIVES,
    CONDITIONS, THREE_MM)
import ibc_public
import os
import numpy as np
from nilearn.input_data import NiftiMasker
from sklearn.model_selection import ShuffleSplit
from nilearn.regions import Parcellations


def hierarchical_k_means(X, n_clusters):
    """ use a recursive k-means to cluster X"""
    from sklearn.cluster import MiniBatchKMeans
    n_big_clusters = int(np.sqrt(n_clusters))
    mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_big_clusters, batch_size=1000,
                          n_init=10, max_no_improvement=10, verbose=0,
                          random_state=0).fit(X)
    coarse_labels = mbk.labels_
    fine_labels = np.zeros_like(coarse_labels)
    q = 0
    for i in range(n_big_clusters):
        n_small_clusters = int(
            n_clusters * np.sum(coarse_labels == i) * 1. / X.shape[0])
        n_small_clusters = np.maximum(1, n_small_clusters)
        mbk = MiniBatchKMeans(init='k-means++', n_clusters=n_small_clusters,
                              batch_size=1000, n_init=10, max_no_improvement=10, verbose=0,
                              random_state=0).fit(X[coarse_labels == i])
        fine_labels[coarse_labels == i] = q + mbk.labels_
        q += n_small_clusters

    def _remove_empty_labels(labels):
        vals = np.unique(labels)
        inverse_vals = - np.ones(labels.max() + 1).astype(np.int)
        inverse_vals[vals] = np.arange(len(vals))
        return inverse_vals[labels]

    return _remove_empty_labels(fine_labels)


# List all subjects and do parcellation then save it
clustering_methods = ['k_means', 'ward']
datasets = ["ibc", 'hcp']
n_pieces = 150
for dataset in datasets:
    if dataset == 'ibc':
        data_dir = os.path.join(THREE_MM, 'group')

        # Access to the data
        subject_list = SUBJECTS
        task_list = ['archi_standard', 'archi_spatial', 'archi_social',
                     'archi_emotional', 'hcp_language', 'hcp_social', 'hcp_gambling',
                     'hcp_motor', 'hcp_emotion', 'hcp_relational', 'hcp_wm', ]

        df = data_parser(derivatives=THREE_MM, subject_list=SUBJECTS,
                         conditions=CONDITIONS, task_list=task_list)
        conditions = df[df.modality == 'bold'].contrast.unique()
        n_conditions = len(conditions)
        paths_train = []
        subjects = ['sub-%02d' %
                    i for i in [1, 2, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15]]
        n_subjects = len(subjects)

        for subject in subjects:
            ap_path = [df[df.acquisition == 'ap'][df.subject == subject][
                df.contrast == condition].path.values[-1] for condition in conditions]
            paths_train.append(ap_path)

        # mask_path = os.path.join(data_dir, 'resampled_gm_mask.nii.gz')
        mask_path = "/storage/tompouce/tbazeill/ibc/gm_mask_3mm.nii.gz"
        mask_memory = "/storage/tompouce/tbazeill/ibc_cache"
        masker = NiftiMasker(
            memory_level=5, memory=mask_memory)
        masker.fit(mask_path)
        path_to_results = "/storage/tompouce/tbazeill/ibc/"
    elif dataset == "hcp":
        path_to_data = "/storage/store/data/HCP900/glm"
        path_to_results = "/storage/tompouce/tbazeill/hcp"
        mask_path = os.path.join(path_to_results, '3mm_mask_gm.nii.gz')
        mask_memory = "/storage/tompouce/tbazeill/hcp_cache"
        masker = NiftiMasker(
            memory_level=5, memory=mask_memory, mask_img=mask_path)
        masker.fit()

        subjects = [100206, 109123, 118528, 129634, 138534, 149741,
                    158338, 168745, 178748, 191942, 201414, 211922, 257845, 322224]
        contrasts = ["EMOTION/LR/z_maps/z_FACES.nii.gz", "EMOTION/LR/z_maps/z_SHAPES.nii.gz", "GAMBLING/LR/z_maps/z_PUNISH.nii.gz", "GAMBLING/LR/z_maps/z_REWARD.nii.gz", "LANGUAGE/LR/z_maps/z_MATH.nii.gz", "LANGUAGE/LR/z_maps/z_STORY.nii.gz", "MOTOR/LR/z_maps/z_AVG.nii.gz", "MOTOR/LR/z_maps/z_LH.nii.gz",  "MOTOR/LR/z_maps/z_RF.nii.gz", "MOTOR/LR/z_maps/z_LF.nii.gz", "MOTOR/LR/z_maps/z_T.nii.gz", "MOTOR/LR/z_maps/z_CUE.nii.gz",     "MOTOR/LR/z_maps/z_RH.nii.gz", "RELATIONAL/LR/z_maps/z_MATCH.nii.gz", "RELATIONAL/LR/z_maps/z_REL.nii.gz",
                     "WM/LR/z_maps/z_0BK_PLACE.nii.gz",  "WM/LR/z_maps/z_2BK_FACE.nii.gz", "WM/LR/z_maps/z_0BK_BODY.nii.gz",  "WM/LR/z_maps/z_0BK_TOOL.nii.gz",   "WM/LR/z_maps/z_2BK.nii.gz", "WM/LR/z_maps/z_BODY.nii.gz", "WM/LR/z_maps/z_TOOL.nii.gz", "WM/LR/z_maps/z_0BK_FACE.nii.gz", "WM/LR/z_maps/z_2BK_PLACE.nii.gz", "WM/LR/z_maps/z_0BK.nii.gz",   "WM/LR/z_maps/z_2BK_BODY.nii.gz", "WM/LR/z_maps/z_2BK_TOOL.nii.gz", "WM/LR/z_maps/z_FACE.nii.gz",  "WM/LR/z_maps/z_PLACE.nii.gz", "SOCIAL/LR/z_maps/z_RANDOM.nii.gz", "SOCIAL/LR/z_maps/z_TOM.nii.gz"]
        paths_train, paths_test = [], []
        for subject in subjects:
            subject_paths_train = []
            subject_paths_test = []
            for contrast in contrasts:
                path_t = os.path.join(
                    path_to_data, str(subject) + '/' + contrast)
                subject_paths_train.append(path_t)
                subject_paths_test.append(path_t.replace("LR", "RL"))
            paths_train.append(subject_paths_train)

    for i, path in enumerate(paths_train):
        X_ = masker.transform(path)
        if type(X_) == list:
            X_ = np.concatenate(X_, axis=0)
        X_ = X_.T
        rs = ShuffleSplit(n_splits=1,
                          test_size=.8, random_state=0)
        for train_index, _ in rs.split(X_.T):
            train_index = train_index
            break
        for clustering_method in clustering_methods:
            if clustering_method == 'k_means':
                labels = hierarchical_k_means(X_[:, train_index], n_pieces)
                im_labels = masker.inverse_transform(labels)
            elif clustering_method == 'ward':
                ward = Parcellations(method='ward', n_parcels=n_pieces,
                                     standardize=False, smoothing_fwhm=0, memory_level=1,
                                     verbose=1)
                ward.fit(masker.inverse_transform(X_[:, train_index].T))
                im_labels = ward.labels_img_
            im_labels.to_filename(os.path.join(
                path_to_results, "%s_%s_parcellation.nii.gz" % (str(i), clustering_method)))

# %%
from nilearn.plotting import plot_roi
%matplotlib inline
clustering_methods = ['k_means', 'ward']
datasets = ["ibc", 'hcp']
path_to_results = '/storage/tompouce/tbazeill/'
for dataset in datasets:
    for i in range(13):
        cut_coords = None
        for clustering_method in clustering_methods:
            path = os.path.join(path_to_results, "%s/%s_%s_parcellation.nii.gz" %
                                (dataset, str(i), clustering_method))
            first_plot = plot_roi(
                path, title="%s %s, %s parcellation" % (dataset, str(i), clustering_method), display_mode='xz', cut_coords=cut_coords)
            cut_coords = first_plot.cut_coords

# %%
