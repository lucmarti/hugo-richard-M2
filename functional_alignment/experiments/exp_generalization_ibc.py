import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from sklearn.linear_model import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
from srm import SRM
import os
import logging
from load_data import load_forrest, load_ibc
from time import time
from utils import generate_train_test
n_jobs = 10

#IBC_config

logging.basicConfig(level=logging.DEBUG, filename="logfile_map_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

# mask_dir = "/storage/store/data/ibc/derivatives/"
# mask_path=mask_dir + "group/gm_mask.nii.gz"

mask_path = "/storage/workspace/hrichard/results/exp13/mask_ibc_roi.nii.gz"

data_dir = "/storage/data/openfmri/ds113/"
storage_dir = "/storage/tompouce/hrichard/ibc/"

data_dir = "/storage/store/data/ibc/derivatives/"

subjects = [1, 2, 4, 5, 6, 7, 8, 9, 12]
runs = ["Trn01",
        "Trn02",
        "Trn03",
        "Trn04",
        "Trn05",
        "Trn06",
        "Trn07",
        "Trn08",
        "Trn09",
        "Val01",
        "Val02",
        "Val03",
        "Val04",
        "Val05",
        "Val06",
        "Val07",
        "Val08",
        "Val09"]

X_, masker, mask_img, dataset = load_ibc(
    dataset="ibc",
    subjects=subjects,
    runs=runs,
    mask_path=mask_path,
    data_path=data_dir + "sub-%02d/ses*/func/wrdcsub*%02s*.nii.gz"
)

X = [np.concatenate([X_[j][i] for j in len(X_)], axis=1) for i in range(len(X_[0]))]

label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"
print("Loading of data ... Done")

X = [np.concatenate([X[j][i] for j in range(len(X) - 1)], axis=1) for i in range(len(X[0]))]
print("Transposing data... Done")

for i in range(len(X)):
    print(X[i].shape)

labels = create_labels(X[0], mask_img.get_data(), n_pieces=2)

# # For resilient clustering
# if os.path.isfile(label_path_files):
#     labels = np.load(label_path_files)
# else:
#     labels = create_labels(X[0][0], mask_img.get_data(), n_pieces=1000)
#     np.save(label_path_save, labels)
#     label_img = masker.inverse_transform(labels)
#     nib.save(
#         label_img,
#         label_path_img
#     )


def predict(algo, algo_name, i_train_subjects, i_test_subject, i_train_data, i_test_data, data, storage_d):
    Y_ = np.zeros_like(data[i_test_subject[0]][:, i_test_data])
    total_time = 0
    if isinstance(algo, PieceWiseAlignment):
        for i in i_train_subjects:
            # Y_i_test is the prediction for subject test from subject i
            logging.info("Train subject: " + str(i))
            logging.info("Test subject: " + str(i_test_subject[0]))
            filename = storage_d + \
                       algo_name + \
                       str(i) + \
                       "".join(map(str, i_test_subject)) + \
                       "".join(map(str, i_train_data)[:10]) + \
                       ".temp"

            if os.path.isfile(filename):
                algo.load(filename)
                logging.info("No fitting needed, we use memory")
            else:
                algo.fit(data[i][:, i_train_data], data[test_subject[0]][:, i_train_data])
                logging.info("fit time: " + str(algo.fit_time_))
            algo.dump(filename)

            Y_i_test = algo.transform(data[i][:, i_test_data])
            logging.info("transform time: " + str(algo.transform_time_))
            Y_ += Y_i_test
            total_time += algo.fit_time_ + algo.transform_time_

        Y_ /= len(i_train_subjects)

    elif isinstance(algo, SRM):
        filename = storage_dir + \
                   algo_name + \
                   "".join(map(str, i_train_data[:10])) + \
                   ".temp"

        if os.path.isfile(filename):
            algo.load(filename)
            logging.info("No fitting needed, we use memory")
        else:
            algo.fit([data[k][:, i_train_data] for k in range(len(data))])
            algo.dump(filename)
            logging.info("fit time: " + str(algo.fit_time_))

        shared_response = algorithm.transform([data[k][:, i_test_data] for k in i_train_subjects]
                                              , index=i_train_subjects)
        logging.info("transform time: " + str(algo.transform_time_))
        Y_ = algorithm.inverse_transform(shared_response, index=i_test_subject)[0]
        logging.info("inverse transform time: " + str(algo.inverse_transform_time_))

        total_time = algo.fit_time_ + algo.transform_time_ + algo.inverse_transform_time_

    return Y_, total_time


algorithms = [
    (SRM(20, max_iter=10, algo_name="pca"), "srm_pca"),
    (SRM(20, max_iter=10, algo_name="ortho"), "srm_ortho"),
    (PieceWiseAlignment(labels, method="hyperalignment", n_jobs=n_jobs),"hyperalignment"),
    (PieceWiseAlignment(labels, method=RidgeCV(alphas=(1e-6,
                                                       1e-5,
                                                       1e-4,
                                                       1e-3,
                                                       1e-2,
                                                       1e-1,
                                                       1,
                                                       1e1,
                                                       1e2,
                                                       1e3,
                                                       1e4,
                                                       1e5,
                                                       1e6)), n_jobs=n_jobs),"ridgeCV"),
    (PieceWiseAlignment(labels, method="mean", n_jobs=n_jobs), "mean"),
    (PieceWiseAlignment(labels, method=RRR(k=25, alpha=0.003), n_jobs=n_jobs), "RRR"),
]

n_splits = 2
rs = ShuffleSplit(n_splits=n_splits, test_size=.1, random_state=0)
time_list = []
for train_subjects, test_subject in generate_train_test(subjects):
    print("TRAIN subject:" +
          train_subjects.__str__() +
          "TEST subject:" +
          test_subject.__str__())
    for algorithm, name in algorithms:
        print(name)
        mean_exp_var = None
        for train_X, test_X in rs.split(X[0][0]):
            print("TRAIN data:" +
                  train_X[:10].__str__() +
                  "TEST data:" +
                  test_X[:10].__str__())
            # Y_test is the prediction for subject test from all other subjects
            Y_test, time_pred = predict(algorithm, name, train_subjects, test_subject, train_X, test_X, X, storage_dir)
            print("prediction time: ", time_pred)
            time_list.append([train_X[:10] + test_X[:10], train_subjects, test_subject, name, time_pred])
            var_e = (Y_test - X[test_subject[0]][:, test_X]).var(axis=1)
            print("score" + str((1 - var_e).mean()))
            logging.info("score" + str((1 - var_e).mean()))
            exp_var = 1 - var_e
            if mean_exp_var is None:
                mean_exp_var = exp_var
            else:
                mean_exp_var += exp_var

        mean_exp_var /= n_splits
        nib.save(
            masker.inverse_transform(mean_exp_var),
            "/storage/workspace/hrichard/results/exp13/" +
            dataset +
            "alignment" +
            "_algo" +
            name +
            "subject_" + str(test_subject[0]) +
            "_exp13.nii"
        )
logging.info("Done")
