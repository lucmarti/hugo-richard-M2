import nibabel as nib
import numpy as np
import logging
import sys
sys.path.append("../")
from functional_alignment.load_data import load_forrest, load_ibc
from functional_alignment.pca_srm import PCASRM
from functional_alignment.probabilistic_srm import ProbabilisticSRM
from functional_alignment.deterministic_srm import DeterministicSRM
from functional_alignment.dictionary_srm import DictionarySRM
from nilearn.input_data import MultiNiftiMasker
import os.path

n_jobs = 10

#Forrest_config
logging.basicConfig(level=logging.DEBUG, filename="logfile_leftoutsubject_benchmark_roi", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")
mask_dir = "/storage/data/openfmri/ds113/sub001/BOLD/task001_run001/"
data_dir = "/storage/data/openfmri/ds113/"

subjects = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20]
runs = [1, 2, 3, 4, 5, 6, 7]

splits = []
for i in range(len(subjects)):
    split_train = []
    split_test = [i]
    for j in range(len(subjects)):
        if j != i:
            split_train.append(j)
    split_test = np.array(split_test)
    split_train = np.array(split_train)
    splits.append((split_train, split_test))

for i in range(len(splits)):
    logging.debug(splits[i])

mask_path = "/storage/workspace/hrichard/M2_internship/results/exp5/mask_forrest_roi2.nii.gz"
mask_memory = "/storage/tompouce/hrichard/forrest_roi/forrest_cache_roi/"
mask_img = nib.load(mask_path)
masker = mask_path

X, dataset = load_forrest(
    dataset="forrest_roi",
    subjects=subjects,
    runs=runs,
    data_path=data_dir + "sub%03d/BOLD/task001_run%03d/bold_dico_dico7Tad2grpbold7Tad_nl.nii.gz",
)

X_train = []
X_test = []
for i in range(len(X)):
    logging.debug("Masking Data for subject %i / %i "%(i + 1, len(X)))
    Xi_train = []
    for j in range(len(X[i])):
        X_ij = X[i][j]
        if j == len(X[i]) - 1:
            Xi_test = [X_ij]
        else:
            Xi_train.append(X_ij)
    X_train.append(Xi_train)
    X_test.append(Xi_test)

X_train = []
X_test = []
for i in range(len(X)):
    logging.debug("Masking Data for subject %i / %i "%(i + 1, len(X)))
    Xi_train = []
    for j in range(len(X[i])):
        X_ij = X[i][j]
        if j == len(X[i]) - 1:
            Xi_test = [X_ij]
        else:
            Xi_train.append(X_ij)
    X_train.append(Xi_train)
    X_test.append(Xi_test)

#IBC_config

# logging.basicConfig(level=logging.DEBUG, filename="logfile_alignment_compare_srm_ibc", filemode="a+",
#                         format="%(asctime)-15s %(levelname)-8s %(message)s")
#
# mask_dir = "/storage/store/data/ibc/derivatives/"
# data_dir = "/storage/store/data/ibc/derivatives/"
#
# subjects = [1, 2, 4, 5, 6, 7, 8, 9]
#
# X, masker, mask_img, dataset = load_ibc(
#     dataset="ibc",
#     subjects=subjects,
#     runs=[1, 2, 3],
#     mask_path=mask_dir + "group/gm_mask.nii.gz",
#     data_path=data_dir + "sub-%02d/ses*/func/wrdcsub*Trn%02d*.nii.gz"
# )

#X_train = [np.column_stack([X[i][k] for k in range(len(runs)-1)]) for i in range(len(X))]
#X_test = [X[i][len(runs)-1] for i in range(len(X))]

for k in [25, 50, 100, 200]:
    mask_params = {
        "detrend": True,
        "standardize": True,
        "memory": mask_memory,
        "mask": masker,
        "mask_strategy": "epi",
        "n_jobs": 5,
        "memory_level": 5
    }

    algorithms = [
        (PCASRM(n_components=k, **mask_params), "PCA"),
        (DeterministicSRM(n_components=k, speed="very_fast", **mask_params), "VeryFastDetSRM"),
        (DeterministicSRM(n_components=k, **mask_params), "FastDetSRM"),
        (ProbabilisticSRM(n_components=k, **mask_params), "ProbSRM"),
        (DictionarySRM(k, alpha=1, init_method="ortho_srm", **mask_params), "FastDictSRM_1"),
    ]

    mask_params["mask_img"] = mask_params["mask"]
    del mask_params["mask"]
    mask = MultiNiftiMasker(**mask_params).fit()

    for algorithm, name in algorithms:
        logging.info(name)
        for splits_i in range(len(splits)):
            train_index, test_index = splits[splits_i]
            logging.info("Splits")
            x_train = [X_train[i] for i in train_index]
            x_test = [X_test[i] for i in train_index]

            s_train = algorithm.fit_transform(x_train)
            s_test = algorithm.transform(x_test)

            algorithm.add_basis(X_train[test_index[0]], s_train)
            Y = algorithm.inverse_transform(s_test, index=-1)
            X_true = mask.transform(X_test[test_index[0]][0]).T

            var_e = (X_true - Y).var(axis=1)
            exp_var = 1 - var_e
            logging.info("Score")
            logging.info(exp_var.mean())
            result_directory = "/storage/workspace/hrichard/M2_internship/results/exp16_roi_leftout/k_%i/" % k
            if not os.path.exists(result_directory):
                os.makedirs(result_directory)
            nib.save(
                mask.inverse_transform(exp_var),
                result_directory +
                dataset +
                "alignment_srm" +
                "_algo" +
                name +
                "subject_" + str(test_index[0]) +
                "_exp10.nii"
            )
            logging.info("Done")
