import nibabel as nib
from sklearn.model_selection import ShuffleSplit
import numpy as np
from reduced_rank_regression import RRR
from ridge_cv import RidgeCV
from piecewise_alignment import PieceWiseAlignment, create_labels
from srm import SRM
import os
import logging
from load_data import load_ibc
from utils import generate_train_test, load_img
from nilearn.image import index_img
from deterministic_srm import DeterministicSRM
from rrr_cv import RRRCV
import random
from nilearn.input_data import NiftiMasker
from template_alignment import TemplateAlignment

n_jobs = 1

#IBC_config

logging.basicConfig(level=logging.DEBUG, filename="logfile_datainfluence_ibc", filemode="a+",
                        format="%(asctime)-15s %(levelname)-8s %(message)s")

# mask_dir = "/storage/store/data/ibc/derivatives/"
# mask_path=mask_dir + "group/gm_mask.nii.gz"

mask_path = "/storage/workspace/hrichard/results/exp13/mask_ibc_roi2.nii.gz"
storage_dir = "/storage/tompouce/hrichard/ibc_roi/ibc_roi_fit/"
memory="/storage/tompouce/hrichard/ibc_roi/ibc_roi_cache"

data_dir = "/storage/store/data/ibc/derivatives/"

subjects = [1, 2, 4, 5, 6, 7, 8, 9, 11, 13]
runs = ["Trn01",
        "Trn02",
        "Trn03",
        "Trn04",
        "Trn05",
        "Trn06",
        "Trn07",
        "Trn08",
        "Trn09",
        "Val01",
        "Val02",
        "Val03",
        "Val04",
        "Val05",
        "Val06",
        "Val07",
        "Val08"]

X, masker, mask_img, dataset = load_ibc(
    dataset="ibc",
    subjects=subjects,
    runs=runs,
    mask_path=mask_path,
    data_path=data_dir + "sub-%02d/ses*/func/wrdcsub*Clips*%s*.nii.gz",
    memory=memory
)

label_path_files = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_save = "/storage/workspace/hrichard/results/exp9/label_array_ibc.nii.npy"
label_path_img = "/storage/workspace/hrichard/results/exp9/label_piecewise_alignment_ibc.nii"

print(X[0, 0])

img_00 = index_img(X[0, 0], 0)
print(img_00.shape)
X_00 = NiftiMasker(
mask_img=mask_path,
memory=memory,
).fit().transform(img_00).T
labels = create_labels(X_00, mask_img.get_data(), n_pieces=2)

# # For resilient clustering
# if os.path.isfile(label_path_files):
#     labels = np.load(label_path_files)
# else:
#     labels = create_labels(X[0][0], mask_img.get_data(), n_pieces=1000)
#     np.save(label_path_save, labels)
#     label_img = masker.inverse_transform(labels)
#     nib.save(
#         label_img,
#         label_path_img
#     )


def predict(Y_test, algo, algo_name, i_train_subjects, i_test_subject, i_train_runs, i_test_runs, data, storage_d):
    Y_ = np.zeros_like(Y_test)
    total_time = 0
    if isinstance(algo, PieceWiseAlignment):
        for i in i_train_subjects:
            # Y_i_test is the prediction for subject test from subject i
            logging.info("Train subject: " + str(i))
            logging.info("Test subject: " + str(i_test_subject[0]))
            logging.info("Train runs: " + i_train_runs.__str__())
            logging.info("Test runs: " + i_test_runs.__str__())

            filename = storage_d + \
                       algo_name + \
                       str(i) + \
                       "-".join(map(str, i_test_subject)) + \
                       "-".join(map(str, i_train_runs)) + \
                       ".temp"

            algo.fit(data[i, :][i_train_runs], data[i_test_subject[0], :][i_train_runs])
            logging.info("fit time: " + str(algo.fit_time_))
            algo.dump(filename)

            Y_i_test = algo.transform(data[i, :][i_test_runs])
            logging.info("transform time: " + str(algo.transform_time_))
            logging.info("local score")
            logging.info(1 - (Y_test - Y_i_test).var(axis=1).mean())
            Y_ += Y_i_test
            total_time += algo.fit_time_ + algo.transform_time_

        Y_ /= len(i_train_subjects)

    elif isinstance(algo, DeterministicSRM) or isinstance(algo, TemplateAlignment):
        filename = storage_dir + \
                   algo_name + \
                   "-".join(map(str, i_train_runs)) + \
                   ".temp"

        if os.path.isfile(filename):
            algo.load(filename)
            logging.info("No fitting needed, we use memory")
        else:
            algo.fit(data[:, i_train_runs])
            algo.dump(filename)
            logging.info("fit time: " + str(algo.fit_time_))

        print(data.shape)
        print(i_train_subjects)
        print(i_train_runs)

        shared_response = algorithm.transform(data[i_train_subjects, :][:, i_test_runs],
                                              index=i_train_subjects)

        logging.info("transform time: " + str(algo.transform_time_))
        Y_ = algorithm.inverse_transform(shared_response, index=i_test_subject)[0]
        logging.info("inverse transform time: " + str(algo.inverse_transform_time_))

        total_time = algo.fit_time_ + algo.transform_time_ + algo.inverse_transform_time_

    return Y_, total_time


algorithms = [
    (DeterministicSRM(20, max_iter=10, masker=masker, scaling=True), "scaled_srm_ortho"),
    (DeterministicSRM(20, max_iter=10, masker=masker), "srm_ortho"),
    (PieceWiseAlignment(labels, masker=masker, method="hyperalignment", n_jobs=n_jobs),"hyperalignment"),
    (PieceWiseAlignment(labels, masker=masker, method=RidgeCV(alphas=(
                                                       1e3,
                                                       1e4,
                                                       1e5,
                                                       )), n_jobs=n_jobs),"ridgeCV"),
    (PieceWiseAlignment(labels, masker=masker, method="mean", n_jobs=n_jobs), "mean"),
    (
        PieceWiseAlignment(labels, masker=masker, method=RRRCV(
            ks=np.arange(1, 100, 5),
            alphas=[1e-2, 1e-1, 1, 10, 1e2, 1e3]
        ), n_jobs=n_jobs), "RRRCV"
    ),
]

n_splits = 1
rs = ShuffleSplit(n_splits=n_splits, test_size=.1, random_state=0)
time_list = []

random.seed(0)
for train_subjects, test_subject in random.sample(generate_train_test(subjects), 5):
    print("TRAIN subject:" +
          train_subjects.__str__() +
          "TEST subject:" +
          test_subject.__str__())
    for algorithm, name in algorithms:
        print(algorithm)
        random.seed(0)
        for train_runs_list, test_runs in rs.split(X[0]):
            for i in range(1, len(train_runs_list), 2):
                for j in range(2):
                    train_runs = random.sample(train_runs_list, i)

                    print("TRAIN runs:" +
                          train_runs.__str__() +
                          "TEST runs:" +
                          test_runs.__str__())

                    Y = load_img(masker, X[test_subject, test_runs])

                    # Y_test is the prediction for subject test from all other subject
                    Y_test, time_pred = predict(Y,
                                                algorithm,
                                                name,
                                                train_subjects,
                                                test_subject,
                                                train_runs,
                                                test_runs,
                                                X,
                                                storage_dir)

                    print("prediction time: ", time_pred)
                    var_e = (Y_test - Y).var(axis=1)
                    print("score" + str((1 - var_e).mean()))
                    logging.info("score" + str((1 - var_e).mean()))
                    exp_var = 1 - var_e
                    nib.save(
                        masker.inverse_transform(exp_var),
                        "/storage/workspace/hrichard/results/exp14/" +
                        dataset +
                        "alignment" +
                        "_algo" +
                        name +
                        "subject_" + str(test_subject[0]) +
                        "trainruns_" + "-".join(map(str, train_runs)) +
                        "testruns_" + "-".join(map(str, test_runs)) +
                        "_exp14.nii"
                    )

                    if os.path.isfile("/storage/workspace/hrichard/results/exp14/" +
                        dataset +
                        "_timepred_alignment" +
                        "_algo" +
                        name +
                        "subject_" + str(test_subject[0]) +
                        "trainruns_" + "-".join(map(str, train_runs)) +
                        "testruns_" + "-".join(map(str, test_runs)) +
                        "_exp14.nii"):
                        print("time already recorded")
                    else:
                        with open("/storage/workspace/hrichard/results/exp14/" +
                            dataset +
                            "_timepred_alignment" +
                            "_algo" +
                            name +
                            "subject_" + str(test_subject[0]) +
                            "trainruns_" + "-".join(map(str, train_runs)) +
                            "testruns_" + "-".join(map(str, test_runs)) +
                            "_exp14.nii", "w") as time_file:
                            time_file.write(str(time_pred))

logging.info("Done")
