from glob import glob
import numpy as np
import joblib
import os
from sklearn.linear_model import RidgeCV
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import ShuffleSplit
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt


res_path = "/storage/tompouce/hrichard/retreat_2019/big_basis/distance_matrix"
paths = glob("/storage/tompouce/hrichard/retreat_2019/big_basis/basis*.npy")
paths = ["/storage/tompouce/hrichard/retreat_2019/big_basis/basis_" + str(i) + ".npy"
         for i in range(len(paths))]
fig_path = "/storage/tompouce/hrichard/retreat_2019/figures/big_basis_distance_matrix.pdf"


class DummyRegressor:
    def __init__(self):
        self.p = 0

    def fit(self, X, Y):
        self.p = np.mean(Y)
        return self

    def predict(self, X):
        return self.p


def create_distance_matrix(basis_paths, res_path):
    if os.path.exists(res_path):
        return joblib.load(res_path)
    else:
        n_components = np.load(basis_paths[0]).shape[0]
        distance_matrix = np.zeros((n_components, len(basis_paths), len(basis_paths)))

        for i, path1 in enumerate(basis_paths):
            print(i, len(basis_paths), path1)
            for j, path2 in enumerate(basis_paths):
                basis_1 = np.load(path1)
                basis_2 = np.load(path2)

                for k in range(n_components):
                    distance_matrix[k, i, j] = basis_1[k].dot(basis_2[k])

        joblib.dump(distance_matrix, res_path)
    return distance_matrix


distance_matrix = create_distance_matrix(paths, res_path)
Y = np.array(joblib.load("/storage/tompouce/hrichard/retreat_2019/liste_ages.gz"))

shuffle = ShuffleSplit(n_splits=5, train_size=0.8, test_size=0.2)

result = []
for train, test in shuffle.split(range(distance_matrix.shape[1])):
    for name_algo in [
        ("ridge", RidgeCV(alphas=tuple(10 ** k for k in range(-5, 5)))),
        ("dummy", DummyRegressor()),
        ("random forrest", RandomForestRegressor())
    ]:
        name, algo = name_algo
        X_preds_train = []
        X_preds_test = []
        for k in range(distance_matrix.shape[0]):
            X_train = distance_matrix[k, :, :][train, :][:, train]
            X_test = distance_matrix[k, :, :][test, :][:, train]
            algo.fit(X_train, Y[train])
            X_preds_train.append(algo.predict(X_train))
            Y_pred = algo.predict(X_test)
            X_preds_test.append(Y_pred)
            mae = np.mean(np.abs(Y[test] - Y_pred))
            result.append([mae, train.__str__(), name, k])

        X_preds_train = np.array(X_preds_train).T
        X_preds_test = np.array(X_preds_test).T
        rf = RandomForestRegressor()
        rf.fit(X_preds_train, Y[train])
        Y_pred = rf.predict(X_preds_test)
        mae = np.mean(np.abs(Y[test] - Y_pred))
        for k in range(distance_matrix.shape[0]):
            result.append([mae, train.__str__(), "RF_" + name, k])

result = pd.DataFrame(result, columns=["mae", "split", "algo", "component"])
plt.figure()
sns.lineplot(x="component", y="mae", hue="algo", data=result)
plt.savefig(fig_path)
plt.close()








