from functional_alignment.fastSRM import FastSRM
from glob import glob
import joblib
import nilearn
nilearn.CHECK_CACHE_VERSION = False

paths = glob("/storage/tompouce/hrichard/retreat_2019/camcan/*task-Movie*preproc*.nii.gz")
input_paths = [[p] for p in paths]
print(input_paths)
atlas_path = "/storage/workspace/hrichard/M2_internship/parcellations/Parcellations/MNI/" \
             "Schaefer2018_800Parcels_17Networks_order_FSLMNI152_2mm.nii.gz"

# Introducing fastSRM
srm = FastSRM(n_components=20,
              mask='/home/parietal/hrichard/cogspaces_data/mask/hcp_mask.nii.gz',
              atlas=atlas_path,
              memory="/storage/tompouce/hrichard/retreat_2019/",
              temp_dir="/storage/tompouce/hrichard/retreat_2019/",
              verbose=1,
              standardize=True,
              detrend=True,
              t_r=2,
              )

srm.fit(input_paths)
shared_response = srm.transform(input_paths)
joblib.dump(shared_response, "/storage/tompouce/hrichard/retreat_2019/shared_response")
