from nilearn.input_data import NiftiLabelsMasker
from glob import glob
from sklearn.covariance import LedoitWolf
import joblib
import nilearn
from nilearn.image import high_variance_confounds


nilearn.CHECK_CACHE_VERSION = False

# load atlas
atlas_path = '/storage/store/data/nilearn_data/basc_multiscale_2015/'\
             'template_cambridge_basc_multiscale_nii_sym/template_cambridge'\
             '_basc_multiscale_sym_scale444.nii.gz'


exp_params = {
        "detrend": True,
        "standardize": True,
        "memory": "/storage/tompouce/hrichard/retreat_2019/",
        "mask_strategy": "epi",
        "n_jobs": 1,
        "memory_level": 5,
        "low_pass": 0.1,
        "high_pass": 0.01,
        "t_r": 2,
        "verbose": 1,
        "smoothing_fwhm": 5,
}

masker = NiftiLabelsMasker(atlas_path, detrend=True, standardize=True, low_pass=0.1,
                           memory="/storage/tompouce/hrichard/retreat_2019/",
                           memory_level=1,
                           high_pass=0.01, t_r=2, smoothing_fwhm=5).fit()


paths = glob("/storage/tompouce/hrichard/retreat_2019/camcan/*task-Movie*preproc*.nii.gz")
confounds = []

for path in paths:
    sub = path.split("_task-Movie_space-MNI152NLin2009cAsym_desc-preproc_bold.nii.gz")[0]
    confounds.append(sub + "_task-Movie_desc-confounds_regressors.tsv")


covs = []
for path, confound in zip(paths, confounds):
    print(path)
    confound = high_variance_confounds(path)
    print("confound done")
    X = masker.transform(path, confounds=confound)
    print("masker done", X.shape)
    ledoit = LedoitWolf()
    ledoit.fit(X)
    print("ledoit wolf done")
    cov = ledoit.covariance_
    print("cov shape", cov.shape)
    covs.append(cov)

joblib.dump(covs, "/storage/tompouce/hrichard/retreat_2019/fonctional_connectivity")
