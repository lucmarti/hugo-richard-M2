RETREAT 2019
---------------

In order to use FastSRM on drago (it is even simpler to do it on your own computer)

* Connect to drago

``` ssh -L 8888:localhost:8888 drago```

* Clone this repository

``` git clone https://gitlab.inria.fr/hrichard/hugo-richard-M2.git```

* Go to the retreat folder and install functional alignment


``` cd hugo-richard-M2```

``` cd retreat```

``` make install```

```source venv/bin/activate```

* Open the demo notebook

```cd demo```


```jupyter notebook --port 8888```

* Open your browser with the address

```localhost:8888/```