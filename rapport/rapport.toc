\select@language {english}
\contentsline {section}{\numberline {1}Declaration of academic integrity}{3}{section.1}
\contentsline {section}{\numberline {2}Introduction}{4}{section.2}
\contentsline {paragraph}{Context}{4}{section*.2}
\contentsline {paragraph}{Objective}{4}{section*.3}
\contentsline {paragraph}{}{4}{section*.4}
\contentsline {section}{\numberline {3}fMRI: looking into brains}{4}{section.3}
\contentsline {subsection}{\numberline {3.1}BOLD Signal and Scanners}{4}{subsection.3.1}
\contentsline {subsection}{\numberline {3.2}Registration methods: from anatomy-based alignment to function-based alignment}{5}{subsection.3.2}
\contentsline {paragraph}{Anatomy-based alignment}{5}{section*.5}
\contentsline {paragraph}{Shortcomings of anatomy-based alignment}{5}{section*.6}
\contentsline {paragraph}{A new approach: Data-based alignment}{6}{section*.7}
\contentsline {section}{\numberline {4}Feature alignment algorithms}{6}{section.4}
\contentsline {subsection}{\numberline {4.1}Definitions}{6}{subsection.4.1}
\contentsline {paragraph}{Voxels and timeframes}{6}{section*.8}
\contentsline {paragraph}{Sessions and timecourses}{6}{section*.9}
\contentsline {paragraph}{Linear time-independent algorithms}{6}{section*.10}
\contentsline {paragraph}{Measure of performance}{8}{section*.11}
\contentsline {subsection}{\numberline {4.2}Hyperalignment \cite {hyperalignment} and \cite {hyper2}}{8}{subsection.4.2}
\contentsline {subsubsection}{\numberline {4.2.1}Mapping}{8}{subsubsection.4.2.1}
\contentsline {paragraph}{A simple example with 2 subjects and 2 sessions: a one-to-one mapping}{8}{section*.12}
\contentsline {paragraph}{A straight-forward generalization: a many-to-one mapping}{9}{section*.13}
\contentsline {paragraph}{Resolution of the optimization problem in the training phase}{9}{section*.14}
\contentsline {paragraph}{Searchlight procedure}{11}{section*.15}
\contentsline {subsubsection}{\numberline {4.2.2}Template estimation}{12}{subsubsection.4.2.2}
\contentsline {subsubsection}{\numberline {4.2.3}Comments on hyperalignment}{13}{subsubsection.4.2.3}
\contentsline {subsection}{\numberline {4.3}Shared response model \cite {srm}}{13}{subsection.4.3}
\contentsline {subsubsection}{\numberline {4.3.1}Mapping}{14}{subsubsection.4.3.1}
\contentsline {subsubsection}{\numberline {4.3.2}Low dimension template}{14}{subsubsection.4.3.2}
\contentsline {subsubsection}{\numberline {4.3.3}Comments on the shared response model}{15}{subsubsection.4.3.3}
\contentsline {section}{\numberline {5}Our method for building new feature alignment algorithms}{15}{section.5}
\contentsline {subsection}{\numberline {5.1}Mapping methods}{15}{subsection.5.1}
\contentsline {subsubsection}{\numberline {5.1.1}Identity regression (or mean)}{15}{subsubsection.5.1.1}
\contentsline {subsubsection}{\numberline {5.1.2}Scaled orthogonal regression}{15}{subsubsection.5.1.2}
\contentsline {subsubsection}{\numberline {5.1.3}Ridge Regression}{16}{subsubsection.5.1.3}
\contentsline {subsubsection}{\numberline {5.1.4}Reduced rank regression}{16}{subsubsection.5.1.4}
\contentsline {subsection}{\numberline {5.2}Template building}{17}{subsection.5.2}
\contentsline {subsubsection}{\numberline {5.2.1}Dictionary Learning}{17}{subsubsection.5.2.1}
\contentsline {subsubsection}{\numberline {5.2.2}Principal component analysis}{18}{subsubsection.5.2.2}
\contentsline {subsubsection}{\numberline {5.2.3}Scaled shared response model}{18}{subsubsection.5.2.3}
\contentsline {subsubsection}{\numberline {5.2.4}Reinforcement mapping}{18}{subsubsection.5.2.4}
\contentsline {section}{\numberline {6}Experiments}{19}{section.6}
\contentsline {subsection}{\numberline {6.1}Dataset}{19}{subsection.6.1}
\contentsline {subsection}{\numberline {6.2}Benchmark of mapping procedures: generalization to different but similar data}{20}{subsection.6.2}
\contentsline {subsection}{\numberline {6.3}Benchmark of template building procedure: generalization to different but similar data}{20}{subsection.6.3}
\contentsline {subsection}{\numberline {6.4}Benchmark of mapping algorithms: generalization to a completely new experiment}{26}{subsection.6.4}
\contentsline {section}{\numberline {7}Conclusion}{26}{section.7}
\contentsline {section}{\numberline {8}Future Work}{26}{section.8}
\contentsline {section}{References}{31}{section.8}
\contentsline {section}{\numberline {9}Appendix}{33}{section.9}
\contentsline {subsection}{\numberline {9.1}Proof: coefficient of determination and explained variance }{33}{subsection.9.1}
