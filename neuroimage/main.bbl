\begin{thebibliography}{}

\bibitem [\protect \citeauthoryear {%
Anderson%
\ \protect \BOthers {.}}{%
Anderson%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
anderson2016enabling}
\APACinsertmetastar {%
anderson2016enabling}%
\begin{APACrefauthors}%
Anderson, M\BPBI J.%
, Capota, M.%
, Turek, J\BPBI S.%
, Zhu, X.%
, Willke, T\BPBI L.%
, Wang, Y.%
\BDBL {}Norman, K\BPBI A.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Enabling factor analysis on thousand-subject
  neuroimaging datasets} {Enabling factor analysis on thousand-subject
  neuroimaging datasets}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {2016 IEEE International Conference on Big Data (Big
  Data)} {2016 ieee international conference on big data (big data)}\ (\BPGS\
  1151--1160).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Bellec%
, Rosa-Neto%
, Lyttelton%
, Benali%
\BCBL {}\ \BBA {} Evans%
}{%
Bellec%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2010}}%
}]{%
bellec2010multi}
\APACinsertmetastar {%
bellec2010multi}%
\begin{APACrefauthors}%
Bellec, P.%
, Rosa-Neto, P.%
, Lyttelton, O\BPBI C.%
, Benali, H.%
\BCBL {}\ \BBA {} Evans, A\BPBI C.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2010}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Multi-level bootstrap analysis of stable clusters in
  resting-state fMRI} {Multi-level bootstrap analysis of stable clusters in
  resting-state fmri}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Neuroimage}{51}{3}{1126--1139}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
\APACcitebtitle {Brain Imaging Analysis Kit}}{%
\APACcitebtitle {Brain Imaging Analysis Kit}}{%
{\protect \APACyear {{\protect \bibnodate {}}}}%
}]{%
brainiak}
\APACinsertmetastar {%
brainiak}%
\APACrefbtitle {Brain Imaging Analysis Kit.} {Brain imaging analysis kit.}
\newblock
\APACrefYearMonthDay{{\protect \bibnodate {}}}{}{}.
\newblock
\begin{APACrefURL} \url{http://brainiak.org.} \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
P\BHBI H.~Chen%
\ \protect \BOthers {.}}{%
P\BHBI H.~Chen%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
chen2016convolutional}
\APACinsertmetastar {%
chen2016convolutional}%
\begin{APACrefauthors}%
Chen, P\BHBI H.%
, Zhu, X.%
, Zhang, H.%
, Turek, J\BPBI S.%
, Chen, J.%
, Willke, T\BPBI L.%
\BDBL {}Ramadge, P\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A convolutional autoencoder for multi-subject fMRI data
  aggregation} {A convolutional autoencoder for multi-subject fmri data
  aggregation}.{\BBCQ}
\newblock
\APACjournalVolNumPages{arXiv preprint arXiv:1608.04846}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
P\BHBI H\BPBI C.~Chen%
\ \protect \BOthers {.}}{%
P\BHBI H\BPBI C.~Chen%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2015}}%
}]{%
chen2015reduced}
\APACinsertmetastar {%
chen2015reduced}%
\begin{APACrefauthors}%
Chen, P\BHBI H\BPBI C.%
, Chen, J.%
, Yeshurun, Y.%
, Hasson, U.%
, Haxby, J.%
\BCBL {}\ \BBA {} Ramadge, P\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2015}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A reduced-dimension fMRI shared response model} {A
  reduced-dimension fmri shared response model}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {Advances in Neural Information Processing Systems}
  {Advances in neural information processing systems}\ (\BPGS\ 460--468).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Guntupalli%
\ \protect \BOthers {.}}{%
Guntupalli%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2016}}%
}]{%
guntupalli2016model}
\APACinsertmetastar {%
guntupalli2016model}%
\begin{APACrefauthors}%
Guntupalli, J\BPBI S.%
, Hanke, M.%
, Halchenko, Y\BPBI O.%
, Connolly, A\BPBI C.%
, Ramadge, P\BPBI J.%
\BCBL {}\ \BBA {} Haxby, J\BPBI V.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2016}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A model of representational spaces in human cortex} {A
  model of representational spaces in human cortex}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cerebral cortex}{26}{6}{2919--2934}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Hanke%
\ \protect \BOthers {.}}{%
Hanke%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2014}}%
}]{%
hanke2014high}
\APACinsertmetastar {%
hanke2014high}%
\begin{APACrefauthors}%
Hanke, M.%
, Baumgartner, F\BPBI J.%
, Ibe, P.%
, Kaule, F\BPBI R.%
, Pollmann, S.%
, Speck, O.%
\BDBL {}Stadler, J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2014}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A high-resolution 7-Tesla fMRI dataset from complex
  natural stimulation with an audio movie} {A high-resolution 7-tesla fmri
  dataset from complex natural stimulation with an audio movie}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Scientific data}{1}{}{140003}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Mensch%
, Mairal%
, Thirion%
\BCBL {}\ \BBA {} Varoquaux%
}{%
Mensch%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2018}}%
}]{%
mensch2018extracting}
\APACinsertmetastar {%
mensch2018extracting}%
\begin{APACrefauthors}%
Mensch, A.%
, Mairal, J.%
, Thirion, B.%
\BCBL {}\ \BBA {} Varoquaux, G.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Extracting Universal Representations of Cognition across
  Brain-Imaging Studies} {Extracting universal representations of cognition
  across brain-imaging studies}.{\BBCQ}
\newblock
\APACjournalVolNumPages{arXiv preprint arXiv:1809.06035}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Nishimoto%
\ \protect \BOthers {.}}{%
Nishimoto%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2011}}%
}]{%
nishimoto2011reconstructing}
\APACinsertmetastar {%
nishimoto2011reconstructing}%
\begin{APACrefauthors}%
Nishimoto, S.%
, Vu, A.%
, Naselaris, T.%
, Benjamini, Y.%
, Yu, B.%
\BCBL {}\ \BBA {} Gallant, J\BPBI L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2011}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Reconstructing visual experiences from brain activity
  evoked by natural movies} {Reconstructing visual experiences from brain
  activity evoked by natural movies}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Current Biology}{21}{19}{1641--1646}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Pinho%
\ \protect \BOthers {.}}{%
Pinho%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2018}}%
}]{%
ibc}
\APACinsertmetastar {%
ibc}%
\begin{APACrefauthors}%
Pinho, A.%
, Amadon, A.%
, Ruest, T.%
, Fabre, M.%
, Dohmatob, I., E.and~Denghien%
, Ginisty, C.%
\BDBL {}Thirion, B.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
\APACrefbtitle {Individual brain charting.} {Individual brain charting.}
\newblock
\begin{APACrefURL} [{2018}]\url{https://project.inria.fr/IBC/} \end{APACrefURL}
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Poldrack%
\ \protect \BOthers {.}}{%
Poldrack%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2013}}%
}]{%
poldrack2013toward}
\APACinsertmetastar {%
poldrack2013toward}%
\begin{APACrefauthors}%
Poldrack, R\BPBI A.%
, Barch, D\BPBI M.%
, Mitchell, J.%
, Wager, T.%
, Wagner, A\BPBI D.%
, Devlin, J\BPBI T.%
\BDBL {}Milham, M.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2013}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Toward open sharing of task-based fMRI data: the
  OpenfMRI project} {Toward open sharing of task-based fmri data: the openfmri
  project}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Frontiers in neuroinformatics}{7}{}{12}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Schaefer%
\ \protect \BOthers {.}}{%
Schaefer%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
schaefer2017local}
\APACinsertmetastar {%
schaefer2017local}%
\begin{APACrefauthors}%
Schaefer, A.%
, Kong, R.%
, Gordon, E\BPBI M.%
, Laumann, T\BPBI O.%
, Zuo, X\BHBI N.%
, Holmes, A\BPBI J.%
\BDBL {}Yeo, B\BPBI T.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Local-global parcellation of the human cerebral cortex
  from intrinsic functional connectivity MRI} {Local-global parcellation of the
  human cerebral cortex from intrinsic functional connectivity mri}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Cerebral Cortex}{28}{9}{3095--3114}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Shvartsman%
\ \protect \BOthers {.}}{%
Shvartsman%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
shvartsman2017matrix}
\APACinsertmetastar {%
shvartsman2017matrix}%
\begin{APACrefauthors}%
Shvartsman, M.%
, Sundaram, N.%
, Aoi, M\BPBI C.%
, Charles, A.%
, Wilke, T\BPBI C.%
\BCBL {}\ \BBA {} Cohen, J\BPBI D.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Matrix-normal models for fMRI analysis} {Matrix-normal
  models for fmri analysis}.{\BBCQ}
\newblock
\APACjournalVolNumPages{arXiv preprint arXiv:1711.03058}{}{}{}.
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Turek%
, Ellis%
, Skalaban%
, Turk-Browne%
\BCBL {}\ \BBA {} Willke%
}{%
Turek%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2018}}%
}]{%
turek2018capturing}
\APACinsertmetastar {%
turek2018capturing}%
\begin{APACrefauthors}%
Turek, J\BPBI S.%
, Ellis, C\BPBI T.%
, Skalaban, L\BPBI J.%
, Turk-Browne, N\BPBI B.%
\BCBL {}\ \BBA {} Willke, T\BPBI L.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Capturing Shared and Individual Information in fMRI
  Data} {Capturing shared and individual information in fmri data}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {2018 IEEE International Conference on Acoustics, Speech
  and Signal Processing (ICASSP)} {2018 ieee international conference on
  acoustics, speech and signal processing (icassp)}\ (\BPGS\ 826--830).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Turek%
, Willke%
, Chen%
\BCBL {}\ \BBA {} Ramadge%
}{%
Turek%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2017}}%
}]{%
turek2017semi}
\APACinsertmetastar {%
turek2017semi}%
\begin{APACrefauthors}%
Turek, J\BPBI S.%
, Willke, T\BPBI L.%
, Chen, P\BHBI H.%
\BCBL {}\ \BBA {} Ramadge, P\BPBI J.%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2017}{}{}.
\newblock
{\BBOQ}\APACrefatitle {A semi-supervised method for multi-subject fMRI
  functional alignment} {A semi-supervised method for multi-subject fmri
  functional alignment}.{\BBCQ}
\newblock
\BIn{} \APACrefbtitle {2017 IEEE International Conference on Acoustics, Speech
  and Signal Processing (ICASSP)} {2017 ieee international conference on
  acoustics, speech and signal processing (icassp)}\ (\BPGS\ 1098--1102).
\PrintBackRefs{\CurrentBib}

\bibitem [\protect \citeauthoryear {%
Vodrahalli%
\ \protect \BOthers {.}}{%
Vodrahalli%
\ \protect \BOthers {.}}{%
{\protect \APACyear {2018}}%
}]{%
vodrahalli2018mapping}
\APACinsertmetastar {%
vodrahalli2018mapping}%
\begin{APACrefauthors}%
Vodrahalli, K.%
, Chen, P\BHBI H.%
, Liang, Y.%
, Baldassano, C.%
, Chen, J.%
, Yong, E.%
\BDBL {}others%
\end{APACrefauthors}%
\unskip\
\newblock
\APACrefYearMonthDay{2018}{}{}.
\newblock
{\BBOQ}\APACrefatitle {Mapping between fMRI responses to movies and their
  natural language annotations} {Mapping between fmri responses to movies and
  their natural language annotations}.{\BBCQ}
\newblock
\APACjournalVolNumPages{Neuroimage}{180}{}{223--231}.
\PrintBackRefs{\CurrentBib}

\end{thebibliography}
