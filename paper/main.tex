\documentclass[a4paper]{article}

%% Language and font encodings
\usepackage[english]{babel}
\usepackage[utf8x]{inputenc}
\usepackage[colorinlistoftodos]{todonotes}

%% Sets page size and margins
\usepackage[a4paper,top=3cm,bottom=2cm,left=3cm,right=3cm,marginparwidth=1.75cm]{geometry}

%% Useful packages
\usepackage{amsmath}
\usepackage{graphicx}
\usepackage{amsfonts}
\usepackage{apacite}
\usepackage{multicol}
\usepackage{bbm}

\title{Interpretable shared response model}
\author{Hugo Richard, Bertrand Thirion, Jonathan Pillow}

\begin{document}
\maketitle

\begin{abstract}
The functional variability between subjects constitutes a fundamental challenge specific to human brain understanding. The shared response model (SRM) \cite{chen2015reduced} projects subjects data in a low dimensional shared space in which inter-subject variability is reduced. However it is challenging to interpret data in shared space as coordinates are linked to spatial maps that are not sparse, not smooth and not positive. 

In this work, we provide an interpretable shared response model (ISRM) with sparsity, smoothness and positivity constrains on spatial maps releasing the orthogonality constrain of the original SRM algorithm. We show ISRM yields more interpretable maps than SRM and better reconstruction error on two different datasets. 
\end{abstract}

\section{Introduction}
There is a very important biological variability between human brains both in an anatomical and functional point of view. This variability between human brains results in differences in the spatial arrangement of functional responses that can be observed through functional imaging. Standard registration methods (see the review \cite{brett2002problem}) only make it possible to match brain organization across individuals at a coarse scale, hence they typically lose  fine-grain details.
The ongoing accumulation of fmri data (see for example NeuroVault \cite{gorgolewski2015neurovault}) opens the way for a data-based approach to reduce inter-subjects variability.

The shared response model sees each brain image as a weighted sum of orthogonal spatial maps. The weights corresponds to coordinates in a low dimensional space and are shared from one subject to an other whereas the spatial maps represents the subject's specific axis in voxel space. Intuitively the shared response is highly linked to the stimuli while the spatial maps are informative of differences between subjects. In particular, the spatial maps should allow one to see what is common and what differs in the spatial arrangement of functional responses of each subject. However the orthogonal constrains in the original SRM model yields real valued maps where almost all voxels are activated and with no clear correlation between nearby voxels. Thus these maps are very difficult to interpret.

We introduce an interpretable SRM model (ISRM) where orthogonality constrains on spatial maps are replaced by sparsity, smoothness and positivity constrains. We show that our model produces maps that are smooth, sparse and positive. We show that interpretable maps can give better results in term of generalization to new data.

\section{Related work}
Orthogonality constrains are present in many models aiming at reducing inter-subject variability such as PCA, hyperalignment (including using searchlight) \cite{guntupalli2016model} and in some variants of the shared response model such as S-SRM \cite{chen2016convolutional}.
However, the orthogonality constrains are not supported by any biological evidence and several models have already relaxed these constrains such as \cite{shvartsman2017matrix} using matrix normal priors instead and \cite{chen2016convolutional} using a small autoencoder. 

Focusing on interpretability, topographic factor analysis (TFA) \cite{manning2014topographic} sees each brain image as a weighted sum of Gaussian blobs which yields an interpretable model but has never been cast to the SRM framework.

To the best of our knowledge ISRM is the first model that use intuitive constrains to yield interpretable maps while reaching best performance than orthogonal SRM on various dataset.

Like in \cite{varoquaux2011multi} and \cite{spacenet}, we use graph-net penalty to impose smoothness and l1-penalty to impose sparsity. We add positivity constrains to increase interpretability and show that this can be done without losing in performance. The constrains we use and the optimization problem we solve can be seen as one step of smooth sparse online dictionary learning (see \cite{dohmatob2016learning}). We build upon this work even though the optimization scheme we use is slightly different.

\section{The shared response model (SRM)}
The shared response model is a latent factor model. The brain images of subject $i$ are stored in a matrix $X_i \in\mathbb{R}^{V, T}$ where $V$ is the number of voxels and $T$ the number of timeframes (the number of acquired brain images). Each brain image $X_i[:, t]$ of subject $i$ at time $t$ is seen as a weighted sum of $K$ subject specific orthogonal spatial maps $W_i \in\mathbb{R}^{V, K}$. The weights $S \in \mathbb{R}^{K, T}$ are shared between subjects.

For simplicity we introduce the matrices $X \in \mathbb{R}^{NV, T}$ and  $W \in \mathbb{R}^{NV, T}$ such that:

\begin{multicols}{2}
\begin{equation*}
X = 
\begin{bmatrix}
    X_{1} \\
    X_{2} \\
    \vdots \\
    X_{N}
\end{bmatrix}
\end{equation*}

\begin{equation*}
W = 
\begin{bmatrix}
    W_{1} \\
    W_{2} \\
    \vdots \\
    W_{N}
\end{bmatrix}
\end{equation*}
\end{multicols}

Formally any shared response model is defined by: 
\[
X = WS + \epsilon
\]

where the model for the spatial maps $W$, the shared response $S$ and the noise $\epsilon$ needs to be defined. Figure~\ref{fig:conceptual_figure1} illustrates how shared response model are defined.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/conceptual_figure1}
\caption{ \textbf{A}: The raw fmri data are seen as a weighted combination of subject specific spatial maps with additive noise. The weights are shared between subjects and constitute the shared response to the stimuli.
\textbf{B}: The sparsity and smoothness of each spatial maps are controlled by parameters $\alpha$ and $\beta$. In our case the noise $\epsilon$ follows a normal law with mean 0 and identity covariance ($\sigma_{\epsilon}$ is the identity matrix).}
\label{fig:conceptual_figure1}
\end{figure}


\subsection{Deterministic SRM model (DetSRM)}
The deterministic orthogonal SRM model assumes Gaussian noise with the same variance for all subjects.

Formally the model reads:
\begin{align*}
&X_i[:, t] \sim \mathcal{N}(W_i S[:, t], \sigma^2) \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_i^T W_i = I_K
\end{align*}

Minimizing the log-likelihood we obtain the following optimization problem:
\begin{align*}
&\text{argmin}_{W_1, \cdots, W_N, S} \sum_{i=1}^N ||X_i - W_iS||^2 \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_i^T W_i = I_K
\end{align*}

This can be solved efficiently using alternate minimization on 
$(W_1, \cdots , W_N )$ and $S$. Each of the corresponding minimization problem has a closed form solution:
\begin{align*}
\forall k, &\text{argmin}_{W_k} \sum_{i=1}^N ||X_i - W_iS||^2 = U_k V_k  \\
& \text{where} \ U_k, D_k, V_k = SVD(X_k S^T)
\end{align*}
where $SVD$ means singular value decomposition and 
\[
\text{argmin}_{S} \sum_{i=1}^N ||X_i - W_iS||^2 = \frac{W_i^T X_i}{N}  \\
\]

Convergence is typically reached after $n_{iter} = 10$ iterations.

\subsection{Probabilistic SRM model (ProbSRM)}
In the probabilistic model setting the shared response is learned using its covariance matrix and the variance of the Gaussian noise $\sigma_1, \cdots , \sigma_N$ is assumed different for different subjects. In the original paper, an intercept is also learned but since we remove the mean of each time-course as a preprocessing step it is of no use here.

Formally the model reads:
\begin{align*}
&S[:, t] \sim \mathcal{N}(0, \Sigma_s) \\
&X_i[:, t] \sim \mathcal{N}(W_i S[:, t], \sigma_i^2) \\
&\text{such that } \forall i \in \{1,\cdots, N\} \  W_i^T W_i = I_K
\end{align*}

Optimization is done using an EM algorithm described in \cite{chen2015reduced}. Convergence is typically reached after $n_{iter} = 10$ iterations.

\section{Interpretable SRM (ISRM) model}
Let us denote $W \in \mathbb{R}^{NV, K}$ the concatenation of spatial maps of all subjects and $X \in \mathbb{R}^{NV, T}$ the concatenation of the data of all subjects . Like in the DetSRM model we want to find $W$ and $S$ such that $||WS - X||^2$ is small. However we impose specific constrains on $W$ in order to obtain interpretable maps.

We formally define our objective as:
\begin{align}
\text{argmin}_{\{W_1 \geq 0, \cdots, W_N \geq 0 \}} \sum_{n=1}^N \frac{||X_n - W_nS||^2}{2T} + \alpha \frac{||W_n||_1}{K} + \beta \frac{||grad(W_n)||_2^2}{2K}
\label{srm_pen}
\end{align}
where $grad$ denotes the 3D spatial gradient, $\alpha$ is the sparsity controlling parameter, $\beta$ is the smoothness controlling parameter, $S$ is the shared response and $W_1, \cdots W_N$ are the spatial maps for each subject.

A graphical model is available in Figure~\ref{fig:conceptual_figure2}.

\begin{figure}
\centering
\includegraphics[scale=0.5]{figures/conceptual_figure2}
\caption{The sparsity and smoothness of each spatial maps are controlled by parameters $\alpha$ and $\beta$. In our case the noise $\epsilon$ follows a normal law with mean 0 and identity covariance ($\sigma_{\epsilon}$ is the identity matrix).}
\label{fig:conceptual_figure2}
\end{figure}


\subsection{A fast deterministic shared response model FastSRM is used to find the shared response}
We design a fast deterministic orthogonal SRM to find the shared response. Both deterministic and probabilistic SRM algorithm can be implemented in an efficient way when all data $X_1, \cdots, X_N$ fit in memory (see \cite{brainiak}). When it is not the case, it is straightforward to implement an equivalent version where only one subject is loaded at the time. When refering to ProbSRM including in our experiments results, we used the implementation of Brainiak slightly modified so that it can run even when all the subjects' data cannot fit in memory.
However, loading all subject at each iteration is rather slow and quickly becomes a bottleneck.

We introduce $FastSRM$, a faster algorithm (in terms of the number of time subjects need to be loaded in memory) very close to DetSRM. First we should notice than whenever $S$ is found, finding $W$ is straightforward and can be done in an online fashion (loading the data only once).  On the other hand, finding $S$ using data projected in a low dimensional space makes sense because the weight to give to each spatial maps should not change too much between reduced and full maps as long as the projection preserves useful information. 

Therefore we first project our data $X_1, \cdots, X_N$ using one principal components analysis with $K$ components for each subject. This yields reduced data $rX_1, \cdots, rX_N$ where each $rX_i \in \mathbb{R}^{K, T}$. We use reduced data to find $S$ by applying the deterministic orthogonal SRM algorithm. After $S$ is found, we sequentially load all the data $X_1, \cdots, X_N$ to find $W_1, \cdots, W_N$.

Note that we don't need to compute the full singular value decomposition to project the data. Only the first components are necessary which makes it possible to use randomized projection methods to gain time (see \cite{halko2009finding} and the randomized PCA implementation of scikit learn \cite{scikitlearn}).


\subsection{Optimization}
The initialization gives us a good estimate of $S$. We use it to find a better estimate of $W$.
Because the objective function (\ref{srm_pen}) is separable we can optimize it separately for each subject. 

The optimization function for each subject $i$ can be written as:
\begin{align}
&\text{argmin}_{W} F_i(W)
\label{w_iopt}
\end{align}
where 
\[ 
F_i(W) = \frac{||X_i - WS||^2}{2T} +  \frac{\beta}{2K} tr(W^T G W) + \frac{\alpha}{K} |W| + \mathbbm{1}_{W \geq 0}
\]
with $\mathbbm{1}$ is the indicator function: 
\begin{align*}
\mathbbm{1}_{W \geq 0} &= 0 && \text{if } W \geq 0 \\
&= +\inf && \text{otherwise}
\end{align*}
and $G = L_x^T L_x + L_y^T L_y + L_z^T L_z$ where $L_x$, $L_y$, $L_z$ are the laplacians with respect to each of the 3D dimension.

Problem \ref{w_iopt} is convex and the objective function $F_i$ can be seen as a sum $f_i + g_i$ where
\[
f_i(W) = \frac{||X_i - WS||^2}{2T} +  \beta \frac{tr(W^T G W)}{2K}
\]
is smooth and convex and 
\[
g_i(W) = \frac{\alpha}{2K} |W| + \mathbbm{1}_{W \geq 0}
\]
is convex.

We use coordinate descent to minimize Problem~\ref{w_iopt}.
Note that our optimization scheme can be applied to any kind of separable and convex function $g$ and with any kind of sparse matrix $G$ (whenever the number of elements in any line of $G$ is low compared to $V$). Therefore $G$ can represent any kind of local information we might have on correlation between brain areas. 
% \begin{align*}

% \end{align*}

% \begin{align*}
% \frac{\delta F_i}{\delta W[v, k]} &= - ((X_i - WS) S^T)[v, k] + (\beta (L_x^T L_x + L_y^T L_y + L_z^TL_z)W) [v, k] + \frac{\delta g_i}{\delta W[v, k]} \\
% &= - (X_i[v] - W[v]S) S[k]^T + G[v, :]W[:, k] + \frac{\delta g_i}{\delta W[v, k]} \\
% &\begin{aligned}
% = &- (X_i[v] - \sum_{\tilde{k}\neq k}W[v, \tilde{k}]S[\tilde{k}] - W[v, k]S[k]) S[k]^T +\sum_{\tilde{v}\neq v} G[v, \tilde{v}]W[\tilde{v}, k] + G[v, v]W[v, k]  \\
% & + \frac{\delta g_i}{\delta W[v, k]}
% \end{aligned} \\
% &= W[v, k] (||S[k]||_2^2 + G[v, v]) + R_1 +  \frac{\delta g_i}{\delta W[v, k]} \\
% &= W[v, k] (||S[k]||_2^2 + G[v, v]) + R_1 +  \frac{\delta g_i}{\delta W[v, k]}
% \end{align*}

% Where 
% \[
% G = \beta (L_x^T L_x + L_y^T L_y + L_z^TL_z)
% \]

% and 
% \[
% R_1 = - (X_i[v] - \sum_{\tilde{k}\neq k}W[v, \tilde{k}]S[\tilde{k}]) S[k]^T +\sum_{\tilde{v}\neq v} G[v, \tilde{v}]W[\tilde{v}, k]
% \]
% There we need to distinguish two options. Either t: 
% \begin{align*}
% &\frac{\delta F_i}{\delta W[v, k]} = 0 \\
% &\iff  W[v, k] (||S[k]||_2^2 + G[v, v]) + R_1 +  \alpha = 0
% & \iff
% \end{align*}
% \todo{Detail the derivations and make the update rule appear}


% We note that the proximal operator of $g$ can be computed component-wise since $g$ is a separable function.
% \begin{align*}
% \text{prox}_{g}(V)[i, j] &= \text{argmin}_x(\alpha ||x||_1 + \mathbbm{1}_{x \geq 0}  + \frac{||x - V[i, j]||^2}{2}) \\
% &= \max(V[i, j] - \alpha, 0)
% \end{align*}

% This opens the way for a proximal gradient descent algorithm. The derivative of the $f$ is given by:
% \[
% \nabla f (W_i) = W_i^T (W_iS - X_i) + \beta  \ \text{lap}(W_i) + \gamma \frac{N - 1}{N} (W_i - \frac{\sum_{n=1}^N W_n}{N})
% \]

% % We use a monotone version of Fista (mFISTA \cite{beck2009mfista}) to optimize $F$. The lipschitz constant $L$ of $\nabla f$ is computed using the power method to find operator highest eigenvalue. During the first iteration of ISRM, the basis $W$ are initialized randomly. If the similarity parameter $\gamma$ is non zero, it is driving each the first estimated maps towards the mean of random basis. This can slow the training. In order to only use $\gamma$ when it is meaningfull, we set it to zero during the first iteration.

% % When the step size is chosen to be below $\frac{1}{L}$ , ISTA has a rate of convergence of $\frac{1}{n}$ after $n$ steps whereas FISTA has a rate of convergence of $\frac{1}{n^2}$ after $n$ steps (see \cite{beck2009ista}). However in practice, especially at the beginning of the training procedure, the step-size can be increased above $\frac{1}{L}$ without leading to divergence. Adaptive step-size are often use instead of a fixed step size to speed up training (see \cite{becker2011templates}). 

% % Our backtracking procedure has three phases. The initial step size is chosen to be $\frac{1}{L}$. At each iteration we double the step size until the total error stop decreasing. When the error stop decreasing, we backtrack and reduced the step size by half. After this iteration the value of the step size stays constant when total error is decreasing. When the total error is increase we backtrack and reduce the step size by half (or set to $\frac{1}{L}$ if it is falling under $\frac{1}{L}$).

% % In order to minimize the number of iteration needed we initialize the gradient descent using $W_i = X_i S^T (S S^T)^{-1}$. 

% \subsubsection{Finding local optimal shared response $S$ using ensemble techniques}
% Once $W_i$ has been computed for every subject, the objective needs to be minimized with respect to $S$. The problem we want to solve is then:
% \begin{align}
% &\text{argmin}_{\{S,  \forall k ||S[k]||^2 \leq 1  \}} H(S)
% \label{Sopt}
% \end{align}
% where 
% \[ 
% H(S) = \frac{||X - WS||^2}{2}
% \]

% Problem~\ref{Sopt} is also convex but does not have a close form solution. $W$ cannot be assumed to hold in memory but we could use the procedure described in \cite{mairal2009online} to solve the problem.

% However, because we do not want $S$ to depend to much on the subjects used to compute it and in order to be able to interpret $S$ as the average projection of all subjects, we solve Problem~\ref{Sopt} for every single subjects and set $S$ to be the mean of all solutions. 

% Formally we solve for all subjects $i \in \{1, \cdots, N\}$:
% \begin{align}
% &\text{argmin}_{S_i} H(S_i)
% \end{align}
% where 
% \[ 
% H(S_i) = \frac{||X_i - WS_i||^2}{2}
% \]

% and set 
% \[
% S = \frac{S_1 + \cdots + S_N}{N}
% \]

% \subsection{FastDictSRM a special case of ISRM that speeds up computation}
% ISRM with no smoothness and no similarity enforcing parameter ($\beta = 0$ and $\gamma = 0$) is standard dictionary learning. It can be solved using proximal gradient descent (like for any instance of ISRM) or using the algorithm of \cite{mairal2009online}. 

% In order to speed up the process, we use a procedure similar to the one used for fastSRM. First we project the data in a low dimensional space using PCA. Then we use standard dictionary learning to find $S$. When $S$ is found, the rest of the procedure to find each $W_i$ is a sparse coding problem that we solve using proximal coordinate gradient descent. Both online dictionary learning and sparse coding algorithms are implemented in scikit-learn \cite{pedregosa2011scikit} and nilearn \cite{abraham2014machine}.

% This procedure allows us to make experiments using the full brain. It gives an insight of what performance ISRM could achieve on the entire brain.
%%%%% This is not necessary %%%%%
% Using a similar method as in online dictionary learning \cite{mairal2009online} we introduce:
% \[
% A = \sum_{n=1}^N A_n
% \]
% where $A_n = W_n^T W_n$ and 
% \[
% B = \sum_{n=1}^N B_n
% \]
% where $B_n = W_n^T X_n$.

% $A$ and $B$ can easily be updated whenever $W_i$ changes by subtracting the old term and add the new one.

% It can be shown that Problem~\ref{Sopt} is equivalent to:
% \begin{align}
% &\text{argmin}_{\{S,  \forall k ||S[k]||^2 \leq 1  \}} I(S)
% \label{Sopt2}
% \end{align}
% where
% \[ 
% I(S) = tr(SS^T A) - 2 tr(S^T B)
% \]

% The derivative of $I$ is given by:
% \[
% \nabla I(S) = AS - B
% \]

% Let $S^*$ be the solution of $AS = B$. If it satisfies the constrains then it is the solution of Problem~\ref{Sopt}.
% However we do not want our solution to depend to much of the specific choice of subjects to compute it. This is why we decide to use an ensemble scheme where we set
% \[
% S^* = \frac{S^*_1 + \cdots + S^*_N}{N}
% \]
% where $S^*_i$ is the solution of $A_iS = B_i$ given by $S^*_i = (W_i^T W_i)^{-1}W_i^T X_i$.

% If $S$ does not satisfy the constrains, we normalize every too large component of $S$ and modify $W$ so that the product $WS$ is unchanged. Note that this procedure should not affect too much sparsity, smoothness and similarity of the spatial maps.



\section{Experiments}
\subsection{Dataset}

All dataset used are freely available (see \cite{ibc} for the IBC dataset and \\
\url{http://studyforrest.org} for the Forrest dataset).

\paragraph{IBC Dataset} A video stimuli is presented to participants. It was first introduced in \cite{nishimoto2011reconstructing} and was used to study different levels of representation of visual content in brain activity. The data were acquired at Neurospin using a 3T scanner which has a spatial resolution of 1.5mm (more details in \cite{ibc}. The acquisition was divided in several sessions. We focus on 17 sessions and 10 participants so that all 10 participants have participated in the 17 sessions.

\paragraph{FORREST Dataset} Participants were listening to an audio transcription of the movie Forrest Gump. This dataset contains 19 subjects and 8 sessions. These data were acquired using a 7T scanner with a spatial resolution of 1mm (see more details in \cite{hanke2014high}. They are available in openfmRI \cite{poldrack2013toward}. More information about the forrest project can be found at \url{http://studyforrest.org}.

% \paragraph{SHERLOCK Dataset} 16 Participant are scanned while watching the movie Sherlock. They were asked to recall the movie afterwards using as much time as they wanted.
%\todo{Describe in more details}


\subsection{fMRI reconstruction: Evaluate the ability to recover brain activations in a co-smoothing setting}
We denote $X^{-s}$ brain recordings of all sessions but session $s$:
\[
X^{-s} = X_1^{-s}, \cdots, X_N^{-s}
\]
 where $X_i^{-s}$ refers to the brain recordings of subject $i$ using all session but session $s$.
 
Spatial maps computed using data of all sessions but session $s$ are denoted:
\[
W^{-s} = W_1^{-s}, \cdots, W_N^{-s}
\]

Similarly the data from all subject but $i$ acquired during session $s$ are denoted:
\[
X^{s}_{-i} = X_1^{s},\cdots,X^{s}_{i-1}, X^{s}_{i+1}, \cdots X_N^{s}
\]

We evaluate our model using a co-smoothing setting. 

In a first step all brain recordings for all sessions but one $X^{-s}$ are used for learning subjects spatial maps $W^{-s}$. 

In a second step we focus on the leftout session $s$ and use all subjects but one $X^{s}_{-i}$ to compute a shared response for the leftout session $S^{s}$.
\[
S^{s} = \frac{\sum_{n=1, n\neq i}^N (W_n^T W_n)^{-1} W_n^T X_n}{N - 1}
\]

From $S^{s}$ and $W_i^{-s}$ we compute $\hat{X_i}{s}$ which the prediction for the brain activity of the leftout subject during the leftout session $X_i{s}$.
\[
\hat{X_i}{s} = W_i S^{s}
\]


An illustration of the cosmoothing procedure is available in Figure~\ref{fig:experiment}

%\subsection{Scene classification}
%\todo{Describe how that work}


\begin{figure}
\centering
\includegraphics[scale=0.35]{figures/experiment}
\caption{All sessions but one (red $X$) are used to compute spatial maps $W$ for every subjects (bottom left). The unused session of all subjects but one (blue $X^4_1$, $X^4_2$) and corresponding spatial maps (red $W_1$ and $W_2$) are used to compute the shared response for the unused session (blue $S^4$ top right). Then using the spatial maps of the left-out subject ($W_3$) and the shared response of the unused session ($S^4$) we compute a prediction for the data of the left-out subject during the unused session (violet $\widehat{X}^4_3$).
The performance of the model is measured by comparing the prediction $\widehat{X}^4_3$ and true data $X^4_3$ using the coefficient of determination.}
\label{fig:experiment}
\end{figure}


The performance is measured voxel-wise using the coefficient of determination $c$ as a similarity measure.
For any two timecourses $x$ and $y$ we define the coefficient of determination by:

\[
c(x, y) = 1 - \frac{\sum_t (x[t] - y[t])^2}{\sum_t (y[t] - \overline{y}[t])^2} 
\]

where $\overline{y} = \frac{\sum_{t=1}^T y[t]}{T}$.

Following the leave-one-out cross validation scheme, all our experiments are done several times with a different leftout subject to reconstruct. We report the average of the results for all left out subjects.

\section{Results}
We show that ISRM leads to interpretable maps and to lower reconstruction error than orthogonal SRM. We first show the influence of each parameters on the interpretability of spatial maps and on performance on the train set of the forrest dataset. Then we use specific values of the parameters and show on two real datasets that it produces best performance on test sets than the orthogonal SRM while being more interpretable.

% Before comparing spatial maps of different algorithm we should make sure the ordering of maps has no influence. To this end, and just like in \cite{mensch2016compressed}, we reorder each spatial maps using the Hungarian algorithm so that two maps $M_0, M_1 \in \mathbb{R}^V$ that have the same component number have maximum correlation. The correlation $corr$ is defined by:
% \[
% corr(M_0, M_1) = \frac{M_0^T M_1}{\sqrt{M_0^TM_0 M_1^TM_1}}
% \]

\subsection{Reconstruction error and spatial maps in regions of interest using Forrest dataset: influence of each parameters}
We focus on a region of interest (ROI) and use the Forrest dataset to perform our preliminary experiments. This allows us to better understand the influence of each parameter on spatial maps and performance.

To find the region of interest we first perform the co-smoothing experiment on the full brain using orthogonal SRM. Then we select the regions with the highest coefficient of determination (coefficient of determination above a threshold). Finally we use mathematical morphology techniques and perform an opening and closing with a ball of radius 3. This leads to several smooth regions in the auditory cortex containing $9684$ voxels in total. Let us stress that this procedures favors orthogonal SRM since the regions of interests are chosen where it performs best.

First let us focus on the influence of each parameter on spatial maps.
\begin{itemize}
\item We measure performance using the number of voxels that have coefficient of determination above 0.05. This measures the number of well predicted voxels. (higher is better).
\item We measure sparsity by counting the mean number of zero coefficients in spatial maps (higher is more sparse)
\item We measure smoothness error with the l2 norm of the gradient (lower is more smooth)
\end{itemize}

In figure~\ref{fig:parameter_landscape}, we measure reconstruction error, sparsity and smoothness on a grid of parameters to see how they evolve.

There is a large value of parameters for which PCA, ProbSRM and FastDetSRM are outperformed. The simplest choice to have good reconstruction error when interpretability is not an issue would be to just put on the positivity constrain and leave $\alpha$ and $\beta$ to 0.
Looking carefully at performance landscape we can see that there is an optimal sparsity level $\alpha = 500$ for which the number of well explained voxels is the highest (above $8900$). Raising sparsity level also tends to moderately increase smoothness (due to the contracting effect of the l1 norm).
On the other hand increasing smoothness degrades performance and lower the level of sparsity so it should only be used when we need interpretable maps.

The parameter landscape does not change much with the number of components. When dropping positivity constrains results tend to be slightly worse than without.

%\todo{Scale parameters in function of timeframes and components}
%\todo{Show that thresholding or smoothing DetSRM basis is a lot worse than using smoothing and sparsity constrains making this a tool to use instead of smoothing when you want to interpret things}


\begin{figure}
\centering
\includegraphics[scale=0.6]{figures/performance_landscape_positive_50.pdf}
\includegraphics[scale=0.6]{figures/sparsity_landscape_positive_50.pdf}
\includegraphics[scale=0.6]{figures/smoothness_landscape_positive_50.pdf}
\begin{tabular}{llll}
 &PCA  &ProbSRM & FastDetSRM  \\
 Performance & 7094  &8712  & 8727 \\
 Smoothness & -  & -  & 1.78 \\
 Sparsity & 0.78 & 0.78 & 0.78 
\end{tabular}
\caption{Performance, Sparsity and Smoothness of ISRM, PCA, ProbSRM and FastDetSRM: We see that ISRM achieves best performance for a large range of parameters. We can see that for a good choice of parameters such as $\alpha = \beta = 1000$ ISRM beats all other algorithms both in interpretability and performance.}
\label{fig:parameter_landscape}
\end{figure}


\subsection{Benchmark of ISRM and ProbSRM on Forrest and IBC dataset in terms of reconstruction error using regions of interest}
Combining all parameters of ISRM, it is possible to get spatial maps that are more interpretable while still having good performance. 


We benchmarked ISRM against orthogonal SRM on the Forrest and IBC dataset. We compute region of interest that corresponds to areas where ProbSRM performs the best and smooth them using mathematical morphology techniques. 

We see that our model produces the best results in region of interests and the more interpretable maps on two different datasets (see Figure~\ref{fig:ibc_roi_results} and Figure~\ref{fig:forrest_roi_results}). 

\begin{figure}
\centering
\caption{IBC dataset: Comparing performance (top, higher is better), sparsity (middle, higher is better) and smoothness (bottom, lower is better) of ProbSRM, PCA, ISRM with no constrains (only positivity constrains), ISRM with sparsity and smoothness constrains ($\alpha = 10.$ and $\beta=100.$) on IBC dataset (top) and Forrest dataset (bottom). ISRM outperforms all other algorithm in terms of performance and interpretability. Note that we did not compute the smoothness of PCA because the differences in the norm of the spatial maps makes the comparison unfair.}
\includegraphics[scale=0.6]{figures/nvoxels_ibc.pdf}
\includegraphics[scale=0.6]{figures/sparsity_ibc_roi.pdf}
\includegraphics[scale=0.6]{figures/smoothness_ibc_roi.pdf}
\label{fig:ibc_roi_results}
\end{figure}

\begin{figure}
\centering
\caption{Forrest Dataset: Comparing performance (top, higher is better), sparsity (middle, higher is better) and smoothness (bottom, lower is better) of ProbSRM, PCA, ISRM with no constrains (only positivity constrains), ISRM with sparsity and smoothness constrains ($\alpha = 10.$ and $\beta=100.$) on IBC dataset (top) and Forrest dataset (bottom). ISRM outperforms all other algorithm in terms of performance and interpretability. Note that we did not compute the smoothness of PCA because the differences in the norm of the spatial maps makes the comparison unfair.}
\includegraphics[scale=0.6]{figures/nvoxels_forrest.pdf}
\includegraphics[scale=0.6]{figures/sparsity_forrest_roi.pdf}
\includegraphics[scale=0.6]{figures/smoothness_forrest_roi.pdf}
\label{fig:forrest_roi_results}
\end{figure}

% \subsection{Benchmark of FastDictSRM, FastSRM and ProbSRM using the entire brain on Forrest and IBC dataset in terms of reconstruction error and Sherlock dataset for scene classification}
% We apply FastDictSRM, solving a special case of ISRM on the entire brain and compare its performance against ProbSRM and FastDetSRM (the algorithm used for the intialization procedure of ISRM) on IBC and Forrest dataset. The mean coefficient of determination and the number of voxels with a coefficient of determination above 0.05 for each algorithm are given in Figure~\ref{fig:full_brain_results_mes}.

% Looking at the coefficient of determination per voxels we can see that FastDictSRM is less overfitting than the two other method (in regions were there is no information, the coefficient of determination is close to zero whereas it is negative in ProbSRM and FastSRM). The performance of FastDictSRM and ProbSRM are approximately the same. It shows that the projection of the data in reduced space using PCA reduces noise more than it destroys the signal.
% Comparing ProbSRM and FastDictSRM, we see that sparsity constrains lead to performance improvement especially when the number of components is large.

% When looking at activation maps (see Figure~\ref{fig:full_brain_maps}), FastDictSRM yields maps that are more sparse than ProbSRM and FastSRM.

% \textbf{TODO} Describe sherlock experiment 

% See figure~\ref{fig:sherlock}
% \begin{figure}
% \centering
% \caption{Scene classification performance on sherlock dataset. PCA leads to a better accuracy than all SRM-based algorithms.}
% \includegraphics[scale=0.7]{figures/sherlock_fullbrain_unweighted.pdf}
% \includegraphics[scale=0.7]{figures/sherlock_fullbrain_weighted.pdf}
% \label{fig:sherlock}
% \end{figure}

% \begin{figure}
% \centering
% \caption{Mean coefficient of determination and number of voxels with coefficient of determination above 0.05 on Forrest (top) and IBC (bottom) datasets}
% \includegraphics[scale=0.4]{figures/fullBrain_mean_forrest.pdf}
% \includegraphics[scale=0.4]{figures/fullBrain_nvoxels_forrest.pdf}
% \includegraphics[scale=0.4]{figures/fullBrain_mean_ibc.pdf}
% \includegraphics[scale=0.4]{figures/fullBrain_nvoxels_ibc.pdf}
% \label{fig:full_brain_results_mes}
% \end{figure}



% \begin{figure}
% \centering
% \caption{Sparsity of spatial maps (the mask used contains 607681 voxels). While the spatial maps produced by ProbSRM are full, FastDictSRM with 200 components is able to set more than 400000 voxels to zero.}
% \includegraphics[scale=0.8]{figures/fullbrain_forrest_sparsity_landscape.pdf}
% \label{fig:full_brain_maps}
% \end{figure}

\section{Discussion}
Orthogonality constrains yield efficient computations but is not supported by any biological evidence, it also leads to spatial maps that are difficult to interpret. Our interpretable shared response model (ISRM) yields positive smooth and sparse maps. ISRM also yields better results than the original shared response model on the two dataset we tested. 

% Although SRM produces interpretable and accurate results, it is not fast enough to be applied to the entire brain. In the special case where only sparsity constrains are enforced on spatial maps, we present a fast algorithm FastDictSRM that can be applied to the entire brain and produces sparse spatial maps. We show that it yields better results than the original SRM algorithm.

Future work should focus on using ISRM to produce a template for the entire brain.  One path to explore would be to apply the algorithm on brain parcellations (the computations on each parcel can be done in parallel).

% \section{Notations}
% \begin{figure}[h!]
%     \centering
%     \begin{tabular}{c|c|}
%          Symbol & Description  \\
%         \hline
%          $N$ & Number of subjects \\
%          $V$ & Number of voxels \\
%          $T$ & Number of timeframes\\
%          $K$ & Number of compomemts \\
%          $W_n$ & Voxel basis of subject $n$ - size ($V$, $K$) \\
%          $S$ & Common time response or loadings - size ($K$, $T$) \\
%          $E_n$ & Noise for subject $n$ - size ($V$, $T$) \\
%          $X_n$ & Data for subject $n$ - size ($V$, $T$)\\
%          $rX_n$ & Reduced data for subject $n$ - size ($K$, $T$)\\
%          $W$ & concatenated basis (voxel wise) - size ($NV$, $K$) \\
%          $X$ & concatenated data (voxel wise) - size($NV$, $T$)
%     \end{tabular}
% \end{figure}

\bibliographystyle{apacite}
\bibliography{biblio}

\end{document}