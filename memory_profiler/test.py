import numpy as np
import time
t0 = time.time()
print(time.time() - t0)
A = np.random.rand(100000) * 1000
print(time.time()- t0)
A = np.round(A, 0)
print(time.time()- t0)
B = np.array([A == value for value in np.unique(A)])
print(B.shape)
print(time.time()- t0)
C = np.random.rand(*B.shape)
